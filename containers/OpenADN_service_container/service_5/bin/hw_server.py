#! /usr/local/bin/python3
#
#   Request-reply service in Python
#   Connects REP socket to tcp://localhost:5560
#   Expects "Hello" from client, replies with "World"
#
#import zmq
import os
import sys
from appfabric.openadn.appfabricSocketLib import appfabricSocket
from appfabric.openadn.appfabricSocketLib.errors import *
from appfabric.common.unbufferedStream import unbuffered
import time
import logging
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES
from Crypto import Random
from aes_cbc import AESCipher 
from hashlib import md5

sys.stdout = unbuffered(sys.stdout)
home="/home/appfabric-alpha"
footprint = os.environ ["SERVICE_NAME"] + ":" + os.environ["SERVICE_ID"] + "\n"
greetings =" Encrypted Response "
greetings2 = "\n******************\n Encryption Initiated  \n*******************\n"
print (greetings2)
'''
os.chdir(os.path.expanduser("./"))
module_lgr = logging.getLogger("main")
module_lgr.disable_existing_loggers = False
module_lgr.setLevel(logging.DEBUG)
log_fh = logging.FileHandler(log_file_name)
log_fh.setLevel(logging.DEBUG)
frmt = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
log_fh.setFormatter(frmt)
module_lgr.addHandler(log_fh)

module_lgr.info("I was started")
'''
try:
	
	print ("\n ## Encryption Service is ready ## \n")
	sock = appfabricSocket.appfabricSocket(os.environ["IPC_COMM"],3)
	sock.connect()
	print ("Encryption Service: connected to appfabric socket")
	clientName =[]
	clientNo=0
	sock.register_to_poller (appfabricSocket.POLLIN)
	while True:
		if sock.poll(1) == appfabricSocket.MSG_READY:
			
			msg = sock.recv_msg()
			########################################## 
			# Added by Tara 
			# This code encrypt and sign the message to be send back to the clients 
			# Only if everything was verified to be legal 
			##########################################
			try: 		
				# If it was not verified, then don't do anything 	
				if (msg["Anomolous"] == 1 or msg["Anomolous"] == 2 or msg["Anomolous"] == 3 or msg["Anomolous"] == 4 or msg["Anomolous"] == 5):
					continue
				else: 
					msg["DATA"] += greetings
			except:	
				msg["DATA"] += greetings
			# Get server private key to sign messages 
			f = open("%s/AppFabric/experiments/physical_machines/privateKey.txt"%home,"r") #Location 
			print("\n####### Encrypting response message with client public key #####\n")
			key=("%s"%f.read()) #Read 
			key= RSA.importKey(key)	#And import 
			#If the message is sent with the secret key, then encrypt the reponse and sent it back 
			if (msg["APP_HDR"]["headers"]["CryptoType"]=="SKS"):
				f = open("%s/AppFabric/experiments/physical_machines/SecretKeys2/%s.bin"%(home,msg["APP_HDR"]["headers"]["User"]),"rb") #Secret key location
				message= f.read() #read 
				message=message.split(b"    ")
				key2= message[1] #key
				iv= message[0] #nonce 
				key_to_be_used= md5(key2).hexdigest()
				Secretkey= AES.new(key_to_be_used,AES.MODE_CBC,iv) #import 
				enc=AESCipher(key2.decode()).encrypt(msg["DATA"],iv) #encrypt the message 
				msg["DATA"]= "%s"%enc
			#If it is the first message then a secret key should be agreed and thus sent back to the client some nonce and randoms
			#The response message should include: encrypted message with client public key, signature with server public key and secret key values (encrypted) 
			elif(msg["APP_HDR"]["path"]=="f"):
				User ="%s"%msg["APP_HDR"]["headers"]["User"]# Get the user name 	
				User=key.decrypt(eval("%s"%User)).decode("utf-8")# Decrypt it 
				f = open("%s/AppFabric/experiments/physical_machines/ClientKeys/%s.txt"%(home,User),"r")#Read public key
				public_key=("%s"%f.read())
				public_key2= RSA.importKey(public_key) #Import public key
				send_msg=public_key2.encrypt(("%s"%msg["DATA"]).encode('utf-8'),64) #encrypt the message with the public key of server 
				print("*************** Signing the encrypted message ************ ")
				msg["DATA"]= "%s"%send_msg # Attach it 
				hash = SHA256.new(msg["DATA"].encode('utf-8')).digest() #Get the hash
				signature = key.sign(hash, '') #Sign it 
				sig= ("(%s,)"%signature)
				msg["DATA"]+="   %s"%sig #attach signature 
				#secret key 
				key2= "BELLOW"
				key_to_be_used= md5(key2.encode('utf8')).hexdigest()
				iv= Random.get_random_bytes(16)
				AES_KEY= AES.new(key_to_be_used,AES.MODE_CBC,iv)
				#save secret key for future use
				SecretKey= open("SecretKeys/%s.bin"%User,"wb")
				[SecretKey.write(x) for x in (iv,b"    ",key2.encode('utf8'))]
				SecretKey.close()
				#encrypt secret key values 
				Secretkey=public_key2.encrypt(key2.encode('utf-8'),64)
				iv=public_key2.encrypt(iv,64)
				msg["DATA"]+="   %s"%Secretkey #attach to the message 
				msg["DATA"]+="   %s"%iv
			#If it is public key crypto.. then just sign and encrypt the response 
			elif (msg["APP_HDR"]["headers"]["CryptoType"]=="PKI"): 
				User ="%s"%msg["APP_HDR"]["headers"]["User"]			
				f = open("%s/AppFabric/experiments/physical_machines/ClientKeys/%s.txt"%(home,User),"r")
				public_key=("%s"%f.read())
				public_key2= RSA.importKey(public_key)
				send_msg=public_key2.encrypt(("%s"%msg["DATA"]).encode('utf-8'),64)
				print("*************** Signing the encrypted message ************ ")
				msg["DATA"]= "%s"%send_msg
				hash = SHA256.new(msg["DATA"].encode('utf-8')).digest()
				signature = key.sign(hash, '')
				sig= ("(%s,)"%signature)
				msg["DATA"]+="   %s"%sig
			#print(msg)
			#i = 3
			print (" Message to be sent \n %s"%msg)
			#os.system("sudo sh runimage5.sh")

			#stime.sleep(5)
			sock.send_msg(msg)
		else:
			continue
except Appfabric_Socket_Error as e:
	print (e.msg)

sock.close()

