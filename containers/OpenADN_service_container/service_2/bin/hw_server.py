#! /usr/local/bin/python3
#
#   Request-reply service in Python
#   Connects REP socket to tcp://localhost:5560
#   Expects "Hello" from client, replies with "World"
#
#import zmq

import sys
import os
from appfabric.openadn.appfabricSocketLib import appfabricSocket
from appfabric.openadn.appfabricSocketLib.errors import *
from appfabric.common.unbufferedStream import unbuffered
# added for security 
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto import Random
from Crypto.Cipher import AES
from aes_cbc import AESCipher
from hashlib import md5
import base64
#end of the additions 
import time
import logging


BS=16
pad= lambda s: s + (BS-len(s) % BS)*chr(BS-len(s)% BS).encode()
uppad = lambda s: s [:-ord(s[len(s)-1:])]

#TODO move chdir to the time when process is forked
#TODO separate out logging of service from logging of the comChannel
#os.chdir("/home/openflow/AppFabric/host1/OpenADN_container/service_1/bin")

sys.stdout = unbuffered(sys.stdout)
home="/home/appfabric-alpha"
print (home)
#log_file_name = os.environ["SERVICE_NAME"] + ".log"
footprint = os.environ ["SERVICE_NAME"] + ":" + os.environ["SERVICE_ID"] + "\n"
greetings = "Verified Signature ==> "
greetings2 = "\n******************\n Service 2: Signature Verification \n*******************\n"
print (greetings2)

'''
os.chdir(os.path.expanduser("./"))
module_lgr = logging.getLogger("main")
module_lgr.disable_existing_loggers = False
module_lgr.setLevel(logging.DEBUG)
log_fh = logging.FileHandler(log_file_name)
log_fh.setLevel(logging.DEBUG)
frmt = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
log_fh.setFormatter(frmt)
module_lgr.addHandler(log_fh)

module_lgr.info("Hellooooooooooo...I was started")
'''
try:
	print ("\n ## Signature Verification is ready ## \n")
	sock = appfabricSocket.appfabricSocket(os.environ["IPC_COMM"],3)
	sock.connect()
	sock.register_to_poller (appfabricSocket.POLLIN)
	print ("\n Signature Verification service: connected to appfabric socket\n")
	client = 1 ; 
	clientName =[]
	clientNo=0 
	while True:
		if sock.poll(1) == appfabricSocket.MSG_READY:
			msg = sock.recv_msg()
			#print(msg)
			#########################################
			# ADDED BY TARA FOR SECURITY PURPOSES 
			# This code recieves a msg from the client and checks for its integrity 
			# If the integrity is varified, the message will be send to the other services 
			# Otherwise C-port is report with a malicious behavior 
			# In addition, it needd to save the client public key if the client is new (the first time it commincate with this local controller) 
			##########################################
			if (msg["APLS_HDR"]["IN_PORT_NUM"]== 0):
				print("\n ********************************************* \n Processing Request number %d \n" %client)
				# If the message recieved uses SKS (Secret Key Sharing), this is not for the first message 
				if (msg["APP_HDR"]["headers"]["CryptoType"]=="SKS"):
					f = open("%s/AppFabric/experiments/physical_machines/SecretKeys/%s.bin"%(home,msg["APP_HDR"]["headers"]["User"]),"rb") # Get the client-server secret key (This key is agreed on after the first msg and both parties has it
					enc= msg["APP_HDR"]["headers"]["Data"] #Encryted message 
					#Read the public key. The key is saved with the nonce and key value so we can retrieve the original key 
					message= f.read()
					message=message.split(b"    ")
					key2= message[1] #key
					iv= message[0] #nonce 
					key_to_be_used= md5(key2).hexdigest()
					Secretkey= AES.new(key_to_be_used,AES.MODE_CBC,iv)
					#Decrypt the message 
					m = AESCipher(key2.decode()).decrypt(enc.encode(),iv)
					print ("message that was sent encrypted %s"%m)
					msg["DATA"] = "Message Decrytion ==> "	
				# If this is the first clinet --> server message recieved, it should contain the public key of the client to be saved at the server 	
				elif(msg["APP_HDR"]["path"]=="f"):
					#Read server private ky 
					f = open("%s/AppFabric/experiments/physical_machines/privateKey.txt"%home,"r")
					key=("%s"%f.read())
					key= RSA.importKey(key)
					# Decrypt some of the client messages 
					User ="%s"%msg["APP_HDR"]["headers"]["User"] #username encrypted 
					Password ="%s"%msg["APP_HDR"]["headers"]["Password"] #password encrypted
					Password=key.decrypt(eval("%s"%Password)).decode("utf-8") #Decrypt
					User=key.decrypt(eval("%s"%User)).decode("utf-8") #Decrypt
					public_key= msg["APP_HDR"]["headers"]["Key"] #get the key 
					public_key= public_key.replace("\\n","\n") #correct formatting
					public_key= public_key.strip('b')#correct formatting
					public_key= public_key.strip('\'')#correct formatting
					public_key2= RSA.importKey(public_key)# import it from txt 
					text_file = open("ClientKeys/%s.txt"%User, "wb")#save it to file 
					text_file.write(public_key2.exportKey("PEM", passphrase=None, pkcs=1))
					text_file.close()
					# validate username, password, message  
					signature=msg["APP_HDR"]["headers"]["Signature"] #Signature
					sig= ("(%s,)"%signature)
					msg2= msg["APP_HDR"]["path"] 
					hash = SHA256.new(msg2.encode('utf-8')).digest() #hash value
					#just some msgs to print 
					print(" \n request from User:%s\n"%User) 
					print(" \n Key %s Signature:%s\n"%(public_key,signature))
					print("\n####### Packet Entered Signature Veritication Service #####\n")
					print (" Public key \n:%s \n "%public_key)
					print(" Recieved Signature \n%s \n"%sig)
					#Validation process
					if(public_key2.verify(hash, eval(sig)) and Password=='VIDE'): #Validated
						print("*********************** The signature is varified and password is correct***************************\n") 
						msg["DATA"] += greetings 			
					else: #Not valid
						print("*********************** The signature is rejected ***************************\n")
						#Send to C-conroller 
						msg["Anomolous"]= 1 
						msg["User"]=User
						msg["Service"]="Service2"
						msg["DATA"] += "\n*************************************************************\n"
						print (msg)
				#Message recieved with public key infastructure (not the first message)
				elif (msg["APP_HDR"]["headers"]["CryptoType"]=="PKI"):
					#Server private key
					f = open("%s/AppFabric/experiments/physical_machines/privateKey.txt"%home,"r")
					key=("%s"%f.read())
					key= RSA.importKey(key)
					#Read and decrypt the data
					Data="%s"%msg["APP_HDR"]["headers"]["Msg"]
					Data= "%s"%key.decrypt(eval("%s"%Data)).decode("utf-8")
					User ="%s"%msg["APP_HDR"]["headers"]["User"]
					#Get user public key 
					f = open("%s/AppFabric/experiments/physical_machines/ClientKeys/%s.txt"%(home,User),"r")
					public_key=("%s"%f.read())				
					public_key2= RSA.importKey(public_key)
					#Get and varify the signature 
					url= msg["APP_HDR"]["path"]
					signature=msg["APP_HDR"]["headers"]["Signature"]
					hash = SHA256.new(Data.encode('utf-8')).digest()
					sig= ("(%s,)"%signature)
					#Some messages to print 
					print(" \n request: Get %s User:%s\n"%(msg["APP_HDR"]["path"],msg["APP_HDR"]["headers"]["User"]))
					print(" \n Key %s Signature:%s\n"%(public_key,msg["APP_HDR"]["headers"]["Signature"]))
					#print(msg)
					print("\n####### Packet Entered Signature Veritication Service #####\n")
					print (" Public key \n:%s \n "%public_key)
					print(" Recieved Signature \n%s \n"%sig)
					#Varifying signature
					if(public_key2.verify(hash, eval(sig))):
						print("*********************** The signature is varified ***************************\n") 
						
						msg["DATA"] += greetings 			
					else: 
						print("*********************** The signature is rejected ***************************\n")
						msg["Anomolous"]= 1 
						msg["User"]=User
						msg["Service"]="Service2"
						msg["DATA"] += "\n*************************************************************\n"
				client = client+1
			sock.send_msg(msg)
				 
			
		else:
			continue
except Appfabric_Socket_Error as e:
	#module_lgr.info("I was started")(e.msg)
	pass
#module_lgr.info("closing down")
sock.close()

