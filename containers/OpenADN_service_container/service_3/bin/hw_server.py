#! /usr/local/bin/python3
#
#   Request-reply service in Python
#   Connects REP socket to tcp://localhost:5560
#   Expects "Hello" from client, replies with "World"
#
#import zmq
import os
import sys
from appfabric.openadn.appfabricSocketLib import appfabricSocket
from appfabric.openadn.appfabricSocketLib.errors import *
from appfabric.common.unbufferedStream import unbuffered
import time
import logging

#TODO move chdir to the time when process is forked
#TODO separate out logging of service from logging of the comChannel
#os.chdir("/home/openflow/AppFabric/host1/OpenADN_container/service_1/bin")

# FOR MININET SIMULATIONS
sys.stdout = unbuffered(sys.stdout)

#log_file_name = os.environ["SERVICE_NAME"] + ".log"
footprint = os.environ ["SERVICE_NAME"] + ":" + os.environ["SERVICE_ID"] + "\n"
greetings = "Service 3" 
print (greetings)

'''
os.chdir(os.path.expanduser("./"))
module_lgr = logging.getLogger("main")
module_lgr.disable_existing_loggers = False
module_lgr.setLevel(logging.DEBUG)
log_fh = logging.FileHandler(log_file_name)
log_fh.setLevel(logging.DEBUG)
frmt = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
log_fh.setFormatter(frmt)
module_lgr.addHandler(log_fh)

module_lgr.info("I was started")
'''
try:
	print ("Hi, I am service 3")	
	sock = appfabricSocket.appfabricSocket(os.environ["IPC_COMM"],3)
	print ("service3: connecting to appfabric socket")
	sock.connect()
	print ("service3: connected to appfabric socket")
	sock.register_to_poller (appfabricSocket.POLLIN)

	while True:
		if sock.poll(1) == appfabricSocket.MSG_READY:
			msg = sock.recv_msg()
			# Do something with the message
			
			# even if only to to proove your existance
			try: 			
				if (msg["Anomolous"] == 1):
					continue
				else: 
					msg["DATA"] += greetings
			except:	
				msg["DATA"] += greetings
			sock.send_msg(msg)

		else:
			continue
except Appfabric_Socket_Error as e:
	print (e.msg)

sock.close()

