#! /usr/local/bin/python3
#
#   Request-reply service in Python
#   Connects REP socket to tcp://localhost:5560
#   Expects "Hello" from client, replies with "World"
#
#import zmq
import os
import sys
from appfabric.openadn.appfabricSocketLib import appfabricSocket
from appfabric.openadn.appfabricSocketLib.errors import *
from appfabric.common.unbufferedStream import unbuffered
import time
import logging

#TODO move chdir to the time when process is forked
#TODO separate out logging of service from logging of the comChannel
#os.chdir("/home/openflow/AppFabric/host1/OpenADN_container/service_1/bin")


# for mininet simulations
sys.stdout = unbuffered(sys.stdout)
home="/home/appfabric-alpha"
#log_file_name = os.environ["SERVICE_NAME"] + ".log"
footprint = os.environ ["SERVICE_NAME"] + ":" + os.environ["SERVICE_ID"] + "\n"
greetings = " Web Server Reply ==> "
greetings2 = "\n******************\n FIREWALL Initiated \n*******************\n"
print (greetings2)


try:
	print ("\n ## FIREWALL is ready ## \n")
	sock = appfabricSocket.appfabricSocket(os.environ["IPC_COMM"],3)
	#print ("Web Server: connecting to appfabric socket\n")
	sock.connect()
	print ("FIREWALL: connected to appfabric socket\n")
	lx=[]
	#######################################################################
	# ADDED BY TARA 
	# The following parts of the code read the application configuration and check if the clients are meeting them or no
	f = open(os.path.expanduser("%s/AppFabric/experiments/physical_machines/NC/NC_Config_Node1.txt"%home),"r")
	#f= open(os.path.expanduser("~/AppFabric/experiments/physical_machines/NC/NC_Config_Node1.txt"),"r") # Read 
	# Arrange them in an array 
	# Note: I am assuming a fixed configuration for now. It should be changed later on 
	for line in f:
		l=line.replace('\n','')
		lx.append(l.split(': '))
	NC_Config={
		"%s"%lx[1][0]: "%s"%lx[1][1].strip(),
		"%s"%lx[2][0]: "%s"%lx[2][1].strip(),
		"%s"%lx[3][0]: "%s"%lx[3][1].strip().split(","),
		"%s"%lx[4][0]: "%s"%lx[4][1].strip()
	}
	No_Requests = 0 
	clients=[]	
	clients_dest=[[],[]]
	No_of_requests_per_client_dest={}
	No_of_requests_per_client={}
	sock.register_to_poller (appfabricSocket.POLLIN)
	while True:
		if sock.poll(1) == appfabricSocket.MSG_READY:
			print("\n####### Packet Entered FIREWALL ##### ")
			print("\n FIREWALL PROCESSING ")
			msg = sock.recv_msg()
			# Do something with the message
			# even if only to to proove your existance
			try: 			
				if (msg["Anomolous"] == 1): #IF the message was not verified don't do anything 
					continue
				else: 
					msg["DATA"] += greetings
			except:	
				msg["DATA"] += greetings 
			if(msg["APP_HDR"]["path"]!="f"): #Not the first message, we are not counting the first message in our calucluation as it is a setup message 
				No_Requests+=1
				if ("%s"%msg["APP_HDR"]["headers"]["User"] not in clients ): #Client is not seen yet 
					clients.append("%s"%msg["APP_HDR"]["headers"]["User"]) # ADD to the list of clients 
					No_of_requests_per_client["%s"%msg["APP_HDR"]["headers"]["User"]] = 1 #increase number of requests 
				else:
					No_of_requests_per_client["%s"%msg["APP_HDR"]["headers"]["User"]]+=1 #increase number of requests 
			if(msg["APP_HDR"]["path"]!="f"):
				print(clients_dest)
				if ([msg["APP_HDR"]["headers"]["User"],msg["APP_HDR"]["path"]] not in clients_dest):
					clients_dest.append([msg["APP_HDR"]["headers"]["User"],msg["APP_HDR"]["path"]]) #Client destinations 
					No_of_requests_per_client_dest["%s"%[msg["APP_HDR"]["headers"]["User"],msg["APP_HDR"]["path"]]] = 1
				else:
					No_of_requests_per_client_dest["%s"%[msg["APP_HDR"]["headers"]["User"],msg["APP_HDR"]["path"]]]+=1
			if(msg["APP_HDR"]["path"]!="f"):
				print (NC_Config["BANNED_DEST"])
				#If it is a band destination then send notification to c-controller 
				if(msg["APP_HDR"]["path"] in NC_Config["BANNED_DEST"]): # If it is a banned destination
					print ("Banned dest. anomolous") 
					msg["Anomolous"]=2 
					msg["User"]="%s"%msg["APP_HDR"]["headers"]["User"]
					msg["Service"]="Service2"
					msg["NoOfRequests"]=No_Requests 
					msg["NoOfRequestsPerClient"]=No_of_requests_per_client["%s"%msg["APP_HDR"]["headers"]["User"]]
					msg["NoOfRequestsPerClient_dest"]=No_of_requests_per_client_dest["%s"%[msg["APP_HDR"]["headers"]["User"],msg["APP_HDR"]["path"]]]
				#If it is a band type of traffic then send notification to c-controller 				
				elif(msg["APP_HDR"]["command"] != NC_Config["TYPE_OF_TRAFFIC"]):
					print ("Invalid traffic anomolous")
					msg["Anomolous"]=3 
					msg["User"]="%s"%msg["APP_HDR"]["headers"]["User"]
					msg["Service"]="Service2"
					msg["NoOfRequests"]=No_Requests 
					msg["NoOfRequestsPerClient"]=No_of_requests_per_client["%s"%msg["APP_HDR"]["headers"]["User"]]
					msg["NoOfRequestsPerClient_dest"]=No_of_requests_per_client_dest["%s"%[msg["APP_HDR"]["headers"]["User"],msg["APP_HDR"]["path"]]]
				#If the requests are too many in limited time  then send notification to c-controller (from the same client)			
				elif(No_Requests >= int(NC_Config["MAX_REQUESTS"])): 
					print ("maximum requests acceeded")
					msg["Anomolous"]=4 
					msg["User"]="%s"%msg["APP_HDR"]["headers"]["User"]
					msg["Service"]="Service2"
					msg["NoOfRequests"]=No_Requests 
					msg["NoOfRequestsPerClient"]=No_of_requests_per_client["%s"%msg["APP_HDR"]["headers"]["User"]]
					msg["NoOfRequestsPerClient_dest"]=No_of_requests_per_client_dest["%s"%[msg["APP_HDR"]["headers"]["User"],msg["APP_HDR"]["path"]]]
				#If the requests are too many in limited time  then send notification to c-controller (from the system)				
				elif (msg["APP_HDR"]["path"]!="f" and No_of_requests_per_client["%s"%msg["APP_HDR"]["headers"]["User"]]>=int(NC_Config["MAX_REQUESTS_PER_CLIENT"])): 
					print ("maximum requests for client %s acceeded"%No_of_requests_per_client["%s"%msg["APP_HDR"]["headers"]["User"]])	
					msg["Anomolous"]=5 
					msg["User"]="%s"%msg["APP_HDR"]["headers"]["User"]
					msg["Service"]="Service2"
					msg["NoOfRequests"]=No_Requests 
					msg["NoOfRequestsPerClient"]=No_of_requests_per_client["%s"%msg["APP_HDR"]["headers"]["User"]]
					msg["NoOfRequestsPerClient_dest"]=No_of_requests_per_client_dest["%s"%[msg["APP_HDR"]["headers"]["User"],msg["APP_HDR"]["path"]]]
			
			sock.send_msg(msg)
		else:
			continue
except Appfabric_Socket_Error as e:
	print (e.msg)

sock.close()

