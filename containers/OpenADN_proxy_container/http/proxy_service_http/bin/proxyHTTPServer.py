#! /usr/local/bin/python3

#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


'''

	This is the implementation of a multi-threded http server.
	The proxy server acts the front end intercepting user connections.
	It accepts user connections and hands to a new thread for handling the connection.
	At the same time, it starts a new session thread for the connection that acts as the backend of 
	the server connecting to the AppFabric platform pPort.



	 [HTTP connection handler #1]----|			 |----[User Session Manager Thread #1] ---- [pPort handler thread #1] \
		    			 |                       | 								       \	
	 [HTTP connection handler #2]----|     			 |----[User Session Manager Thread #2] ---- [pPort handler thread #2]   \
		                         |                       |     									 \
	 [HTTP connection handler #3]----|---- proxyHTTPServer---|----[User Session Manager Thread #3] ---- [pPort handler thread #3]     -- pPort 
    	                                 |			 |									 /
                                 (...)---|			 |--- (...)								/	
	                                 |			 |								       /		
	 [HTTP connection handler #N]----|	    		 |----[User Session Manager Thread #N] ---- [pPort handler thread #N] /
		

	It creates a python dictionery object from the message with the following labels:
		- APP_HDR: It parses the HTTP header and puts the relevant fields in the APP_HDR
		- APLS_HDR: Creates a new APLS header and intializes the fields to None
		- DATA: The message data field is conserved in the DATA item
	
	The proxy creates a poolthread containing empty threads before-hand and assigns a free thread to a new user connection.
'''

import sys
from http.server import HTTPServer, BaseHTTPRequestHandler
from socketserver import ThreadingMixIn

from appfabric.common import util
from appfabric.common.errors import *
from appfabric.common.msgTypes import *
from appfabric.common.unbufferedStream import unbuffered

import threading
import pickle
import zmq
import os
import sessionManagementThread
from proxyMsgTypes import *
from time import sleep

class Handler(BaseHTTPRequestHandler):
	
	protocol_version = "HTTP/1.1"
#	request_version = protocol_version
	
	def __init__(self,request, cliAddr, connectString, context, server):
		self.numRequests = 0
		self.request = request
		self.cliAddr = cliAddr
		self.server = server
		self.connectString = connectString
		self.context = context
		self.sessionManagerPort =  self.context.socket(zmq.DEALER)
#		self.inactivityTimer = threading.Timer(1.0, self.handleInactivity)
	
		super(Handler, self).__init__(self.request, self.cliAddr, self.server)
	
	def handle(self):
		""" Handle multiple requests if necessary"""
		self.close_connection = 1
	#	self.inactivityTimer.start()
		self.handle_one_request()
		while not self.close_connection:
			try:
				self.handle_one_request()
			except:
				
				""" send close signal to the sessionManagerPort and close the socket"""
				break
		self.connection_shutdown()

	def connection_shutdown(self):		
		
		self.finish()
		closeConnectionMsg = {}
	#	print ("proxy: Closing session")	
		self.create_OpenADN_msgFrame(closeConnectionMsg,MSG_TYPE_CONTROL_CLOSE)
		self.sessionManagerPort.send_pyobj(closeConnectionMsg)
		self.sessionManagerPort.close()
	"""
	def handle_one_request(self)
		#overriding handle_one_request to add a timer to monitor inactivity
		try:
			self.raw_requestline = self.rfile.readline(65537)
			self.inactivityTimer.cancel()
			if len(self.raw_requestline) > 65536:
				self.requestline = ''
				self.request_version = ''
				self.command = ''
				self.send_error(414)
				return
			if not self.raw_requestline:
				self.close_connection = 1
				return
			if not self.parse_request():
				# An error code has been sent, just exit
				return
			mname = 'do_' + self.command
			if not hasattr(self, mname):
				self.send_error(501, "Unsupported method (%r)" % self.command)
				return
			method = getattr(self, mname)
			method()
			self.wfile.flush() #actually send the response if not already done.
	"""		
				
	def do_CONNECT(self):
	#	print ("Connection request to session manager element from thread %s"%(threading.currentThread().getName()))
		# Open connection to sessionManagerPort
		#print ("ConnectString = %s" %(self.connectString))
		count = 0
		max_count =1000
		while True:
			try:
				self.sessionManagerPort.connect(self.connectString)
				#print("HTTP Proxy: Connected to session manager port: %s"%(threading.currentThread().getName()))
				break
			except zmq.ZMQError as e:
				#TODO Handle error
			#	print("Could not connect to session manager port %s"%(e.__str__()))
				pass
			sleep(1)
			count += 1
		#	if count > max_count:
		#		return False		
		return True
		
	def do_PROXY(self):
		if self.numRequests == 0:
			# no connections yet connect to the proxy handler
	#		print ("\nsetting up connection to session manager element")
			if self.do_CONNECT() == True:
				self.numRequests += 1
			else:
				print ("Connection to session manager element failed")	

		else:
		#	print ("send directly over existing connection to session manager element")
			self.numRequests += 1
			#TODO close this http stream
		internal_msg_rep = {}
		self.create_OpenADN_msgFrame(internal_msg_rep,MSG_TYPE_DATA)
		#print(internal_msg_rep)
	#	print ("Sending Data message to session manager Port: %s"%(internal_msg_rep))
		self.sessionManagerPort.send_pyobj(internal_msg_rep)
		
		receivedMessage = self.sessionManagerPort.recv_pyobj()
	#	print ("Received from proxyPort", receivedMessage)
		return receivedMessage		

	def create_OpenADN_msgFrame(self, _new_msg_rep, _msgType):
		#print("HELLO")
		#format msg with OpenADN Msg Framing  
		if _msgType == MSG_TYPE_DATA:
			APP_HDR =  {	
					"host":self.headers.get("host"),
					"command": self.command,
					"path": self.path,
					"version": self.request_version,
					"headers" :dict(self.headers)
				}	
				
			APLS_HDR  = {
					"MSG_TYPE": _msgType,	
					"PROXY_PORT_LIST": [],
					"IN_PORT_NUM":None,
					"OUT_PORT_NUM": None 
				}

		
			if self.command == "POST":
				DATA = self.rfile.read(\
					int(self.headers.get("content-length")) )
			else:
				DATA = ""


			#create the OpenADN Message Frame
			_new_msg_rep["DATA"] = DATA
			_new_msg_rep["APP_HDR"] = APP_HDR
			_new_msg_rep["APLS_HDR"] = APLS_HDR 

		elif _msgType == MSG_TYPE_CONTROL_CLOSE:
			APLS_HDR ={
					"MSG_TYPE": _msgType
				}
			_new_msg_rep["APLS_HDR"] = APLS_HDR 
			#msg.append(APLS_HDR)
					
	def do_GET(self):
		receivedMessage = self.do_PROXY()
		self.send_response(200)
		DATA = receivedMessage["DATA"]
		self.send_header('content-length', len(DATA))
	#	receivedMessage.insert(DATA,2)
		self.end_headers()
		self.wfile.write(bytes(DATA, 'UTF-8'))
		return

	def finish(self):
		if not self.wfile.closed:
			self.wfile.flush()
		self.wfile.close()
		self.rfile.close()

	def handleInactivity(self):
	#	print ("\n\nTimer Fired\n\n")
		return
	def log_message(self, format, *args):
		return;


class ThreadedHTTPServer(HTTPServer):
	"""Handle requests in a separate thread."""
	""" Override the process_request_thread in the ThreadingMixIn class"""
	""" Method running in the thread method"""
	def __init__(self,context, serverAddress, RequestHandlerClass, **kwargs):
		self.context = context  #zmq.Context()
		self.threadPool = []
		self.threadPoolMin = 5 #TODO Make this configurable by the controller
		self.threadPoolMax = 10 #TODO Make this configurable by the controller
		self.inprocPathString = kwargs ["INPROC_PATH_STRING"]
		self.ipcPathString  = kwargs ["IPC_PATH_STRING"]

		#setup control connection with the pSwitchPort
		try:
			self.sipPort_control = self.context.socket(zmq.DEALER)
		except zmq.ZMQError as e:
			#TODO Log error
			print ("ProxyServer error: %s"%(e.__str__()))
			exit()
			
		while True:
			try:
				self.sipPort_control.connect(util.createConnectString("ipc",self.ipcPathString, "service_control"))
				break
			except zmq.ZMQError as e:
				#TODO: Log the error 
				print (e.msg)
				sleep(1)
				continue
			
		
		super(ThreadedHTTPServer,self).__init__(serverAddress, RequestHandlerClass)
	
 	
	def initialize_threads_in_threadPool(self):
	#	print ("Add threads to the threadpool")
		# Ask for threadPoolMax threads and the switch returns the number of threads that it can accomodate 
	#	num_threads_requested = self.threadPoolMax - len(self.threadPool)

		num_threads_requested = self.threadPoolMax
		print ("HTTP PRoxy: Add threads to the threadpool  ===== %s"%(num_threads_requested))
		msg = pickle.dumps ({ "TYPE" : MAX_EMPTY_PROXY_PORTS_AVAILABLE_QUERY, 'REQUEST':num_threads_requested})
		# send the request for allocating new threads to proxySwitch	
		try:	
			self.sipPort_control.send(msg)
		except zmq.ZMQError as e:
			#TODO: Log the error
			print (e.msg)
			exit()
	
		rep_msg = self.sipPort_control.recv()		
		rep_msg = pickle.loads(rep_msg)
				
	
		num_threads_allocated = rep_msg["ALLOCATED"]
		print ("HTTP Proxy Num threads alloted ===================  %s"%(num_threads_allocated))
		if num_threads_allocated > 0:
			self.threadPoolMax = num_threads_allocated
		
			# If proxy switch is not ready
		
			for i in range (num_threads_allocated):
				t = sessionManagementThread.sessionManagementThreadHandler(self.context, self.ipcPathString, self.inprocPathString)
				#t.daemon = False
		
				self.threadPool.append(t)
				t.start()

	# Overrides the process request in BaseServer --> ... --> HTTPServer Class	
	def process_request(self, request, client_address):

		while True:
			if len(self.threadPool)> 0:
				sessionManagementThread = self.threadPool.pop()
				break  # keep repeatedly asking for threads to the proxySwitch
		
				# Out of threads in thread pool, generally will happen if the proxy switch does not allocate threads for a while 
			else:	
				self.initialize_threads_in_threadPool()
			
		connectString  =  util.createConnectString("inproc", self.inprocPathString, "proxy_server",str(sessionManagementThread.name))
		
		t = threading.Thread(target = self.process_request_thread,args = (request, client_address, connectString, self.context))
		t.start()
		if len(self.threadPool) <= self.threadPoolMax/2:	
			# pre-emptively start asking for threads
			self.initialize_threads_in_threadPool()
		
	
	def process_request_thread(self, request, client_address, connectString, context):
		""" assign a thread to process the request"""
		try:
			self.finish_request(request, client_address, connectString, context)
			self.shutdown_request(request)
		except:
			#TODO Revisit this
			self.handle_error(request, client_address)
			self.shutdown_request(request)
	
	def finish_request(self,request, client_address, connectString, context): 
		self.RequestHandlerClass(request, client_address, connectString, context, self)

	
def main():
	# for mininet simulation output
	sys.stdout = unbuffered(sys.stdout)
	
	ipcPathString = os.environ["IPC_COMM"]
	port = int(os.environ["PORT"])
	ip = os.environ["IP_ADDR"]
	serviceID = os.environ["SERVICE_ID"]
	serviceName = os.environ ["SERVICE_NAME"]
	
	try:
		inprocPathString = util.createPathString("inproc","/tmp/inproc",serviceName, str(serviceID))
	except Invalid_Connect_String_Error as e:
		#TODO log error and exit
		sys.exit()
	try:
		context = zmq.Context()
	except zmq.ZMQError as e:
		#TODO log error
		sys.exit()
	try:  	
		server = ThreadedHTTPServer(context,( ip , port), Handler, IPC_PATH_STRING = ipcPathString, INPROC_PATH_STRING = inprocPathString)
		print ('Starting server, use Ctrl-C  to stop')
		server.initialize_threads_in_threadPool()
	except:
		#TODO define new eror type and exit
		sys.exit()
	server.serve_forever()


class unbuffered:
	def __init__(self, stream):
		self.stream = stream
	def write(self, data):
		self.stream.write(data)
		self.stream.flush()
	def __getattr__(self, attr):
		return getattr(self.stream, attr)

if __name__ == '__main__':
	try:
		sys.stdout = unbuffered(sys.stdout)
	
	except :
		pass
	main()
