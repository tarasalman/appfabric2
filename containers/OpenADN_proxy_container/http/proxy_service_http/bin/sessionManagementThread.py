#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#





import os
import sys
import threading
import zmq
import pickle
from appfabric.common import util
from appfabric.common.msgTypes import *
from appfabric.common.errors import *
from appfabric.common.constants import *

from proxyMsgTypes import *
from time import sleep

class sessionManagementThreadHandler (threading.Thread):

	def __init__(self,context, ipcPathString, inprocPathString):
		super(sessionManagementThreadHandler, self).__init__()
		self.context = context
	#	self.getProxyPortIDMsg = bytes("GET_EMPTY_PROXY_PORT", 'UTF-8')
		self.sipPort_control = self.context.socket(zmq.DEALER)
		self.sipPort_data = self.context.socket(zmq.DEALER)
		self.proxyServerPort = self.context.socket(zmq.DEALER)
		self.ipcPathString = ipcPathString
		self.inprocPathString =  inprocPathString
		
		self.poller = zmq.Poller()
		self.portID = None
		self.wf_portNum = None

	def run(self):
	#	print ("Session Management Thread started")
		# Get the connection string from the proxy Switch 
		emptyProxyPortReqMsg = pickle.dumps({'TYPE':GET_EMPTY_PROXY_PORT_REQ})
		
		while True:
			try:
				self.sipPort_control.connect(util.createConnectString("ipc",self.ipcPathString, "service_control"))
				break
			except zmq.ZMQError as e:
				sleep(1)
				continue

		try:
			self.poller.register(self.sipPort_control,zmq.POLLIN)
		except zmq.ZMQError as e:
			print("Connection Error :%s"%(e.errno))
			#TODO Decide what to do in case of failure to connect
		except Invalid_Connect_String_Error as e:
			print ("Invalid connect String", e.msg)
		
		#print ("Session Management Thread started 2")
	#	while (True):
		self.sipPort_control.send(emptyProxyPortReqMsg)		
		while True:
			socks = dict(self.poller.poll(100))
			if socks.get(self.sipPort_control) == zmq.POLLIN:
				msg = pickle.loads(self.sipPort_control.recv())
				if msg["TYPE"] == GET_EMPTY_PROXY_PORT_REP and msg["STATUS"] == SUCCESS:
				#	print ("Received GET_EMPTY_PROXY_PORT_REP 1")
					self.portID = msg["PORT_ID"]
				#	print ("Received GET_EMPTY_PROXY_PORT_REP 2")
					self.wf_portNum = msg ["WF_PORT_NUM"]
				#	print ("Received GET_EMPTY_PROXY_PORT_REP 3")
					break
	
	#	print ("SMT: 3: %s, %s"%(self.portID, self.wf_portNum) )
		#print ("PORT ID =", self.portID)
		#	self.sipPort_control.close()
		
		# setup  the proxy switch connection
		while True:
			try:
				self.sipPort_data.connect(util.createConnectString("ipc",self.ipcPathString, "service_data",self.portID))
				break
			except zmq.ZMQError as e:	
				sleep(1)
				continue
	
		try:	
			self.poller.register(self.sipPort_data, zmq.POLLIN)
		except zmq.ZMQError as e:
                        #TODO Handle error
			print("Could not connect to proxy switch port", e.errno)
			exit()
		except Invalid_Connect_String_Error as e:
			print ("Invalid connect String", e.msg)

		# TODO: Ask for filter and rules from the proxy switch for the session
		
		# create local msg queue to communicate with the user session thread
	#	print ("SMT: boot strapping over: %s, %s"%(self.portID, self.wf_portNum))
		try:
			if not os.path.exists("/tmp/inproc/proxyServer"):
				 os.makedirs("/tmp/inproc/proxyServer")
			proxyServerConnectString = util.createConnectString("inproc",self.inprocPathString,"proxy_server", str(threading.current_thread().name))
			self.proxyServerPort.bind(proxyServerConnectString)
			self.poller.register(self.proxyServerPort, zmq.POLLIN)
		#	print ("SessionManager: %s"%(proxyServerConnectString))
		except zmq.ZMQError as e:
			print("Could not bind to local msg queue", e.errno)
			exit()
		except Invalid_Connect_String_Error as e:
			print ("Invalid connect String", e.msg)
			exit()
			#TODO: Handle error
		# block on receive from local msg queue and proceed only after that
	#	print ("SMT: Ready to accept connections from the proxy server")	
		while True:
			try:
				socks = dict(self.poller.poll(10))
			except zmq.ZMQError as e:
				pass
		#	print ("Session Manager Port")
			if socks.get(self.sipPort_data):	
				
				msg = self.sipPort_data.recv_pyobj()
				
				self.proxyServerPort.send_pyobj(msg) 

			if socks.get (self.proxyServerPort):	
				msg = self.proxyServerPort.recv_pyobj()
				APLS_HDR = msg["APLS_HDR"]
				if APLS_HDR["MSG_TYPE"]== MSG_TYPE_DATA:
					APLS_HDR["PROXY_PORT_LIST"].append(self.portID)
				#	APLS_HDR["PORT_NUM"] = self.wf_portNum 
				#	print ("Session Manager message", msg)
				#	APLS_HDR["PATH_ID"] = 100
				#	msg.append(APLS_HDR)
					#print(msg)
					self.sipPort_data.send_pyobj(msg)
				if APLS_HDR["MSG_TYPE"]== MSG_TYPE_CONTROL_CLOSE:
				#	msg.append(APLS_HDR)
				#	self.proxyServerPort.send_pyobj()
				#	print ("Session port : Closing conn")
					self.sipPort_data.send_pyobj(msg)
					self.sipPort_control.close()
					break
		""" remove the session manager thread"""
	#	self.proxySwitchPort.close()
	#	self.proxyServerPort.close()


		# close the thread
#		self.close_request(msg["REQUEST"])	 
		# TODO handle errors
		"""
		except:
            		self.handle_error(request, client_address)
            		self.shutdown_request(request)
		"""




 
			





