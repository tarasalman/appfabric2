<?xml version="1.0"?>

<classifier_config>
	<classifier in_port = "0">
		<rule id="1.1" pref= "1" >		
			<hdr> ["APP_HDR", "path"] </hdr>
			<pattern> .html </pattern>
			<outport> 1 </outport>
		</rule>
		
		 <rule id="1.2" pref= "2" >  
			<hdr> ["APP_HDR", "path"] </hdr>
               		<pattern> .shtml </pattern>
			<outport> 2 </outport>
       		 </rule> 
	
	 	<rule id= "1.3" pref= "3" >  
                	<hdr> ["APP_HDR"] </hdr>
               		<pattern> . </pattern>
               	 	<outport> 2 </outport>
        	</rule> 
	</classifier>

	<classifier in_port = "1">
		<rule id= "2.1" pref= "1" >  
                	<hdr> ["APP_HDR"] </hdr>
                	<pattern> . </pattern>
               		<outport> 0 </outport>
        	</rule> 
	</classifier>

	<classifier in_port = "2">
        	<rule id= "3.1" pref= "1" >
                	<hdr> ["APP_HDR"] </hdr>
               		<pattern> . </pattern>
               	 	<outport> 0 </outport>
        	</rule>
	</classifier>
</classifier_config>
