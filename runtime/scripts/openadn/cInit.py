import os
import sys
import threading
from appfabric.common import util
from appfabric.openadn.cPort import cPort
from appfabric.common.unbufferedStream import unbuffered
import logging
from signal import *
import zmq
import uuid
import pdb


sys.stdout = unbuffered(sys.stdout)
global globalRun
globalRun = True

#sys.path.append (os.path.join(os.path.expanduser("~/"),"AppFabric","platfrom","dp"))

# Setting up global context
def main():
	instance_id = str(uuid.uuid4())
	# TODO Read a config file and setup the environment, Setup environment parameter
	#os.environ ["LOGS_DIR_BASE"] =  "/home/openflow/AppFabric/runtime/log"
	os.environ ["LOGS_DIR_BASE"] =  "/var/log/AppFabric"
	os.environ ["SCRIPTS_DIR"] = "~/AppFabric/runtime/scripts/openadn"
#	os.environ ["SERVICE_CONFIG_FILE"] = "services.cfg"
#	os.environ ["SERVICE_CONFIG_PATH"] = "~/AppFabric/runtime/configurations"
	os.environ ["SERVICE_CONTAINER_BINPATH"] = "~/AppFabric/containers/OpenADN_service_container/"
#	os.environ ["PROXY_SERVICE_CONFIG_FILE"] = "proxy_services.cfg"	
#	os.environ ["PROXY_SERVICE_CONFIG_PATH"] = "~/AppFabric/runtime/configurations"
	os.environ ["PROXY_SERVICE_CONTAINER_BINPATH"] = "~/AppFabric/containers/OpenADN_proxy_container/"	
	#setting up logging
	'''
	module_lgr = logging.getLogger("main")
	module_lgr.disable_existing_loggers = False
	module_lgr.setLevel(logging.DEBUG)
	log_fh = logging.FileHandler(os.path.join(util.createPathString(os.path.expanduser(os.environ["LOGS_DIR_BASE"])), instance_id), mode = "w+")
	log_fh.setLevel(logging.DEBUG)
	frmt = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
	log_fh.setFormatter(frmt)
	module_lgr.addHandler(log_fh)
	'''
	'''
	# setting up load logger
	load_lgr = logging.getLogger("load")
	load_lgr.disable_existing_loggers = False
	load_lgr.setLevel(logging.INFO)
	log_fh_load = logging.FileHandler(os.path.join(util.createPathString(os.path.expanduser(os.environ["LOGS_DIR_BASE"]),"load"), instance_id), mode = "w+")
	log_fh_load.setLevel(logging.INFO)
	frmt = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
	log_fh_load.setFormatter(frmt)
	load_lgr.addHandler(log_fh_load)
	'''
	# setting up local environment
	local_env = {}
	local_env["CONTEXT"] = zmq.Context()
	local_env["BROKER_ID"] = instance_id
	local_env["CONTROLLER_CONNECT_STRING"] = "tcp://" + str(os.environ["LC_ADDR"]) + ":" +  str(os.environ["LC_PORT"])
	local_env["WAIT_INTERVAL"] = 100
	local_env["EXT_ADDR"] = os.environ.get("IP_ADDR")
	#ADDED BY TARA 
	# Initial setting of the application configuration (To be recieved from .sh and sent to c port (saved there) 
	local_env["MAX_RESOURCE_LIMIT"] = os.environ["MAX_RESOURCE_LIMIT"]
	local_env["MAX_REQUESTS"]=os.environ["MAX_REQUESTS"] 
	local_env["MAX_REQUESTS_PER_CLIENT"]=os.environ["MAX_REQUESTS_PER_CLIENT"] 
	local_env["BANNED_DEST"]=os.environ["BANNED_DEST"]	
	local_env["TYPE_OF_TRAFFIC"]=os.environ["TYPE_OF_TRAFFIC"]
	local_env["HOST_NAME"]=os.environ["HOST_NAME"]
	local_env["NODE_HOST_DIR"]=os.environ["NODE_HOST_DIR"]
	
	try:
		controlSocketHandler = cPort.controlSocketHandler(local_env)
		#print ("controller connect string", local_env["CONTROLLER_CONNECT_STRING"] )
		controlSocketHandler.start()
	
	except:
		#module_lgr.debug ("Error: unable to start control Thread")
		sys.exit()	
	
	signal(SIGINT, sigHandler)

	while  globalRun == True:
		pause()

	controlSocketHandler.join ()
#	print ("Returned to main thread")
	local_env["CONTEXT"].destroy ()
#	print ("destroyed the context")
	sys.exit()


def sigHandler (rSignal, frame):
	signal(SIGINT, SIG_IGN)
	module_lgr.info ("Ctrl-C received")
	print ("Ctrl-C received")
#	pdb.set_trace()
#	controlSocketHandler.join ()
#	print ("Returned to main thread")
	global globalRun
	globalRun = False
#	sys.exit()
#	signal(SIGINT, SIG_DFL) 
#	os.kill (os.getpid(), SIGINT)

	#signal(SIGINT, sigHandler)

	#Waiting on the signal
	#signal(SIGINT, sigHandler)
	#while True:
	#	pause()

if __name__ == '__main__':
	main()
