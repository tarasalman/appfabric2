#!/usr/local/bin/python3

import os
import sys
import threading
from appfabric.openadn.tPort import tPort
from appfabric.common import util
from appfabric.common.errors import *
from appfabric.common.unbufferedStream import unbuffered
import logging
from signal import *
import zmq
import uuid
import pdb
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto import Random

unbufferedWrite = unbuffered(sys.stdout)

# Setting up global context
def main(): 
	#unbufferedWrite.write("inside tInit")
	os.environ ["LOGS_DIR_BASE"] =  "/var/log/AppFabric"
	#setting up logging
	module_lgr = logging.getLogger("main")
	module_lgr.disable_existing_loggers = False
	module_lgr.setLevel(logging.DEBUG)
	log_fh = logging.FileHandler(os.path.join(util.createPathString(os.path.expanduser(os.environ["LOGS_DIR_BASE"])),os.environ["BROKER_ID"]),mode = "w")
	log_fh.setLevel(logging.DEBUG)
	frmt = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
	log_fh.setFormatter(frmt)
	module_lgr.addHandler(log_fh)


	#unbufferedWrite.write("Starting tPort")	
	try:
		tPortControlThreadHandler = tPort.tPortControlThreadHandler()
		tPortControlThreadHandler.start()
	except Tport_Init_Error as e:
		module_lgr.debug ("Error: unable to start tPort control Thread:", e.msg )
		sys.exit()
	print ("*********************************************************") 
	print ("Alpha Datacenter Starting Its Node Controller") 
	print ("*********************************************************") 	
	print ("Node Controller Started and Connected to Local Controller \n Allowing Global Controller To Deploy its Services on Alpha Datacenter\n")
	# ADDED BY TARA
	# PKI generation for NC 
	# To be removed later  
	#random_generator = Random.new().read
	#key = RSA.generate(1024, random_generator)
	#print("%s"random_generator) 
	#Server_public_key = key.publickey().exportKey("PEM", passphrase=None, pkcs=1)
	#text_file = open("NC/PUKey.txt", "wb")
	#text_file.write(Server_public_key)
	#text_file.close()
	#text_file = open("NC/PRKey.txt", "wb")
	#text_file.write(key.exportKey("PEM", passphrase=None, pkcs=1))
	#text_file.close()
	tPortControlThreadHandler.join()
	
	sys.exit()


if __name__ == '__main__':
	main()
