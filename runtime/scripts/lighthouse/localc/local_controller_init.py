#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#





from appfabric.lighthouse.localc.lc_controller import lc_controller
import zmq
import os
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto import Random

if __name__ == '__main__':

        # for Mininet based experiments
#       sys.stdout = unbuffered(sys.stdout)

       # os.environ["RESOURCE_DRIVER_CONFIG"] = os.path.expanduser("~/AppFabric/runtime/configurations/resource_drivers.cfg")
       # os.environ["DRIVERS_DIR"] = "~/driver_mininet/drivers"
       # os.environ["SERVICE_CONFIG_PATH"] = os.path.expanduser("~/AppFabric/runtime/configurations/services.cfg")

	context = zmq.Context()
	try:	
		#ADDED BY TARA 
		#PKI Gerenation for LC 
		#To be removed later
		#print(str(os.environ["SITE_NAME"]))
		random_generator = Random.new().read #Random
		key = RSA.generate(1024, random_generator) #Private key
		#print("%s"random_generator) 
		Server_public_key = key.publickey().exportKey("PEM", passphrase=None, pkcs=1) #Public key
		text_file = open("LC/PUKey%s.txt"%str(os.environ["SITE_NAME"]), "wb")
		text_file.write(Server_public_key)
		text_file.close()
		text_file = open("LC/PRKey%s.txt"%str(os.environ["SITE_NAME"]), "wb")
		text_file.write(key.exportKey("PEM", passphrase=None, pkcs=1))
		text_file.close()
		controller = lc_controller(context)
		controller.start()
	except Controller_Init_Error as e:
        #       self.unbufferedWrite.write ("Error starting controller", e.msg()) 
		sys.exit()

        #TODO propagate kill signals to the threads and close them properly

