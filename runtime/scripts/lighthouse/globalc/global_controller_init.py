#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


from appfabric.lighthouse.globalc.gc_controller import gc_controller
import zmq 
import os
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto import Random

if __name__ == '__main__':

        # for Mininet based experiments
#       sys.stdout = unbuffered(sys.stdout)
        os.environ["HOME"] = os.path.expanduser("~")
        os.environ["SERVICE_CONFIG_FILE"] = os.path.expanduser("~/AppFabric/runtime/configurations/services.cfg")
        os.environ["WORKFLOW_CONFIG_FILE"] = os.path.expanduser("~/AppFabric/runtime/configurations/workflowConfig.cfg")
        os.environ["CLASSIFIER_FILES_PATH"] = os.path.expanduser("~/AppFabric/runtime/configurations/classifiers")
        context = zmq.Context()

        try:
		#ADDED BY TARA 
		#PKI Generation for GC 
		#To be removed later 
                random_generator = Random.new().read
              #  key = RSA.generate(1024, random_generator)
		#print("%s"random_generator) 
               # Server_public_key = key.publickey().exportKey("PEM", passphrase=None, pkcs=1)
               # text_file = open("GC/PUKey.txt", "wb")
               # text_file.write(Server_public_key)
               # text_file.close()
               # text_file = open("GC/PRKey.txt", "wb")
               # text_file.write(key.exportKey("PEM", passphrase=None, pkcs=1))
               # text_file.close()
                controller = gc_controller(context)
                controller.start()
        except Controller_Init_Error as e:
                print("Error starting controller", e.msg())
                sys.exit()

        #TODO propagate kill signals to the threads and close them properly
