from distutils.core import setup


setup(  name='AppFabric',
	version='0.1',
	author='Subharthi Paul',
	author_email= 'subharthipaul@gmail.com',
	package_dir={'appfabric': 'src'},
	packages= [     'appfabric', 
			'appfabric.openadn', 
			'appfabric.openadn.cPort',
			'appfabric.openadn.pPort',
			'appfabric.openadn.sPort',
			'appfabric.openadn.tPort',
			'appfabric.openadn.appfabricSocketLib',
			'appfabric.common',
			'appfabric.lighthouse',
			'appfabric.lighthouse.globalc',
			'appfabric.lighthouse.localc',
			'appfabric.lighthouse.ns'
			]
	)
