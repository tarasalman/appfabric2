#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import os, sys
from socket import *
import fcntl
from select import select
import getopt, struct
from appfabric.common import genericTable, util, tableDefs
from appfabric.common.unbufferedStream import unbuffered
import zmq
import struct
import subprocess
import threading
import logging
import copy
from appfabric.common.errors import *
from appfabric.common.msgTypes import *
# This port will be assigned per host (Physical/VM)
# 1. It will tunnel packets over UDP tunnels
# 2. It wil also act as a switch: Doing ID/Locator mapping
# 3. Directly connects to  packet level MBs 

#sys.stdout = unbuffered(sys.stdout)

class tPortControlThreadHandler (threading.Thread):

	def __init__(self):
		super(tPortControlThreadHandler, self).__init__()
		'''
		## Setup module logger first
		self.module_lgr = logging.getLogger("main.tPortControlThread")
		self.module_lgr.propagate = True
		'''	
		# reading the environemnt variables and setting global parameters
		try:
			self.portCfg = {}
			self.portCfg= {"TC_CONNECTION_STRING": os.environ["IPC_PATH"],
					"TUN_IF_ADDR": os.environ["IF_ADDR"],
					"BROKER_ID": os.environ["BROKER_ID"],
					"TUN_IF_NAME":("tun-" + str(os.environ["BROKER_ID"]))[0:16],
					"UDP_PORT": 8888 # Magic number 
					}
		except:
			raise  Tport_Init_Error("Check the env parameter")	
		packetServicesTable = genericTable.table(tableDefs.serviceTableDef)
		
		self.context = zmq.Context()
		self.tcPort = self.context.socket(zmq.DEALER) 
		self.tcPort.setsockopt(zmq.LINGER, 0) 
		self.poller = zmq.Poller()

	


	def run (self):
	
		# Initialize/create  the tun interface
		TUN_IF = self.initialize_tun()
	
		# Establish control channel	
		try: 
			self.tcPort.connect(self.portCfg["TC_CONNECTION_STRING"])
			self.poller.register (self.tcPort, zmq.POLLIN)	
		except zmq.ZMQError as e:
			raise Tport_Init_Error("Could not create tPort-cPort connection")
			exit()	

		#TODO create a I/O thread to listen on UDP port 
		#TODO create a I/O thread to listen on tun port

	 	# Start Polling the interfaces
		init_msg_sent_flag = False
		while True:
			try:
				socks= dict(self.poller.poll(1))
			except:
				pass
	#		packet = list(os.read(TUN_IF.fileno(), 2048))
#			print (packet)
			if init_msg_sent_flag == False:
				self.tcPort.send_pyobj(I_CT_PORT_INIT_MSG)
				init_msg_sent_flag = True
			if  socks.get(self.tcPort) == zmq.POLLIN:
			#	self.module_lgr.info ("Message to UDP tunnel Port from control Thread")
				# Control messages are never multipart
				msg = self.tcPort.recv_pyobj()
				cmd = msg ["TYPE"]
				if cmd == I_CT_PORT_STOP:
					#print ("tPort: Received Port stop command from cPort")
					break
	#			if cmd == I_CT_SERVICE_START:
					
			#packet = list(os.read(TUN_IF.fileno(), 2048))
			#print ("Packet =", packet)


		self.interface_stop()



	def initialize_tun(self):	
		try:
			# Open the tun device file
			TUN_IF = open("/dev/net/tun", 'r+b',buffering=0)
			
			# Ask it to allocate a new tun interace IF_NAME, 0x0001 = IFF_TUN, 0x0002 = IFF_TAP
			
			ifr = struct.pack('16sh', copy.deepcopy(self.portCfg["TUN_IF_NAME"]).encode('utf-8'), 0x0001) 
			fcntl.ioctl(TUN_IF, 0x400454ca, ifr)
			
			#Inititialize the interface
			subprocess.check_call(' '.join(['ifconfig',self.portCfg["TUN_IF_NAME"],self.portCfg["TUN_IF_ADDR"],'up']),shell= True)
		except:
			raise UDP_Tunnel_Port_Init_Error("UDP tunnel Port Init failed")
		
		return TUN_IF		



	def interface_stop(self):
		print ("closing tPort")
		self.tcPort.close()
		try:
			subprocess.check_call(' '.join(["ifconfig", self.portCfg["TUN_IF_NAME"],"down"]),shell= True)
		except:
			print ("Could not remove the tun interface")
		self.context.destroy()
		sys.exit()	

'''

if __name__ == '__main__':
        main()

'''
"""
	def join(self, timeout = None):
                self.stopRequest.clear()
        #       self.lgr.info ("Ctrl-C Handling: set the event flag")
                super(udpTunnelPortHandler,self).join(timeout)

"""

"""

MAGIC_WORD = "Wazaaaaaaaaaaahhhh !"

TUNSETIFF = 0x400454ca
IFF_TUN   = 0x0001
IFF_TAP   = 0x0002

TUNMODE = IFF_TUN
MODE = 0
DEBUG = 0

def usage(status=0):
    print "Usage: tunproxy [-s port|-c targetip:port] [-e]"
    sys.exit(status)

opts = getopt.getopt(sys.argv[1:],"s:c:ehd")

for opt,optarg in opts[0]:
    if opt == "-h":
        usage()
    elif opt == "-d":
        DEBUG += 1
    elif opt == "-s":
        MODE = 1
        PORT = int(optarg)
    elif opt == "-c":
        MODE = 2
        IP,PORT = optarg.split(":")
        PORT = int(PORT)
        peer = (IP,PORT)
    elif opt == "-e":
        TUNMODE = IFF_TAP
        
if MODE == 0:
    usage(1)


f = os.open("/dev/net/tun", os.O_RDWR)
ifs = ioctl(f, TUNSETIFF, struct.pack("16sH", "toto%d", TUNMODE))
ifname = ifs[:16].strip("\x00")

print "Allocated interface %s. Configure it and use it" % ifname

s = socket(AF_INET, SOCK_DGRAM)

try:
    if MODE == 1:
        s.bind(("", PORT))
        while 1:
            word,peer = s.recvfrom(1500)
            if word == MAGIC_WORD:
                break
            print "Bad magic word for %s:%i" % peer
        s.sendto(MAGIC_WORD, peer)
    else:
        s.sendto(MAGIC_WORD, peer)
        word,peer = s.recvfrom(1500)
        if word != MAGIC_WORD:
            print "Bad magic word for %s:%i" % peer
            sys.exit(2)
    print "Connection with %s:%i established" % peer
    
    while 1:
        r = select([f,s],[],[])[0][0]
        if r == f:
            if DEBUG: os.write(1,">")
            s.sendto(os.read(f,1500),peer)
        else:
            buf,p = s.recvfrom(1500)
            if p != peer:
                print "Got packet from %s:%i instead of %s:%i" % (p+peer)
                continue
            if DEBUG: os.write(1,"<")
            os.write(f, buf)
except KeyboardInterrupt:
    print "Stopped by user."
"""
