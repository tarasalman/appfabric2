           #
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import os
import sys
import zmq
import threading
import logging
import signal
from time import sleep

from appfabric.common import runStates, genericTable, tableDefs,processSpawn,heartbeatReqThread
from appfabric.common.unbufferedStream import unbuffered
from appfabric.common.errors import *
from appfabric.common.runStates import *
from appfabric.common.msgTypes import *
from appfabric.common.constants import *
from appfabric.common import classifier
from appfabric.common import util
from appfabric.common.aes_msg import AESCipher_Msg

class intServicePortHandler (threading.Thread):
	
	def __init__(self, context, msg):
		super (intServicePortHandler, self).__init__()
		self.stopRequest = threading.Event()
		self.stopRequest.set()
		# save the message to use as a return message too
		self.saved_initMsg = msg		
		'''
		# Setting up the logger
		self.module_lgr = logging.getLogger("main.controlThread.servicePortHandler")
		self.module_lgr.propagate = True
		'''
		# Configuring Port Parameters
		self.portCfg = { }
		# get configuration from the controller message
		try:
			self.portCfg = {
				"SERVICE_NAME": msg["SERVICE_NAME"],
				"SERVICE_ID": msg["SERVICE_ID"],
				"EXECUTABLE": msg ["EXECUTABLE"],
				"ARGS": msg ["ARGS"],
				"CONTEXT": context,
				"PROCESS_INTERFACE_STATE":PROCESS_INTERFACE_INACTIVE,
				"IPC_COMM":util.createPathString("tmp",
								"ipc",
								msg["SERVICE_NAME"], 
								str(msg["SERVICE_ID"])
								),
				"INPROC_COMM":util.createPathString("tmp",
								"inproc", 
								msg["SERVICE_NAME"], 
								str(msg["SERVICE_ID"]),
								)
				}
		except KeyError:
			self.module_lgr.debug ("Error: Configuration message for the service is not complete")
			raise Service_Port_Init_Error ("Error: Oops!! The service configuration has errors. Cannot instatiate service")
	
		except Invalid_Connect_String_Error as e:
			raise Service_Port_Init_Error(e.msg)

		# get an ID for the object for logging errors
		self.ID = "SERVICE_PORT"+ "/" + msg["SERVICE_NAME"] + "/"+ str (msg["SERVICE_ID"])
		
		# TODO: May be src and destination addresses are not required
	#	self.egressPortTable = genericTable.table(tableDefs.egressPortTableDef)
	#	self.ingressPortTable = genericTable.table(tableDefs.ingressPortTableDef)
		self.egressPortTable = {}
		self.ingressPortTable = {}
		self.appRouter = None
		
		

		#creating the ports
		
		# for communication with the controlPort
		self.sicPort = self.portCfg["CONTEXT"].socket(zmq.DEALER)
		self.sicPort.setsockopt(zmq.LINGER, 0)
		
		# for data communication between port and service 
		self.sipPort_data = self.portCfg["CONTEXT"].socket(zmq.DEALER) 		
		self.sipPort_data.setsockopt(zmq.LINGER, 0)		
	
		# for control msgs between port and service
		self.sipPort_control = self.portCfg["CONTEXT"].socket(zmq.DEALER)
		self.sipPort_control.setsockopt(zmq.LINGER, 0)
	
		# Hearbeat socket
		self.sipPort_heartbeat = self.portCfg["CONTEXT"].socket (zmq.DEALER)
		self.sipPort_heartbeat.setsockopt(zmq.LINGER, 0)

		#setting up the poller
		self.poller = zmq.Poller()

		# Initializing the hearbeat thread object
		self.heartbeatReportThreadHandler = None
	
		# Setting up the logger
		self.module_lgr = None
		self.module_lgr_fh = None
		self.init_module_logger()

		# set unbuffered stream
		self.unbufferedWrite= unbuffered(sys.stdout)
	
	
	def  init_module_logger(self):
		self.module_lgr = logging.getLogger("sPort")
		self.module_lgr.disable_existing_loggers = False
		self.module_lgr.setLevel(logging.DEBUG)
		self.module_lgr_fh = logging.FileHandler(os.path.join(util.createPathString(os.path.expanduser(os.environ["LOGS_DIR_BASE"]),"sPort"), self.portCfg["SERVICE_ID"]), mode = "w+")		
		self.module_lgr_fh.setLevel(logging.DEBUG)
		frmt = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		self.module_lgr_fh.setFormatter(frmt)
		self.module_lgr.addHandler(self.module_lgr_fh)
		
		

	#def sigChldHandler(signal, frame):
	#	self.module_lgr.info("<%s> <%d> has exited", self.portCfg["SERVICE_NAME"], self.portCfg["SERVICE_ID"])
			
	def run(self):
	#	self.module_lgr.info ("Start Interface thread for <%s>", self.ID)
#		signal.getsignal(signal.SIGCHLD, self.sigChldHandler)

		try:
			self.sipPort_data.bind(util.createConnectString ("ipc",self.portCfg["IPC_COMM"],"service_data"))
			self.sipPort_control.bind(util.createConnectString( "ipc",self.portCfg["IPC_COMM"],"service_control"))
			self.poller.register(self.sipPort_data, zmq.POLLIN)
			self.poller.register(self.sipPort_control, zmq.POLLIN)
		except zmq.ZMQError as e:
			self.module_lgr.debug("Error:<%s>:%s",self.ID,e.__str__())
			self.module_lgr.debug(e.msg)
			#TODO Inform about failure to control thread 
		
		while True:
			try:
				self.sicPort.connect( util.createConnectString("inproc", self.portCfg["INPROC_COMM"], "cthread_data"))
				break
			except:
				sleep(1)
				continue
		try:	
			self.poller.register(self.sicPort,zmq.POLLIN)
			#TODO inform the control thread
		except zmq.ZMQError as e:
			self.module_lgr.debug("Error:<%s>:%s",self.ID,e.__str__())
			self.module_lgr.debug(e.msg)
			#TODO Inform about failure to control thread 

		
		path = os.path.join(os.path.expanduser(os.environ ["SERVICE_CONTAINER_BINPATH"]), self.portCfg["SERVICE_NAME"],"bin")
		#self.unbufferedWrite.write ("PATH = ", path)
	#	self.module_lgr.info ("spawning new process for service %s", self.ID)

                #Environemnt passed to the new service 
		env = {
			"IPC_COMM": self.portCfg["IPC_COMM"],
			"SERVICE_NAME":self.portCfg["SERVICE_NAME"],
			"SERVICE_ID":str(self.portCfg["SERVICE_ID"]),
			"NUM_PORTS_MAX": str(10)
			}

		launched_service = False	
		while self.stopRequest.isSet():
		#	self.unbufferedWrite.write "Running service thread for - ", self.portCfg["SERVICE_NAME"]

			try:
				socks = dict(self.poller.poll(1))			
			except zmq.ZMQError as e:
				pass

			if launched_service == False:
			#	self.unbufferedWrite.write ("sPort: Trying to spawn new process for service %s"%(self.portCfg["SERVICE_NAME"]))
				try:
					childPID = processSpawn.spawnProcess(path,self.portCfg["EXECUTABLE"], self.portCfg["ARGS"],env)
				except Process_Spawn_Error as e:
					self.module_lgr.debug (e.msg)
					self.unbufferedWrite.write (e.msg)
					self.service_stop()
					#TODO inform control thread
				self.portCfg["PROCESS_INTERFACE_STATE"]= PROCESS_INTERFACE_LAUNCHED
				self.portCfg ["PROCESS_ID"] = childPID
			#	self.module_lgr.info(self.portCfg)
				launched_service = True
			#	self.unbufferedWrite.write ("sPort: Launched service %s"%(self.portCfg["SERVICE_NAME"]))
			if socks.get(self.sicPort)==zmq.POLLIN:
				msg = {}
				msg = self.sicPort.recv_pyobj()
				
			
				if msg["TYPE"] == I_CS_ADD_EGRESS_LINK_REQ:
					self.add_egress_link(**msg)	

				elif msg["TYPE"] == I_CS_ADD_INGRESS_LINK_REQ:
					self.add_ingress_link(**msg)

				elif msg["TYPE"] == I_CS_ADD_APP_ROUTER_REQ:	
					#self.unbufferedWrite.write("Received App Routing Table")
					self.add_app_router(**msg)
				elif msg["TYPE"] ==CB_RESUME_ANOM: 
					print ("I am in sPort")
				else:
					pass	

				
				
			if socks.get(self.sipPort_control)==zmq.POLLIN:
				msg = {}
				msg = self.sipPort_control.recv_pyobj()
		
					
				if msg["TYPE"] == I_PS_INIT:
					#self.unbufferedWrite.write ("%s"%self.saved_initMsg)
					#set process state to active					
					self.portCfg["PROCESS_INTERFACE_STATE"]= PROCESS_INTERFACE_ACTIVE
					# setting up connection to the hearbeat socket
					try:
						self.sipPort_heartbeat.bind(util.createConnectString("inproc",self.portCfg["INPROC_COMM"], "service_heartbeat_report"))
						self.poller.register(self.sipPort_heartbeat, zmq.POLLIN)
					except zmq.ZMQError as e:
						self.module_lgr.debug("Error:hearbeat socket: <%s>: %s",self.ID, e.__str__())
						#TODO: Send this failure event to control thread
						continue
					# Initialize and start the heartbeat thread
					try:
						#Initialize heartbeat thread
					#	self.module_lgr.info ("Starting Heartbeat Req Thread <%s>", self.ID )
						self.heartbeatReportThreadHandler = heartbeatReqThread.heartbeatReqThread(**self.portCfg)
						self.heartbeatReportThreadHandler.start()	
					except:
						self.module_lgr.debug("Error:Hearbeat Req thread: <%s>",self.ID)						
						# TODO: Send failure report to control thread
							
					# Send Init successful message to control thread
					self.saved_initMsg["TYPE"] = I_SC_INIT
					self.saved_initMsg["STATUS"] = SUCCESS
			#		self.unbufferedWrite.write (self.saved_initMsg)
			#		self.unbufferedWrite.write ("sPort: sending I_SC_INIT after starting service %s"%(self.saved_initMsg["SERVICE_NAME"]))		
					self.sicPort.send_pyobj(self.saved_initMsg)
			#		self.unbufferedWrite.write ("sPort: sending I_SC_INIT after starting service %s"%(self.saved_initMsg["SERVICE_NAME"]))		
				else:
					#TODO 
					pass
				
			if socks.get(self.sipPort_data)==zmq.POLLIN:
				msg = {}
				msg = self.sipPort_data.recv_pyobj()
			#	self.module_lgr.info(msg)
				outport =  self.appRouter.match_pattern(msg)
				try: 
					# ADDED BY TARA 
					# This part of the code configure the message recieved from the service about client anomolies 
					# The message will eventually reach global controller so all node controller and client variables are added here  
					if (msg["Anomolous"]== 1):
						new_msg ={
							"User": msg["User"], #User name 
							"Service": self.portCfg["SERVICE_NAME"], #From which services 
							"TYPE":I_SC_SERVICE_ANOM_INTEGRITY, #message type
							"WF_ID":self.saved_initMsg["WF_ID"], #the workflow 
							"SourceIP": msg['APP_HDR']["headers"]["SourceIP"], #client IP
							"SourcePort": msg['APP_HDR']["headers"]["SourcePort"], #Client port 
							"DestinationURL":msg['APP_HDR']["path"], #the destination path
							"DestinationIP":msg['APP_HDR']["host"].split(":")[0], #Destination IP
							"DestinationPort":msg['APP_HDR']["host"].split(":")[1], #Port 
							"WF_INSTANCE_ID":self.saved_initMsg["WF_INSTANCE_ID"], #workflow instant 
							"RM_ID":self.saved_initMsg["RM_ID"],# resource manger 
							"SERVICE_ID": self.saved_initMsg ["SERVICE_ID"], #SErvice ID
							"PROXY_PORT_LIST":msg["APLS_HDR"]["PROXY_PORT_LIST"],
							"NoOfRequests": msg["NoOfRequests"], #number of requests (all) 
							"NoOfRequestsPerClient":msg["NoOfRequestsPerClient"], # number of request (per client)
							"NoOfRequestsPerClient_dest":msg["NoOfRequestsPerClient_dest"] #number of request to the same destination(per client)
					#NOTE: probably not all the information are needed but I am just sending them all in case  		
						}
						print ("Anomlous message type %s detected from User %s "%(msg["Anomolous"],msg["User"]))
					if (msg["Anomolous"]== 2):
						new_msg ={
							"User": msg["User"],
							"Service": self.portCfg["SERVICE_NAME"],
							"TYPE":I_SC_SERVICE_ANOM_DEST,
							"WF_ID":self.saved_initMsg["WF_ID"], 
							"SourceIP": msg['APP_HDR']["headers"]["SourceIP"],
							"DestinationURL":msg['APP_HDR']["path"],
							"DestinationIP":msg['APP_HDR']["host"].split(":")[0],
							"DestinationPort":msg['APP_HDR']["host"].split(":")[1],
							"SourcePort": msg['APP_HDR']["headers"]["SourcePort"],
							"WF_INSTANCE_ID":self.saved_initMsg["WF_INSTANCE_ID"],
							"RM_ID":self.saved_initMsg["RM_ID"],
							"SERVICE_ID": self.saved_initMsg ["SERVICE_ID"],
							"PROXY_PORT_LIST":msg["APLS_HDR"]["PROXY_PORT_LIST"],
							"NoOfRequests": msg["NoOfRequests"],
							"NoOfRequestsPerClient":msg["NoOfRequestsPerClient"],
							"NoOfRequestsPerClient_dest":msg["NoOfRequestsPerClient_dest"]
						}
						print ("Anomlous message type %s detected from User %s "%(msg["Anomolous"],msg["User"])) 
					if (msg["Anomolous"]== 3):
						new_msg ={ 
							"User": msg["User"],
							"Service": self.portCfg["SERVICE_NAME"],
							"TYPE":I_SC_SERVICE_ANOM_TRAFFIC,
							"SourceIP": msg['APP_HDR']["headers"]["SourceIP"],
							"SourcePort": msg['APP_HDR']["headers"]["SourcePort"],
							"DestinationURL":msg['APP_HDR']["path"],
							"DestinationIP":msg['APP_HDR']["host"].split(":")[0],
							"DestinationPort":msg['APP_HDR']["host"].split(":")[1],
							"WF_ID":self.saved_initMsg["WF_ID"], 
							"WF_INSTANCE_ID":self.saved_initMsg["WF_INSTANCE_ID"],
							"RM_ID":self.saved_initMsg["RM_ID"],
							"SERVICE_ID": self.saved_initMsg ["SERVICE_ID"],
							"NoOfRequests": msg["NoOfRequests"],
							"NoOfRequestsPerClient":msg["NoOfRequestsPerClient"],
							"NoOfRequestsPerClient_dest":msg["NoOfRequestsPerClient_dest"]
						}
						print ("Anomlous message type %s detected from User %s "%(msg["Anomolous"],msg["User"]))
					if (msg["Anomolous"]== 4):
						new_msg ={ 
							"User": msg["User"],
							"Service": self.portCfg["SERVICE_NAME"],
							"SourceIP": msg['APP_HDR']["headers"]["SourceIP"],
							"SourcePort":msg['APP_HDR']["headers"]["SourcePort"],
							"DestinationURL":msg['APP_HDR']["path"],
							"DestinationIP":msg['APP_HDR']["host"].split(":")[0],
							"DestinationPort":msg['APP_HDR']["host"].split(":")[1],
							"TYPE":I_SC_SERVICE_ANOM_MAX_REQUEST,
							"WF_ID":self.saved_initMsg["WF_ID"], 
							"WF_INSTANCE_ID":self.saved_initMsg["WF_INSTANCE_ID"],
							"RM_ID":self.saved_initMsg["RM_ID"],
							"SERVICE_ID": self.saved_initMsg ["SERVICE_ID"],
							"PROXY_PORT_LIST":msg["APLS_HDR"]["PROXY_PORT_LIST"],
							"NoOfRequests": msg["NoOfRequests"],
							"NoOfRequestsPerClient":msg["NoOfRequestsPerClient"],
							"NoOfRequestsPerClient_dest":msg["NoOfRequestsPerClient_dest"]
						}
						print ("Anomlous message type %s detected from User %s "%(msg["Anomolous"],msg["User"])) 
					if (msg["Anomolous"]== 5):
						new_msg ={ 
							"User": msg["User"],
							"SourceIP": msg['APP_HDR']["headers"]["SourceIP"],
							"SourcePort":msg['APP_HDR']["headers"]["SourcePort"],
							"Service": self.portCfg["SERVICE_NAME"],
							"DestinationURL":msg['APP_HDR']["path"],
							"DestinationIP":msg['APP_HDR']["host"].split(":")[0],
							"DestinationPort":msg['APP_HDR']["host"].split(":")[1],
							"TYPE":I_SC_SERVICE_ANOM_MAX_CLIENT_REQ,
							"WF_ID":self.saved_initMsg["WF_ID"], 
							"WF_INSTANCE_ID":self.saved_initMsg["WF_INSTANCE_ID"],
							"RM_ID":self.saved_initMsg["RM_ID"],
							"SERVICE_ID": self.saved_initMsg ["SERVICE_ID"],
							"PROXY_PORT_LIST":msg["APLS_HDR"]["PROXY_PORT_LIST"],
							"NoOfRequests": msg["NoOfRequests"],
							"NoOfRequestsPerClient":msg["NoOfRequestsPerClient"],
							"NoOfRequestsPerClient_dest":msg["NoOfRequestsPerClient_dest"]
						}
						print ("Anomlous message type %s detected from User %s "%(msg["Anomolous"],new_msg["User"]))
					
					# ADDED By Tara 
					# The follwoing code encrypt the message before sending it to the global controller ... 	
					# The secret key was set from the beginning between the two 	
					f = open("/home/openstack/AppFabric/experiments/physical_machines/SecretKeys/NC_LC.bin","rb") # Get the NC_LC secret key 							(This key is agreed on after the first msg and both parties has it)			
					message= f.read()
					message=message.split(b"    ")
					key2= message[1] #key
					iv= message[0] #nonce 
					msg2= new_msg.pop("TYPE") # Type is not encrypted so that it can be recognized in the if statement 
					new_msg= AESCipher_Msg(key2.decode()).encrypt(new_msg,iv) # Decrypt the whole message 	
					new_msg["TYPE"]= msg2 # return the message TYPE
					self.sicPort.send_pyobj(new_msg)
					msg = self.sicPort_control.recv_pyobj()
					print (msg)
				except: 
					pass 
				if outport == -2:
					# drop message
					self.unbufferedWrite.write ("\nsPort: Dropping message at service %s"%(self.portCfg["SERVICE_NAME"]))
				else:
				#	self.unbufferedWrite.write ("sPort: Forwarding message at service %s to port num = %s"%(self.portCfg["SERVICE_NAME"], outport))
					#print(msg)
					msg["APLS_HDR"]["OUT_PORT_NUM"] = outport
					self.egressPortTable[outport].send_pyobj(msg)		 	
			
			if socks.get(self.sipPort_heartbeat)==zmq.POLLIN:
				try:
					msg= self.sipPort_heartbeat.recv_pyobj()
					if msg["TYPE"] == I_MGMT_SERVICE_DEAD:
					#	self.module_lgr.info("Dead: <%s>", self.ID)	
						#TODO: Update service state
						#TODO inform control thread
						#TODO close the heartbeat thread
						pass		
				except: 
					pass
					
			# poll the ingress ports 
			for ingressPortNum, ingressPortSocket in self.ingressPortTable.items():
		#		self.unbufferedWrite.write "\n Still Alive", self.portCfg["SERVICE_NAME"] 
				if  socks.get(ingressPortSocket) == zmq.POLLIN:
					try:
						
						msg = ingressPortSocket.recv_pyobj()
						msg["APLS_HDR"]["IN_PORT_NUM"] = ingressPortNum
					
					#	self.unbufferedWrite.write ("sPort: Message received by sPort in %s"%(self.portCfg["SERVICE_NAME"]))
					#	APLS_HDR = msg.pop()
						
					except:
						pass
					try:
						self.sipPort_data.send_pyobj(msg)
					except zmq.zmqError as e:
						if e.errno == zmq.EAGAIN:
							self.module_lgr.debug ("Message could not not be sent to:<%s>", self.ID)
							#TODO: Handle this by raising some flags
							pass
		# stop the service interface	
		self.service_stop()

	
	def service_stop(self):
		#closing the service depends upon the state
		if self.portCfg["PROCESS_INTERFACE_STATE"] >= PROCESS_INTERFACE_LAUNCHED:
			try:
				os.killpg(self.portCfg["PROCESS_ID"],signal.SIGKILL)
				#self.unbufferedWrite.write("\nkilled msg service <%s>", self.ID)
			except OSError as e:
				self.module_lgr.debug ("Unable to kill process %d for <%s>: %s", self.portCfg["PROCESS_ID"], self.ID, e.__str__())
				pass
		else:	
		#	self.module_lgr.info("Need to remove process2")
	#		os.killpg(self.portCFG["PROCESS_ID"],signal.SIGKILL)
			pass
		self.heartbeatReportThreadHandler.join()
		self.unbufferedWrite.write ("\nclosed hearbeat thread" )
		self.sipPort_data.close()
		self.sipPort_control.close()
		self.sicPort.close()
		for portNum, socket in self.ingressPortTable.items():
			socket.close()
		for portNum, socket in self.egressPortTable.items():
			socket.close()

	def join(self, timeout = None):
	#	self.module_lgr.info(" Killing the service interface for:<%s>", self.ID)
		self.stopRequest.clear()
	#	self.service_stop()
        #       self.lgr.info ("Ctrl-C Handling: set the event flag")
		super(intServicePortHandler,self).join(timeout)

	
	def add_egress_link(self, **msg):
		if msg["TRANSPORT"] == "INPROC":
		
			try:
				connectString = util.createConnectString(
									"inproc",
									"tmp/inproc/",
									msg["SERVICE_NAME"],
									str(msg["SERVICE_ID"]),
									str(msg["PORT_NUM"])
									)
			except OSError as e:
		#		self.unbufferedWrite.write ("Service Port: connect String cannot be computed")
				raise Service_Port_Add_Egress_Port_Failed (e.msg)
		elif msg["TRANSPORT"] == "TCP":
			connectString = "tcp://*"
				
	#	self.unbufferedWrite.write ("Service Port: Connect String =", connectString)				

		try:
			egressPort = self.portCfg["CONTEXT"].socket(zmq.DEALER)
			egressPort.setsockopt(zmq.LINGER, 0)			

			if msg["TRANSPORT"] == "INPROC":						        
				egressPort.bind(connectString)
				msg["LINK_CONNECT_STRING"] = connectString 
			#	self.unbufferedWrite.write ("Service Port: Adding Egress Link : Bound to INPROC %s", connectString)
			elif msg["TRANSPORT"] == "TCP":
				ipOption = chr(97)+chr(4)+chr(1)+chr(10)
			#	flag = egressPort.setsockopt_unicode(zmq.IP_OPTIONS,ipOption)
				portNum = egressPort.bind_to_random_port(connectString)
			#	flag = egressPort.setsockopt_unicode(zmq.IP_OPTIONS,ipOption)
				msg["LINK_CONNECT_STRING"] = util.createConnectString("tcp", str(os.environ["IP_ADDR"]), str(portNum))
		except zmq.ZMQError as e:
				self.unbufferedWrite.write ("Binding Error")
				self.module_lgr(e.msg)
				#TODO: Send back message of failure to control Thread
		
		self.egressPortTable[int(msg["PORT_NUM"])] = egressPort


		msg["TYPE"] = I_SC_ADD_EGRESS_LINK_REP
		msg["STATUS"]= SUCCESS
		self.sicPort.send_pyobj(msg)




	def add_ingress_link(self, **msg):
		self.unbufferedWrite.write ("Service Port: Adding ingress")	
		try:
			ingressPort = self.portCfg["CONTEXT"].socket(zmq.DEALER)
			ingressPort.setsockopt(zmq.LINGER, 0)			
		except zmq.ZMQError as e:
			self.unbufferedWrite.write ("Service Thread: Ingress link cannot be added", e.__str__())
			self.module_lgr(e.msg)

		while True:	
			try:	
				ingressPort.connect(str(msg["LINK_CONNECT_STRING"]))
				break
			except:
				sleep(1)
				continue
		try:
			self.poller.register(ingressPort, zmq.POLLIN)
		except zmq.ZMQError as e:
			self.unbufferedWrite.write ("Service Thread: Ingress link cannot be added", e.__str__())
			self.module_lgr(e.msg)
			#TODO: Send back message of failure to control Thread
		
		#Add ingress port to the table
		self.ingressPortTable[int(msg["PORT_NUM"])] = ingressPort
		
		msg["TYPE"] = I_SC_ADD_INGRESS_LINK_REP
		msg["STATUS"]= SUCCESS
		
		self.sicPort.send_pyobj(msg)		

	
	def add_app_router(self, **msg):
	#	self.unbufferedWrite.write ("Service: %s"%(msg))
		classifier_type = msg["APP_ROUTING_TABLE"]["classifier_type"]
		classifier_list = msg["APP_ROUTING_TABLE"]["classifier_list"]

		self.appRouter = classifier.classifier(classifier_type)
		self.appRouter.create_appRoutingTable (classifier_list)
	#	self.appRouter.self.unbufferedWrite.write_appRoutingTable()
		msg["TYPE"] = I_SC_ADD_APP_ROUTER_REP
		msg["STATUS"] = SUCCESS
		self.sicPort.send_pyobj(msg)
		

