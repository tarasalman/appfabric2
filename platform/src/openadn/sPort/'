import os
import sys
import zmq
import threading
import logging
from appfabric.common import runStates, genericTable, tableDefs,processSpawn,heartbeatReqThread
from appfabric.common.errors import *
from appfabric.common.runStates import *
from appfabric.common.msgTypes import *
from appfabric.common.constants import *
import signal
from appfabric.common import util

class intServicePortHandler (threading.Thread):
	
	def __init__(self, context, msg):
		super (intServicePortHandler, self).__init__()
		self.stopRequest = threading.Event()
		self.stopRequest.set()
		# save the message to use as a return message too
		self.saved_initMsg = msg		

		# Setting up the logger
		self.lgr_module = logging.getLogger("main.controlThread.servicePortHandler")
		self.lgr_module.propagate = True
	
		# Configuring Port Parameters
		self.portCfg = { }
		# get configuration from the controller message
		try:
			self.portCfg = {
				"SERVICE_NAME": msg["SERVICE_NAME"],
				"SERVICE_ID": msg["SERVICE_ID"],
				"EXECUTABLE": msg ["EXECUTABLE"],
				"ARGS": msg ["ARGS"],
				"CONTEXT": context,
				"PROCESS_INTERFACE_STATE":PROCESS_INTERFACE_INACTIVE,
				"IPC_COMM":util.createPathString("tmp",
								"ipc",
								msg["SERVICE_NAME"], 
								str(msg["SERVICE_ID"])
								),
				"INPROC_COMM":util.createPathString("tmp",
								"inproc", 
								msg["SERVICE_NAME"], 
								str(msg["SERVICE_ID"]),
								)
				}
		except KeyError:
			self.lgr_module.debug ("Error: Configuration message for the service is not complete")
			raise Service_Port_Init_Error ("Error: Oops!! The service configuration has errors. Cannot instatiate service")
	
		except Invalid_Connect_String_Error as e:
			raise Service_Port_Init_Error(e.msg)

		# get an ID for the object for logging errors
		self.ID = "SERVICE_PORT"+ "/" + msg["SERVICE_NAME"] + "/"+ str (msg["SERVICE_ID"])
		
		# TODO: May be src and destination addresses are not required
		self.egressPortTable = genericTable.table(tableDefs.egressPortTableDef)
		self.ingressPortTable = genericTable.table(tableDefs.ingressPortTableDef)
		self.appRoutingTable = appRouting
		#creating the ports
		
		# for communication with the controlPort
		self.sicPort = self.portCfg["CONTEXT"].socket(zmq.DEALER)
		self.sicPort.setsockopt(zmq.LINGER, 0)
		
		# for data communication between port and service 
		self.sipPort_data = self.portCfg["CONTEXT"].socket(zmq.DEALER) 		
		self.sipPort_data.setsockopt(zmq.LINGER, 0)		
	
		# for control msgs between port and service
		self.sipPort_control = self.portCfg["CONTEXT"].socket(zmq.DEALER)
		self.sipPort_control.setsockopt(zmq.LINGER, 0)
	
		# Hearbeat socket
		self.sipPort_heartbeat = self.portCfg["CONTEXT"].socket (zmq.DEALER)
		self.sipPort_heartbeat.setsockopt(zmq.LINGER, 0)

		#setting up the poller
		self.poller = zmq.Poller()

		# Initializing the hearbeat thread object
		self.heartbeatReportThreadHandler = heartbeatReqThread.heartbeatReqThread(**self.portCfg)
	

	#def sigChldHandler(signal, frame):
	#	self.lgr_module.info("<%s> <%d> has exited", self.portCfg["SERVICE_NAME"], self.portCfg["SERVICE_ID"])
			
	def run(self):
		self.lgr_module.info ("Start Interface thread for <%s>", self.ID)
#		signal.getsignal(signal.SIGCHLD, self.sigChldHandler)

		try:
			self.sipPort_data.bind(util.createConnectString ("ipc",self.portCfg["IPC_COMM"],"service_data"))
			self.sipPort_control.bind(util.createConnectString( "ipc",self.portCfg["IPC_COMM"],"service_control"))
			self.poller.register(self.sipPort_data, zmq.POLLIN)
			self.poller.register(self.sipPort_control, zmq.POLLIN)

			self.sicPort.connect( util.createConnectString("inproc", self.portCfg["INPROC_COMM"], "cthread_data"))
			self.poller.register(self.sicPort,zmq.POLLIN)
			#TODO inform the control thread
		except zmq.ZMQError as e:
			self.lgr_module.debug("Error:<%s>:%s",self.ID,e.__str__())
			self.lgr_module.debug(e.msg)
			#TODO Inform about failure to control thread 

		
		path = os.path.join(os.path.expanduser(os.environ ["SERVICE_CONTAINER_BINPATH"]), self.portCfg["SERVICE_NAME"],"bin")
		#print ("PATH = ", path)
		self.lgr_module.info ("spawning new process for service %s", self.ID)

                #Environemnt passed to the new service 
		env = {
			"IPC_COMM": self.portCfg["IPC_COMM"],
			"SERVICE_NAME":self.portCfg["SERVICE_NAME"],
			"SERVICE_ID":str(self.portCfg["SERVICE_ID"]),
			"NUM_PORTS_MAX": str(10)
			}
		try:   
			childPID = processSpawn.spawnProcess(path,self.portCfg["EXECUTABLE"], self.portCfg["ARGS"],env)
		except Process_Spawn_Error as e:
			self.lgr_module.debug (e.msg)
			print (e.msg)
			self.service_stop()
			#TODO inform control thread

		self.portCfg["PROCESS_INTERFACE_STATE"]= PROCESS_INTERFACE_LAUNCHED
		self.portCfg ["PROCESS_ID"] = childPID
		self.lgr_module.info(self.portCfg)
	
		while self.stopRequest.isSet():
		#	print "Running service thread for - ", self.portCfg["SERVICE_NAME"]

			try:
				socks = dict(self.poller.poll(1))			
			except zmq.ZMQError as e:
				pass
			if socks.get(self.sicPort)==zmq.POLLIN:
				msg = { }
				while True:
					try:
					#	print "Stuck here 1"
						msg = self.sicPort.recv_pyobj()
						more = self.sicPort.getsockopt(zmq.RCVMORE)
						if more != True:
							break
					except: 
						pass
				try:
					if msg["TYPE"] == I_CS_ADD_EGRESS_LINK_REQ:
						self.add_egress_link(**msg)	

					if msg["TYPE"] == I_CS_ADD_INGRESS_LINK_REQ:
						self.add_ingress_link(**msg)	
				except:
					pass	

				'''
				if msg["TYPE"] == I_CS_ADD_APP_ROUTING_TABLE_REQ:
					print("Received App Routing Table")
					##self.add_app_routing_table(**msg)
				'''
			if socks.get(self.sipPort_control)==zmq.POLLIN:
				msg = { }
				try:		
					msg = self.sipPort_control.recv_pyobj()
				except:
					pass
		
				try:		
					if msg["TYPE"] == I_PS_INIT:
						#print ("sPort: Received I_PS_INIT from service: %s"%(msg))
						#set process state to active
						self.portCfg["PROCESS_INTERFACE_STATE"]= PROCESS_INTERFACE_ACTIVE
						# setting up connection to the hearbeat socket
						try:
							self.sipPort_heartbeat.bind(util.createConnectString("inproc",self.portCfg["INPROC_COMM"], "service_heartbeat_report"))
							self.poller.register(self.sipPort_heartbeat, zmq.POLLIN)
						except zmq.ZMQError as e:
							self.lgr_module.debug("Error:hearbeat socket: <%s>: %s",self.ID, e.__str__())
							#TODO: Send this failure event to control thread
							continue
						# Initialize and start the heartbeat thread
						try:
							#Initialize heartbeat thread
							self.lgr_module.info ("Starting Heartbeat Req Thread <%s>", self.ID )
							heartbeatReportThreadHandler = heartbeatReqThread.heartbeatReqThread(**self.portCfg)
							self.heartbeatReportThreadHandler.start()	
						except:
							self.lgr_module.debug("Error:Hearbeat Req thread: <%s>",self.ID)						
							# TODO: Send failure report to control thread
							continue	
						# Send Init successful message to control thread
						self.saved_initMsg["TYPE"] = I_SC_INIT
						self.saved_initMsg["STATUS"] = SUCCESS
						print (self.saved_initMsg)
						self.sicPort.send_pyobj(self.saved_initMsg)
				except:
					#TODO 
					pass
				
			if socks.get(self.sipPort_data)==zmq.POLLIN:
				msg ={ }
				try:
					msg = self.sipPort_data.recv_pyobj()
				except:
					pass
				self.lgr_module.info(msg)
				APLS_HDR = msg.pop()
				try:
			#		interface = self.egressPortTable.lookup_row_by_primary_key(APP_TAG=msg["APP_TAG"],PATH_ID=msg["PATH_ID"])
					interface = self.egressPortTable.lookup_row_by_primary_key(**APLS_HDR)
					msg.insert(2, APLS_HDR)

				except Row_Lookup_Error as e:
					self.lgr_module.debug(e.msg)
					self.lgr_module.debug("Dropping message: <%s>", self.ID)
					print ("Dropping Msg")
					pass
				else:
					interface["SOCKET"].send_pyobj(msg)
					self.lgr_module.info("Received Data message from <%s>",self.ID)
			
			if socks.get(self.sipPort_heartbeat)==zmq.POLLIN:
				msg = { }
				try:
					msg= self.sipPort_heartbeat.recv_pyobj()
					if msg["TYPE"] == I_MGMT_SERVICE_DEAD:
						self.lgr_module.info("Dead: <%s>", self.ID)	
						#TODO: Update service state
						#TODO inform control thread
						#TODO close the heartbeat thread
							
				except: 
					pass
					
			# poll the ingress ports 
			for ingressPort in self.ingressPortTable:
		#		print "\n Still Alive", self.portCfg["SERVICE_NAME"] 
				if  socks.get(ingressPort["SOCKET"]) == zmq.POLLIN:
					msg = { }
					try:
						msg = ingressPort["SOCKET"].recv_pyobj()
					except:
						pass
					try:
						self.lgr_module.info("Sending Data message to:<%s>",self.ID)
						#	print ("Sending Data message to ", self.portCfg["SERVICE_NAME"],self.portCfg["SERVICE_ID"], msg)
						try:
							self.sipPort_data.send_pyobj(msg)
						except zmq.zmqError as e:
							if e.errno == zmq.EAGAIN:
								self.lgr_module.debug ("Message could not not be sent to:<%s>", self.ID)
								#TODO: Handle this by raising some flags
								pass
					except:	
						pass
		# stop the service interface	
		self.service_stop()

	
	def service_stop(self):
		#closing the service depends upon the state
		if self.portCfg["PROCESS_INTERFACE_STATE"] >= PROCESS_INTERFACE_LAUNCHED:
			try:
				os.killpg(self.portCfg["PROCESS_ID"],signal.SIGKILL)
				print("killed msg service <%s>", self.ID)
			except OSError as e:
				self.lgr_module.debug ("Unable to kill process %d for <%s>: %s", self.portCfg["PROCESS_ID"], self.ID, e.__str__())
				pass
		else:	
			self.lgr_module.info("Need to remove process2")
	#		os.killpg(self.portCFG["PROCESS_ID"],signal.SIGKILL)
		self.heartbeatReportThreadHandler.join()
		print ("closed hearbeat thread" )
		self.sipPort_data.close()
		self.sipPort_control.close()
		self.sicPort.close()
		for port in self.ingressPortTable:
		#	print (port)
			port["SOCKET"].close()
		for port in self.egressPortTable:
		#	print (port)
			port["SOCKET"].close()
		

	def join(self, timeout = None):
		self.lgr_module.info(" Killing the service interface for:<%s>", self.ID)
		self.stopRequest.clear()
	#	self.service_stop()
        #       self.lgr.info ("Ctrl-C Handling: set the event flag")
		super(intServicePortHandler,self).join(timeout)

	
	def add_egress_link(self, **msg):
		if msg["TRANSPORT"] == "INPROC":
		
			try:
				connectString = util.createConnectString(
									"inproc",
									"tmp/inproc/",
									msg["SERVICE_NAME"],
									str(msg["SERVICE_ID"]),
									str(msg["PORT_NUM"])
									)
			except OSError as e:
		#		print ("Service Port: connect String cannot be computed")
				raise Service_Port_Add_Egress_Port_Failed (e.msg)
		elif msg["TRANSPORT"] == "TCP":
			connectString = "tcp://*"
				
	#	print ("Service Port: Connect String =", connectString)				

		try:
			egressPort = self.portCfg["CONTEXT"].socket(zmq.DEALER)
			egressPort.setsockopt(zmq.LINGER, 0)			

			if msg["TRANSPORT"] == "INPROC":						        
				egressPort.bind(connectString)
				msg["LINK_CONNECT_STRING"] = connectString 
			#	print ("Service Port: Adding Egress Link : Bound to INPROC %s", connectString)
			elif msg["TRANSPORT"] == "TCP":
				ipOption = chr(97)+chr(4)+chr(1)+chr(10)
			#	flag = egressPort.setsockopt_unicode(zmq.IP_OPTIONS,ipOption)
				portNum = egressPort.bind_to_random_port(connectString)
			#	flag = egressPort.setsockopt_unicode(zmq.IP_OPTIONS,ipOption)
				msg["LINK_CONNECT_STRING"] = util.createConnectString("tcp", str(os.environ["IP_ADDR"]), str(portNum))
		#		msg["CONNECT_TO_PORT_NUM"] = portNum
		#		msg["CONNECT_TO_IP_ADDRESS"] = "127.0.0.1"
		        #	flag = egressPort.setsockopt_unicode(zmq.OPENADN,str("Hi"))
		except zmq.ZMQError as e:
				print ("Binding Error")
				self.lgr_module(e.msg)
				#TODO: Send back message of failure to control Thread
			# Add egress port to the table
		msg["SOCKET"] = egressPort
		try:
			self.egressPortTable.insert_row_from_kvpairlist(**msg)
		except Invalid_Row_Entry as e:
			self.lgr.debug(e.msg)
			#Send error message to control Thread
			print ("Failed to update egress port table")
	#	print ("\n\n Egress table for ", self.portCfg["SERVICE_NAME"])
	#	self.egressPortTable.print_table()
		msg["TYPE"] = I_SC_ADD_EGRESS_LINK_REP
		msg["STATUS"]= SUCCESS
		del msg["SOCKET"]
		del msg["INTERFACE_TYPE"]
		del msg["INTERFACE_STATE"]
		self.sicPort.send_pyobj(msg)




	def add_ingress_link(self, **msg):
		print ("Service Port: Adding ingress")	
		try:
			ingressPort = self.portCfg["CONTEXT"].socket(zmq.DEALER)
			ingressPort.setsockopt(zmq.LINGER, 0)			

			ingressPort.connect(str(msg["LINK_CONNECT_STRING"]))
			self.poller.register(ingressPort, zmq.POLLIN)
		except zmq.ZMQError as e:
			print ("Service Thread: Ingress link cannot be added", e.__str__())
			self.lgr_module(e.msg)
			#TODO: Send back message of failure to control Thread
		#Add ingress port to the table

		msg["SOCKET"] = ingressPort
		try:
			self.ingressPortTable.insert_row_from_kvpairlist(**msg)
		except Invalid_Row_Entry as e:
			self.lgr.debug(e.msg)
			 #TODO Send error message to control Thread
		msg["TYPE"] = I_SC_ADD_INGRESS_LINK_REP
		msg["STATUS"]= SUCCESS
		del msg["SOCKET"]
		
		#print ("\n\nIngress Table for", self.portCfg["SERVICE_NAME"])
		#ingressPortTable.print_table()
		#print ("\n\n")	
		self.sicPort.send_pyobj(msg)		

	'''
	def add_app_routing_table(self, **msg):
		print (msg)
		
	'''

