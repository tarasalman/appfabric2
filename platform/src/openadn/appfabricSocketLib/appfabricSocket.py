#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import zmq
from appfabric.common.msgTypes import *
from .channelStates import * 
from .errors import *
from appfabric.common import util
from appfabric.common import heartbeatRepThread
import logging
from time import sleep

# this class is not thread safe. It is a shared resource that is shared by all threads.A thread may submit mesages to it and the messages wil be queued by the zmq layer
#TYPE:100 : INIT
#TYPE:101 : KEEPALIVE

#TODO: NO multipart messages

NOBLOCK= 1
BLOCK = 2

POLLIN  = 1
POLLOUT = 2

MSG_READY = 1
MSG_NOT_READY = 2

class appfabricSocket:
	def __init__(self, path, _numIOThreads=None):
		self.context = zmq.Context()
		self.IPC_COMM = path
		if _numIOThreads == None:
			numIOThreads = 1
		else:
			numIOThreads = _numIOThreads
		self.context = zmq.Context(io_threads = numIOThreads)
		self.dataSocket = self.context.socket (zmq.DEALER)
		self.dataSocket.setsockopt (zmq.LINGER, 0)
		
		self.controlSocket = self.context.socket (zmq.DEALER)
		self.controlSocket.setsockopt (zmq.LINGER, 0)
		
		self.channelState = CREATED
		self.heartbeatRepHandlerThread = heartbeatRepThread.heartbeatRepThread(self.context, path)
		self.poller = zmq.Poller()
	
	# for using zmq poller, Later wrap the poller function too
	def register_to_poller(self, flag=None):
		if flag == None:
			flag = POLLIN
		try:
			if flag == POLLIN:
				self.poller.register(self.dataSocket,zmq.POLLIN)
			else:
				self.poller.register(self.dataSocket, zmq.POLLOUT)

		except zmq.ZMQError as e:
			raise Appfabric_Socket_Error (e.__str__())

	#TODO Support POLLIN for now, implement POLLOUT later
	def poll(self,timeout=None):
		socks = dict(self.poller.poll(timeout))
		if  socks.get(self.dataSocket) == zmq.POLLIN:
			return MSG_READY
		else:
			return MSG_NOT_READY
		
#	def get_socket:
#		return self.dataSocket
	
	def connect(self):
		#TODO Set the errno
		maxTries = 1000
		numTries = 0	
		while True:
			try:
				self.dataSocket.connect(util.createConnectString("ipc",self.IPC_COMM, "service_data"))
				break
			except zmq.ZMQError as e:
				if numTries < maxTries:
					numTries +=1
				#	sleep(1)
					continue
				else:
					raise Appfabric_Socket_Error (e.__str__())
		numTries = 0
		while True:
			try:
				self.controlSocket.connect(util.createConnectString("ipc",self.IPC_COMM, "service_control"))
				break
			except zmq.ZMQError as e:
				if numTries < maxTries:
					numTries +=1
				#	sleep(1)
					continue
				else:
					print ("could not initialize service")
					raise Appfabric_Socket_Error (e.__str__())
		try:	
			self.heartbeatRepHandlerThread.start()
			#TODO define messages
			print ("service Sending I_PS_INIT to sPort")
			msg = {}
			msg["TYPE"] = I_PS_INIT
			self.controlSocket.send_pyobj(msg)
		except zmq.ZMQError as e:
			raise Appfabric_Socket_Error (e.__str__())	

	#TODO: stop copying data
	def close(self):
		self.socket.close()
		self.heartbeatRepHandlerThread.join()

	
	def send_msg(self, msg, flag=None):
		#TODO temporarily
	#	print ("\n\nService1 sending message 2")	
	#	self.socket.send_pyobj(self.getMessage(TYPE= "I_PS_DATA_X", MSG = msg))
		if flag == None:
			flag = BLOCK
		if flag == BLOCK:
			self.dataSocket.send_pyobj (msg)
		else:
			self.dataSocket.send_pyobj(msg,zmq.NOBLOCK)

	#TODO wrap in try block
	def recv_msg (self,flag=None):
		if flag == None:
			flag = NOBLOCK
		msg = { }
		if flag == BLOCK:
			while True:
				msg = self.dataSocket.recv_pyobj()
				more = self.dataSocket.getsockopt(zmq.RCVMORE)
				if more!=True:
					break

		elif flag == NOBLOCK:
			while True:
				msg = self.dataSocket.recv_pyobj(zmq.NOBLOCK)
				more = self.dataSocket.getsockopt(zmq.RCVMORE)
				if more!=True:
					break
		return msg
				

	



