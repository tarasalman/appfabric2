#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import zmq
from appfabric.common.msgTypes import *
from appfabric.common.constants import *
from appfabric.common.errors import *
from appfabric.common.runStates import *
from appfabric.common import tableDefs
from appfabric.common import util
from ..sPort import sPort
from ..pPort import pSwitchPort

def cb_proxy_port_config_report_load (self,kwargs):
			
	try:
		#print ("looking up proxy service table to add egress/ingress link")
		row = self.activeProxyServiceTable.lookup_row_by_primary_key(
										SERVICE_NAME = kwargs["SERVICE_NAME"], 
										SERVICE_ID = kwargs["SERVICE_ID"]
										)
		#print ("Found proxy service, sernding link setup message to it")
	except Row_Lookup_Error as e:
		print ("Could not find proxy service in proxy service table")
		raise WF_Activate_Error ("Could not find Workflow Proxy to activate: %s"%(e.msg))
		
	row["SERVICE_INTERFACE"].send_pyobj(kwargs)
