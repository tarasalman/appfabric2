#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import zmq
from appfabric.common.msgTypes import *
from appfabric.common.constants import *
from appfabric.common.errors import *
from appfabric.common.runStates import *
from appfabric.common import processSpawn,tableDefs,util,readConfig
from ..sPort import sPort
from ..pPort import pSwitchPort
from . import udp_tunnel_port_init
from time import sleep
import os
import re


def bootstrap(self):
                #load service configuration table from config file
                '''
                try:
                       self.serviceTable = readConfig.readConfig(os.environ["SERVICE_CONFIG_FILE"],os.environ['SERVICE_CONFIG_PATH'])
                       self.proxyServiceTable = readConfig.readConfig(os.environ["PROXY_SERVICE_CONFIG_FILE"],os.environ['PROXY_SERVICE_CONFIG_PATH'])
                      # print (self.serviceTable.getConfig())
                      # print (self.proxyServiceTable.getConfig())

                except readConfig.Read_Config_Error as e:
                        raise Control_Thread_Bootstrap_Error (e.msg)
		# Report to controller that the broker is up and running and wait for controller to respond
                '''
		
                while True:
                        try:
                                self.controlPort.connect(self.controllerAddressString)
                                break
                        except:
                                sleep(1)
                                continue
                try:
                        self.poller.register (self.controlPort, zmq.POLLIN)
                except zmq.ZMQError as e :
                        raise Control_Thread_Bootstrap_Error ("Error:Broker-Controller connection:",e.msg )

                # Start 3-way handshake with the controller
                init_sent_flag = False
                while True:
                        try:
                                socks= dict(self.poller.poll(1))
                        except zmq.ZMQError:
                                print ("Error")
                                pass
                        #self.unbufferedWrite.write ("h1")
                        if init_sent_flag == False:
                                BC_INIT_SYN_MSG["BROKER_ID"] = self.portCfg["BROKER_ID"]
                                BC_INIT_SYN_MSG["EXT_ADDR"] = self.portCfg["EXT_ADDR"]

                                self.controlPort.send_pyobj(BC_INIT_SYN_MSG)
                               # print ("sent BC_INIT_SYN_MSG to controller")
                              #  self.lgr.info("Broker-Controller sending BC_INIT message sent")
                                init_sent_flag = True
                        #self.unbufferedWrite.write ("h2")
                        if  socks.get(self.controlPort) == zmq.POLLIN:
                                msg = self.controlPort.recv_pyobj()
                                if msg['TYPE'] == CB_INIT_SYN_ACK:
                                       #print ("Received CB_INIT_SYN_ACK")
                                        try:
                                        	# Close the old controller connection
                                                self.poller.unregister(self.controlPort)
                                                self.controlPort.disconnect(self.controllerAddressString)
                                        except zmq.ZMQError as e:
                                                 print ("Error ", e.__str__())
                                                 raise Control_Thread_Bootstrap_Error(e.__str__())

                                        # open a new control port
                                        self.controllerAddressString = re.sub(r':[0-9]+',(":"+str(msg["PORT"])),self.controllerAddressString)
                                       #  print (self.controllerAddressString)
                                        while True:
                                                try:
                                                        self.controlPort.connect(self.controllerAddressString)
                                                        break
                                                except:    
                                                       sleep(1)
                                                       continue

                                        try:
                                                self.poller.register (self.controlPort, zmq.POLLIN)
                                        except zmq.ZMQError as e:
                                                print ("Error", e.__str__())
                                                raise Control_Thread_Bootstrap_Error(e.__str__())
                                        # Inititialize the controller init message
                                        # Have to wait for the CB_INIT_ACK to get the IP address of the IF
                                        self.portCfg["TUNNEL_IF_ADDR"] = msg["TUNNEL_IF_ADDR"]
                                        try:
                                                udp_tunnel_port_init.udp_tunnel_port_init(self)
                                        except UDP_Tunnel_Port_Init_Error as e:
                                              #  self.lgr.info ("udp tunnel port init failed in", self.portCfg["BROKER_ID"])
                                                self.unbufferedWrite.write("udp tunnel port init failed in", self.portCfg["BROKER_ID"])
                                                raise Control_Thread_Bootstrap_Error (e.msg)  
                                        # preparing the BC_INIT_ACK message 
                                        BC_INIT_ACK_MSG["BROKER_ID"] = self.portCfg["BROKER_ID"]
                                        BC_INIT_ACK_MSG["AVAILABLE_RESOURCE"] = self.portCfg["AVAILABLE_RESOURCE"]
                                        BC_INIT_ACK_MSG["STATUS"] = SUCCESS
                                        self.controlPort.send_pyobj(BC_INIT_ACK_MSG)
                                        break
                                    
