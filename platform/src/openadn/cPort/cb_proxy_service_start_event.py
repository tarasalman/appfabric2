import zmq
import copy
from appfabric.common.msgTypes import *
from appfabric.common.constants import *
from appfabric.common.errors import *
from appfabric.common.runStates import *
from appfabric.common import tableDefs
from appfabric.common import util
from ..sPort import sPort
from ..pPort import pSwitchPort


def proxy_service_start_event (self, msg):
               # try:
                      #  cisPort = None
                      #  service =self.proxyServiceTable.getConfig(SERVICE_NAME = msg["SERVICE_NAME"])
                      #  serviceExecutable = service["EXECUTABLE"]
               # except Read_Config_Error as e:
                #        raise Proxy_Service_Start_Error (e.msg) 
                               
                          
                try:
                        proxySwitchPortHandler = pSwitchPort.proxySwitchPortThread(self.context,msg)
                except Proxy_Service_Start_Error as e:
                        raise Proxy_Service_Start_Error (e.msg)
		
                try:
                        cisPort = self.context.socket(zmq.DEALER)
                        cisPort.setsockopt (zmq.LINGER, 0)

                        cisPort.bind(util.createConnectString("inproc",
									"/tmp/inproc",
									msg["SERVICE_NAME"],
									str(msg["SERVICE_ID"]),
									"cthread_data")
									)
                except zmq.ZMQError as e:
                        print ("Error")
                        raise Proxy_Switch_Start_Error (e.__str__())
                except Invalid_Connect_String_Error as e:
                        raise Proxy_Switch_Start_Error (e.msg)
                try:
                        self.poller.register(cisPort,zmq.POLLIN)
                except zmq.ZMQError as e:
                        print ("Error: Could not regiser proxy Switch Port into the poller", e.__str__())
                        cisPort.close()
                        raise Proxy_Service_Start_Error (e.__str__())


                try:
                        proxySwitchPortHandler.start()
                except:
                        print ("Error: Failed to start thread for proxySwitchPortHandler")               
                        raise Proxy_Service_Start_Error ("Failed to start thread for proxySwitchPortHandler")
                # update the active proxy service table
                proxy_service_table_update = copy.deepcopy(msg)
                proxy_service_table_update.update ({
                        "SERVICE_INTERFACE":cisPort,
                        "SERVICE_INTERFACE_STATE":SERVICE_INTERFACE_INACTIVE,
                        "THREAD_ID":proxySwitchPortHandler,
                })
                try:
                        self.activeProxyServiceTable.insert_row_from_kvpairlist(**proxy_service_table_update)
                     #   print ("active service table")
                     #   self.activeProxyServiceTable.print_table()
                except Invalid_Row_Entry as e:
                        print ("Invalid table entry", e.msg)
                        raise Proxy_Service_Start_Error (e.msg) 


