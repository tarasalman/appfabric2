#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import zmq
import os
from appfabric.common.msgTypes import *
from appfabric.common.constants import *
from appfabric.common.errors import *
from appfabric.common.runStates import *
from appfabric.common import processSpawn
from appfabric.common import tableDefs
from appfabric.common import util
from ..sPort import sPort
from ..pPort import pSwitchPort


def udp_tunnel_port_init(self):
                try:
                        ipcPathString = util.createConnectString("ipc","tmp/ipc",
                                                                "tunnel",
                                                                str(self.portCfg["BROKER_ID"])
                                                                )
                except Invalid_Connect_String_Error as e:
                        raise UDP_Tunnel_Port_Init_Error (e.msg)

                #inprocPathString =os.path.join("inproc:///tmp/inproc", msg["SERVICE_NAME"],str( msg["SERVICE_ID"]))
                # TODO Hard code the path, executable and arguments for now
                #path = "/home/openflow/AppFabric/host1/tPort"
                path = os.path.expanduser(os.environ["SCRIPTS_DIR"])
               # print (path)
                executable = "tInit.py"
                arguments = "NULL"
              #  print (ipcPathString)
                env = {
                        "IPC_PATH": ipcPathString,
                        "IF_ADDR":self.portCfg["TUNNEL_IF_ADDR"],
                        "BROKER_ID": self.portCfg["BROKER_ID"]
                        }
               # print (env)
                try:
                        self.udpTunnelPort = self.context.socket(zmq.DEALER)
                        self.udpTunnelPort.setsockopt(zmq.LINGER, 0)
                        self.udpTunnelPort.bind(ipcPathString)
                        self.poller.register(self.udpTunnelPort, zmq.POLLIN)
                except zmq.ZMQError as e:
                        print ("Unable to open udp tunnel port")
                        raise UDP_Tunnel_Port_Init_Error(e.msg)

               # print ("starting new process for tPort")

                try:
                        childPID = processSpawn.spawnProcess(path,executable, arguments, env)
                except Process_Spawn_Error as e:
                        print (" ".join(["Failed to start UDP tunnel Port:",e.msg]))
                        raise UDP_Tunnel_Port_Init_Error(" ".join(["Failed to start UDP tunnel Port:",e.msg]))
                # Waiting for udp tunnel to come up
                #TODO Add a timer to the wait and notify if unsuccessful within timeout
                msg = self.udpTunnelPort.recv_pyobj()
                if msg ["TYPE"] == I_CT_PORT_INIT:
                       # print ("udp tunnel port initialized: %s"%(self.portCfg["BROKER_ID"]))
                       # self.lgr.info ("udp tunnel port initialized: %s"%(self.portCfg["BROKER_ID"]))
                       pass
