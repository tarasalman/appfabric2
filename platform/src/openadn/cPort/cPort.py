#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#

'''

		cPort is the control port through whcih the control plane talks to a data plane node



'''

import os
import sys
import logging
from appfabric.common import genericTable, tableDefs, processSpawn, util, readConfig
from appfabric.common.errors import *
from appfabric.common.msgTypes import *
from appfabric.common.runStates import *
from appfabric.common.constants import *
from appfabric.common  import xmlToDict
from appfabric.common.unbufferedStream import unbuffered


from . import cb_service_start_event, cb_proxy_service_start_event 
from . import cb_service_port_config_add_link, udp_tunnel_port_init 
from . import cb_service_port_config_add_app_routing_table
from . import cb_proxy_port_config_activate_wf
from . import cb_proxy_port_config_stop_notifications
from . import cb_proxy_port_config_report_load
from . import cb_proxy_port_release_wf_instance
from . import cb_proxy_port_config_resume_notifications
from . import cb_service_port_config_anom_response
from . import cthread_bootstrap, cthread_stop

from ..sPort import sPort
from ..pPort import pSwitchPort

import zmq
import threading
import uuid
import signal
import time
import pdb
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto import Random
from appfabric.common.aes_msg import AESCipher_Msg
import ast
# TODO: write exit status to a file to know the status when satrted
# From a failure - restart only the broker
# bootup - the whole host


class controlSocketHandler (threading.Thread):

	def __init__ (self,env):
		super (controlSocketHandler, self).__init__()
		self.stopRequest = threading.Event()
		self.stopRequest.set()	
 	
		self.activeProxyServiceTable = genericTable.table(tableDefs.activeProxyServiceTableDef)
		self.activeServiceTable =  genericTable.table(tableDefs.activeServiceTableDef)
		self.linkTable = genericTable.table(tableDefs.linkTableDef)
		self.poller = zmq.Poller()
		self.context = env["CONTEXT"]
			
		self.controlPort= self.context.socket(zmq.DEALER)
		self.controlPort.setsockopt (zmq.LINGER, 0)
		'''
		self.lgr = logging.getLogger("main.controlThread")
		self.lgr.propagate = True
		'''
		self.controllerAddressString = env["CONTROLLER_CONNECT_STRING"]
		#Added By Tara 	
		# The following code initiate node controller (application) variables that are defined in 04-node_conroller.sh 
		self.portCfg = { }
		self.portCfg["BROKER_ID"] =  env["BROKER_ID"]
		self.portCfg["EXT_ADDR"] = env["EXT_ADDR"]
		self.portCfg["TUNNEL_IF_ADDR"]	= "0.0.0.0"
		x = env["MAX_RESOURCE_LIMIT"]
		self.portCfg["AVAILABLE_RESOURCE"] = float(x) if '.' in x else int(x)
		#Those are the new variables 
		self.portCfg["MAX_REQUESTS"]= int(env["MAX_REQUESTS"])
		self.portCfg["MAX_REQUESTS_PER_CLIENT"]= int(env["MAX_REQUESTS_PER_CLIENT"])
		self.portCfg["BANNED_DEST"]= env["BANNED_DEST"]
		self.portCfg["TYPE_OF_TRAFFIC"]= env["TYPE_OF_TRAFFIC"]		
		self.portCfg["HOST_NAME"]= env["HOST_NAME"]
		self.portCfg["NODE_HOST_DIR"]= env["NODE_HOST_DIR"]
		self.portCfg["key"]= "LCNC"	
		self.portCfg["iv"]= Random.get_random_bytes(16)
		
		try:
			#Save them to file 
			text_file = open("NC/NC_Config_%s.txt"%env["HOST_NAME"], "wb")
			text_file.write(bytes("AVAILABLE_RESOURCE: %s \n"%(env["MAX_RESOURCE_LIMIT"]), 'UTF-8'))
			text_file.write(bytes("MAX_REQUESTS: %s \n"%(env["MAX_REQUESTS"]), 'UTF-8'))
			text_file.write(bytes("MAX_REQUESTS_PER_CLIENT: %s \n"%(env["MAX_REQUESTS_PER_CLIENT"]), 'UTF-8'))
			text_file.write(bytes("BANNED_DEST: %s \n"%(env["BANNED_DEST"]), 'UTF-8'))
			text_file.write(bytes("TYPE_OF_TRAFFIC: %s \n"%(env["TYPE_OF_TRAFFIC"]), 'UTF-8'))
			text_file.close() 
			#secret key with NC
			#text_file = open("SecretKeys/NC_LC.bin", "wb")
			#self.portCfg["AES_KEY"]= AES.new(key_to_be_used,AES.MODE_CBC,iv)
			#save secret key for future use
			#[text_file.write(x) for x in (self.portCfg["iv"],b"    ",self.portCfg["key"].encode())]
			#text_file.close()
		except: 
			pass 
		#Control channel to UDP tunnel Port
		self.udpTunnelPort = None
		self.udpTunnelPortHandler = None

		
		# setting up logger
		self.module_lgr = None
		self.module_lgr_fh = None
		#self.init_module_logger()
		
		# set unbuffered stream
		self.unbufferedWrite= unbuffered(sys.stdout)
		#self.unbufferedWrite.write("Node Controller profile is saved in Local Controller") 
		self.init_module_logger()

	def  init_module_logger(self):
		self.module_lgr = logging.getLogger("cPort")
		self.module_lgr.disable_existing_loggers = False
		self.module_lgr.setLevel(logging.DEBUG)
		self.module_lgr_fh = logging.FileHandler(os.path.join(util.createPathString(os.path.expanduser(os.environ["LOGS_DIR_BASE"]),"cPort"), self.portCfg["BROKER_ID"]), mode = "w+")
		self.module_lgr_fh.setLevel(logging.DEBUG)
		frmt = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		self.module_lgr_fh.setFormatter(frmt)
		self.module_lgr.addHandler(self.module_lgr_fh)		
	
	
	def run(self):
		try:
			cthread_bootstrap.bootstrap(self)
		except Control_Thread_Bootstrap_Error as e:
			# Exit the thread with an error and clse the control socket connection 
			self.controlPort.close()
			sys.exit (e.msg)
		#self.lgr.info ("Inside control thread")
		
		while self.stopRequest.isSet():
		
			try:	
				socks= dict(self.poller.poll(1))
			except zmq.ZMQError as e:
				#This will throw an error during shutdown of the sockets
				# Ignore it
				pass

			if  socks.get(self.controlPort) == zmq.POLLIN:
				msg = {}
				try:
					msg = self.controlPort.recv_pyobj()
				except zmq.ZMQError as e:
					self.unbufferedWrite.write ("Error: %s"%(e.__str__()))
				except Exception as ex:
					template = "An exception of type {0} occured. Arguments:\n{1!r}"
					err_message = template.format(type(ex).__name__, ex.args)
					self.unbufferedWrite.write (err_message)
				cmd = msg["TYPE"]
				#print(msg)
				if cmd == CB_SERVICE_START_REQ:
					try:
						
						
						#self.unbufferedWrite.write("%s"%msg)
						cb_service_start_event.service_start_event(self, msg)
						
					except Service_Init_Failed as e:
						#TODO inform the controller of the failure
						self.module_lgr.debug (e.msg)

							
				elif cmd == CB_SERVICE_PORT_CONFIG_ADD_EGRESS_REQ:
					try:	
						cb_service_port_config_add_link.service_port_config_add_link(self,MODE_ADD_EGRESS,msg)
						#TODO Send controller BC_EGRESS_PORT_INITIALIZED message	
					except Service_Port_Config_Add_Link_Failed as e:
						#TODO inform controller of failure
						self.module_lgr.debug ("Adding egress port failed")
						self.unbufferedWrite.write ("Adding egress port failed")
						msg["TYPE"] = BC_SERVICE_PORT_CONFIG_ADD_EGRESS_REP
						msg["STATUS"] = FAIL
						self.controlPort.send_pyobj(msg)

				elif cmd == CB_SERVICE_PORT_CONFIG_ADD_INGRESS_REQ:
					try: 	
						cb_service_port_config_add_link.service_port_config_add_link(self,MODE_ADD_INGRESS, msg)
					except Service_Port_Config_Add_Link_Failed as e:
						#TODO inform controller of failure
						self.module_lgr.debug ("Adding ingress port failed")
                                         
				elif cmd == CB_SERVICE_PORT_CONFIG_ADD_APP_ROUTER_REQ:
					try:
						cb_service_port_config_add_app_routing_table.cb_service_port_config_add_app_routing_table(self,msg)
					except Service_Port_Config_Add_AppRouting_Table_Error as e:
						#TODO inform controller of failure
						self.module_lgr.debug ("Adding app routing table failed")	
					
				elif cmd == CB_PROXY_START_REQ:
					try:
						cb_proxy_service_start_event.proxy_service_start_event(self,msg)
					except Proxy_Service_Start_Error as e:
						#TODO inform the controller
						self.module_lgr.debug ("Proxy switch port could not be started", e.msg)

				elif cmd == CB_SERVICE_PORT_CONFIG_ACTIVATE_WF_REQ:		
					try:
						cb_proxy_port_config_activate_wf.cb_proxy_port_config_activate_wf(self, msg)				
					except WF_Activate_Error as e:
						self.module_lgr.debug (e.msg)
	
	
				elif cmd == CB_STOP_OVERLOAD_NOTIFICATIONS:
					cb_proxy_port_config_stop_notifications.cb_proxy_port_config_stop_notifications(self, msg)
				
				elif cmd == CB_RESUME_OVERLOAD_NOTIFICATIONS:
					cb_proxy_port_config_resume_notifications.cb_proxy_port_config_resume_notifications(self,msg)
				#From global controller, response on the anomolous message sent from client 
				elif cmd == CB_RESUME_ANOM:
					# Added By Tara 
					# The following code decrypt the response from GC (encrypted by the LC) 
					#Asumming that the NC and LC have a secret key sharing 
					f = open(os.path.expanduser("~/AppFabric/experiments/physical_machines/SecretKeys/NC_LC.bin"),"rb") # Get the NC_LC secret key 							(This key is agreed on after the first msg and both parties has it)
					message= f.read()
					message=message.split(b"    ")
					key2= message[1] #key
					iv= message[0] #nonce 
					msg2= msg.pop("TYPE") # Type is not encrypted so that it can be recognized in the if statement 

					#print (msg)
					try:
						msg= AESCipher_Msg(key2.decode()).encrypt(msg,iv) # Decrypt the whole message 
					except:
						pass 	
					msg["TYPE"]= msg2 # return the message TYPE
					msg["PROXY_PORT_LIST"]=ast.literal_eval(msg["PROXY_PORT_LIST"])
					#print(msg)
					cb_service_port_config_anom_response.cb_service_port_config_anom_response(self,msg) 

				elif cmd == CB_REPORT_LOAD_REQ:
					cb_proxy_port_config_report_load.cb_proxy_port_config_report_load(self, msg)

				elif cmd == CB_PROXY_WFINSTANCE_RELEASE_REQ:
					cb_proxy_port_release_wf_instance.cb_proxy_port_release_wf_instance(self, msg)
				else:
					pass
			
			# From service ports
			for activeServiceEntry in self.activeServiceTable:
				
				if  socks.get(activeServiceEntry["SERVICE_INTERFACE"]) == zmq.POLLIN:
					msg = activeServiceEntry["SERVICE_INTERFACE"].recv_pyobj()
					#TODO Check the message and either update or send to Central Controller
				#	self.lgr.info("Message received from Service Port Handler")
				#	self.lgr.info(msg)
					cmd = msg["TYPE"]
				#	print ("*********************************************************************************************")
					if cmd == I_SC_INIT:
						#f = open(os.path.expanduser("~/AppFabric/experiments/physical_machines/NC/PRKey.txt"),"r")
						#key=("%s"%f.read())
						#save client public key at node controller 
						#key= RSA.importKey(key)
						activeServiceEntry["SERVICE_INTERFACE_STATE"]= SERVICE_INTERFACE_ACTIVE
						self.activeServiceTable.insert_row_from_kvpairlist(**activeServiceEntry)
						msg["AVAILABLE_RESOURCE"]= self.portCfg["AVAILABLE_RESOURCE"] 
						msg["MAX_REQUESTS"]=self.portCfg["MAX_REQUESTS"]
						msg["MAX_REQUESTS_PER_CLIENT"]=self.portCfg["MAX_REQUESTS_PER_CLIENT"]
						msg["BANNED_DEST"]=self.portCfg["BANNED_DEST"]
						msg["TYPE_OF_TRAFFIC"]=self.portCfg["TYPE_OF_TRAFFIC"]
						msg["NC_NAME"]=self.portCfg["HOST_NAME"] 
						self.portCfg["AVAILABLE_RESOURCE"] -= msg["RESOURCE_REQ"]
						msg["TYPE"]= BC_SERVICE_START_REP
						msg["AVAILABLE_RESOURCE"] = self.portCfg["AVAILABLE_RESOURCE"]
						f = open(os.path.expanduser("~/AppFabric/experiments/physical_machines/SecretKeys/NC_LC.bin"),"rb") # Get the NC_LC secret key 							(This key is agreed on after the first msg and both parties has it)
						message= f.read()
						message=message.split(b"    ")
						key2= message[1] #key
						iv= message[0] #nonce	
						enc=AESCipher_Msg(key2.decode()).encrypt(msg,iv) # encrypt the whole message with a known secret key
						enc["TYPE"]= BC_SERVICE_START_REP
						self.controlPort.send_pyobj(enc) #Message is sent to local controller/global controller with node conrtoller config. 
					elif cmd in (
							I_SC_SERVICE_ANOM_INTEGRITY,
							I_SC_SERVICE_ANOM_DEST,
							I_SC_SERVICE_ANOM_TRAFFIC,
							I_SC_SERVICE_ANOM_MAX_REQUEST,
							I_SC_SERVICE_ANOM_MAX_CLIENT_REQ
							):
						#print (" cPort Recieved anomolous signal") 
						self.controlPort.send_pyobj(msg) #IF messages where anomolous then just pass them
					elif cmd == I_SC_SERVICE_FAILED:
						pass
					elif cmd == I_SC_SERVICE_DATA: 
						
						pass
					elif cmd == I_SC_ADD_EGRESS_LINK_REP:
						msg["INTERFACE_STATE"]= LINK_TABLE_INTERFACE_STATE_ACTIVE
						try:
							self.linkTable.insert_row_from_kvpairlist(**msg)
						except Invalid_Row_Entry as e:
                	                                self.module_lgr.debug(e.msg)
                        	                        #TODO Inform controller
						msg["TYPE"] = BC_SERVICE_PORT_CONFIG_ADD_EGRESS_REP
					
						self.controlPort.send_pyobj(msg)
					
					elif cmd == I_SC_ADD_INGRESS_LINK_REP:
						msg["INTERFACE_STATE"]= LINK_TABLE_INTERFACE_STATE_ACTIVE
						try:
							self.linkTable.insert_row_from_kvpairlist(**msg)
						except Invalid_Row_Entry as e:
							self.module_lgr.debug(e.msg)
                                                        #TODO Inform controller
						msg["TYPE"] = BC_SERVICE_PORT_CONFIG_ADD_INGRESS_REP
						self.controlPort.send_pyobj(msg)	

				
					elif cmd == I_SC_ADD_APP_ROUTER_REP:
						msg["TYPE"] = BC_SERVICE_PORT_CONFIG_ADD_APP_ROUTER_REP 
						self.controlPort.send_pyobj(msg)
					
					else: 
						pass
			if  self.udpTunnelPort is not None:
				if socks.get(self.udpTunnelPort) == zmq.POLLIN:
					self.unbufferedWrite.write ("\nMessage recieved from tun interface\n")
			
			# check the proxy Switch Port
			for activeProxyServiceEntry in self.activeProxyServiceTable:
				if  socks.get(activeProxyServiceEntry["SERVICE_INTERFACE"]) == zmq.POLLIN:
					msg = activeProxyServiceEntry["SERVICE_INTERFACE"].recv_pyobj()
					cmd = msg["TYPE"]
					#print(cmd)
					if cmd == I_PRC_PORT_INIT_INFO:
						msg["TYPE"] = BC_PROXY_START_REP
						self.portCfg["AVAILABLE_RESOURCE"] -= msg["RESOURCE_REQ"]
						msg["AVAILABLE_RESOURCE"] = self.portCfg["AVAILABLE_RESOURCE"] 
						self.controlPort.send_pyobj(msg)		
						#TODO: Add to the proxyservice table
					elif cmd == I_SC_ADD_EGRESS_LINK_REP:
						msg["INTERFACE_STATE"]= LINK_TABLE_INTERFACE_STATE_ACTIVE
						try:
							self.linkTable.insert_row_from_kvpairlist(**msg)
						except Invalid_Row_Entry as e:
							self.module_lgr.debug(e.msg)
							#TODO Inform controller
						msg["TYPE"] = BC_SERVICE_PORT_CONFIG_ADD_EGRESS_REP
						self.controlPort.send_pyobj(msg)
					elif cmd == I_SC_ADD_INGRESS_LINK_REP:
						msg["INTERFACE_STATE"]= LINK_TABLE_INTERFACE_STATE_ACTIVE
						try:
							self.linkTable.insert_row_from_kvpairlist(**msg)
						except Invalid_Row_Entry as e:
							self.module_lgr.debug(e.msg)
							#TODO Inform controller
						msg["TYPE"] = BC_SERVICE_PORT_CONFIG_ADD_INGRESS_REP
						self.controlPort.send_pyobj(msg)
					elif cmd == I_SC_ADD_APP_ROUTER_REP:
						msg["TYPE"] = BC_SERVICE_PORT_CONFIG_ADD_APP_ROUTER_REP
						self.controlPort.send_pyobj(msg)

					elif cmd == I_SC_ACTIVATE_WF_REP:
						msg["TYPE"] = BC_SERVICE_PORT_CONFIG_ACTIVATE_WF_REP
						self.controlPort.send_pyobj(msg)
					
					elif cmd == BC_OVERLOAD_NOTIFICATION:
						self.controlPort.send_pyobj(msg)
					
					elif cmd == BC_SUSPEND_WF_INSTANCE_NOTIFICATION:
						self.controlPort.send_pyobj(msg)

					elif cmd == BC_RESUME_WF_INSTANCE_NOTIFICATION:
						self.controlPort.send_pyobj(msg)
		
					elif cmd == BC_REPORT_LOAD_REP:
						self.controlPort.send_pyobj(msg)
	
					elif cmd == BC_PROXY_WFINSTANCE_RELEASE_REP:
						self.controlPort.send_pyobj(msg)
	
		#cleanly closing thread when join is called
		cthread_stop.thread_stop(self)
	

	def join(self, timeout = None):
		#pdb.set_trace()
		self.stopRequest.clear()
		super(controlSocketHandler,self).join(timeout)



