#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import zmq
import copy
from appfabric.common.msgTypes import *
from appfabric.common.constants import *
from appfabric.common.errors import *
from appfabric.common.runStates import *
from appfabric.common import tableDefs, util
#from common import readConfig
from ..sPort import sPort

def service_start_event (self, msg):
              #  print ("Service start event: %s"%(msg["SERVICE_NAME"]) )
                # Find service in the service table
                cisPort = None  
                try:
                      # service = self.serviceTable.lookup_row_by_primary_key(SERVICE_NAME = msg["SERVICE_NAME"])
                      # service = self.serviceTable.getConfig(SERVICE_NAME = msg["SERVICE_NAME"])       
                        serviceExecutable = msg["EXECUTABLE"]
                        serviceType = msg["SERVICE_TYPE"]
                except Read_Config_Error as e:
                        raise Service_Init_Failed (e.msg)  
                
                if (serviceType == SERVICE_TYPE_MSG):
                        try:
                                inprocPathString = util.createConnectString("inproc","tmp/inproc",
                                                                        msg["SERVICE_NAME"],
                                                                        str(msg["SERVICE_ID"]),
									"cthread_data"
                                                                        )
                        except Invalid_Connect_String_Error as e:
                                raise Service_Init_Failed (e.msg)

                        try:
                                cisPort = self.context.socket(zmq.DEALER)
                                cisPort.setsockopt(zmq.LINGER, 0)
                                cisPort.bind(inprocPathString)
                                self.poller.register(cisPort, zmq.POLLIN)
                        except zmq.ZMQError as e:
                                raise Service_Init_Failed (e.msg)

                       # self.lgr.info ("spawning new process for <%s>"%(self.portCfg["BROKER_ID"]))
                       # print ("spawning new process for <%s>"%(self.portCfg["BROKER_ID"]))
                        try:                    
                                servicePort = sPort.intServicePortHandler(self.context,msg)
                        except Service_Configuration_Error as e:
                                self.module_lgr.debug ("Error: Failed to initialize thread for <%s>", self.portCfg["BROKER_ID"])
                                raise Service_Init_Failed(e.msg)
                        # Start a new thread
                      #  self.lgr.info( "starting service <%s>"%(self.portCfg["BROKER_ID"]))
                        #print ("starting service <%s>" %(self.portCfg["BROKER_ID"]))
                        try:
                                servicePort.start()
                        except:
                                self.module_lgr.debug ("Failed to start service port handler thread <%s>", self.portCfg["BROKER_ID"])
                                # kill the thread
                                servicePort.join()
                                raise Service_Init_Failed (e.msg)
                                                
                elif (serviceType == SERVICE_TYPE_PACKET):
                        print ("Service Type: Packet")
                        cisPort = self.udpTunnelPort
        #               servicePort = self.udpTunnelPortHandler
                        servicePort = None
                        # forward the CB_SERVICE_START to the tPort
                        msg["TYPE"] = I_CT_SERVICE_START 
                        cisPort.send_pyobj(msg,zmq.NOBLOCK)


                # Update active services table: If you could start the service in thelast step; 
                # you should be able to insert the entry; check beforehand
                activeServiceTable_update = copy.deepcopy(msg)	
                activeServiceTable_update.update ({
                        "SERVICE_INTERFACE":cisPort,    
                        "SERVICE_INTERFACE_STATE":SERVICE_INTERFACE_INACTIVE,
                        "THREAD_ID":servicePort,
                })      
                try:
                        self.activeServiceTable.insert_row_from_kvpairlist(**activeServiceTable_update)
                       # print ("active service table")
                        #self.activeServiceTable.print_table()
                except Invalid_Row_Entry as e:
                        print ("Invalid table entry", e.msg)
                        # No thread launched. Thread object will be cleaned up
                        if (serviceType == "SERVICE_TYPE_MSG"):
                                servicePort.join()
                        elif (serviceType == "SERVICE_TYPE_PACKET"):
                                # send a service remove command
                                msg["TYPE"] = I_CT_SERVICE_STOP
                                cisPort.send_pyobj(msg,zmq.NOBLOCK)
 
                        raise Service_Init_Failed ("Invalid Row Entry")


