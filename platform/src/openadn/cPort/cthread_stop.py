#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import zmq
from appfabric.common.msgTypes import *
from appfabric.common.constants import *
from appfabric.common.errors import *
from appfabric.common.runStates import *
from appfabric.common import processSpawn, tableDefs, util
from ..sPort import sPort
from ..pPort import pSwitchPort


def thread_stop(self):
               # self.lgr.info ("closing control port thread")
                print ("cPort: thread stop")
                self.controlPort.close()
                for activeServiceEntry in self.activeServiceTable:
                        # activeServiceEntry["SERVICE_INTERFACE"].close()
                        if activeServiceEntry["THREAD_ID"] is not None:
                               # self.lgr.info("closing service port <%s><%d>", activeServiceEntry["SERVICE_NAME"], activeServiceEntry["SERVICE_ID"])
                                self.poller.unregister(activeServiceEntry["SERVICE_INTERFACE"])
                                activeServiceEntry["SERVICE_INTERFACE"].close()
                                activeServiceEntry["THREAD_ID"].join()
                                #self.lgr.info("closed service port <%s><%d>", activeServiceEntry["SERVICE_NAME"], activeServiceEntry["SERVICE_ID"])

                for activeProxyServiceEntry in self.activeProxyServiceTable:
                        print ("closing pSwitchPort")
                        if activeProxyServiceEntry["THREAD_ID"] is not None:
                                self.poller.unregister(activeProxyServiceEntry["SERVICE_INTERFACE"])
                                activeProxyServiceEntry["SERVICE_INTERFACE"].close()
                                activeProxyServiceEntry["THREAD_ID"].join()
                                #self.lgr.info("closed service port <%s><%d>", activeProxyServiceEntry["SERVICE_NAME"], activeProxyServiceEntry["SERVICE_ID"])

      
                if self.udpTunnelPort is not None:
                        self.udpTunnelPort.send_pyobj(I_CT_PORT_STOP_MSG, zmq.NOBLOCK)
                        self.udpTunnelPort.close()



