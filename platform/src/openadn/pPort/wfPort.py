#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#

'''
		wfPort is the workflow prot through whcih a workflow connects to  pPort


'''




import os, sys
import zmq
import uuid
import logging
from appfabric.common import genericTable
from appfabric.common import tableDefs
from appfabric.common.constants import *
from appfabric.common.errors import *
from appfabric.common.msgTypes import *
from appfabric.common import  util
from appfabric.common import classifier
from appfabric.common.unbufferedStream import unbuffered

class wfPort:
	
	def __init__(self, _index, _context, _poller):
		self.context = _context
		self.wfPortState = FREE
		self.wfPortNum = _index  
		self.wfPortLoad = 0
		self.egressPortTable = {}
		self.ingressPortTable = {}
		self.appRouter = None
		self.poller = _poller
		self.wfPort_load_capacity = 0
		self.wfPort_load_notification_level = 0
		self.wfPortLoadNotificationFlag= True
		self.wfID = None
		self.wfInstanceID = None
		self.wfPort_suspend_flag = False
		self.unbufferedWrite= unbuffered(sys.stdout)
		
	# For re-using the port
	def clear_port(self):
		####
		# Note cannot delete the wfPort because it shares the self.poller variable with the main thread
		####
		for egressPortID, egressPort in self.egressPortTable.items(): 
			egressPort.close()

		for ingressPortID, ingressPort in self.ingressPortTable.items():
			self.poller.unregister(ingressPort)
			ingressPort.close()
		# set attributes to the unused state
		self.wfPortState = FREE
		self.wfPortLoad = 0
		self.egressPortTable = {}
		self.ingressPortTable = {}
		self.appRouter = None
		self.wfPort_load_capacity = 0
		self.wfPort_load_notification_level = 0
		self.wfPortLoadNotificationFlag= True
		self.wfID = None
		self.wfInstanceID = None
		self.wfPort_suspend_flag = False

	def get_wfID(self):
		return self.wfID
	
	def get_wfPort_suspend_flag(self):
		return self.wfPort_suspend_flag

	def get_wfInstanceID(self):
		return self.wfInstanceID		

	def get_ingressPortTable(self):
		return self.ingressPortTable 
	
	def get_egressPortTable (self):
		return self.egressPortTable

	def get_appRouter(self):
		return self.appRoutingTable
	
	def get_wfPortLoadNotificationFlag(self):
		return self.wfPortLoadNotificationFlag

	def get_wfPortLoad(self):
		return self.wfPortLoad	
	
	def update_wfPortLoad(self, _increment):
		self.wfPortLoad += _increment
	
	def get_wfPortNum(self):
		return self.wfPortNum	

	def get_wfPort_load_notification_level(self):
		return self.wfPort_load_notification_level
	

	def get_wfPort_load_capacity(self):
		return self.wfPort_load_capacity
	
	def set_wfPort_suspend_flag(self, _flag):
                self.wfPort_suspend_flag = _flag

	def set_wfPortLoadNotificationFlag(self, _flag):
		self.wfPortLoadNotificationFlag = _flag

	def set_wfID(self, _wfID):
		self.wfID = _wfID
	
	def set_wfInstanceID(self, _wfInstanceID):
		self.wfInstanceID = _wfInstanceID

	def set_wfPort_load_notification_level(self, _notification_level):
		self.wfPort_load_notification_level = _notification_level

	def set_wfPort_load_capacity(self, _wfPort_load_capacity):
		self.wfPort_load_capacity = _wfPort_load_capacity
	
	def set_wfPortState(self, _state):
		self.wfPortState= _state

	def get_wfPortState(self):	
		return self.wfPortState
	
	
	def route_msg(self, _msg):
		# serial match where the preference is in descending order
		outport =  self.appRouter.match_pattern(_msg)
		_msg["APLS_HDR"]["OUT_PORT_NUM"] = int(outport)
			
	def add_egressPort(self, msg):
		msg["INTERFACE_TYPE"] = LINK_TABLE_INTERFACE_TYPE_EGRESS
		
		if msg["TRANSPORT"] == "INPROC":
			try:
				connectString = util.createConnectString("inproc",
									"tmp/inproc/",
									msg["SERVICE_NAME"],
									str(msg["SERVICE_ID"]),
									str(msg["WF_PORT_NUM"])
									)
			except OSError as e:
				raise Service_Port_Add_Egress_Port_Failed (e.msg)
                
		elif msg["TRANSPORT"] == "TCP":
			connectString = "tcp://*"
		
		try:
			egressPort = self.context.socket(zmq.DEALER)
			egressPort.setsockopt(zmq.LINGER, 0)
			
			if msg["TRANSPORT"] == "INPROC":
				egressPort.bind(connectString)
				msg["LINK_CONNECT_STRING"] = connectString
			elif msg["TRANSPORT"] == "TCP":
				ipOption = chr(97)+chr(4)+chr(1)+chr(10)
				portNum = egressPort.bind_to_random_port(connectString)
				msg ["LINK_CONNECT_STRING"] = util.createConnectString("tcp", str(os.environ["IP_ADDR"]),str(portNum))
		except zmq.ZMQError as e:
			self.module_lgr(e.msg)
	
		self.egressPortTable[int(msg["PORT_NUM"])] = egressPort		
		msg["TYPE"] = I_SC_ADD_EGRESS_LINK_REP
		msg["STATUS"] = SUCCESS
		return msg

	def add_ingressPort(self, msg):
		try:
			ingressPort = self.context.socket(zmq.DEALER)
			ingressPort.setsockopt(zmq.LINGER, 0)
			ingressPort.connect (msg["LINK_CONNECT_STRING"])
			self.poller.register(ingressPort, zmq.POLLIN)
		except zmq.ZMQError as e:
			self.module_lgr(e.msg)
			#TODO: Send back message of failure to control Thread

	
		#Add ingress port to the table
		self.ingressPortTable[int(msg["PORT_NUM"])] = ingressPort
		
		msg["TYPE"] = I_SC_ADD_INGRESS_LINK_REP
		msg["STATUS"] = SUCCESS
		
		
		return msg

	def add_app_router (self, msg):
		classifier_type = msg["APP_ROUTING_TABLE"]["classifier_type"]
		classifier_list = msg["APP_ROUTING_TABLE"]["classifier_list"]	
		self.appRouter = classifier.classifier(classifier_type)
		self.appRouter.create_appRoutingTable (classifier_list)
		msg["TYPE"] = I_SC_ADD_APP_ROUTER_REP
		msg["STATUS"] = SUCCESS
		return msg 
