#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import os
import sys
import zmq
import uuid
import pickle
import threading
import logging
import signal
import logging
import subprocess
from time import sleep

from .proxyMsgTypes import *
from .wfPort  import wfPort


from appfabric.common.unbufferedStream import unbuffered
from appfabric.common import util, processSpawn
from appfabric.common.msgTypes import *
from appfabric.common.constants import *
from appfabric.common.runStates import *
from appfabric.common.errors import *
from appfabric.common import genericTable
from appfabric.common import tableDefs

'''


	This module implements the pPort, also called the proxy switch port.
	Depending on the available resources in the host, the controller launches a pPort with a
	pre-configured number of workflows that it can support. 
	
	The pPort automatically starts a proxy server (in AppFabric/containers/OpenADN_proxy_container).
	The proxy service acts as the interface between AppFabric-aware services and AppFabric-unaware services)
	In particular, between an user and the application: 	

				User process (AppFabric-unaware service such as a web brower) 
				--> AppFabric HTTP or JSON Proxy  
				--> pPort 
				--> sPort
				--> AppFaric-aware services

	Multiple  workflows share the same pPort and the proxy server attached to it. The number of 
	workflows that a pPort can support is configured by the controller through the NUM_WORKFLOWS parameter.
	Each workflow if attached to a separate worflow ingress port (wfPort). When a new session request arrives at the proxy server, 
	it requests the pPort to provide it with a new port. The pPort provides a new session port for each new
	session and attaches the session to one of the wfPorts. Each of these workflows have the name -> IP mapping  
	in the nameserver. Therefore, the pPort is responsible for balancing load incident on the prosy server across the active workflows.
	
	The pPort send overload messages to the controller when its load crsses a certain pre-configured threshold. 
	The controller then launches another workflow instance, either in the same pPort or a separate pPort. 
	Similarly when the average load across the workflow goes down below a certain preconfigured limit, an active workflow 
	is de-commisioned. 

	An ordering chain is fallowed when starting and removing workflows from the system. For example, worflow 1 precesded workflow 2
	and may be represented as workflow 1-> workflow 2. When the average load/workflow is below a certain value, 
	workflows are de-comminsioned in the reverse order in whcih they were started. also, in this case when workflow 2 is shut down,
	workflow 1 is informed about this. this is because, workflow 1 is now again responsible for spawning new workflows when it 
	is overlaoded.
	 




 
	 -------         -------			            -------		
	|       |	| 	|    				   |	    |	
	| User 1|  --> 	| Proxy |          ------------          / |session |\           ------------ 
	|	|	| Port	| \       |		|       /  | port 1 | \         | 	      |	      ---------
	 -------	 -------   \      |		|      /   |	    |  \        |             |      |         |
				    \---- |		|-----/	    -------     \-------|             | ---- | wfPort 1| ----> workflow 1
    	   .				  |		|                      	        |             |      |         |
           .			     ---- |    Proxy	|-----			   -----|     pPort   |	      ---------
	   .		             ---- |    Server	|-----		           -----|             |
					  | 		|			        |	      |       ---------
	 -------         -------    /---- |     	|-----\ 	        /-------|	      |      |         |
        |       |       |       |  /      |		|      \    -------    /        |             | ---- |wfPort M |-----> workflow M
        | User N|  -->  | Proxy | /       | 		|       \  |        | /         |             |      |         |
        |       |       | Port  |          -------------         \ |session |/		 ------------         ---------
         -------         -------				   | port N | 
								   |        |
								    -------

'''



class proxySwitchPortThread (threading.Thread):

	def __init__(self, context, msg):
		super (proxySwitchPortThread, self).__init__()
		self.stopRequest = threading.Event()
		self.stopRequest.set()
	
		# Save the init message
		self.saved_initMsg =  msg
	

		#TODO Port Configurations
		self.portCfg = {}
		try:
			self.portCfg = {
					"CONTEXT": context,
					"SERVICE_NAME": msg ["SERVICE_NAME"],
					"SERVICE_ID": msg ["SERVICE_ID"],
					"WF_ID": msg["WF_ID"],
					"EXECUTABLE": msg["EXECUTABLE"],
					"PROTO_TYPE": msg ["PROTO_TYPE"],
					"ARGS": msg ["ARGS"],
					"PORT": msg ["PORT"],
					"NUMPORTS": msg["NUMPORTS"],
					"NUM_WORKFLOWS": msg["NUM_WORKFLOWS"],
					"PROCESS_INTERFACE_STATE":PROCESS_INTERFACE_INACTIVE,
					"PROCESS_ID": None,
					"IPC_COMM": util.createPathString("/tmp/ipc",
									msg["SERVICE_NAME"],
									str(msg["SERVICE_ID"]),
									),
					"INPROC_COMM": util.createPathString("/tmp/inproc",
									msg ["SERVICE_NAME"],
									str(msg["SERVICE_ID"]),
									)
                                      }
		except KeyError:
			self.module_lgr.debug ("Error: Configuration message for the proxy service is not complete")
			raise Proxy_Server_Init_Error ("Error: Configuration message for the proxy service is not complete")
		except Invalid_Connect_String_Error as e:
			self.module_lgr.debug (e.msg)
			raise Proxy_Server_Init_Error (e.msg)
		
		# get an ID for the object for logging errors		
		self.ID = "SERVICE_PORT"+ "/" + msg["SERVICE_NAME"] + "/"+ str (msg["SERVICE_ID"])
	
		
		self.poller = zmq.Poller()
		
	
		# Initialize the sicPort : pPort - Cthread
		self.sicPort = self.portCfg["CONTEXT"].socket(zmq.DEALER)
		self.sicPort.setsockopt(zmq.LINGER, 0)
		try:
			self.sicPort_connectString = util.createConnectString("inproc",self.portCfg["INPROC_COMM"], "cthread_data")
		except Invalid_Connect_String_Error as e:
			raise Proxy_Server_Init_Error (e.msg)


		#Initialize the sipPort: pPort - Service 
		try:
			self.sipPort_control = self.portCfg["CONTEXT"].socket(zmq.ROUTER)
			self.sipPort_control.setsockopt(zmq.LINGER, 0)
		except zmq.zmqError as e:
			raise Proxy_Server_Init_Error (e.__str__())
		try:
			self.sipPort_control_connectString = util.createConnectString("ipc",self.portCfg["IPC_COMM"], "service_control") 
		except Invalid_Connect_String_Error as e:
			raise Proxy_Server_Init_Error (e.msg)

		# The proxy has two level switching
		# 1. switch 1: from input ports from the proxy server -> wfInstance mapping
		# 2. switch 2: The wfPort is itself a switch with multiple ingress and egress
 
			
		# Switch 1: 
		self.freePortList = []

		# When to pre-allocate ports
		self.lowerWaterMark = int(self.portCfg["NUMPORTS"])/2
		self.activePorts = {}
	
		# Switch2:
		self.wfPorts = {}
		for wfPortNum in range(self.portCfg["NUM_WORKFLOWS"]):
			self.wfPorts[wfPortNum] = wfPort(wfPortNum, self.portCfg["CONTEXT"], self.poller)
		
		# a list maintaining only the active WF Ports		
		self.activeWFPortList = []	

		# setting up module logger
		self.module_lgr = None
		self.module_lgr_fh = None
		self.init_module_logger()

		# Setting up load logger
		self.load_lgr = None
		self.log_fh_load = None
		self.init_load_logger()

		# set unbuffered stream
		self.unbufferedWrite= unbuffered(sys.stdout)



	def  init_module_logger(self):
		self.module_lgr = logging.getLogger("pPort")
		self.module_lgr.disable_existing_loggers = False
		self.module_lgr.setLevel(logging.DEBUG)
		self.module_lgr_fh = logging.FileHandler(os.path.join(util.createPathString(os.path.expanduser(os.environ["LOGS_DIR_BASE"]),"pPort"), self.portCfg["SERVICE_ID"]), mode = "w+")
		self.module_lgr_fh.setLevel(logging.DEBUG)
		frmt = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		self.module_lgr_fh.setFormatter(frmt)
		self.module_lgr.addHandler(self.module_lgr_fh)


	def init_load_logger(self):
		self.load_lgr = logging.getLogger("load")
		self.load_lgr.disable_existing_loggers = False
		self.load_lgr.setLevel(logging.INFO)
		fname = os.path.join(util.createPathString(os.path.expanduser(os.environ["LOGS_DIR_BASE"]),"load"),self.portCfg["SERVICE_ID"])
		self.log_fh_load = logging.FileHandler(fname, mode = "w+")
		self.log_fh_load.setLevel(logging.INFO)
		
		# For live graph plots
		# launching the live graph as a subprocess
		# Ideally the logging info should be sent to a separate dashboard function to be displayed
		#TODO: Implement a monitoring dashboard
		self.load_lgr.addHandler(self.log_fh_load)
		#if os.environ["LOCAL_GRAPHING"] == "ON":
		#new_env = os.environ.copy()
		#new_env["DISPLAY"]="localhost:10.0"
		#graphing_args = ["java", "-jar", os.environ.get("GRAPHING_CMD")]
		#graphing_args = r"java -jar /home/appfabric/AppFabric/experiments/mininet_simulations/live_graph/LiveGraph.2.0.beta01.Complete.jar"
		
		#subprocess.Popen("xterm", shell=True)
		
								

		col_hdr = "time|"
		self.load_lgr.info("##|## ")
		for i in range(self.portCfg["NUM_WORKFLOWS"]): 
			if i == self.portCfg["NUM_WORKFLOWS"] - 1: 	
				col_hdr_str = "workflow_" + str(i)
				col_hdr += col_hdr_str
			else:
				col_hdr_str = "workflow_" + str(i)
				col_hdr += col_hdr_str + "|"
 			
		self.load_lgr.info(col_hdr)	
	
		frmt = logging.Formatter('%(asctime)s | %(message)s')
		self.log_fh_load.setFormatter(frmt)
		self.load_lgr.addHandler(self.log_fh_load)

	
	def pre_allocate_proxy_port(self):
		self.allocate_proxy_ports_RR(self.portCfg["NUMPORTS"])
			
	
	def allocate_proxy_ports_RR(self, num_ports_to_add):
		active_wf_port_count = len(self.activeWFPortList)
		self.unbufferedWrite.write ("\nnumber of active_wf_ports = %s"%(active_wf_port_count))
		if active_wf_port_count > 0:
			temp_list_of_wf_ready_to_accept_new_sessions = []
			for wfPortNum in self.activeWFPortList:
				wfPort = self.wfPorts[wfPortNum]
				if wfPort.get_wfPortLoad() <= wfPort.get_wfPort_load_capacity() :
					temp_list_of_wf_ready_to_accept_new_sessions.append(wfPortNum)
				else:
					# Make sure the controller knows about this state and suspends this wf instance
					# till further notice 
					if wfPort.get_wfPort_suspend_flag() == False:
						self.notify_controller("WF_INSTANCE_SUSPEND", WF_PORT= wfPort)	
			
			if len(temp_list_of_wf_ready_to_accept_new_sessions) == 1:
				self.create_proxy_port(num_ports_to_add, wfPortNum)
			
			# None is in a state to accept new connections
			# All the workflows are overlaoded 
			# divide equally among all active workflows
			elif len(temp_list_of_wf_ready_to_accept_new_sessions) == 0: 
				count = 0
				while True:
					wfPortNum = self.activeWFPortList.pop()
					wfPort = self.wfPorts[wfPortNum]
					self.create_proxy_port(1, wfPortNum)
					self.activeWFPortList.insert(0,wfPortNum)
					count +=1

					if count == num_ports_to_add:
						break
			# divide among active wf's that can take new sessions
			else:		
				count = 0			
				while True:
					wfPortNum = temp_list_of_wf_ready_to_accept_new_sessions.pop()
					wfPort = self.wfPorts[wfPortNum]
					self.create_proxy_port(1, wfPortNum)
					temp_list_of_wf_ready_to_accept_new_sessions.insert(0,wfPortNum)
					count +=1
					if count == num_ports_to_add:
						break


	def notify_controller(self, _notification, **kwargs):
		if _notification == "WF_INSTANCE_SUSPEND":
			wfPort = kwargs["WF_PORT"]
			msg = {
				"TYPE": BC_SUSPEND_WF_INSTANCE_NOTIFICATION,
				"WF_ID":self.portCfg["WF_ID"],
				"WF_INSTANCE_ID": wfPort.get_wfInstanceID(),
				"SERVICE_ID": self.portCfg["SERVICE_ID"]
				}
				
				
			wfPort.set_wfPort_suspend_flag (True)
			self.sicPort.send_pyobj(msg)

		elif _notification == "WF_INSTANCE_OVERLOAD":
			wfPort = kwargs["WF_PORT"]
			msg = {
				"TYPE": BC_OVERLOAD_NOTIFICATION,
				"LOAD":wfPort.get_wfPortLoad(),
				"WF_ID":wfPort.get_wfID(),
				"WF_INSTANCE_ID":wfPort.get_wfInstanceID(),
				"SERVICE_ID": self.portCfg["SERVICE_ID"],
				"WF_PORT_NUM": wfPort.get_wfPortNum()
				}
			self.unbufferedWrite.write ("\nSending overload notification")
			self.sicPort.send_pyobj(msg)

		elif _notification == "WF_INSTANCE_RESUME":
			wfPort = kwargs["WF_PORT"]
			msg = {
				"TYPE": BC_RESUME_WF_INSTANCE_NOTIFICATION,
				"WF_ID":self.portCfg["WF_ID"],
				"WF_INSTANCE_ID": wfPort.get_wfInstanceID(),
				"SERVICE_ID": self.portCfg["SERVICE_ID"]
				}
			wfPort.set_wfPort_suspend_flag (False)
			self.sicPort.send_pyobj(msg)
		

	
	def create_proxy_port(self, _numPorts, _wfPortNum):
		for i in range(_numPorts):
			portID = str(uuid.uuid4())[:16]
			proxyPortSocket = self.portCfg["CONTEXT"].socket(zmq.DEALER)			
			proxyPortSocket.setsockopt(zmq.LINGER, 0)
			connectString  = util.createConnectString("ipc", self.portCfg["IPC_COMM"], "service_data",portID)
			proxyPortSocket.bind(connectString)
			# register to poller later, when it is activated
			portInfo = {
					"PORT_ID": portID,
					"WF_PORT_NUM":_wfPortNum,
					"SOCKET": proxyPortSocket
				}
			self.freePortList.append(portInfo)  


	def manage_proxy_port_assignment(self):
		try:
			portInfo = self.freePortList.pop()
		except:
			self.allocate_proxy_ports_RR(self.portCfg["NUMPORTS"])
		try:
			self.poller.register(portInfo["SOCKET"], zmq.POLLIN)
			
			# update the port load
			self.wfPorts[portInfo["WF_PORT_NUM"]].update_wfPortLoad(1)
			self.unbufferedWrite.write("\nPort= %s, Load = %s"%(portInfo["WF_PORT_NUM"],self.wfPorts[portInfo["WF_PORT_NUM"]].get_wfPortLoad()))
			
			if self.wfPorts[portInfo["WF_PORT_NUM"]].get_wfPortLoad() >= self.wfPorts[portInfo["WF_PORT_NUM"]].get_wfPort_load_notification_level(): 
				if self.wfPorts[portInfo["WF_PORT_NUM"]].get_wfPortLoadNotificationFlag() == True:
					self.notify_controller("WF_INSTANCE_OVERLOAD", WF_PORT = self.wfPorts[portInfo["WF_PORT_NUM"]])

							
			self.activePorts[portInfo["PORT_ID"]] = portInfo 

			return {"TYPE": GET_EMPTY_PROXY_PORT_REP,
				"STATUS":SUCCESS, 
				"PORT_ID": portInfo["PORT_ID"], 
				"WF_PORT_NUM":portInfo["WF_PORT_NUM"]
				}  
			
		except zmq.ZMQError as e:
                        #TODO Raise error and handle it. for now exit
                        #e = sys.exc_info()[0]
			self.unbufferedWrite.write ("ERROR:%s"%(e.__str__()))
			exit()

				

	def run(self):
		try:
			# SwitchServerPort to get control messages from the proxy frontend
			
			self.sipPort_control.bind(self.sipPort_control_connectString)
			self.poller.register(self.sipPort_control, zmq.POLLIN)
			
		except zmq.ZMQError as e:
			#TODO: Report the error to controller
			self.module_lgr.debug(e.__str__())	
			self.unbufferedWrite.write ("Error in starting proxySwitchPortThread:", e.__str__())
			self.saved_initMsg["TYPE"] = I_PRC_PORT_INIT_INFO
			self.saved_initMsg["STATUS"] = FAIL
			self.sicPort.send_pyobj(self.saved_initMsg)
			self.service_stop()

		while True:
			
			try:			
				# Control Port to talk to the control thread
				self.sicPort.connect(self.sicPort_connectString)
				break
			except:
				sleep(1)
				continue
		try:	
			self.poller.register(self.sicPort, zmq.POLLIN)

		except zmq.ZMQError as e:
			#TODO: Report the error to controller
			self.module_lgr.debug(e.__str__()) 
			self.unbufferedWrite.write ("Error in starting proxySwitchPortThread:", e.__str__())
			self.saved_initMsg["TYPE"] = I_PRC_PORT_INIT_INFO
			self.saved_initMsg["STATUS"] = FAIL
			self.sicPort.send_pyobj(self.saved_initMsg) 
			self.service_stop()

		# set environemnt for the process 
		env = {
			"SERVICE_NAME":self.portCfg["SERVICE_NAME"],
			"SERVICE_ID":str(self.portCfg["SERVICE_ID"]),
			"IP_ADDR": os.environ.get("IP_ADDR"),
			"PORT": str(self.portCfg["PORT"]),	
			"IPC_COMM": self.portCfg["IPC_COMM"]
		}
		
		# path 
		path  = os.path.join(os.path.expanduser(os.environ ["PROXY_SERVICE_CONTAINER_BINPATH"]),
							self.portCfg["PROTO_TYPE"], 
							self.portCfg["SERVICE_NAME"],
							"bin")
		self.module_lgr.info ("spawning new process for service %s", self.ID)
		try:
			childPID = processSpawn.spawnProcess(path,self.portCfg["EXECUTABLE"], self.portCfg["ARGS"],env)
		except Process_Spawn_Error as e:
			self.module_lgr.debug (e.msg)
			self.unbufferedWrite.write (e.msg)
			
			self.saved_initMsg["TYPE"] = I_PRC_PORT_INIT_INFO
			self.saved_initMsg["STATUS"] = FAIL
			self.sicPort.send_pyobj(self.saved_initMsg)
			self.service_stop()
		self.portCfg["PROCESS_INTERFACE_STATE"]= PROCESS_INTERFACE_ACTIVE
		self.portCfg ["PROCESS_ID"] = childPID
		self.saved_initMsg["TYPE"] = I_PRC_PORT_INIT_INFO
		self.saved_initMsg["STATUS"] = SUCCESS
		self.sicPort.send_pyobj(self.saved_initMsg)	
		while self.stopRequest.isSet():
			
			# To support live graph format	
			load_str = ""
			for i in range(self.portCfg["NUM_WORKFLOWS"]):	
				if i == self.portCfg["NUM_WORKFLOWS"] - 1:
					load_str += str(self.wfPorts[i].get_wfPortLoad()) 			
				else:
					load_str +=str(self.wfPorts[i].get_wfPortLoad())+"|"
		

			# Note loads of all  active as well as inactive workflows 	
			self.load_lgr.info("%s"%(load_str))			

			""" Note the port IDs that need to be deleted before next iteration"""
			deletedPortIDList = []
			try:
				socks = dict(self.poller.poll(100))
			except zmq.ZMQError as e:
				pass
			if socks.get(self.sicPort) == zmq.POLLIN:
				msg = self.sicPort.recv_pyobj()
				cmd = msg ["TYPE"]
			
				if cmd == I_CS_ADD_INGRESS_LINK_REQ:
					rep_msg = self.wfPorts[msg["WF_PORT_NUM"]].add_ingressPort(msg)
					self.sicPort.send_pyobj(rep_msg)

				elif cmd == I_CS_ADD_EGRESS_LINK_REQ:

					# TODO: This is not nice: Add messages to assign a wfPort to the workflow first
					# This approach may lead to problems if the controller's state is somehow polluted
					
					rep_msg = self.wfPorts[msg["WF_PORT_NUM"]].add_egressPort(msg)
					self.sicPort.send_pyobj(rep_msg)
				
					
				elif cmd == I_CS_ADD_APP_ROUTER_REQ:
					rep_msg = self.wfPorts[msg["WF_PORT_NUM"]].add_app_router(msg)
					self.sicPort.send_pyobj(rep_msg)	
	
				elif cmd == I_CS_ACTIVATE_WF_REQ:
					if self.wfPorts[msg["WF_PORT_NUM"]].get_wfPortState() == FREE:
						self.wfPorts[msg["WF_PORT_NUM"]].set_wfPortState(ACTIVE)
						self.wfPorts[msg["WF_PORT_NUM"]].set_wfPort_load_capacity(msg["WF_PORT_LOAD_CAPACITY"])
						self.wfPorts[msg["WF_PORT_NUM"]].set_wfPort_load_notification_level(msg["WF_PORT_LOAD_NOTIFICATION_LEVEL"])
						self.wfPorts[msg["WF_PORT_NUM"]].set_wfID(msg["WF_ID"])
						self.wfPorts[msg["WF_PORT_NUM"]].set_wfInstanceID(msg["WF_INSTANCE_ID"])
							

						# for easy lookup of the active ports 
						self.activeWFPortList.append(msg["WF_PORT_NUM"])
						
						self.pre_allocate_proxy_port()

						msg["TYPE"] = I_SC_ACTIVATE_WF_REP
						msg["STATUS"] = SUCCESS
						self.sicPort.send_pyobj(msg)
					else:
						msg["TYPE"] = I_SC_ACTIVATE_WF_REP
						msg["STATUS"] = FAIL
						msg["REASON"] = WF_PORT_ALREADY_ACTIVE 
						self.sicPort.send_pyobj(msg)
						self.sicPort.send_pyobj(rep_msg)
		
				elif cmd == CB_STOP_OVERLOAD_NOTIFICATIONS:
					 self.wfPorts[msg["WF_PORT_NUM"]].set_wfPortLoadNotificationFlag(False)

				elif cmd == CB_RESUME_OVERLOAD_NOTIFICATIONS:
					self.unbufferedWrite.write("\n\n\n----------- Resuming Overload notifications -----------\n\n\n")
					self.wfPorts[msg["WF_PORT_NUM"]].set_wfPortLoadNotificationFlag(True)
				#If an anomolous message is recieved on this particular message, then the following response would be sent back to client (this response is coming all the way from global controller)
				elif cmd == CB_RESUME_ANOM: 
					#print( msg)
					portID = msg["PROXY_PORT_LIST"].pop()
					msg["DATA"]="ANOMOLOUS BEHAVIOR DETECTED FOR THIS MESSGAE"
					self.activePorts[portID]["SOCKET"].send_pyobj(msg)

				elif cmd == CB_REPORT_LOAD_REQ:
					portLoad = self.wfPorts[msg["WF_PORT_NUM"]].get_wfPortLoad()
					msg["TYPE"] = BC_REPORT_LOAD_REP
					msg["LOAD"] = portLoad	
					self.sicPort.send_pyobj(msg)
		
				elif cmd == CB_PROXY_WFINSTANCE_RELEASE_REQ:
					if self.wfPorts[msg["WF_PORT_NUM"]].get_wfPortState() == ACTIVE:
						self.wfPorts[msg["WF_PORT_NUM"]].set_wfPortState(RELEASED)
						if msg["WF_PORT_NUM"] in self.activeWFPortList:
							del self.activeWFPortList[msg["WF_PORT_NUM"]]

			if socks.get(self.sipPort_control) == zmq.POLLIN:
				proxyConnID = self.sipPort_control.recv()
				msg= pickle.loads(self.sipPort_control.recv())
				
				if msg["TYPE"] == GET_EMPTY_PROXY_PORT_REQ:
					rep_msg = pickle.dumps(self.manage_proxy_port_assignment())
					self.sipPort_control.send(proxyConnID, zmq.SNDMORE)
					self.sipPort_control.send(rep_msg)
				
				elif msg["TYPE"] == MAX_EMPTY_PROXY_PORTS_AVAILABLE_QUERY:
					#TODO: just check if there is a workflow
					# check for overlaod conditions later
					num_active_wf_ports = len(self.activeWFPortList)
					
					if num_active_wf_ports > 0:
						allocated = self.portCfg["NUMPORTS"]
						if (len(self.freePortList) - allocated) <= self.lowerWaterMark:
							self.allocate_proxy_ports_RR(self.portCfg["NUMPORTS"])
					else:
						allocated = 0


					rep_msg =pickle.dumps( {
						"TYPE":MAX_EMPTY_PROXY_PORTS_AVAILABLE_REP,
						"ALLOCATED": allocated
						})
					self.sipPort_control.send(proxyConnID, zmq.SNDMORE)
					self.sipPort_control.send(rep_msg)

						
 
			for portID, portInfo in self.activePorts.items():
				if socks.get(portInfo["SOCKET"])== zmq.POLLIN:	
					msg = {}
					msg  = portInfo["SOCKET"].recv_pyobj()
					if msg["APLS_HDR"]["MSG_TYPE"] == MSG_TYPE_DATA:
						msg["APLS_HDR"]["IN_PORT_NUM"] = -1	
						self.wfPorts[portInfo["WF_PORT_NUM"]].route_msg(msg)
						#print(msg)
						if msg["APLS_HDR"]["OUT_PORT_NUM"] != -2:
							if msg["APLS_HDR"]["OUT_PORT_NUM"] == -1:
								# send back to proxy service
								portID = msg["APLS_HDR"]["PROXY_PORT_LIST"].pop()
								try:
									self.activePorts[portID]["SOCKET"].send_pyobj(msg, zmq.NOBLOCK)
								except zmq.ZMQError as e:
									pass
							else:
								self.wfPorts[portInfo["WF_PORT_NUM"]].get_egressPortTable()[msg["APLS_HDR"]["OUT_PORT_NUM"]].send_pyobj(msg)
						else: 
							self.unbufferedWrite.write ("\npSwitchPort: Dropping Message")
					
					if msg["APLS_HDR"]["MSG_TYPE"] == MSG_TYPE_CONTROL_CLOSE:
						portInfo["SOCKET"].close()
						#TODO remove from switching table
						deletedPortIDList.append(portID)
			
			# poll the ingress port table
			for i in range(len(self.wfPorts)):
				if self.wfPorts[i].get_wfPortState() == ACTIVE or self.wfPorts[i].get_wfPortState() == RELEASED:
					for ingress_portNum, ingress_socket in self.wfPorts[i].get_ingressPortTable().items(): 
						if  socks.get(ingress_socket) == zmq.POLLIN:
							msg = ingress_socket.recv_pyobj()
							#self.unbufferedWrite.write ("Return Msg recived at pSwitchPort: %s"%(msg["DATA"]))
							msg["APLS_HDR"]["IN_PORT_NUM"] = ingress_portNum
							self.wfPorts[i].route_msg(msg)
												
							if msg["APLS_HDR"]["OUT_PORT_NUM"] != -2:	
								if msg["APLS_HDR"]["OUT_PORT_NUM"] == -1:
									portID = msg["APLS_HDR"]["PROXY_PORT_LIST"].pop()
									self.activePorts[portID]["SOCKET"].send_pyobj(msg)
								else:
									self.wfPorts[i].get_egressPortTable()[msg["APLS_HDR"]["OUT_PORT_NUM"]].send_pyobj(msg)
							else:
								self.unbufferedWrite.write ("\npSwitchPort: Dropping Message")
			""" Deleting the deleted portIDs"""
			while len(deletedPortIDList) > 0:
				deadPort = deletedPortIDList.pop()
				wf_portNum = self.activePorts[deadPort]["WF_PORT_NUM"]
				self.wfPorts[wf_portNum].update_wfPortLoad(-1)
				# if the wfPort has been free'd wait till it has served all the existing workflows
				if self.wfPorts[wf_portNum].get_wfPortState() == RELEASED and self.wfPorts[wf_portNum].get_wfPortLoad() == 0:
					msg = {}
					msg = {
						"TYPE":BC_PROXY_WFINSTANCE_RELEASE_REP,
						"WF_INSTANCE_ID":self.wfPorts[wf_portNum].get_wfInstanceID(),
						"WF_ID": self.portCfg["WF_ID"],
						"SERVICE_ID": self.portCfg["SERVICE_ID"],
						"WF_PORT_NUM": wf_portNum
					}
					self.sicPort.send_pyobj(msg)

					# Refresh the wfPort for new workflows to be attached
					self.wfPorts[wf_portNum].clear_port()

				else:
					if self.wfPorts[wf_portNum].get_wfPortLoad() <= self.wfPorts[wf_portNum].get_wfPort_load_notification_level():
						if self.wfPorts[wf_portNum].get_wfPort_suspend_flag() == True:	
							self.notify_controller("WF_INSTANCE_RESUME", WF_PORT= self.wfPorts[wf_portNum])

					
				self.unbufferedWrite.write ("\npSwitchPortThread: Load = %s"%(self.wfPorts[wf_portNum].get_wfPortLoad()))
				del self.activePorts[deadPort]
	
		""" Cleaning up before exiting the thread"""
		deletedPortIDList = [ ]
		for portID in self.activePortTable:
			self.activePortTable[portID].close()
			deletedPortIDList.append(portID)
		while len(deletedPortIDList)> 0:
			deadPort = deletedPortIDList.pop()
			del self.activePortTable[deadPort]

	def service_stop(self):
		self.sicPort.close()
		if self.portCfg["PROCESS_INTERFACE_STATE"] >= PROCESS_INTERFACE_LAUNCHED:
			try:
				os.killpg(self.portCfg["PROCESS_ID"],signal.SIGKILL)
			except OSError as e:
				self.module_lgr.debug ("Unable to kill process %d for <%s>: %s", self.portCfg["PROCESS_ID"], self.ID, e.__str__())
				pass
		for portID in self.activePortTable:
			self.activePortTable[portID].close()
		for i in range(len(self.wfPorts)):
			for portnum, socket in self.wfPorts[i].get_egressPortTable().iteritems():
				socket.close()
			for portnum, socket in self.wfPorts[i].get_ingressPortTable().iteritems():
				socket.close()

		self.sipPort_control.close()

	def join(self, timeout = None):
		self.stopRequest.clear()
		self.service_stop()
		super(proxySwitchPortThread,self).join(timeout)
		
