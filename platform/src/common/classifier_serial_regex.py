#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import sys
import os
import ast
import re
from .errors import *
from .classifier_regex import classifier_regex


class  classifier_serial_regex(classifier_regex):
      
	def __init__(self):
		super(classifier_serial_regex, self).__init__()
		self.classifier_type = "regex_serial"
 	
	def print_classifiers(self):
	#	for classifier in self.classifiers:
	#		print ("filter: %s, outport: %s"%(classifier["filter_string"], classifier["outport"]))  
		print ("%s"%(self.classifiers))
	
	def print_appRoutingTable(self, _ID = None):
		for key,val in self.appRoutingTable.items():
			for entry in val:	
				print ("ID:%s  In Port: %s, Pattern: %s , Out Port: %s"%(_ID, key, entry["pattern"], entry["outport"]))

	def create_classifierTable(self, _classifierFilePath, _classifierFileName):
		super(classifier_serial_regex, self).create_classifierTable(_classifierFilePath, _classifierFileName, self.classifier_type)



if __name__ == '__main__':
	msg1 = "Hello World"
	msg2 = "Hello Universe"
	msg3 = "Hello Guys"
	msg4 = "Hell is here"
	
	list_outports = 4
	try:
		classifier = classifier_serial_regex()
	except Classifier_Config_Error as e:
		print (e.msg)
	
	classifier.create_classifierTable("./classifiers/", "test.cls")
	classifier.print_classifiers()
	print (classifier.verify_outport(list_outports))
	print (classifier.match_pattern(msg1))
	print (classifier.match_pattern(msg2))
	print (classifier.match_pattern(msg3))
	print (classifier.match_pattern(msg4))
	


