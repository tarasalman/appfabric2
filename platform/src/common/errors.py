#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


class Error(Exception):
	pass

class Connect_Failed_Exception(Error):
	''' Exception raised whenever the control message trying to change a row is malforme'''
	def __init__(self,msg):
		self.msg = msg



#General error from the Control thread
class Control_Thread_Initialization_Error (Error):
	def __init__(self,msg):
                self.msg = msg

class Control_Thread_Bootstrap_Error (Error):
	def __init__(self,msg):
		self.msg = msg	

#Specific errors in the control thread
class Control_Thread_Error(Error):
        def __init__(self,msg):
                self.msg = msg
#CT_BOOTSTRAP_ERROR




class Service_Port_Config_Add_Link_Failed (Error):
	def __init__(self,msg):
                self.msg = msg


class Service_Port_Add_Egress_Port_Failed (Error):
	def __init__(self,msg):
		self.msg = msg

class Service_Port_Add_Ingress_Port_Failed (Error):
	def __init__(self,msg):
		self.msg = msg


# Errors form the Service Interface Port
class Service_Port_Init_Error (Error):
	def __init__(self,msg):
		self.msg = msg


#Errors from the UDP Tunnel Port
class UDP_Tunnel_Port_Init_Error (Error):
	def __init__(self,msg):
		self.msg = msg


class Invalid_Row_Entry(Error):
	''' Exception raised whenever the control message trying to change a row is malforme'''
	def __init__(self,msg):
		self.msg = msg


class Row_Delete_Error(Error):
	def __init__(self,msg):
                self.msg = msg

class Row_Lookup_Error(Error):
        def __init__(self,msg):
                self.msg = msg


class Read_Config_Error (Error):
	def __init__(self,msg):
		self.msg = msg


class Service_Configuration_Error(Error):
	def __init__(self, msg):
		self.msg = msg

class Service_Startup_Error (Error):
	def __init__(self, msg):
                self.msg = msg

class Service_Startup_Bootstrap_Error(Error):
	def __init__(self, msg):
                self.msg = msg

class Service_Init_Failed (Error):
	def __init__(self,msg):
                self.msg = msg


class Process_Spawn_Error (Error):
	 def __init__(self, msg):
                self.msg = msg

class General_Failure (Error):
	def __init__(self, msg):
                self.msg = msg

class Invalid_Path_String_Error (Error):
	def __init__(self, msg):
		self.msg = msg

class Invalid_Connect_String_Error(Error):
	def __init__(self, msg):
		self.msg = msg


class Proxy_Service_Start_Error (Error):
	def __init__(self, msg):
		self.msg = msg

class Tport_Init_Error (Error):
	def __init__(self, msg):
		self.msg = msg



#Control Path

class WorkFlow_Init_Error (Error):
	def __init__(self,msg):
		self.msg = msg

class Validate_Path_Error (Error):
	def __init__(self,msg):
		self.msg = msg

class Validate_WFDesc_Error (Error):
	def __init__(self,msg):
		self.msg = msg


class Create_Service_Graph_Error (Error):
	def __init__(self,msg):
		self.msg = msg


class Tokenize_Path_Error(Error):
	def __init__(self,msg):
		self.msg = msg


class Get_Service_Description_Error (Error):
	def __init__(self,msg):
		self.msg = msg
	

class XML_Object_Error (Error):
	def __init__(self,msg):
		self.msg = msg
 
class Driver_Load_Error (Error):
	def __init__(self,msg):
		self.msg = msg

class Resource_Manager_Init_Error(Error):
	def __init__(self,msg):
		self.msg = msg

class Resource_Service_Not_Found_Error (Error):
	def __init__(self,msg):
		self.msg = msg

class RM_BootStrap_Error (Error):
	def __init__(self,msg):
		self.msg = msg

class Check_WF_Deployability_Error(Error):
	def __init__(self,msg):
		 self.msg = msg

class Create_Service_Graph_Error(Error):
	def __init__(self,msg):
		self.msg = msg

class Verify_Service_Graph_Error (Error):
	def __init__(self,msg):
		self.msg = msg

class Classifier_Config_Error(Error):
	def __init__(self,msg):
		self.msg = msg

class Service_Switch_Init_Error (Error):
	def __init__(self,msg):
		self.msg = msg	

class WF_Instance_Init_Error(Error):
	def __init__(self,msg):
		self.msg = msg

class WF_Instance_Deploy_Failed (Error):
	def __init__(self,msg):
		self.msg = msg

class WF_Instance_Start_Services_Failed(Error):
	def __init__(self,msg):
		self.msg = msg

class Proxy_Deployment_Error (Error):
	def __init__(self,msg):
		self.msg = msg

class Service_Port_Config_Add_AppRouting_Table_Error(Error):
	def __init__(self,msg):
		self.msg = msg

class Fake_Nameserver_Init_Error (Error):
	def __init__(self,msg):
		self.msg = msg


class Attribute_Error (Error):
	def __init__(self,msg):
		self.msg = msg

class WF_Activate_Error (Error):
	def __init__(self,msg):
		self.msg = msg
