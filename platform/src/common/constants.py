#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


MODE_ADD_EGRESS = 0
MODE_ADD_INGRESS = 1

LINK_TABLE_INTERFACE_TYPE_EGRESS = 0
LINK_TABLE_INTERFACE_TYPE_INGRESS = 1


SERVICE_TYPE_MSG = 0
SERVICE_TYPE_PACKET = 1 
SERVICE_TYPE_PROXY = 2

# Command Status
SUCCESS = 1
FAIL= 0

# Resource states

RES_INITIALIZED = 0
RES_LAUNCHED = 1
RES_STOPPED = 2
RES_RELAEASED = 3

# Label Actions
LABEL_SWAP  = 0
ENCAP = 1

# switch states
FREE = 0
ACTIVE = 1
RELEASED = 2

# Proxy setup states
PROXY_INIT = 0
PROXY_RESOURCE_REQ_SENT = 1
PROXY_DEPLOYING_PROXY = 2
PROXY_DEPLOYMENT_PENDING = 3


# Proxy Initialization failure: reasons
OUT_OF_RESOURCE = 1
SITES_UNINITIALIZED = 2

# WF activation failure : reasons
WF_PORT_ALREADY_ACTIVE = 3 



# Init wf instance parent
INIT_WF_INSTANCE_ID = 0
