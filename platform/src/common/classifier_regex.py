#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import sys
import os
import ast
import re
import copy
from .errors import *
from . import xmlToDict

class classifier_regex:
	def __init__(self):
		self.classifiers = {}
		self.appRoutingTable = {}
		
		# Only to validate the classifiers specified by the user
		self.outPortSet = set()
		self.inPortSet = set()

	def create_classifierTable(self, _classifierFilePath, _classifierFileName, _classifier_type):
		self.classifiers = {
			"classifier_type": _classifier_type,	
			"classifier_list": {}
			}
		try:
			clsFile = os.path.expanduser(os.path.join(_classifierFilePath,_classifierFileName))
			classifier_config = xmlToDict.ConvertXmlToDict(clsFile).UnWrap()
			classifier = classifier_config["classifier_config"]["classifier"]
		except:
			raise Classifier_Config_Error ("Classifier file read error: check filename and path")	
		# read the wole classifier in memory
		if isinstance(classifier, dict):
			in_port_num = int(classifier["in_port"])
			ruleSet = classifier["rule"]
			if isinstance(ruleSet, dict):
				self.classifiers["classifier_list"][in_port_num] = []
				self.classifiers["classifier_list"][in_port_num].append(ruleSet)
			else:
				self.classifiers["classifier_list"][in_port_num] = ruleSet
		else:
			for in_port_cfg in classifier:
				in_port_num = int(in_port_cfg["in_port"])
				ruleSet = in_port_cfg["rule"]
				if isinstance(ruleSet, dict):
					self.classifiers["classifier_list"][in_port_num] = []
					self.classifiers["classifier_list"][in_port_num].append(ruleSet)
				else:
					self.classifiers["classifier_list"][in_port_num] = ruleSet

		for key, val in self.classifiers["classifier_list"].items():
			if key != -1:
				self.inPortSet.add(key)
			for entry in val:
				try:
					entry["match_keys"] = ast.literal_eval(entry["hdr"])
					del entry["hdr"]
				except:
					raise Classifier_Config_Error ("check the hdr field specification")

				if int(entry["outport"]) != -1:	
					self.outPortSet.add(int(entry["outport"]))
				# validate the regex pattern
				try: 
					validate_re = re.compile(entry["pattern"])
				except:
					print ("Error in compiling regex : %s = %s"%(entry["pattern"], re.DEBUG))


	def verify_outport(self,_outPorts):
		# Take as input any type of specification of outport set and compare
		_outPort_set = set()
		if isinstance(_outPorts, list):
			_outPort_set = set(_outPorts)
		elif isinstance (_outPorts, set):
			_outPort_set = _outPorts
		elif isinstance (_outPorts,int):
			for i in range(_outPorts):
				_outPort_set.add(i)
		
		if _outPort_set == self.outPortSet:
			return True
		else:
			return False

	    
	def get_classifierTable(self):
		return self.classifiers

	def create_appRoutingTable(self, _classifier_list):
		self.appRoutingTable = _classifier_list
	#	print ("Received App routing Table : %s"%(self.appRoutingTable))
		for key,val  in self.appRoutingTable.items():
			# Already validated
		#	print ("key = %s ,  val = %s"%(key,val))	
			for entry in val:
				try:
					regex_pattern = re.compile(entry["pattern"])
					entry["filter"] = regex_pattern
				except:
					print ("Error in compiling regex pattern")
					raise Classifier_Config_Error("Error in compiling regex pattern")	
			#	print ("Hi: %s"%(entry))
		#	print ("Hey")
	#	print ("Hellooooo")
	
	def match_pattern_per_key(self,_val, _temp_match_key, _tableEntry):
	#	print ("Matching in %s: %s"%(_val,_temp_match_key)),
		if isinstance(_val, dict):
	#		print ("which is a dict")
			if len(_temp_match_key) > 0:
				current_key = _temp_match_key.pop(0)	
				try:
					val = _val[current_key]
				except KeyError as e:
					return -2
				
				outport = self.match_pattern_per_key(val,_temp_match_key, _tableEntry)
				return outport
			elif len(_temp_match_key) ==  0 and _tableEntry["pattern"] == ".":
				return _tableEntry["outport"]
			else:
				return -2
		else:
	#		print ("which is not a dict")
			if re.search(_tableEntry["filter"], _val):
				outport = int(_tableEntry["outport"])
				return outport		
			else:
				return -2
		

	# Update later to specify the header where to match the pattern
	def match_pattern(self, _msg):
		outport = -2
		inPortEntry = self.appRoutingTable[_msg["APLS_HDR"]["IN_PORT_NUM"]]
		for tableEntry in inPortEntry:
		#	print ("tableEntry: %s"%(tableEntry))
		
			temp_match_key = copy.deepcopy(tableEntry["match_keys"])
			if len(temp_match_key) > 0:
				current_key = temp_match_key.pop(0)
			#	print ("current key: %s"%(current_key))
				try:
					val = _msg[current_key]
				except KeyError as e:
					return -2

				outport = int(self.match_pattern_per_key(val, temp_match_key, tableEntry))
				if outport != -2:
					break	
			elif len(temp_match_key) == 0 and tableEntry["pattern"] == ".":
				return tableEntry["outport"]
			else:
				return -2
		return outport	
	
	def get_action(self, _ingressPortNum):
		return self.appRoutingTable[_ingressPortNum]["action"]
	def get_appRoutingTable(self):
		return self.appRoutingTable
