from appfabric.common.aes_cbc import AESCipher 



class AESCipher_Msg:
    """
    Usage:
        c = AESCipher('password').encrypt('message')
        m = AESCipher('password').decrypt(c)

    Tested under Python 3 and PyCrypto 2.6.1.

    """

    def __init__(self, key):
        self.key=key
        self.encs= {} 
        self.msgs= {}

    def encrypt(self, msg, iv):
        for k,v in msg.items():
                enc=AESCipher(self.key).encrypt("%s"%v,iv)
                self.encs[k]=enc.decode()
        return self.encs

    def decrypt(self, encs, iv):
        for k,v in encs.items():
                m = AESCipher(self.key).decrypt(("%s"%v).encode(),iv)
                self.msgs[k]=m
        return self.msgs


##
# MAIN
# Just a test.
