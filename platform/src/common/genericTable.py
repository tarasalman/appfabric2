import sys
from .errors import *


class table:

#	tab = [ ] # "list of dicts"
#	columnAttributes = [ ] #list of colun attributes for the table
#	primaryKey = [ ] #allow composite primary key

	def __init__(self,columnList = []): #initate the table
	#	self.columnAttributes = columnList
		self.tab = [ ]
		self. columnAttributes = [ ]
		self.primaryKey=  [ ]
		self.index = -1
		for args in columnList:
			argList = [arg for arg in args.split(':')]
			attr,qualification = (argList + [None] * 2)[:2]
			if qualification == 'key':
				self.primaryKey.append(attr)
				self.columnAttributes.append(attr)
			else:
				self.columnAttributes.append(attr)
	#	print (self.columnAttributes)
	#	print (self.primaryKey)

#Add iterator for the table
	def __iter__(self):
		return tableIterator(self.tab)	



	def insert_row_from_valuelist(self,*valueList):
		#check length 
		if len(valueList) != len(self.columnAttributes):
			raise Invalid_Row_Entry("Error: Number of attributes in values are not equal to the numbr row entries in the table")
		else:
			# Look for existing row with the same identifier and update it
			row= dict(zip(self.columnAttributes, valueList))
			queryKey = { }
			for key in self.primaryKey:
				queryKey[key] = row[key]
			rowNum = self.lookup_row(**queryKey)
			if rowNum == -1:
			#	No previous match found: appending
				self.tab.append(row)
			else: # update an existing row
			#	matching table entry found: updating
				self.tab[rowNum] = row 
			return

	def insert_row_from_kvpairlist(self,**msg):
		#check for primary key in msg
		keyset = set()
		primaryKey_set = set()
		columnAttribute_set = set()
		#TODO: Find a better way of doing this
	#	print ("message:")
	#	print (msg)
		for key in msg:
			keyset.add(key)
		for key in self.primaryKey:
			primaryKey_set.add(key)
		for key in self.columnAttributes:
			columnAttribute_set.add(key)
		# Now compare
		if primaryKey_set.issubset(keyset):
			# update only the fields in the msg
			# first locate the row in the table
			queryKey= { }
			row = { }
			for key in self.primaryKey:
				queryKey[key] = msg[key]
			rowNum = self.lookup_row(**queryKey)
			if rowNum == -1:
                        #       No previous match found: appending
			#	Can append if all entries are present; else raise exception
		#		print (list(keyset.intersection(columnAttribute_set)))
			#	print (list(columnAttribute_set))
				if keyset.intersection(columnAttribute_set) == columnAttribute_set:
					for key in self.columnAttributes:
						row[key] = msg[key]
					self.tab.append(row)
				else:
					raise Invalid_Row_Entry("Error: No existing entry in table. Given entry does not have all the fields and hence does not qualify as a new row entry")
			else: # update an existing row
                        #       matching table entry found: updating
				for key in msg:
					self.tab[rowNum][key] =  msg[key]
			return
  
		else:
			raise Invalid_Row_Entry("Error: No primary key in the message. Without primary key, it is not possible to locate the correct table entry")
		
		return	 
		

	def table_length(self):
		return len (self.tab)

	def delete_row (self, **msg):
		#check for primary key in msg
		keyset = set()
		primaryKey_set = set()
		columnAttribute_set = set()
		#TODO: Find a better way of doing this
		for key in msg:
			keyset.add(key)
		for key in self.primaryKey:
			primaryKey_set.add(key)
		for key in self.columnAttributes:
			columnAttribute_set.add(key)
                # Now compare
		if primaryKey_set.issubset(keyset):
                        # update only the fields in the msg
                        # first locate the row in the table
			queryKey = { }
			for key in self.primaryKey:
				queryKey[key] = msg[key]
			rowNum = self.lookup_row(**queryKey)	
			if rowNum == -1:
				raise Row_Delete_Error("Error: No such row in table")
			else:
				del self.tab[rowNum]
		else:
			raise Row_Delete_Error("Error: Need primary key for deleting row from table ")






	# Update a specific field of the table


	# General Lookup function: Returns first instance of a row
	# Implement lookup_multirow to return multiple rows		

	def lookup_row(self, **lookupFilter):
		queryKey= [ ]
		for key in lookupFilter:
			queryKey.append(lookupFilter[key])
		rowNum = 0
		for row in self.tab:
			rowKey=[]
			for key in lookupFilter:
				rowKey.append(row[key]) #raise an exception if the query's key is not a column element  
			if (rowKey==queryKey):
				return rowNum
			rowNum+=1
		return -1
	
	def lookup_row_by_primary_key(self, **lookupKey):
		queryKey_set = set ()
		primaryKey_set = set ()
		primaryKey_in_query = [ ]
		for key in lookupKey:
			queryKey_set.add(key)
		for key in self.primaryKey:
			primaryKey_set.add(key)
			try:
				primaryKey_in_query.append(lookupKey[key])
			except KeyError as e:
				raise Row_Lookup_Error ("Error: Primary key required to lookup row by primary key ")
				 
		if primaryKey_set.issubset (queryKey_set):
			#rowNum = 0
			for row in self.tab:
				rowKey= [ ]
				for key in self.primaryKey:
					rowKey.append(row[key]) #raise an exception if the query's key is not a column element  
			#	print ("\n", rowKey, primaryKey_in_query)
				if (rowKey==primaryKey_in_query):
					return row
                        	#rowNum+=1
			raise Row_Lookup_Error ("Error: primary key not found in Table" )

		else:
			 raise Row_Lookup_Error ("Error: Primary key required to lookup row by primary key ")
					

	def print_table(self):
		for row in self.tab:
			print (row)

	def print_row(self, **lookupFilter):
		rowNum= self.lookup_row(**lookupFilter)
		if rowNum != -1:
			print (self.tab[rowNum])
 





class tableIterator:
	
	def __init__(self, table):
		self.numRows = len(table)
		self.index = 0
		self.tab = table

	def __next__(self):
		if self.index == self.numRows:
			raise StopIteration
		else:
			self.index+=1
			return self.tab[self.index -1] 
	

'''	
	def print_row(self, key):
	
	def lookup(self, **filter)
		for row in self.tab:
			for k,v in filter.iteritems():
				if row[k] != str(v) :
					break
				else:
					return row
		return None
'''		
'''
def query_lod(lod, filter=None, sort_keys=None):
    if filter is not None:
        lod = (r for r in lod if filter(r))
    if sort_keys is not None:
        lod = sorted(lod, key=lambda r:[r[k] for k in sort_keys])
    else:
        lod = list(lod)
    return lod

def lookup_lod(lod, **kw):
    for row in lod:
        for k,v in kw.iteritems():
            if row[k] != str(v): break
        else:
            return row
    return None

'''
