#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#

# Spawning new processes as daemons (in their own sessions)

import os
import sys
import logging
from .errors import *


BASE = "~/zmq_sources/host1"
SERVICE_DIR = "OpenADN_container/"
UMASK= 0
MAXFD = 1024
SERVICE_INIT_CONFIGURATIONS = "cfg" 
BIN_DIR = "bin" 
SERVICE_LOGS_DIR = "log"

#setting up logging
#module_lgr = logging.getLogger("main.controlThread.spawnDaemon")
#module_lgr.propagate= True


def spawnProcess(path,executable,args, kwargs):
	#kwargs: when the controller sends a CB_SERVICE_START message, the broker fetches the service from the service table and sends it to kwargs: containing the service name, service ID, service LOG file name template
#	module_lgr = logging.getLogger(__name__)

#	module_lgr.info ("Inside spawnDaemon")
	#print ("Inside Service Daemon")
#	cfg_file = serviceName +"_base.cfg"
#	try:
#		cfg_fh = open(os.path.join(BASE,SERVICE_INIT_CONFIGURATIONS,cfg_file), 'r')
#	except IOError as e:
#		print ("past 1")
#		module_lgr.info("No configuration file for Service %s", serviceName)
#		pass

	try:
		pid= os.fork()
	except OSError as e:
		#module_lgr.info("fork() failed")
		raise Process_Spawn_Error("Error: 1st fork failed: %s [%d]" % (e.strerror, e.errno))

	if (pid == 0):
	#	module_lgr.info("just before disconnecting from parent")	
		#print ("just before disconnecting from parent")
		try:
			os.setpgid(0, 0)
		except OSError as e:
			#module_lgr.info("Raised a OS error, Cannot setpgid () in child process")
			raise Process_Spawn_Error(" ".join(["Exec Failed:", e.strerror]))
	

#		try:
#			pid=os.fork() #fork a second child
#		except OSError, e:
#			raise Exception, "%s [%d]" %(e.strerror, e.errno)
		#if (pid == 0):   #inside second child
		#print ("Pass2")
		try:
			#os.chdir(os.path.expanduser(path))
			#print ("Pass3")
			os.umask(UMASK)
		except OSError as e:
		#	module_lgr.info(" ".join(["Exec Failed:", e.strerror]))
			raise Process_Spawn_Error(" ".join(["Exec Failed:", e.strerror]))
		try:
			maxfd = os.sysconf("SC_OPEN_MAX")
		except (AttributeError, ValueError):
			maxfd = MAXFD
		'''
		for fd in range(3,maxfd):
			try:
				os.close(fd)
			except OSError: # ERROR, fd wasn't open to begin with (ignored)
				pass
		'''
		#print ("Pass4") 
   	 	#redirect stdin, stdout and stderr to /dev/null
	#	fd = os.open("/dev/null", os.O_RDWR) # standard input (0)
	#	open("/home/openflow/zmq_sources/LOG_FILE", 'w+') # STDOUT
	#	os.dup2(fd, 0)
	#	os.dup2(0, 1)
	#	os.dup2(0, 2)

#		module_lgr.info(env)
		#print ("just before exec'ing")
	#	print (os.path.join(os.path.expanduser(BASE),SERVICE_DIR,serviceName,BIN_DIR,executable))
		try:
			#print (os.path.join(path,executable))
			#os.execlpe(os.path.join(os.path.expanduser(BASE),SERVICE_DIR,serviceName,BIN_DIR,executable), "hw_server, NULL",kwargs)
			os.execlpe(os.path.join(path,executable)," ".join([executable, args]), kwargs)
		except OSError as e:
				print ("Exec Failed: %s [%d]" % (e.strerror, e.errno))
				#module_lgr.info("Exec failed")	
				raise Process_Spawn_Error(" ".join(["Exec Failed:", e.strerror]))
		#	os._exit(255)
		#else:
		#	os._exit(0) #child exiting 				      


	else:
		try:
			os.setpgid(pid,pid)
		except OSError:
			#module_lgr.info("Raised a OS error: Cannot setpgid () in parent process")
			pass
		#print ("business as usual")
		return pid
		#while True:
			#continue



