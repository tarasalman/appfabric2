#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


from . import xmlToDict
import pprint
import os, sys

classifier_list = {}
	
outPortSet = set()
inPortSet = set()	
		
classifier_config_dict = xmlToDict.ConvertXmlToDict("classifiers/test.cls")

classifier = classifier_config_dict["classifier_config"]["classifier"]

if isinstance(classifier, dict):
	print("dict")
	in_port_num = int(classifier["in_port"])
	ruleSet = classifier["rule"]
	if isinstance(ruleSet, dict):
		classifier_list[in_port_num] = []
		classifier_list[in_port_num].append(ruleSet)
	else:
		classifier_list[in_port_num] = ruleSet
else:
	for in_port_cfg in classifier:
		in_port_num = int(in_port_cfg["in_port"])
		ruleSet = in_port_cfg["rule"]
		if isinstance(ruleSet, dict):
			# Only one rule
			classifier_list[in_port_num] = []
			classifier_list[in_port_num].append(ruleSet)
		else:
			classifier_list[in_port_num] = ruleSet

for key, val in classifier_list.items():
								
	inPortSet.add(key)
	for entry in val:
		outPortSet.add(int(entry["outport"])) 

pprint.pprint(classifier_list)
print (inPortSet)
print (outPortSet)


