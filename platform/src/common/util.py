#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import sys
import os
from .errors import *
import errno




def createPathString(*args):
	if not args[0].startswith("/"): 
		pathString = "/"
	else:
		pathString = ""
	for key in args:
		pathString = os.path.join(pathString,key)
		
	try:	
		os.makedirs(os.path.join(pathString))
	except OSError as e:
		if e.errno != errno.EEXIST:
			raise Invalid_Path_String_Error("Could not generate valid pathString")
			 
		pass
	return pathString


def createConnectString(transport, *args):
#	pathElements = args[0:len(args)-1]
	if transport == "inproc" or transport == "ipc":
		pathElements = args[0:len(args)-1]
		
		filename = args [len(args)-1]	
	#	print ("args: %s, %s" %(pathElements, filename))
		try:
			pathString = createPathString(*pathElements)
		except Invalid_Path_String_Error as e:
			raise Invalid_Connect_String_Error(e.msg)
		pathString= os.path.join(pathString,filename)
		return str(transport)+ ("://") + pathString
	elif transport ==  "tcp":
		pathElements = args[0:len(args)]
		if len(pathElements) > 2 or len(pathElements)< 1:
			raise Invalid_Path_String_Error("Usage: Either (transport,ip:port)  or (transport, ip, port)")
		if len(pathElements) == 2:
			ip = pathElements[0]
			port = pathElements[1]
			pathString = ip + ":" + port
		elif len(pathElements) == 1:
			pathString = pathElements[0]
		
	return str(transport)+ ("://") + pathString




 


