import os
import sys
import zmq
import threading
import logging
from .msgTypes import *
from .errors import *
from . import util
import pickle


class heartbeatRepThread (threading.Thread):
	
	def __init__(self, context, path):
		super (heartbeatRepThread, self).__init__()
		self.stopRequest = threading.Event()
		self.stopRequest.set()
		self.IPC_COMM = path
		self.context = context
		self.heartbeatSocket = self.context.socket(zmq.DEALER) 
		self.poller = zmq.Poller()
	#	self.poller.register(self.heartbeatSocket,zmq.POLLIN)

		self.module_lgr = logging.getLogger("main.commChannel.heartbeatRepThread")
		self.module_lgr.propagate = True


	
	def run(self):
		# connect to the hearbeat socket
	#	self.module_lgr.info(self.IPC_PATH_HEARTBEAT)
		try:
			self.heartbeatSocket.bind(util.createConnectString("ipc",self.IPC_COMM, "service_heartbeat"))
		except:
			#TODO inform of failure to the main commChannel
			self.module_lgr.debug("Could not bind to heartbeat socket")
			
		try:
			self.poller.register(self.heartbeatSocket,zmq.POLLIN)
		except:
			 #TODO inform of failure to the main commChannel
			self.module_lgr.debug("Could not register heartbeat socket to the poller")
		
	#	self.module_lgr.info ("Ready to receive heartbeat queries")
		print ("hearbeatREP initialized") 
		while self.stopRequest.isSet():
			try:
				socks = dict(self.poller.poll(1))
			except zmq.ZMQError as e:
				continue
		
			if socks.get(self.heartbeatSocket)==zmq.POLLIN:
				#self.module_lgr.debug ("Received heartbeat query")
				while True:
					try:
						msg = self.heartbeatSocket.recv_pyobj()
						more = self.heartbeatSocket.getsockopt(zmq.RCVMORE)
						if more != True:
							break
					except zmq.ZMQError as e:
						self.module_lgr.debug("Error in receiving heartbeat query msg ")
						pass
				#self.module_lgr.debug (msg)
				if msg["TYPE"] == I_SP_HEARTBEAT_REQ:
					#self.module_lgr.info("Received heartbeat request")
					self.heartbeatSocket.send_pyobj(I_PS_HEARTBEAT_REP_MSG, zmq.NOBLOCK)
		# Preparing to exit
	#	self.module_lgr.info("closing hearbeat socket")				
		self.heartbeatSocket.close()
	
	def join(self, timeout= None):	
	#	self.module_lgr.info("Calling join on heartbeat thread")
		self.stopRequest.clear()
		super(heartbeatRepThread,self).join(timeout)

 
