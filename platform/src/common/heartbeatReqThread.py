
import os
import sys
import zmq
import threading
from . import runStates
import logging
from . import heartbeatCfg
from .errors import *
from .msgTypes import *
from . import util
import pickle
from time import sleep
class heartbeatReqThread (threading.Thread):

	def __init__(self, **kwargs):
		super (heartbeatReqThread, self).__init__()
		self.stopRequest = threading.Event()
		self.stopRequest.set()
		
		self.module_lgr = logging.getLogger("main.controlThread.servicePortHandler.heartbeatReqThread")
		self.module_lgr.propagate = True
	#	self.module_lgr.info ("Initializing heartbeat thread ")
		
                # To report hearbeat
		self.IPC_COMM = kwargs["IPC_COMM"]
		self.INPROC_COMM = kwargs ["INPROC_COMM"]
		self.context = kwargs["CONTEXT"]
		self.serviceName = kwargs["SERVICE_NAME"]
		self.serviceID = kwargs ["SERVICE_ID"]

		self.heartbeatQuerySocket = self.context.socket(zmq.DEALER)
		self.heartbeatQuerySocket.setsockopt(zmq.LINGER, 0)
		self.heartbeatReportSocket = self.context.socket(zmq.DEALER)
		self.heartbeatReportSocket.setsockopt(zmq.LINGER, 0)
		self.poller = zmq.Poller()
		#self.poller.register(self.hearbeatQuerySocket,zmq.POLLIN)

	def run(self):
                # connect to the hearbeat socket
		maxTries = 100
		numTries = 0	
		while True:
			try:	
				self.heartbeatQuerySocket.connect(util.createConnectString("ipc",self.IPC_COMM, "service_heartbeat"))
				break
			except:
				sleep(1)
				continue
			
		while True:
			try:
				self.heartbeatReportSocket.connect(util.createConnectString("inproc",self.INPROC_COMM, "service_heartbeat_report"))
				break
			except:
                        	#TODO inform of failure to Service socket
				self.module_lgr.debug ("heartbeat connections failed")
				continue
		try:
			self.poller.register(self.heartbeatQuerySocket,zmq.POLLIN)
		except:
			#TODO inform of failure to Service socket
			self.module_lgr.debug ("heartbeat poller failed")
		
	#	self.module_lgr.info ("heartbeat started")	
	#	self.module_lgr.info (I_SP_HEARTBEAT_QUERY_MSG)
		print ("heartbeat REQ thread started for service: %s"%(self.serviceName))
		while self.stopRequest.isSet():
			i=0
			livenessFlag= False
			while i <= heartbeatCfg.NUM_RETRIES :
				try:
					self.heartbeatQuerySocket.send_pyobj(I_SP_HEARTBEAT_QUERY_MSG,zmq.NOBLOCK, -1)
					wait = heartbeatCfg.WAIT_INTERVAL + (i* heartbeatCfg.INCREMENT)
					#self.module_lgr.debug("Sent heartbeat query to service") 
				except:
					self.module_lgr.debug ("Could not send heartbeat query message")
					break
				try:
					socks = dict(self.poller.poll(wait))
				except:
					pass
				if socks.get(self.heartbeatQuerySocket)==zmq.POLLIN:
					try:
						msg = self.heartbeatQuerySocket.recv_pyobj()
					except:
						pass
					else:
						if msg["TYPE"] == I_PS_HEARTBEAT_REP:
							livenessFlag= True
							#self.module_lgr.debug("Got heartbeat response ")
							break
						else:
							pass
				i=i+1
				
			if livenessFlag == False:
				self.module_lgr.debug ("<%s><%s> is dead", self.serviceName, self.serviceID)
				try:
					self.heartbeatReportSocket.send_pyobj(I_MGMT_SERVICE_DEAD_MSG,zmq.NOBLOCK)
				except:
					pass
							
		# Close the open sockets
		self.heartbeatQuerySocket.close()
		self.heartbeatReportSocket.close()
	def join(self, timeout=None):	
		self.stopRequest.clear() 
		super(heartbeatReqThread,self).join()	
