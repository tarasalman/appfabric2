#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import sys
import os
import ast
from .errors import *

class readConfig: 
	
	def __init__(self,filename, path = "./"):
		self.filename = filename
		self.path = path
		self.keySet = ()
		self.config = []
		self.readCfg()	
	


	def readCfg(self):
		try:
			filepath = os.path.join(self.path,self.filename)
			if filepath.startswith("~"):
				filepath = os.path.expanduser(filepath)
			fileobj = open(filepath, "r")
		except:
			#TODO : return a error
			print ("Read configuration error: check filename and path")
		# read the whole configuration in memory
		# assuming configuration files are relatively small		
		for line in fileobj:
			if line.startswith("<key"):
				try:
					self.keySet = set(keys.strip() for keys in  (str.strip( (line.split(">")[1]), " \n").split(","))) 
				except:
					pass
			if line.startswith ("<cfg>"):
				try:
					self.config.append(ast.literal_eval( str.strip(line.split(">")[1])) )
				except:
					pass
		if not len(self.keySet):
			raise Read_Config_Error ("Error: No Key")
		
		

	def getConfig (self, **lookupKey):
		if len(lookupKey) == 0:
			return self.config
		queryKey_set = set ()
		primaryKey_in_query = [ ]
		for key in lookupKey:
			queryKey_set.add(key)
		for key in self.keySet:
			try:
				primaryKey_in_query.append(lookupKey[key])
			except KeyError as e:
				raise Read_Config_Error ("Error: Primary key required to lookup config by primary key ")
		if self.keySet.issubset (queryKey_set):
			for row in self.config:
				rowKey= [ ]
				for key in self.keySet:
					rowKey.append(row[key]) #raise an exception if the query's key is not a column element  
				if (rowKey==primaryKey_in_query):
					return row
			raise Read_Config_Error ("Error: primary key not found in Table" )
		else:
			raise Read_Config_Error ("Error: Primary key required to lookup row by primary key ")	


