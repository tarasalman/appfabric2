#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#

import sys,os
from time import sleep
import zmq
import threading

from appfabric.common import util
from appfabric.common.msgTypes import * 
from appfabric.common.constants import *
from appfabric.common.unbufferedStream import unbuffered
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto import Random
from appfabric.common.aes_msg import AESCipher_Msg
import ast

class hostStubThread(threading.Thread):
	
	def __init__(self,_hostStubObj,_hostPort, _context):
		super(hostStubThread,self).__init__()
	#	self.print_status ("Initializing host stub thread")
	
		self.hostStubObj = _hostStubObj
	
		self.stopRequest = threading.Event()
		self.stopRequest.set()
			
	
		self.context = _context
		self.poller = zmq.Poller()
		
		#register the host port tranferred from the connection dispatcher
		self.hostPort = _hostPort
		self.poller.register(self.hostPort, zmq.POLLIN)
		
		# Port between host stub and the resource manager
		self.rmPort = self.context.socket(zmq.DEALER)
	#	self.hostStubObj.set_attr("hostStub_to_rm_connection", rmPort)

		# set unbuffered stream
		self.unbufferedWrite= unbuffered(sys.stdout)
		
	def print_status(self, msg):
		self.unbufferedWrite.write("\nHost Stub <%s>:  %s"%(self.hostStubObj.get_attr('resource_extIP'),msg ))


	def run(self):
	#	self.print_status ("Starting host stub thread for %s" %(self.hostStubObj.get_attr("resource_ID")))
		while True:
			try:
				self.rmPort.connect(util.createConnectString(
								"inproc", 
								"tmp/inproc/lighthouse",
								str(os.environ["HOST_NAME"]), 
								("RM_"+ str(threading.current_thread().name))))
				break
			except:
				sleep (1)
				continue
		try:
			self.poller.register(self.rmPort, zmq.POLLIN)
			
			self.rmPort.send_pyobj({
						"TYPE":HOSTSTUB_RM_HOSTSTUB_INIT, 
						"ID":self.hostStubObj.get_attr("resource_ID"),
						"AVAILABLE_RESOURCE":0 
						})
		except zmq.ZMQError as e:
			#TODO how to handle this error? Timeout at the host and at the host. Host may send a request again to the CD
			# log the error
			self.print_status("Error:%s"%(e__str__()))
			sys.exit()
		while self.stopRequest.isSet():
			try:
				socks= dict(self.poller.poll(1))
			except  zmq.ZMQError as e :
				#TODO log the error
				self.print_status ("Error: "+ e.__str__())
				sys.exit()

			if  socks.get(self.hostPort) == zmq.POLLIN:		
				msg = self.hostPort.recv_pyobj()
				cmd = msg["TYPE"]
				if cmd == BC_INIT_ACK and msg["STATUS"] == SUCCESS:
				#	self.print_status ("received BC_INIT_ACK from host")
					self.hostStubObj.set_attr("resource_quota_available", msg["AVAILABLE_RESOURCE"])
					# send a resource update message to resource manaager
					self.send_resource_update_to_rm()
					
				#	self.print_status("connection between controller and host setup successfully")
				
					
				elif cmd in (BC_SERVICE_START_REP, BC_PROXY_START_REP):
					# update the resource quota 
					# Added by Tara					
					# Decrypt the message comming from the NC and eventually send it to GC
					f = open(os.path.expanduser('~/AppFabric/experiments/physical_machines/SecretKeys/NC_LC.bin'),"rb") # Get the NC_LC secret key 						(This key is agreed on after the first msg and both parties has it)
					message= f.read()
					message=message.split(b"    ")
					key2= message[1] #key
					iv= message[0] #nonce 
					msg2= msg.pop("TYPE") # Type is not encrypted so that it can be recognized in the if statement 
					#print (msg)
					try:
						msg= AESCipher_Msg(key2.decode()).decrypt(msg,iv) # Decrypt the whole message 
					except:
						pass 	
					msg["TYPE"]= msg2 # return the message TYPE
					msg["AVAILABLE_RESOURCE"]= float(msg["AVAILABLE_RESOURCE"]) # Change resource from string to int 
					msg["STATUS"]= float(msg["STATUS"])# Change from string to int 
					self.hostStubObj.set_attr("resource_quota_available", msg["AVAILABLE_RESOURCE"])
					service = {
						"SERVICE_NAME": msg["SERVICE_NAME"],
						"SERVICE_ID":msg["SERVICE_ID"]
						}
					self.hostStubObj.serviceList_add_service(service)	
					try: 
						#Saves application configuration at LC 
						text_file = open("LC/NC_Config_%s.txt"%msg["NC_NAME"], "wb")
						text_file.write(bytes("AVAILABLE_RESOURCE: %s \n"%(msg["AVAILABLE_RESOURCE"]), 'UTF-8'))
						text_file.write(bytes("MAX_REQUESTS: %s \n"%(msg["MAX_REQUESTS"]), 'UTF-8'))
						text_file.write(bytes("MAX_REQUESTS_PER_CLIENT: %s \n"%(msg["MAX_REQUESTS_PER_CLIENT"]), 'UTF-8'))
						text_file.write(bytes("BANNED_DEST: %s \n"%(msg["BANNED_DEST"]), 'UTF-8'))
						text_file.write(bytes("TYPE_OF_TRAFFIC: %s \n"%(msg["TYPE_OF_TRAFFIC"]), 'UTF-8'))
						text_file.close()
					except: 					
						#print("hello")
						pass 
					# Add host information to the message to be regstered at the workflow
					# self.print_status("Received BC_SERVICE_START_REP")
					msg["HOST_ID"] = self.hostStubObj.get_attr("resource_ID")					
					msg["HOST_EXT_IP"] = self.hostStubObj.get_attr("resource_extIP")
					msg["HOST_INT_IP"] = self.hostStubObj.get_attr("resource_intIP")	
				#	self.print_status ("Received BC_SERVICE_START_REP or BC_PROXY_START_REP)")
					# send message to resource manager (RM)
					
					
					self.rmPort.send_pyobj(msg)
				# Some messages here are recieved from node controller and just pass to global controller 
				elif cmd in (
						BC_SERVICE_PORT_CONFIG_ADD_EGRESS_REP, 
						BC_SERVICE_PORT_CONFIG_ADD_INGRESS_REP, 
						BC_SERVICE_PORT_CONFIG_ADD_APP_ROUTER_REP,
						BC_SERVICE_PORT_CONFIG_ACTIVATE_WF_REP,
						BC_OVERLOAD_NOTIFICATION,
						BC_SUSPEND_WF_INSTANCE_NOTIFICATION,
						BC_RESUME_WF_INSTANCE_NOTIFICATION,
						BC_REPORT_LOAD_REP,
						BC_PROXY_WFINSTANCE_RELEASE_REP, 
						I_SC_SERVICE_ANOM_INTEGRITY,
						I_SC_SERVICE_ANOM_DEST,
						I_SC_SERVICE_ANOM_TRAFFIC,
						I_SC_SERVICE_ANOM_MAX_REQUEST,
						I_SC_SERVICE_ANOM_MAX_CLIENT_REQ
					):
					# Messages here are recieved from node controller and just pass to global controller 
					if cmd in (
							I_SC_SERVICE_ANOM_INTEGRITY,
							I_SC_SERVICE_ANOM_DEST,
							I_SC_SERVICE_ANOM_TRAFFIC,
							I_SC_SERVICE_ANOM_MAX_REQUEST,
							I_SC_SERVICE_ANOM_MAX_CLIENT_REQ
							): 
						# Added by Tara					
						# Decrypt the message comming from the NC and eventually send it to GC
						f = open(os.path.expanduser("~/AppFabric/experiments/physical_machines/SecretKeys/NC_LC.bin"),"rb") # Get the NC_LC secret key 							(This key is agreed on after the first msg and both parties has it)
						message= f.read()
						message=message.split(b"    ")
						key2= message[1] #key
						iv= message[0] #nonce 
						msg2= msg.pop("TYPE") # Type is not encrypted so that it can be recognized in the if statement 
						#print (msg)
						try:
							msg= AESCipher_Msg(key2.decode()).decrypt(msg,iv) # Decrypt the whole message 
						except:
							pass 	
						msg["TYPE"]= msg2 # return the message TYPE
						msg["PROXY_PORT_LIST"]=ast.literal_eval(msg["PROXY_PORT_LIST"])
						#print(msg)
						if cmd== I_SC_SERVICE_ANOM_INTEGRITY: 
							print ("\n LC Recieved anomolous integrity check failed from %s"%(msg["User"])) 
						if cmd== I_SC_SERVICE_ANOM_DEST: 
							print ("\n LC Recieved invalid destination from %s"%(msg["User"]))
						if cmd== I_SC_SERVICE_ANOM_TRAFFIC: 
							print ("\n LC Recieved invalid traffic from %s"%(msg["User"]))
						if cmd== I_SC_SERVICE_ANOM_MAX_REQUEST: 
							print ("\n LC Recieved NC acceeding maximum resources from when user %s was requesting "%(msg["User"]))
						if cmd== I_SC_SERVICE_ANOM_MAX_CLIENT_REQ: 
							print ("\n LC Recieved user %s acceede max requests"%(msg["User"]))
				#	self.print_status ("BC_SERVICE_PORT_CONFIG_ADD_INGRESS/EGRESS_REP received: %s"%(msg["SERVICE_NAME"]))
					self.rmPort.send_pyobj(msg)		
			
			if socks.get(self.rmPort) == zmq.POLLIN:
				msg = {}
				msg = self.rmPort.recv_pyobj()
				cmd = msg["TYPE"]
				if cmd in (CB_SERVICE_START_REQ, CB_PROXY_START_REQ):
					#self.print_status("received CB_SERVICE_START_REQ:from RM")
					new_resourceQuota = self.hostStubObj.get_attr("resource_quota_available") - msg["RESOURCE_REQ"]
					self.hostStubObj.set_attr("resource_quota_available", new_resourceQuota)
					#print("%s"%msg) 
					self.hostPort.send_pyobj(msg)
				#	self.print_status("CB Service start req sent for service %s"%(msg["SERVICE_NAME"]))
						
				#	self.print_status ("sent CB_SERVICE_START_REQ or CB_PROXY_START_REQ")
				#Messages here are recieved from global controller and passes to local controller 
				elif cmd in (
						CB_SERVICE_PORT_CONFIG_ADD_EGRESS_REQ, 
						CB_SERVICE_PORT_CONFIG_ADD_INGRESS_REQ, 
						CB_SERVICE_PORT_CONFIG_ADD_APP_ROUTER_REQ,
						CB_SERVICE_PORT_CONFIG_ACTIVATE_WF_REQ,
						CB_STOP_OVERLOAD_NOTIFICATIONS,
						CB_REPORT_LOAD_REQ,
						CB_PROXY_WFINSTANCE_RELEASE_REQ,
						CB_RESUME_OVERLOAD_NOTIFICATIONS,
						CB_RESUME_ANOM
						):
					if (cmd== CB_RESUME_ANOM): 
						f = open("/home/openstack/AppFabric/experiments/physical_machines/SecretKeys/NC_LC.bin","rb") # Get the NC_LC secret key 							(This key is agreed on after the first msg and both parties has it)
						message= f.read()
						message=message.split(b"    ")
						key2= message[1] #key
						iv= message[0] #nonce 
						msg2= msg.pop("TYPE") # Type is not encrypted so that it can be recognized in the if statement 
						try:
							msg= AESCipher_Msg(key2.decode()).encrypt(msg,iv) # Decrypt the whole message 
						except:
							pass 	
						#print(msg)
						msg["TYPE"]= msg2 # return the message TYPE
				#	self.print_status("CB_SERVICE_PORT_CONFIG_ADD_EGRESS_REQ sent")
					self.hostPort.send_pyobj(msg)
					#if cmd == CB_SERVICE_PORT_CONFIG_ADD_APP_ROUTER_REQ:
					#	print ("Hi: %s"%(msg))

	def send_resource_update_to_rm(self):
		msg = {
			"TYPE": HOSTSTUB_RM_RESOURCE_REPORT, 
			"ID":self.hostStubObj.get_attr("resource_ID"), 
			"AVAILABLE_RESOURCE": self.hostStubObj.get_attr("resource_quota_available")
			}
		self.rmPort.send_pyobj(msg)




