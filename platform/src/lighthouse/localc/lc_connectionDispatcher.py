#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import os, sys
import copy
import zmq
import threading
import uuid
from time import sleep

from appfabric.common.msgTypes import *
from appfabric.common.errors import *
from appfabric.common import util
from appfabric.common.unbufferedStream import unbuffered
from appfabric.common.ipStr import ipNum, ipStr

from . import hostStubThread
from .hostStub import hostStub
from .lc_resourceManager import resourceManager

#TODO: Add loggers

class lc_connectionDispatcher(threading.Thread):
	

	def __init__(self,_context):
		super(lc_connectionDispatcher, self).__init__()
		
		# set unbuffered stream
		self.unbufferedWrite= unbuffered(sys.stdout)

	#	self.unbufferedWrite.write("Local CD: Intializing lc connection dispatcher...")	
		self.stopRequest = threading.Event()
		self.stopRequest.set()


		self.hostStubThreadList = []	
		self.hostStub_pending_list = []
		
		self.context = _context
		self.hostSocketList = []
		self.controlPort = self.context.socket (zmq.ROUTER)
		self.rManagerPort = self.context.socket (zmq.DEALER)
		self.poller = zmq.Poller()
	#	self.unbufferedWrite.write ("Local CD: Initialized")

	def print_status(self, msg):
		self.unbufferedWrite.write("\nDatacenter controller <%s:%s>:  %s"%(os.environ["ZONE_NAME"],os.environ["SITE_NAME"], msg ))
	

	def run(self):
		tunnel_if_addr_index = 1
	#	self.unbufferedWrite.write ("\nLocal CD: Starting connection dispatcher...")	
		#self.print_status("Starting connection dispatcher...")
		try:
			self.controlPort.bind("tcp://*:1234")
			self.poller.register(self.controlPort, zmq.POLLIN)
		except zmq.ZMQError as e :
			self.unbufferedWrite.write ("Local CD: Error %s" %(e.__str__()))
			sys.exit()
		while True:
			try:
				self.rManagerPort.connect(util.createConnectString("inproc", 
										"tmp/inproc/lighthouse",
										str(os.environ["HOST_NAME"]), 
										"RM_CD"))
				#Tara 
				#Trust calculation should start from here 
				break
			except:
				sleep(1)
				continue
		try:	
			self.poller.register(self.rManagerPort, zmq.POLLIN)

		except zmq.ZMQError as e :
			#TODO log the error
			self.unbufferedWrite.write ("\nLocal CD: Error %s" %(e.__str__()))
			sys.exit()
		except Invalid_Connect_String_Error as e:
			#TODO log error
			self.unbufferedWrite.write ("\nLocal CD: Error %s"%(e.msg))
			sys.exit()
		while self.stopRequest.isSet():
			try:
				socks= dict(self.poller.poll(1))
			except zmq.ZMQError as e:
				#This will throw an error when sockets are shut down
				pass

			if  socks.get(self.controlPort) == zmq.POLLIN:
				frame_id = {}
				frame_msg= {}
				try:
					frame_id = self.controlPort.recv()
					frame_msg = self.controlPort.recv_pyobj() 
				except:
					#self.unbufferedWrite.write ("Local CD: Error in receiving message from host")
					pass	
				if frame_msg["TYPE"] == BC_INIT_SYN:
					# assign a new socket to the connection and pass it to the thread
					# pass the news of the new thread to resource manager
					#self.unbufferedWrite.write ("LC CD :<%s> BC_INIT_SYN from %s"%(count, frame_msg["EXT_ADDR"]))
					hostPort = self.context.socket(zmq.DEALER)
					hostPortNum = hostPort.bind_to_random_port("tcp://*")
					tunnel_if_addr = ipStr(ipNum(192,168,int(os.environ["SITE_NUM"]),tunnel_if_addr_index))
					hostObj = hostStub()
					hostObj.set_attr("resource_ID", frame_msg["BROKER_ID"])
					hostObj.set_attr("resource_extIP",frame_msg["EXT_ADDR"])
					hostObj.set_attr("resource_intIP", tunnel_if_addr)
					hostObj.set_attr("resource_controlPortNum", hostPortNum)
					hostObj.set_attr("resource_quota_available", 0)
					tunnel_if_addr_index += 1
					
					# make copy of the hostObj object, to avoid race condition(hostStubthread updating hostObj before
					# hostInfo has been  read by RM
					
					hostObj_copy = copy.deepcopy(hostObj)

					# Informing the resource manager of the new host connection
					try:
						hThread =  hostStubThread.hostStubThread (hostObj, hostPort, self.context)
					except:
						#TODO for now pass. Let the host send a request again after timing out
						self.unbufferedWrite.write("\nError")
						pass
					# construct the CB_INIT_SYN_ACK msg for the host but do not send it yet
					frame_msg["TYPE"]  = CB_INIT_SYN_ACK
					frame_msg["PORT"]  = hostPortNum
					frame_msg["TUNNEL_IF_ADDR"] =  tunnel_if_addr

					# save the context and wait for resource manager to respond
					hostStub_saved_context ={}
					hostStub_saved_context["resource_id"] = hostObj.get_attr("resource_ID")
					hostStub_saved_context["frame_id"] = frame_id
					hostStub_saved_context["hThread"] = hThread
					hostStub_saved_context["frame_msg"] = frame_msg 

					self.hostStub_pending_list.append(hostStub_saved_context)
					try:
						self.rManagerPort.send_pyobj({
									"TYPE": CD_RM_INFO_HOSTSTUB,
									"THREAD_NAME":str(hThread.name),
									"HOSTSTUB":hostObj_copy.get_hostInfo()
									}, zmq.NOBLOCK)
						#self.unbufferedWrite.write ("LC CD :<%s> sent to RM %s"%(count, frame_msg["EXT_ADDR"]))	
					except zmq.ZMQError as e:
						self.unbufferedWrite.write ("\nLocal CD Error: %s"%(e.__str__()))

			if  socks.get(self.rManagerPort) == zmq.POLLIN:
				msg = self.rManagerPort.recv_pyobj()
				cmd = msg["TYPE"]
				if cmd == RM_CD_ACK_HOSTSTUB:
					index_of_items_to_remove = []
					for i in range (len(self.hostStub_pending_list)):
						if self.hostStub_pending_list[i]["resource_id"] == msg["RESOURCE_ID"]:
						#	self.unbufferedWrite.write ("RM: sending ACK to host %s"%(msg["RESOURCE_ID"]))
							index_of_items_to_remove.append(i)
							hostStub_saved_context = self.hostStub_pending_list[i]
							hostStub_saved_context["hThread"].start()
							try:
								self.controlPort.send(hostStub_saved_context["frame_id"],zmq.SNDMORE)
								self.controlPort.send_pyobj(hostStub_saved_context["frame_msg"])
							except zmq.ZMQError as e:
								self.unbufferedWrite.write (e.__str__())
					self.hostStub_pending_list = [i for j, i in enumerate(self.hostStub_pending_list) if j not in index_of_items_to_remove]
						
'''
if __name__ == '__main__':
		
	# for Mininet based experiments
	sys.stdout = unbuffered(sys.stdout)
	
	os.environ["RESOURCE_DRIVER_CONFIG"] = os.path.expanduser("~/driver_mininet/resource_drivers.cfg")
	os.environ["DRIVERS_DIR"] = "~/driver_mininet/drivers"
	
	context = zmq.Context()
	try:
		# start the resource manager
		rManager = resourceManager(context)
		rManager.start()
	except Resource_Manager_Init_Error as e:
		self.unbufferedWrite.write ("Error starting resource manager ", e.msg())
		sys.exit()
	self.unbufferedWrite.write ("Started resource Manager")
	# start the connection dispatcher
	connectionDispatcher = lc_connectionDispatcher(context)
	connectionDispatcher.start()
'''	
