#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import os, sys
import types
import time
import threading
import zmq
import pprint

from appfabric.common import xmlToDict
from appfabric.common import processSpawn
from appfabric.common import util
from appfabric.common.errors import *
from appfabric.common.msgTypes import *
from appfabric.common.constants import *

class resourceDriverManager(threading.Thread):
	
	def __init__ (self, env):
		super (resourceDriverManager, self).__init__()
		self.context = env["CONTEXT"]
		self.resourceProviders = []
		# read the driver configurations in memory
		#TODO Add error checking
		try:
			resourceDriversConfigDict = xmlToDict.ConvertXmlToDict(os.environ["RESOURCE_DRIVER_CONFIG"])
			conf = resourceDriversConfigDict['driver_config']['driver']	
			# if there is only one entry in the config file conf is a dict, else it is a list
			if isinstance(conf,dict):
				self.resourceDrivers = []
				self.resourceDrivers.append(conf)
			else:
				self.resourceDrivers = conf
		except:
			print ("XML Parse Error: Check your service config file")
		
		self.stopRequest = threading.Event()
		self.stopRequest.set()
		# Add a connection to the resource manager    
		self.resourceManagerConnectString = util.createConnectString("inproc", "tmp/inproc/resource_manager","device_drivers")
		self.resourceManagerPort = self.context.socket(zmq.DEALER)
		self.poller = zmq.Poller()		
		
	def run (self):
	
		# connect to resource manager
		try:
			self.resourceManagerPort.connect(self.resourceManagerConnectString)
			self.poller.register (self.resourceManagerPort,zmq.POLLIN)
		except zmq.ZMQError as e:
			#TODO log the error
			print (e.__str__())
			exit()
		
		# tell resource manager thread that it has successfully spawned
		self.resourceManagerPort. send_pyobj ({"TYPE": DM_INIT})
		
		while self.stopRequest.isSet():
			try:
				socks= dict(self.poller.poll(1))
			except zmq.ZMQError as e:
				pass

			if socks.get(self.resourceManagerPort) == zmq.POLLIN:
				print ("mesage from resource manager port")
				msg = self.resourceManagerPort.recv_pyobj()
				try:
					cmd = msg["TYPE"]
				except KeyError:
					#TODO: log message
					print ("Malformed message, missing TYPE")
					continue

				if cmd == RM_REQ_GET_HOST:
					msg["TYPE"] = DM_REQ_GET_HOST
					try:
						driverName = msg["DRIVER_NAME"]
					except KeyError:
						#TODO: log message
						print ("Malformed message RM_LOAD_DRIVER, missing DRIVER_NAME") 
						continue
					# Load resource driver on demand
					driver = self.get_driver_port(driverName)
					if driver == None:
						try:
							self.load_driver(driverName)
							driver = self.get_driver_port(driverName)
							driver.send_pyobj(msg)
						except Driver_Load_Error  as e:
							 #TODO raise an error
							print (e.msg)
							continue
					else:
						driver.send_pyobj(msg)
							
							
							
			for driverConf in self.resourceProviders:
				driverPort = driverConf["driver"]
				if socks.get(driverPort) == zmq.POLLIN:
					print ("Message from resource provider ")
					msg = self.driverPort.recv_pyobj()
					print (msg)

	def get_driver_port(self, _driverName):
		for driver in self.resourceProviders:
			if driver["name"] == _driverName:
				return driver["driver"]
		return None

	def load_driver(self,_driverName):
		found = False	
		print (len(self.resourceDrivers))
		for i in range(len(self.resourceDrivers)):
			driver = self.resourceDrivers[i]
			pprint.pprint (driver)
			if driver['name'] == _driverName:
				try:
					ipcConnectString = util.createConnectString("ipc", "tmp/ipc/device_drivers", driver["name"])
				except Invalid_Connect_String_Error as e:
					raise Driver_Load_Error ("Could not create IPC path to the driver")

				path = os.path.expanduser(os.environ["DRIVERS_DIR"])
				executable = driver['executable']
				if driver['arguments'] == 'None':
					arguments = ""
				else:
					arguments = driver['arguments']
				env = {}
				env["NAME"] = driver["name"]
				if driver["environment"] != 'None' :
					env["ENVIRONMENT"] = driver["environment"]
				try:
					childPID = processSpawn.spawnProcess(path,executable, arguments, env)
				except  Process_Spawn_Error as e:
					raise Driver_Load_Error (e.msg)
				
				# Open a connection to talk to the driver
				conn = self.context.socket(zmq.DEALER)	
				conn.bind(ipcConnectString)
				# load driver
				
				# Add driver to the list of available resource provicers
				driverInfo = {
						"name": _driverName,
						"driver": conn
						}	
				self.resourceProviders.append(driverInfo)
				self.poller.register (driverInfo["driver"], zmq.POLLIN)		

		if found == False:
			raise Driver_Load_Error ("No such driver has been configured")	
		
		
def test():
	print ("running test")
	os.environ["RESOURCE_DRIVER_CONFIG"] = os.path.expanduser("~/driver_mininet/resource_drivers.cfg")
	os.environ["DRIVERS_DIR"] = "~/driver_mininet/drivers"
	env = {}
	env["CONTEXT"] = zmq.Context()
	resourceManagerConnectString = util.createConnectString("inproc", "tmp/inproc/resource_manager", "control_thread")
	resourceManagerPort = env["CONTEXT"].socket(zmq.DEALER)
	resourceManagerPort.bind (resourceManagerConnectString) 
	poler = zmq.Poller()
	try:
		resourceManagerThread  = resourceManager(env)
	except Resource_Manager_Init_Error as e:
		print (e.msg)

	resourceManagerThread.start()
		
	resourceManagerPort.send_pyobj ({"TYPE":RM_REQ_GET_HOST, "DRIVER_NAME": "mininet" })






if __name__ == '__main__':
	test()
