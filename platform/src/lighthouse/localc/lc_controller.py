#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import os, sys
import zmq
import threading

from appfabric.common.msgTypes import *
from appfabric.common.errors import *
from appfabric.common import util
from appfabric.common.ipStr import ipNum, ipStr
from appfabric.common.unbufferedStream import unbuffered

from . import hostStubThread
from .hostStub import hostStub
from .lc_resourceManager import resourceManager
from .lc_connectionDispatcher import lc_connectionDispatcher
#TODO: Add loggers

class lc_controller(threading.Thread):
	

	def __init__(self,_context):
		super(lc_controller, self).__init__()
		
		self.stopRequest = threading.Event()
		self.stopRequest.set()
		

		self.hostStubThreadList = []	

		self.context = _context
	
		self.rManagerPort = self.context.socket (zmq.DEALER)
		self.connectionDispatcherPort = self.context.socket (zmq.DEALER)
		
		self.rManager = None
		self.connectionDispatcher = None

		self.poller = zmq.Poller()
		
		# set unbuffered stream
		self.unbufferedWrite= unbuffered(sys.stdout)


	def print_status(self, msg):
		self.unbufferedWrite.write("\nDatacenter controller <%s:%s>:  %s"%(os.environ["ZONE_NAME"],os.environ["SITE_NAME"], msg ))

	def run(self):	
		try:
			self.rManagerPort.bind(util.createConnectString("inproc", "tmp/inproc/lighthouse",str(os.environ["HOST_NAME"]), "RM_CONTROLLER"))
			self.poller.register(self.rManagerPort, zmq.POLLIN)
		except zmq.ZMQError as e:
			#TODO log error
			self.unbufferedWrite.write ("LC Controller: Error initializing RM connection socket") 
			sys.exit()
		except Invalid_Connect_String_Error as e:
			#TODO log error
			self.unbufferedWrite.write ("LC:%s"%(e.msg))
			sys.exit()

		try:
			self.rManager = resourceManager(self.context)
		except  Resource_Manager_Init_Error as e:
			#TODO log the error
			self.unbufferedWrite.write ("Error starting resource manager ", e.msg())	
			raise Controller_Init_Error(e.msg)
		
		# start resource manager
		self.rManager.start()
		
		while self.stopRequest.isSet():
			
			try:
				socks= dict(self.poller.poll(1))
			except zmq.ZMQError as e:
				#This will throw an error when sockets are shut down
				pass

			if  socks.get(self.rManagerPort) == zmq.POLLIN:
				msg = self.rManagerPort.recv_pyobj()
				if msg["TYPE"] == RM_CONTROLLER_INIT:
					self.print_status("Resource Manager(RM) started...")
				#	self.unbufferedWrite.write("\nDataceneter Controller: Resource Manager(RM) started...will start Connection Dispatcher(CD) now")
					try:
						self.connectionDispatcher = lc_connectionDispatcher(self.context)
					except Connection_Dispatcher_Init_Error as e:
						#TODO log the error
						self.unbufferedWrite.write ("Error starting connection Dispatcher ", e.msg())
						raise Controller_Init_Error(e.msg)
					#start the local conection dispatcher thread 
					self.connectionDispatcher.start()



'''
if __name__ == '__main__':
		
	# for Mininet based experiments
#	sys.stdout = unbuffered(sys.stdout)
	
	os.environ["RESOURCE_DRIVER_CONFIG"] = os.path.expanduser("~/driver_mininet/resource_drivers.cfg")
	os.environ["DRIVERS_DIR"] = "~/driver_mininet/drivers"
	os.environ["SERVICE_CONFIG_PATH"] = os.path.expanduser("~/driver_mininet/services.cfg")
	
	context = zmq.Context()

	try: 
		controller = lc_controller(context)
		controller.start()
	except Controller_Init_Error as e:
	#	self.unbufferedWrite.write ("Error starting controller", e.msg()) 
		sys.exit()
	
	#TODO propagate kill signals to the threads and close them properly
'''
