#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import os, sys
import types
import time
from time import sleep
import threading
import zmq
import uuid

from appfabric.common import xmlToDict
from appfabric.common import processSpawn
from appfabric.common import util
from appfabric.common.errors import *
from appfabric.common.msgTypes import *
from appfabric.common.constants import *
from . import hostStub
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto import Random
from appfabric.common.unbufferedStream import unbuffered
from appfabric.common.aes_msg import AESCipher_Msg

class resourceManager(threading.Thread):
	
	def __init__ (self, _context):
		super (resourceManager, self).__init__()
		
		# set unbuffered stream
		self.unbufferedWrite= unbuffered(sys.stdout)

	#	self.unbufferedWrite.write ("RM: Initializing resource manager...")
		
		self.rmID = str(uuid.uuid4())
		self.availableResourceQuota = 0 
		self.context = _context
		
		self.stopRequest = threading.Event()
		self.stopRequest.set()


		#TODO when we implement a resource driver
		# read form a config file all the available resource providers
		# read a policy file to request the resources from a particular location
		# For now the resource manager will not implement a resource driver manager 
		#self.key= "LCNC"	
		#self.iv= Random.get_random_bytes(16)
		
		try:
			self.rm_cd_connectString = util.createConnectString("inproc", "tmp/inproc/lighthouse",str(os.environ["HOST_NAME"]), "RM_CD")
			self.rm_controller_connectString = util.createConnectString("inproc", "tmp/inproc/lighthouse",str(os.environ["HOST_NAME"]),"RM_CONTROLLER")
			#secret key with NC
			#text_file = open("SecretKeys/LC_GC.bin", "wb")
			#self.portCfg["AES_KEY"]= AES.new(key_to_be_used,AES.MODE_CBC,iv)
			#save secret key for future use
			#[text_file.write(x) for x in (self.iv,b"    ",self.key.encode())]
			#text_file.close()
		except Invalid_Connect_String_Error as e:
			#TODO log error
			self.unbufferedWrite.write ("RM: Error %s"%(e.msg))
			raise Resource_Manager_Init_Error (e.msg)
	
		self.connectionDispatcherPort = self.context.socket(zmq.DEALER)
		self.controlThreadPort = self.context.socket(zmq.DEALER)
		self.rmStubPort = self.context.socket(zmq.DEALER)
	
		self.poller = zmq.Poller()
		# TODO Add inventory policy file where the administrator can specify how many VMs to acquire
		# from each provider. Set of cloud providers	
		self.temp_inventory = []
		self.active_inventory = []
	#	self.unbufferedWrite.write ("RM:Initialized")

	def print_status(self, msg):
		self.unbufferedWrite.write("\nDatacenter controller <%s:%s>:  %s"%(os.environ["ZONE_NAME"],os.environ["SITE_NAME"],msg ))



	#TODO: write a more robust bootstrap
	# with timeout and repeat trials
	def bootstrap(self):
		''' set up connection with the global wf manager'''
		#TODO introduce a fallback connection for handling failures
		# TODO: As a first step atleast introduce a timeout to allow this to fail graciously without getting hung up
		try:
			rm_gcCD_connectString = util.createConnectString("tcp",os.environ["GC_ADDRESS"], os.environ["GC_PORT"])
		except Invalid_Path_String_Error as e:
			self.unbufferedWrite.write ("RM bootstrap Error: %s"%(e.msg))
			raise RM_BootStrap_Error(e.msg)
		try:
			rmStubPort_temp = self.context.socket(zmq.DEALER)
		except zmq.ZMQError as e:
			self.unbufferedWrite.write ("RM bootstrap Error: %s"%(e.__str__()))
			raise RM_BootStrap_Error(e.__str__())
		
		while True:
			try:
				# Comment added by DEVAL # 21/07/2014
				# This socket is temporarily used by gc_ConnectionDispatcher (GCD) and lc_ResourceManager (LRM)
				# To exchange initial messages on it. Note the command Connect and type "DEALER"
				# Later on, LRM disconnects with it and connects to the rmStubThread (RST)
				rmStubPort_temp.connect(rm_gcCD_connectString)
				break	
			except zmq.ZMQError as e:
				sleep(1)
				continue

		self.poller.register(rmStubPort_temp,zmq.POLLIN)
		sent_flag = False
		while True:
		
			socks= dict(self.poller.poll(1))
			if sent_flag == False:
				rmStubPort_temp.send_pyobj({
							"TYPE": RM_RMSTUB_INIT_SYN, 
							"RM_ID":self.rmID,
							"RM_ADDR":os.environ["IP_ADDR"],
							"SITE_TYPE": os.environ["SITE_TYPE"], 
							"ZONE_NAME": os.environ["ZONE_NAME"], 
							"SITE_NAME": os.environ["SITE_NAME"]
						})
				# wait for ack to get the new port number
				# add timer and resend
				sent_flag = True

			if socks.get(rmStubPort_temp)== zmq.POLLIN:
				msg = rmStubPort_temp.recv_pyobj()
				if msg["TYPE"] == RM_RMSTUB_INIT_SYN_ACK:
					portNum = msg["PORT"]
					#self.unbufferedWrite.write ("RM:%s"%(portNum))
					try:
						# Comment added by DEVAL # 21/07/2014
						# Here LRM disconnects from GCD and connects to a rmST on socket with portNum passed as a parameter
						rmStubPort_temp.disconnect(rm_gcCD_connectString)
						rm_rmStub_connectString =  util.createConnectString("tcp",os.environ["GC_ADDRESS"], str(portNum))
						while True:
							try:
								self.rmStubPort.connect(rm_rmStub_connectString)
								break
							except:
								sleep(1)
								continue	
				
						self.poller.register (self.rmStubPort, zmq.POLLIN)
						self.rmStubPort.send_pyobj({
										"TYPE":RM_RMSTUB_INIT_ACK,
										"AVAILABLE_RESOURCE": self.availableResourceQuota,
										"STATUS": SUCCESS
									})
						#self.unbufferedWrite.write ("RM:sent RM_RMSTUB_INIT_ACK to GC")
					except:
						#TODO log error
						raise RM_BootStrap_Error("Could not bootstrap resource manager, connection to wfm failed")
			
					break
			 

	def run(self):
	
		self.print_status("....Starting resource manager (RM) ")
		try:
			self.bootstrap()
		except RM_BootStrap_Error as e:
			#log error
			self.unbufferedWrite.write (e.msg)
			sys.exit()

		try:
			self.connectionDispatcherPort.bind(self.rm_cd_connectString)
			self.poller.register (self.connectionDispatcherPort,zmq.POLLIN)
		except zmq.ZMQError as e:
			self.unbufferedWrite.write ("RM: Error in opening connections: %s"%(e.__str__()))
			exit()
	
		while True:
			try:	
				self.controlThreadPort.connect(self.rm_controller_connectString)
				break
			except:
				sleep(1)
				continue
		try:
			self.poller.register (self.controlThreadPort, zmq.POLLIN)
		except zmq.ZMQError as e:
			#TODO 
			self.unbufferedWrite.write ("RM: Error in opening connections: %s"%(e.__str__()))
			exit()
		# send a RM init message to controller so that the controller can fire up the connection dispatcher
		self.controlThreadPort.send_pyobj({"TYPE": RM_CONTROLLER_INIT})

		# start listening on the sockets 
		inventory_index_to_be_removed = []
		temp_temp_inventory = []	
		start_count = 1
		temp_count = 1
		while self.stopRequest.isSet():
			# removes items from the temp_inventory and puts them in active inventory
			if len(inventory_index_to_be_removed) > 0:
			#	self.unbufferedWrite.write ("temp inventory = %s, %s"%(self.temp_inventory, inventory_index_to_be_removed))
				for i in range(len(inventory_index_to_be_removed)):
					index = inventory_index_to_be_removed[i]
					self.active_inventory.append(self.temp_inventory[index])
				self.temp_inventory = [i for j, i in enumerate(self.temp_inventory) if j not in inventory_index_to_be_removed]	
			#	self.unbufferedWrite.write ("active inventory = %s"%(self.active_inventory))
				inventory_index_to_be_removed = []
			
			
			if len (temp_temp_inventory) > 0:
				for i in range(len (temp_temp_inventory)):
					# Note: cannot pop here as it will change the size of the list inside the loop
					temp_temp_hostObj = temp_temp_inventory[i]
					self.temp_inventory.append(temp_temp_hostObj)
					self.connectionDispatcherPort.send_pyobj({
										"TYPE":RM_CD_ACK_HOSTSTUB, 
										"RESOURCE_ID":temp_temp_hostObj.get_attr("resource_ID")
										})
				temp_temp_inventory = []			 
		
			try:
				socks= dict(self.poller.poll(100))
			except zmq.ZMQError as e:
				pass
			if socks.get(self.controlThreadPort) == zmq.POLLIN:
				msg = {}
				msg = self.controlThreadPort.recv_pyobj()
				cmd = msg["TYPE"]
			#	if cmd == RM_REQ_GET_HOST:
					# check local inventory
					
					# if local inventory cannot provide resource
				#	self.unbufferedWrite.write ("RM_REQ_GET_HOST")	
			
			if socks.get(self.connectionDispatcherPort) == zmq.POLLIN:
				msg = {}
				try:
					msg = self.connectionDispatcherPort.recv_pyobj()
				except Exception as ex:
					self.unbufferedWrite.write ("RM: Error receiving message %s" %(type(ex).__name__))
					self.unbufferedWrite.write ("RM: Error: %s" %(ex.args))
			
				if msg["TYPE"] == CD_RM_INFO_HOSTSTUB:
					try:
						rm_hostStub_port = self.context.socket(zmq.DEALER)
						rm_hostStub_port.bind(util.createConnectString("inproc", "tmp/inproc/lighthouse",str(os.environ["HOST_NAME"]),("RM_"+msg["THREAD_NAME"])))
						self.poller.register(rm_hostStub_port, zmq.POLLIN)
					except zmq.ZMQError as e:
						#TODO handle this error
						# for now pass it
						self.unbufferedWrite.write ("RM: Error initializing  host stub connection : %s"%(e.__str()))
						pass
					
					try:
						hostStubObj = hostStub.hostStub()
						hostStubObj.set_hostInfo(msg["HOSTSTUB"])
						hostStubObj.set_attr("hostStub_to_rm_connection", rm_hostStub_port)
						#self.unbufferedWrite.write ("RM: <%s> Temporary host stub for : %s"%(temp_count, hostStubObj.get_attr("resource_extIP")))
						temp_count = temp_count + 1
						temp_temp_inventory.append(hostStubObj)
					except:
						#TODO handle this error
						 # for now pass it
						self.unbufferedWrite.write ("RM: Error initializing hostStub object")
						pass
	 	
			for i in range (len(self.temp_inventory)):
				hostStubObj = self.temp_inventory[i]
				msg = {}
				if socks.get(hostStubObj.get_attr("hostStub_to_rm_connection"))== zmq.POLLIN:
					msg = hostStubObj.get_attr("hostStub_to_rm_connection").recv_pyobj()
					if msg["TYPE"] == HOSTSTUB_RM_HOSTSTUB_INIT and msg["ID"] == hostStubObj.get_attr("resource_ID"):
					#	self.unbufferedWrite.write ("RM:<%s>  Host stub started for host: %s"%(start_count, hostStubObj.get_attr("resource_extIP")))
						self.update_resource_quota (msg["AVAILABLE_RESOURCE"],hostStubObj)
						hostStubObj.set_attr("resource_state",RES_LAUNCHED)
						inventory_index_to_be_removed.append(i)
					#	self.unbufferedWrite.write ("RM:inventory index to be removed: %s"%(i))

			for i in range (len(self.active_inventory)):
				hostStubObj = self.active_inventory[i]
				msg = {}
				if socks.get(hostStubObj.get_attr("hostStub_to_rm_connection"))== zmq.POLLIN:
					msg = hostStubObj.get_attr("hostStub_to_rm_connection").recv_pyobj()
					cmd = msg["TYPE"]
					if cmd == HOSTSTUB_RM_RESOURCE_REPORT:
						# handle resource report
				
						self.update_resource_quota (msg["AVAILABLE_RESOURCE"],hostStubObj)
					#	self.unbufferedWrite.write ("\nsending resource update %s"%(hostStubObj.get_attr("resource_extIP")))
						msg["AVAILABLE_RESOURCE"] = self.availableResourceQuota
						self.send_resource_update_to_rmStub()
					
					elif cmd in (BC_SERVICE_START_REP, BC_PROXY_START_REP):
						# update the resource quota
						self.update_resource_quota (msg["AVAILABLE_RESOURCE"],hostStubObj)
						msg["AVAILABLE_RESOURCE"] = self.availableResourceQuota
						# update the active service list in the host
						service = {
								"SERVICE_NAME": msg["SERVICE_NAME"],
								"SERVICE_ID":msg["SERVICE_ID"]
							}
						hostStubObj.serviceList_add_service(service)
						# Added by Tara		
						# The following code encrypt the message to be send to global controller 					
						msg["NC"]=os.environ["IP_ADDR"] #IP
						msg["SITE_TYPE"]= os.environ["SITE_TYPE"] #Site type
						msg["ZONE_NAME"]= os.environ["ZONE_NAME"] #Zone
						msg["SITE_NAME"]= os.environ["SITE_NAME"] #Site name 
						msg2= msg.pop("TYPE") # Type is not encrypted so that it can be recognized in the if statement 
						f = open(os.path.expanduser("~/AppFabric/experiments/physical_machines/SecretKeys/LC_GC.bin"),"rb") # Get the NC_LC secret key 							(This key is agreed on after the first msg and both parties has it)
						message= f.read()
						message=message.split(b"    ")
						key2= message[1] #key
						iv= message[0] #nonce		
						enc=AESCipher_Msg(key2.decode()).encrypt(msg,iv) # encrypt the whole message with a known secret key
						enc["TYPE"]= msg2 # return the message TYPE	
						#self.unbufferedWrite.write ("RM: Received BC_SERVICE_START_REP")
						#send it to the global controller
						#print(enc)
						self.rmStubPort.send_pyobj(enc) 					
					
					elif cmd in (
							BC_SERVICE_PORT_CONFIG_ADD_EGRESS_REP, 
							BC_SERVICE_PORT_CONFIG_ADD_INGRESS_REP, 
							BC_SERVICE_PORT_CONFIG_ADD_APP_ROUTER_REP,
							BC_SERVICE_PORT_CONFIG_ACTIVATE_WF_REP,
							BC_OVERLOAD_NOTIFICATION,
							BC_SUSPEND_WF_INSTANCE_NOTIFICATION,
							BC_RESUME_WF_INSTANCE_NOTIFICATION,
							BC_REPORT_LOAD_REP,
							BC_PROXY_WFINSTANCE_RELEASE_REP, 
							I_SC_SERVICE_ANOM_INTEGRITY,
							I_SC_SERVICE_ANOM_DEST,
							I_SC_SERVICE_ANOM_TRAFFIC,
							I_SC_SERVICE_ANOM_MAX_REQUEST,
							I_SC_SERVICE_ANOM_MAX_CLIENT_REQ
					):
						#Message are recieved from node controller, add to that local controller information and send to global controller 
						if cmd in (
								I_SC_SERVICE_ANOM_INTEGRITY,
								I_SC_SERVICE_ANOM_DEST,
								I_SC_SERVICE_ANOM_TRAFFIC,
								I_SC_SERVICE_ANOM_MAX_REQUEST,
								I_SC_SERVICE_ANOM_MAX_CLIENT_REQ
								): 
							msg["NC"]=os.environ["IP_ADDR"] #IP
							msg["SITE_TYPE"]= os.environ["SITE_TYPE"] #Site type
							msg["ZONE_NAME"]= os.environ["ZONE_NAME"] #Zone
							msg["SITE_NAME"]= os.environ["SITE_NAME"] #Site name 
							# ADDED BY TARA 
							# The following code encrypt the message to be send to the global controller 
							msg2= msg.pop("TYPE") # Type is not encrypted so that it can be recognized in the if statement 		
							f = open(os.path.expanduser("~/AppFabric/experiments/physical_machines/SecretKeys/LC_GC.bin"),"rb") # Get the NC_LC secret key 							(This key is agreed on after the first msg and both parties has it)
							message= f.read()
							message=message.split(b"    ")
							key2= message[1] #key
							iv= message[0] #nonce
							enc=AESCipher_Msg(key2.decode()).encrypt(msg,iv) # encrypt the whole message with a known secret key
							enc["TYPE"]= msg2 # return the message TYPE	
							#self.unbufferedWrite.write ("RM: Received BC_SERVICE_START_REP")
							#send it to the global controller
							msg=enc 

						self.rmStubPort.send_pyobj(msg)
							
			if socks.get(self.rmStubPort)  == zmq.POLLIN:
				msg = {}
				msg = self.rmStubPort.recv_pyobj()
				cmd = msg["TYPE"]
				if cmd == CB_SERVICE_START_REQ:
					# look for host with available resources
					#TODO: Allow adding more than algorithm 
					# for now we implement a simple greedy method
					# later context aware allocation may be allowed
					# each service carries the allocation of its neighbors and higher pref is given to 
					# putting all the neighbors in the same host
					#self.unbufferedWrite.write ("RM: Received CB_SERVICE_START_REQ message from RMSTUB, resource required = %s"%(msg["RESOURCE_REQ"]))
					#for hostStubObj in self.active_inventory:
					if len(self.active_inventory)> 0:
						count = 0
						while True:
							#self.unbufferedWrite.write ("RM: host resource = %s"%(hostStubObj.get_attr("resource_quota_available")))
							hostStubObj  = self.active_inventory.pop()
							if hostStubObj.get_attr("resource_quota_available") > msg["RESOURCE_REQ"]:
								# send to host stub thread
								#self.unbufferedWrite.write ("RM: Sending CB_SERVICE_START_REQ to hostStub")
								# remove resources from resource quote
								# resource will be updated later by the REP message
								new_resourceQuota = hostStubObj.get_attr("resource_quota_available") - msg["RESOURCE_REQ"]
								hostStubObj.set_attr("resource_quota_available", new_resourceQuota)	
								hostStubObj.get_attr("hostStub_to_rm_connection").send_pyobj(msg)
							#	hostStubObj.set_attr("proxyable_flag", False)
								self.active_inventory.insert(0,hostStubObj)
								break
						else:
							count +=1
							self.active_inventory.insert(0,hostStubObj)
						if count == len(self.active_inventory):
							break

				elif cmd == CB_PROXY_START_REQ:
					if len(self.active_inventory)> 0:	
						count = 0		
						while True:                    

							hostStubObj  = self.active_inventory.pop()
							if hostStubObj.get_attr("proxyable_flag") == True:
								if hostStubObj.get_attr("resource_quota_available") > msg["RESOURCE_REQ"]:
									new_resourceQuota = hostStubObj.get_attr("resource_quota_available") - msg["RESOURCE_REQ"]
									hostStubObj.set_attr("resource_quota_available", new_resourceQuota)
									hostStubObj.get_attr("hostStub_to_rm_connection").send_pyobj(msg)
									self.active_inventory.insert(0,hostStubObj)
									hostStubObj.set_attr("proxyable_flag", False)
									break
								else:
									count +=1
									self.active_inventory.insert(0,hostStubObj)
							if count == len(self.active_inventory):
								break
				# SOme message are coming from GC and passing to NC so just pass them 
				elif cmd in (
						CB_SERVICE_PORT_CONFIG_ADD_EGRESS_REQ, 
						CB_SERVICE_PORT_CONFIG_ADD_INGRESS_REQ,
						CB_SERVICE_PORT_CONFIG_ADD_APP_ROUTER_REQ,
						CB_SERVICE_PORT_CONFIG_ACTIVATE_WF_REQ,
						CB_STOP_OVERLOAD_NOTIFICATIONS,
						CB_REPORT_LOAD_REQ,
						CB_PROXY_WFINSTANCE_RELEASE_REQ,
						CB_RESUME_OVERLOAD_NOTIFICATIONS, 
						CB_RESUME_ANOM

					):
					
					for hostStubObj in self.active_inventory:
						if (cmd== CB_RESUME_ANOM): 
							f = open(os.path.expanduser("~/AppFabric/experiments/physical_machines/SecretKeys/LC_GC.bin"),"rb") # Get the NC_LC secret key 							(This key is agreed on after the first msg and both parties has it)
							message= f.read()
							message=message.split(b"    ")
							key2= message[1] #key
							iv= message[0] #nonce 
							msg2= msg.pop("TYPE") # Type is not encrypted so that it can be recognized in the if statement 
							try:
								msg= AESCipher_Msg(key2.decode()).decrypt(msg,iv) # Decrypt the whole message 
							except:
								pass 	
							#print(msg)
							msg["TYPE"]= msg2 # return the message TYPE
						if hostStubObj.get_attr("resource_ID") == msg["HOST_ID"]:
							hostStubObj.get_attr("hostStub_to_rm_connection").send_pyobj(msg)	
							break
				elif cmd == GEN_QUERY_PROXY_RESOURCE:
					msg["TYPE"] = GEN_REP_PROXY_RESOURCE
					msg["RM_ID"] = self.rmID 
					resource_found_flag = False
					for hostStubObj in self.active_inventory:
						if hostStubObj.get_attr("proxyable_flag") == True:
						#	self.unbufferedWrite.write ("resource quota:%s"%(hostStubObj.get_attr("resource_quota_available")))
							if hostStubObj.get_attr("resource_quota_available") > msg["RESOURCE_REQ"]:
								resource_found_flag = True
								break
					if resource_found_flag == True:
						msg["STATUS"] = SUCCESS
					else:
						msg["STATUS"] = FAIL
					#self.print_status ("Proxy res query:%s and resource requested:%s"%(msg, msg["RESOURCE_REQ"]))
					self.rmStubPort.send_pyobj(msg)			
		


	def update_resource_quota (self, _resourceQuota, _hostStubObj):
		prevQuota = _hostStubObj.get_attr("resource_quota_available") 
		_hostStubObj.set_attr("resource_quota_available",_resourceQuota)
		self.availableResourceQuota = self.availableResourceQuota + _resourceQuota - prevQuota
	#	self.unbufferedWrite.write ("RM:Available resource quota %s"%(self.availableResourceQuota))

	def send_resource_update_to_rmStub(self):
		msg = {
			"TYPE": RM_RMSTUB_RESOURCE_REPORT, 
			"ID":self.rmID, 
			"AVAILABLE_RESOURCE": self.availableResourceQuota
			}
		self.rmStubPort.send_pyobj(msg)








def test():
	self.unbufferedWrite.write ("running test")
	os.environ["RESOURCE_DRIVER_CONFIG"] = os.path.expanduser("~/driver_mininet/resource_drivers.cfg")
	os.environ["DRIVERS_DIR"] = "~/driver_mininet/drivers"
	env = {}
	env["CONTEXT"] = zmq.Context()
	resourceManagerConnectString = util.createConnectString("inproc", "tmp/inproc/resource_manager", "control_thread")
	resourceManagerPort = env["CONTEXT"].socket(zmq.DEALER)
	resourceManagerPort.bind (resourceManagerConnectString) 
	poler = zmq.Poller()
	try:
		resourceManagerThread  = resourceManager(env)
	except Resource_Manager_Init_Error as e:
		self.unbufferedWrite.write (e.msg)

	resourceManagerThread.start()
		
	resourceManagerPort.send_pyobj ({"TYPE":RM_REQ_GET_HOST, "DRIVER_NAME": "mininet" })






if __name__ == '__main__':
	test()
