#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#

import copy

from appfabric.common.errors import *
from appfabric.common.constants import *

class rmStub:

	def __init__(self):
		#self.command_interface = None
		self.rmInfo = {
			"rm_quota_available": 0,
			"rm_ID": None,
			"rm_addr" : None,
			"rm_port": None,
			"rm_siteName":None,
			"rm_zoneName": None,
			"rm_siteType": None,
			"rmStub_to_wfm_connection": None
		}
	
	def set_attr(self, _attrName, _attrVal):
		self.rmInfo[_attrName] = _attrVal

	def get_attr(self, _attrName):
		return self.rmInfo[_attrName]
	
	def get_rmInfo(self):
		return self.rmInfo

	def get_rmInfo_passable(self):
		# get all the rmInfo except the rmStub_to_wfm_connection
		# that cannot be passed in a message
		rmInfo_passable = {}
		for key in self.rmInfo:
			if key != "rmStub_to_wfm_connection":
				rmInfo_passable[key] = copy.deepcopy(self.rmInfo[key])
		return rmInfo_passable

	def set_rmInfo(self, _rmInfo):
		self.rmInfo = _rmInfo
'''
	def serviceList_lookup_service (self, **kwargs):
		for service in self.hostInfo["active_serviceList"]:
			if  service["SERVICE_NAME"] == kwargs["SERVICE_NAME"]:
				if service["SERVICE_ID"] == kwargs ["SERVICE_ID"]:
					return service
		
		raise Resource_Service_Not_Found_Error("Service not in active service list in resource " + self.hostInfo["resource_ID"] )

	def serviceList_get_complete_list(self):	
		return self.hostInfo["active_serviceList"]
	
	def serviceList_get_num_services (self):
		return len(self.hostInfo["active_serviceList"])

	def serviceList_add_service (self, _service):
		self.hostInfo["active_serviceList"].append(_service)
		
	def serviceList_remove_service(self, **kwargs):
		index = -1
		for i in range(len(self.hostInfo["active_serviceList"])):
			if self.hostInfo["active_serviceList"][i]["SERVICE_NAME"] == kwargs["SERVICE_NAME"]:
				if self.hostInfo["active_serviceList"][i]["SERVICE_ID"] == kwargs ["SERVICE_ID"]:
					index = i
					break
		if index == -1:
			raise Resource_Service_Not_Found_Error("Service not in active service list in resource " + self.hostInfo["resource_ID"])
		else:
			self.hostInfo["active_serviceList"].pop[index]	

'''
