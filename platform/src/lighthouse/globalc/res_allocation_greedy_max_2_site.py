#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#




def select_site (_siteList, _existing_deployment_scenario):
	(flag, selectedCoreSite, selectedEdgeSite) = check_wf_deployability(_siteList, _existing_deployment_scenario)
	return flag, selectedCoreSite, selectedEdgeSite




def check_wf_deployability (_siteList, _deployment_scenario):
	coreSites = []
	edgeSites = []
	# selection policy:check:
	# 1. If enough resources are available
	# 2. If new sites are available for the deployments (distribution)
	# 3. Within old sites, highest resource available (lowest stress)
	# 4. For new sites, highest resource available
	for site in _siteList:
		siteType = site["rm_siteType"]
		if siteType == "CORE":
			coreSites.append(site)
		if siteType == "EDGE":
			edgeSites.append (site)
		if siteType == "CORE/EDGE" or siteType == "EDGE/CORE":
			coreSites.append(site)
			edgeSites.append (site)
	if _deployment_scenario["WF_REQ_CORE_SITE"] == True and len(coreSites) == 0:
		return False,None,None
	elif _deployment_scenario["WF_REQ_CORE_SITE"] == False:
		core_deployability = True
		selected_coreSite = None
	else:
		#select a core site
		low_pref_site = None
		high_pref_site = None
		high_pref_res_level = 0
		low_pref_res_level = 0

		for site in coreSites:
			if site["rm_quota_available"] >= _deployment_scenario["WF_REQ_CORE_RES"]:
			# checking if there are already instances of the wf in these sites
				if site["rm_ID"] in _deployment_scenario["CORE_SITES"]:
					if site["rm_quota_available"] >= low_pref_res_level:
						low_pref_site = site
						low_pref_res_level = site["rm_quota_available"]
				else:
					if  site["rm_quota_available"] >= high_pref_res_level:
						high_pref_site = site
						high_pref_res_level = site["rm_quota_available"]
			else:
				return False, None, None

			if high_pref_site != None:
				core_deployability = True
				selected_coreSite = high_pref_site
			elif high_pref_site == None and low_pref_site != None:
				core_deployability = True
				selected_coreSite = low_pref_site
			elif high_pref_site == None and low_pref_site == None:
				core_deployability = True
				selected_coreSite = None
	if _deployment_scenario["WF_REQ_EDGE_SITE"] == True and len(edgeSites) == 0:
		return False,None,None
	
	elif _deployment_scenario["WF_REQ_EDGE_SITE"] == False:
		edge_deployability = True
		selected_edgeSite = None
	else:
		#select a edge site
		low_pref_site = None
		high_pref_site = None
		high_pref_res_level = 0
		low_pref_res_level = 0

		for site in edgeSites:
			if site["rm_quota_available"] >= _deployment_scenario["WF_REQ_EDGE_RES"]:
				if site["rm_ID"] in _deployment_scenario["EDGE_SITES"]:
					if site["rm_quota_available"] > low_pref_res_level:
						low_pref_site = site
						low_pref_res_level = site["rm_quota_available"]
				
				else:
					if  site["rm_quota_available"] >= high_pref_res_level:
							high_pref_site = site
							high_pref_res_level = site["rm_quota_available"]
			else:
				return False, None, None
		if high_pref_site != None:
			edge_deployability = True
			selected_edgeSite = high_pref_site
			
		elif high_pref_site == None and low_pref_site != None:	
			edge_deployability = True
			selected_edgeSite = low_pref_site
		
		elif high_pref_site == None and low_pref_site == None:
			edge_deployability = True
			selected_edgeSite = None

	if core_deployability == True and edge_deployability == True:
		if selected_coreSite == None and selected_edgeSite == None:
			raise Check_WF_Deployability_Error("Check the configuration of the workflow, seems like it needs no resources" )
		else:
			return True, selected_coreSite, selected_edgeSite		
		

