#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import os, sys
import types
import time
from time import sleep
import threading
import uuid
import zmq
import pprint

from appfabric.common import xmlToDict
from appfabric.common import processSpawn
from appfabric.common import util
from appfabric.common.errors import *
from appfabric.common.msgTypes import *
from appfabric.common.constants import *
from .rmStub import rmStub
from . import readWFConfig
from . import workflowThread
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto import Random

class gc_workflowManager(threading.Thread):
	
	def __init__ (self, _context):
		super (gc_workflowManager, self).__init__()
		print ("WFM: Initializing workflow manager")
		
		self.context = _context
		
		self.stopRequest = threading.Event()
		self.stopRequest.set()

		#TODO when we implement a workflow driver
		# read form a config file all the available workflow providers
		# read a policy file to request the workflows from a particular location
		# For now the workflow manager will not implement a workflow driver manager
		
		# LG: The global manager accepts the policies for creating the application workflows (AAW) and,
		# LG: the distributed application cloud (AAC) from the administrator through its northbound interface.
		
		# create workflow objects for each workflow in each zone

		wfList = readWFConfig.wfConfig().wfContainer
	
		# TODO: bad choice, change this to a dict
		self.wfThreadList = []

		connectionIndex = 1
		for wf in wfList:
			for zone in wf["wf_zoneList"]:
				wfID = str(uuid.uuid4())
				wfm_wft_connectString = util.createConnectString("inproc", "tmp/inproc/lighthouse/",os.environ["HOST_NAME"],"wfThreads",str (connectionIndex))
				try:
					wfThread = workflowThread.workflowThread(wf,wfID, zone, wfm_wft_connectString,self.context)
				except WorkFlow_Init_Error as e:
					# log error
					print ("WFM: %s"%(e.msg))
					sys.exit()
				wfThreadDesc = {
						"wfID": wfID,	
						"thread":wfThread,
						"zone": zone,
						"connectString":wfm_wft_connectString,
						"connection": None,
						}
				connectionIndex += 1
				self.wfThreadList.append(wfThreadDesc)
							
		try:
			self.wfm_cd_connectString = util.createConnectString("inproc", "tmp/inproc/lighthouse", str(os.environ["HOST_NAME"]),"WFM_CD")
			self.wfm_controller_connectString = util.createConnectString("inproc", "tmp/inproc/lighthouse",str(os.environ["HOST_NAME"]), "WFM_CONTROLLER")
		except Invalid_Connect_String_Error as e:
			print ("WFM: Error - %s"%(e.msg))
			raise Resource_Manager_Init_Error (e.msg)
	
		self.connectionDispatcherPort = self.context.socket(zmq.DEALER)
		self.controlThreadPort = self.context.socket(zmq.DEALER)
	
		self.poller = zmq.Poller()
		# TODO Add inventory policy file where the administrator can specify how many VMs to acquire
		# from each provider. Set of cloud providers	
		self.temp_inventory = []
		self.active_inventory = []
		#print ("WFM: Initialized workflow manager")

		# list to save the workflow threads that have queried for resources
		self.proxy_resource_request_sent_dict = {}
		
	# launch workflows
	def launch_wfThreads(self):
		for wfThreadDesc in self.wfThreadList:
			try:
				wfThreadSocket=  self.context.socket(zmq.DEALER)
				wfThreadSocket.bind (wfThreadDesc["connectString"])
				self.poller.register(wfThreadSocket, zmq.POLLIN)
				wfThreadDesc["connection"]  = wfThreadSocket
				wfThreadDesc["thread"].start()
			except zmq.ZMQError as e:
				print ("WFM: %s"%(e.__str__()))

	def run(self):
		print ("WFM: Starting workflow manager")
		try:
			self.connectionDispatcherPort.bind(self.wfm_cd_connectString)
			self.poller.register (self.connectionDispatcherPort,zmq.POLLIN)
		except zmq.ZMQError as e:
			print ("WFM: %s"%(e.__str__()))
			exit()

		while True:
			try:
				self.controlThreadPort.connect(self.wfm_controller_connectString)
				break
			except:
				sleep(1)
				continue
		try:
			self.poller.register (self.controlThreadPort, zmq.POLLIN)
		except zmq.ZMQError as e:
			#TODO 
			print ("WFM: %s"%(e.__str__()))
			exit()
		
		# launch workflow threads  	
		self.launch_wfThreads()

		# Send a WFM init message to controller so that the controller can fire up the connection dispatcher
		self.controlThreadPort.send_pyobj({"TYPE": WFM_CONTROLLER_INIT})

		# start listening on the sockets
		inventory_index_to_be_removed = []
		temp_temp_inventory = []
		while self.stopRequest.isSet():
			# removes items from the temp_inventory and puts them in active inventory
			if len(inventory_index_to_be_removed) > 0:
				for i in range(len(inventory_index_to_be_removed)):
					index = inventory_index_to_be_removed[i]
					self.active_inventory.append(self.temp_inventory[index])
				self.temp_inventory = [i for j, i in enumerate(self.temp_inventory) if j not in inventory_index_to_be_removed]		
				inventory_index_to_be_removed = []

			# merge the two lists
			if len(temp_temp_inventory) > 0:
				for i in range(len (temp_temp_inventory)):
					# Note: cannot pop here as it will change the size of the list inside the loop
					temp_temp_rmStubObj = temp_temp_inventory[i]
					self.temp_inventory.append(temp_temp_rmStubObj)
					self.connectionDispatcherPort.send_pyobj({
									"TYPE":WFM_CD_ACK_RMSTUB, 
									"RESOURCE_ID":temp_temp_rmStubObj.get_attr("rm_ID")
									})
				temp_temp_inventory = []


			try:
				socks= dict(self.poller.poll(1))
			except zmq.ZMQError as e:
				pass
			
			if socks.get(self.controlThreadPort) == zmq.POLLIN:
				msg = { }
				msg = self.controlThreadPort.recv_pyobj()
				cmd = msg["TYPE"]
					
			
			if socks.get(self.connectionDispatcherPort) == zmq.POLLIN:
				try:
					msg = self.connectionDispatcherPort.recv_pyobj()
				except Exception as ex:
					print ("WFM: Error receiving message - %s, %s"%(type(ex).__name__), ex.args)
					pass

				if msg["TYPE"] == CD_WFM_INFO_RMSTUB :
					try:
						wfm_rmStub_port = self.context.socket(zmq.DEALER)	
						wfm_rmStub_port.bind(util.createConnectString("inproc", "tmp/inproc/lighthouse",str(os.environ["HOST_NAME"]),("WFM_RM_"+msg["THREAD_NAME"])))
						self.poller.register(wfm_rmStub_port,zmq.POLLIN)
					except zmq.ZMQError as e:
						#TODO handle this error
						# for now pass it
						print ("WFM: Error - %s"%(e.__str__()))
					try:
						rmStubObj = rmStub()
						rmStubObj.set_rmInfo(msg["VRM"])
						rmStubObj.set_attr("rmStub_to_wfm_connection", wfm_rmStub_port)
				#		self.temp_inventory.append(rmStubObj)
						temp_temp_inventory.append(rmStubObj)	
					except:
						#TODO handle this error
						 # for now pass it
						print ("WFM: Error initializing RM stub object")
						pass
			
			for i in range (len(self.temp_inventory)):
				rmStubObj = self.temp_inventory[i]
				if socks.get(rmStubObj.get_attr("rmStub_to_wfm_connection"))== zmq.POLLIN:
					msg = rmStubObj.get_attr("rmStub_to_wfm_connection").recv_pyobj()
				#	print ("WFM: RMSTUB_WFM_INIT: %s"%(msg))
					if msg["TYPE"] == RMSTUB_WFM_INIT and msg["ID"] == rmStubObj.get_attr("rm_ID"):
						rmStubObj.set_attr("rm_quota_available", msg["AVAILABLE_RESOURCE"])
				#		print ("----------------------temp-to-active inventory: %s"%(rmStubObj))
						inventory_index_to_be_removed.append(i)

						

			for i in range (len(self.active_inventory)):
				rmStubObj = self.active_inventory[i]
				if socks.get(rmStubObj.get_attr("rmStub_to_wfm_connection"))== zmq.POLLIN:
					msg = rmStubObj.get_attr("rmStub_to_wfm_connection").recv_pyobj()
					cmd = msg["TYPE"]
					#print(cmd)
					if cmd == RMSTUB_WFM_RESOURCE_REPORT:
						rmStubObj.set_attr("rm_quota_available", msg["AVAILABLE_RESOURCE"]) 
						print ("WFM:<%s>: available resource = %s"%( rmStubObj.get_attr("rm_siteName"),rmStubObj.get_attr("rm_quota_available")))

					
					elif cmd in (BC_SERVICE_START_REP, BC_PROXY_START_REP):
						# update the available resource attribute		
						rmStubObj.set_attr("rm_quota_available", msg["AVAILABLE_RESOURCE"])
					#	print ("WFM return msg: %s"%(rmStubObj.get_attr("rm_quota_available")))
						# send it to the right wfThread
						# TODO: This looks bad. convert it to a hash table with wfID as the key
						
						for wfThreadDesc in self.wfThreadList:
							if msg["WF_ID"] == wfThreadDesc["wfID"]:
								wfThreadDesc["connection"].send_pyobj(msg)	
								break
					# Anom messages recieved from LC so just pass them to wfThread
					elif cmd in (
							BC_SERVICE_PORT_CONFIG_ADD_EGRESS_REP, 
							BC_SERVICE_PORT_CONFIG_ADD_INGRESS_REP, 
							BC_SERVICE_PORT_CONFIG_ADD_APP_ROUTER_REP,
							BC_SERVICE_PORT_CONFIG_ACTIVATE_WF_REP,
							BC_OVERLOAD_NOTIFICATION,
							BC_SUSPEND_WF_INSTANCE_NOTIFICATION,
							BC_RESUME_WF_INSTANCE_NOTIFICATION,
							BC_REPORT_LOAD_REP,
							BC_PROXY_WFINSTANCE_RELEASE_REP,	
							I_SC_SERVICE_ANOM_INTEGRITY,
							I_SC_SERVICE_ANOM_DEST,
							I_SC_SERVICE_ANOM_TRAFFIC,
							I_SC_SERVICE_ANOM_MAX_REQUEST,
							I_SC_SERVICE_ANOM_MAX_CLIENT_REQ
						):
						
						for wfThreadDesc in self.wfThreadList:
							if msg["WF_ID"] == wfThreadDesc["wfID"]:
								wfThreadDesc["connection"].send_pyobj(msg)
								break							

					# TODO: implement a timeout to cope with lost messages
					# Ignored for now since all control messages are exchanged over reliable transport
					elif cmd == GEN_REP_PROXY_RESOURCE:
						if msg["STATUS"] ==  SUCCESS:
							if (msg["WF_ID"] in self.proxy_resource_request_sent_dict):
								for wfThreadDesc in self.wfThreadList:
									if msg["WF_ID"] == wfThreadDesc["wfID"]:
										wfThreadDesc["connection"].send_pyobj(msg, zmq.NOBLOCK)
										del self.proxy_resource_request_sent_dict[msg["WF_ID"]]
		
						elif msg["STATUS"] == FAIL:
							if (msg["WF_ID"] in self.proxy_resource_request_sent_dict):
								self.proxy_resource_request_sent_dict[msg["WF_ID"]] -= 1
								if self.proxy_resource_request_sent_dict[msg["WF_ID"]] == 0:
									for wfThreadDesc in self.wfThreadList:
										if msg["WF_ID"] == wfThreadDesc["wfID"]:
											msg["REASON"] = OUT_OF_RESOURCE
											wfThreadDesc["connection"].send_pyobj(msg, zmq.NOBLOCK)
									del self.proxy_resource_request_sent_dict[msg["WF_ID"]]
					#elif cmd == I_SC_SERVICE_ANOMLOUS: 
						#print (" Global Controller recieved  anomolous signal") 
			for wfThreadDesc in self.wfThreadList:	
				if socks.get(wfThreadDesc["connection"]) == zmq.POLLIN:
					msg = wfThreadDesc["connection"].recv_pyobj()
					cmd = msg["TYPE"]
				
					if cmd == WFT_WFM_QUERY_ACTIVE_SITES:
						zone = msg["ZONE"]
						msg ["TYPE"] = WFT_WFM_REP_ACTIVE_SITES 
						msg["SITE_LIST"] =  self.query_active_sites(zone)
						wfThreadDesc["connection"].send_pyobj(msg)
					
					elif cmd == WFT_WFM_QUERY_EDGE_SITES:
						zone = msg["ZONE"]
						msg["TYPE"] = WFT_WFM_REP_EDGE_SITES 
						msg["SITE_LIST"]= self.query_edge_sites(zone)
						wfThreadDesc["connection"].send_pyobj(msg)

					elif cmd == WFT_WFM_QUERY_ACTIVE_SITE_QUOTA:
						print ("WFM: %s"%(WFT_WFM_QUERY_ACTIVE_SITE_QUOTA))

					elif cmd == GEN_QUERY_PROXY_RESOURCE:
						self.query_for_proxy_resource(msg, wfThreadDesc)
					
							
								
					elif cmd in (CB_SERVICE_START_REQ, CB_PROXY_START_REQ):
						for rm in self.active_inventory:
							if rm.get_attr("rm_ID") == msg["RM_ID"]:
								# preemptively subtract the resource that will be used up 
								#print("%s"%msg)
								newResourceQuota = rm.get_attr("rm_quota_available") - msg["RESOURCE_REQ"]
								rm.set_attr("rm_quota_available", newResourceQuota)
								rm.get_attr("rmStub_to_wfm_connection").send_pyobj(msg)
								break
						#	print ("WFM: Resource quota left: %s"%(rm.get_attr("rm_quota_available")))	
					#Responses from GC threats do pass them to resouce manager (To LC)
					elif cmd in (	CB_SERVICE_PORT_CONFIG_ADD_EGRESS_REQ, 
							CB_SERVICE_PORT_CONFIG_ADD_INGRESS_REQ, 
							CB_SERVICE_PORT_CONFIG_ADD_APP_ROUTER_REQ,
							CB_SERVICE_PORT_CONFIG_ACTIVATE_WF_REQ,
							CB_STOP_OVERLOAD_NOTIFICATIONS,
							CB_REPORT_LOAD_REQ,
							CB_PROXY_WFINSTANCE_RELEASE_REQ, 
							CB_RESUME_OVERLOAD_NOTIFICATIONS,
							CB_RESUME_ANOM
						):
					
						for rm in self.active_inventory:
							if rm.get_attr("rm_ID") == msg["RM_ID"]:
								rm.get_attr("rmStub_to_wfm_connection").send_pyobj(msg)
								break
					#elif cmd == I_SC_SERVICE_ANOMLOUS: 
					#	print (" Global Controller recieved  anomolous signal") 
						
						
						
	def query_for_proxy_resource(self, _msg, _wfThreadDesc):
		# TODO: Change the data structure from list o a dict
		# so that sites in a zone can be direclty accessed
		# 1. Check if a request is already pending - if so then drop it
		#      - do not worry about timing of the request, the workflow thread can always resend the request
		# 2. look for sites in the specific zone
		# 3. look if the site is an  edge site
		# 4. The site should have enough resource (Only then there is a possibility of finding a host)
		if _msg["WF_ID"] not in self.proxy_resource_request_sent_dict:
		#	self.proxy_resource_request_sent_list.append(_msg["WF_ID"])
			count = 0
			if len(self.active_inventory) > 0:
				for rm in self.active_inventory:
					if (
						rm.get_attr("rm_zoneName") == _msg["ZONE"] and
						rm.get_attr("rm_siteType") in ("EDGE", "CORE/EDGE","EDGE/CORE")and
						rm.get_attr("rm_quota_available") > _msg["RESOURCE_REQ"]
						):
						print ("sending proxy resource req to %s"%(rm.get_attr("rm_addr")))
						rm.get_attr("rmStub_to_wfm_connection").send_pyobj(_msg)
						count += 1
						#Tara 
						#Trust should go here as well 
				if count > 0:
					self.proxy_resource_request_sent_dict[_msg["WF_ID"]] = count
				else:
					_msg["TYPE"] = GEN_REP_PROXY_RESOURCE
					_msg["STATUS"] = FAIL
					_msg["REASON"] = SITES_UNINITIALIZED
					_wfThreadDesc["connection"].send_pyobj(_msg, zmq.NOBLOCK)
			else:
				_msg["TYPE"] = GEN_REP_PROXY_RESOURCE
				_msg["STATUS"] = FAIL
				_msg["REASON"] = SITES_UNINITIALIZED	
				_wfThreadDesc["connection"].send_pyobj(_msg, zmq.NOBLOCK)
		else:
			print ("Received PROXY_REQ, but Proxy Req is already pending")
	
	def query_edge_sites(self,_zone):
		siteList = []
		if len(self.active_inventory) > 0:
			for rm in self.active_inventory:
				siteType = rm.get_attr("rm_siteType")
				if rm.get_attr("rm_zoneName") == _zone and (siteType == "EDGE" or siteType == "CORE/EDGE" or siteType == "EDGE/CORE"):
					siteList.append(rm.get_rmInfo_passable())
		return (siteList)

	def query_active_sites(self,_zone):
		siteList = []
		if len(self.active_inventory) > 0:
			for rm in self.active_inventory:
				if rm.get_attr("rm_zoneName") == _zone:
					siteList.append(rm.get_rmInfo_passable())
	#	print ("WFM : %s"%(siteList))	
		return siteList









