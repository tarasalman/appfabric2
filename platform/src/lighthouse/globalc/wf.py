#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#

#import readConfig
import sys
import os
import re
import pprint
import uuid

from appfabric.common.errors import *
from appfabric.common import xmlToDict
from appfabric.common import classifier

class wfInstance:
	
	def __init__(self, _wfInstanceID):
		self.wfInstanceDesc = {
			"wfInstanceID": _wfInstanceID,
			"proxyNode": None,
			"coreSite": None,
			"edgeSite": None,
			"load" : 0,
			"wfServiceGraphInstance" : None
		}
	def get_attr(self, _attr):
		return self.wfInstanceDesc[_attr]

	def set_attr (self, _attr, _val):
		if _attr != "wfServiceGraphInstance": 
			self.wfInstanceDesc[_attr] = _val
		else:
			try:
				self.copy_serviceGraph (_val)
			except WF_Instance_Init_Error as e:
				raise WF_Instance_Init_Error (e.msg)

	def copy_serviceGraph(self, _serviceGraph):
		# for each service graph node create a new serviceInstance Node
		serviceInstanceGraph = []
		for serviceNodeObj in _serviceGraph:
			serviceID = str(uuid.uuid4())
			serviceInstanceNodeObj = wfServiceInstanceNode(serviceID)
			serviceInstanceNodeObj.set_attr("serviceNode", serviceNodeObj)
			# create a switch
			try:
				serviceInstanceNodeObj.set_attr("switch",serviceSwitch(serviceNodeObj))		
			except Service_Switch_Init_Error as e:
				#TODO log error and raise error
				raise WF_Instance_Init_Error (e.msg)
			serviceInstanceGraph.append(serviceInstanceNodeObj)
		self.wfInstanceDesc["wfServiceGraphInstance"] = serviceInstanceGraph
	#	for nodeObj in self.wfInstanceDesc["wfServiceGraphInstance"]:
	#		print (".................Service Name: %s, Service ID:%s"%(nodeObj.get_attr_serviceNode("serviceName")  ,nodeObj.get_attr("serviceID")))

	# Functions to manipulate Instance node
	# especially the switch

class serviceSwitch:
	def __init__(self, _serviceNode):
		self.ingressPorts = []
		self.egressPorts = []
		self.classifierTable = {}
	#	if _serviceNode.get_attr("numEgressPorts")> 1:
	#		try:
	#			classifier_t = classifier.classifier(_serviceNode.get_attr("classifier_type"))
	#			
	#			classifier_t.create_classifierTable(os.environ["CLASSIFIER_FILES_PATH"],_serviceNode.get_attr("classifier"))
			
				#verify that the port config in the service config and the classifier match
	#		except Classifier_Config_Error as e:
	#			#TODO log error and raise another error
	#			raise Service_Switch_Init_Error (e.msg)
	#		except KeyError as e:
	#			#TODO log error and raise another
	#			raise Service_Switch_Init_Error (e.msg)

#		else:
		try:	
			classifier_t = classifier.classifier(_serviceNode.get_attr("classifier_type"))
			classifier_t.create_classifierTable(os.environ["CLASSIFIER_FILES_PATH"],_serviceNode.get_attr("classifier"))
		except Classifier_Config_Error as e:
			#TODO log error and raise another error
			print (e.msg)
			raise Service_Switch_Init_Error (e.msg)
		except KeyError as e:
			#TODO log error and raise another
			print (e.__str__())
			raise Service_Switch_Init_Error (e.msg)
		except:
			print("Could not initialize classifier table") 
			raise Service_Switch_Init_Error ('Could not initialize classifier table')

		if classifier_t.verify_outport(_serviceNode.get_attr("numEgressPorts")) == True:
			try:
				self.classifierTable = classifier_t.get_classifierTable()
			except:
				print ("Error reading classifier table")
				raise Service_Switch_Init_Error ("Error reading classifier table")
		else: 
			print ("Outport config of service <%s> in workflow and classifier do not match"%(_serviceNode.get_attr("serviceName")))
			raise Service_Switch_Init_Error ("Outport config of service <%s> in workflow and classifier do not match"%(_serviceNode.get_attr("serviceName")))
	
		
		for ingressPort in _serviceNode.get_attr("ingressPorts"):
			phyIngressPort = phyPort(ingressPort)
			self.ingressPorts.append(phyIngressPort) 
	
		for egressPort in _serviceNode.get_attr("egressPorts"):
			phyEgressPort = phyPort(egressPort)
			self.egressPorts.append(phyEgressPort)	
	
	def get_ingressPorts(self):
		return self.ingressPorts
	
	def get_egressPorts (self):
		return self.egressPorts
	
	def get_classifierTable(self):
		return self.classifierTable

	#def set_ingressPorts(self, _ingressPort):
		
		



class wfServiceInstanceNode:
	
	def __init__(self, _serviceID):
		self.serviceInstanceNode = {
			"serviceNode": None,
			"serviceID": _serviceID,
			"rmID": None,
			"hostID":None,
			"hostPublicIP": None,
			"hostInternalIP": None,
			"switch": None
			}
	
	def get_attr(self, _attr):
		if _attr in self.serviceInstanceNode:
			return self.serviceInstanceNode[_attr]
		elif _attr in self.serviceInstanceNode["serviceNode"].get_keys():
			return self.serviceInstanceNode ["serviceNode"].get_attr(_attr)
		else:
			raise Attribute_Error ("%s is not an atribute of wfInstanceNode of wfServiceNode"%(_attr))
	
	def set_attr (self, _attr, _val):
		if _attr in self.serviceInstanceNode:
			self.serviceInstanceNode[_attr] = _val
		elif _attr in self.serviceInstanceNode ["serviceNode"].get_keys():
			self.serviceInstanceNode ["serviceNode"].set_attr(_attr, _val)
		else:
			raise Attribute_Error ("%s is not an atribute of wfInstanceNode of wfServiceNode"%(_attr))

	def set_attr_serviceNode(self, _attr, _val):
		serviceNode = self.get_attr("serviceNode") 
		serviceNode.set_attr(self, _attr, _val)
	
	def get_attr_serviceNode(self, _attr):
		serviceNode = self.get_attr("serviceNode")
		return serviceNode.get_attr(_attr)	

class wfProxyNode:
	def __init__(self, _serviceNodeObj):
		self.proxyNode = {
			"serviceNode": _serviceNodeObj,
			"serviceID": str(uuid.uuid4()),
			"rmID": None,
			"hostID":None,
			"hostPublicIP": None,
			"hostInternalIP": None,
			"max_load": 0,
			"wf_ports": {},
			"workflowList": [],
			}
	

		
	
	def get_empty_wf_port(self):
		for i in range (len(self.proxyNode["wf_ports"])):
			if self.proxyNode["wf_ports"][i] == None:
				return i
		return -1
			
	def set_wf_port(self, _wfPortNum, _wfInstanceID):
		self.proxyNode["wf_ports"][_wfPortNum] = _wfInstanceID

	def check_empty_wf_ports_left(self):
		for i in range (len(self.proxyNode["wf_ports"])):
			if self.proxyNode["wf_ports"][i] == None:
				return True
		return False

	# if a workflow is killed
	def add_back_wf_port(self, _portNum):
		self.proxyNode["wf_ports"][_portNum] = None


	def set_attr(self, _attr, _val):
		serviceNode = self.proxyNode["serviceNode"]
		if _attr == "workflowList":
			self.proxyNode["workflowList"].append(_val)
	
		elif _attr == "max_load":
			self.proxyNode[_attr] = _val
			for i in range(_val):
				self.proxyNode["wf_ports"][i] = None 

		elif _attr in ("serviceID", "hostID", "hostPublicIP", "hostInternalIP", "rmID"):
			self.proxyNode[_attr] = _val
		else:
			serviceNode.set_attr(_attr, _val)

	def get_attr(self, _attr):
		serviceNode = self.proxyNode["serviceNode"]
		if _attr in ("wf_ports","workflowList","max_load", "serviceID", "hostID", "hostPublicIP", "hostInternalIP", "rmID"):
			return self.proxyNode[_attr] 
	
		else:
			return serviceNode.get_attr(_attr)
	

class wfServiceNode:

	def __init__(self):
		self.serviceNode = {
			"nodeType": None,
			"serviceName": None,
			"serviceType":None,
			"serviceExecutable":None,
			"serviceArgs":None,
			"deploymentType":None,
			"deploymentSite": None,
			"normResIndex": None,
			"numIngressPorts": 0,
			"numEgressPorts": 0,
			"classifier": None,
			"classifier_type": "regex_serial", # default	
			"ingressPorts": [],
			"egressPorts": [],
			"wf_ref": None
		}
	def get_attr (self, _attr):
		return self.serviceNode [_attr]

	def set_attr(self, _attr, _val):
		if _attr == "ingressPorts" or _attr == "egressPorts":
			self.serviceNode[_attr].append(_val)
		else:
			self.serviceNode [_attr] = _val


	def get_keys(self):
		keySet = set()
		for key in self.serviceNode:
			keySet.add(key)
		
		return keySet

class virtPort:
	def __init__(self):
		self.portDesc = {
				"portNum": None,
				"neighbor_serviceName": None,
				"neighbor_ref": None,
			}

	def set_attr(self, _attr,_val):
		self.portDesc[_attr] = _val

	def get_attr(self, _attr):
		return self.portDesc[_attr]
	
	def get_portDesc(self):
		return self.portDesc
	
	def set_portDesc(self, _portDesc):
		self.portDesc = _portDesc

class phyPort:
	def __init__(self,_vPort):
		self.portDesc = {
			"virtPort": _vPort,
			"neighbor_serviceID": None,
			"neighbor_rmID": None,
			"neighbor_hostID": None,
			"neighbor_hostPublicIP": None,
			"neighbor_hostInternalIP": None,
			"neighbor_portNum": None,
			"transport_type": None
		}
		
	def set_attr(self, _attr,_val):
		if _attr in ("portNum", "neighbor_serviceName", "neighbor_ref"):
			self.portDesc["virtPort"].set_attr(_attr, _val)
		else:
			self.portDesc[_attr] = _val
	
	def get_attr(self,_attr):
		if _attr in ("portNum", "neighbor_serviceName", "neighbor_ref"):
			return self.portDesc["virtPort"].get_attr(_attr)
		else:
			return self.portDesc[_attr]

	def get_portDesc(self):
		return self.portDesc	

	def set_portDesc(self, _portDesc):
		self.portDesc = _portDesc

	'''
	def set_attr_vPort (self, _attr,_val):
		vPort = self.get_attr("virtPort")
		vPort.set_attr(_attr,_val)

	def get_attr_vPort(self, _attr):
		vPort = self.get_attr("virtPort")
		return vPort.get_attr(_attr)
	'''


'''		
class serviceSwitchEntry:

	def __init__(self):
		self.serviceSwitchingTableEntry = {
						"INGRESS_PORT": None,	
						"EGRESS_SERVICE": None,
						"ACTION": None
						}				
	def get_attr(self, _attr):
		return self.serviceSwitchingTableEntry[_attr]
	
	def set_attr (self, _attr, _val):
		self.serviceSwitchingTableEntry[_attr] = _val
'''
