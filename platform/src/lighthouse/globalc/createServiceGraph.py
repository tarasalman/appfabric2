#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import sys
import os
import ast
import re
import copy

from appfabric.common.errors import *
from . import wf

def get_service_node (_node, _serviceConfig):
	found_flag = False
	serviceNode = wf.wfServiceNode()
	for i in range(len(_serviceConfig)):
		if _node["service_name"] == _serviceConfig[i]['name']:
			serviceConfig = _serviceConfig[i]
			found_flag =True
			break
	if found_flag == True:
		serviceNode.set_attr("nodeType",_node["node_type"])
		serviceNode.set_attr("serviceName",_node["service_name"])
		serviceNode.set_attr("serviceType",int(serviceConfig['service_type']))
		serviceNode.set_attr("serviceExecutable",serviceConfig['executable'])
		if serviceConfig['service_args'] == None:
			serviceNode.set_attr ("serviceArgs","")
		else:
			serviceNode.set_attr("serviceArgs",serviceConfig['service_args'])
		serviceNode.set_attr ("deploymentSite",serviceConfig["attrs"]["deployment_site"])
		serviceNode.set_attr ("deploymentType",serviceConfig["attrs"]["deployment_type"])
	
		x = serviceConfig['norm_res_index']
		serviceNode.set_attr ("normResIndex",float(x) if '.' in x else int(x))

		serviceNode.set_attr("numIngressPorts",_node["in_ports"])
		serviceNode.set_attr("numEgressPorts",_node["out_ports"])
		try:
				classifier = _node["classifier"]
				serviceNode.set_attr("classifier",classifier)
					
				classifier_type = _node["classifier_type"]
			#	print ("classifier Type: %s"%(classifier_type))
				serviceNode.set_attr("classifier_type",classifier_type)
		
		except KeyError as e:
			raise Get_Service_Description_Error  ("Error: classifier not specified")

		# IN PORTS
		for i in range(_node["in_ports"]):
			in_port = wf.virtPort()
			in_port.set_attr ("portNum", i)
			serviceNode.set_attr("ingressPorts", in_port)
	 	
		for i in range(_node["out_ports"]):
			out_port = wf.virtPort()
			out_port.set_attr ("portNum", i)
			serviceNode.set_attr("egressPorts", out_port)
	else:
		raise Get_Service_Description_Error ("Service in workflow not configured %s " %(_node["service_name"]))

	return serviceNode

def create_serviceGraph(_wf_info,_serviceConfig, _wfDb):
	serviceGraph = []
	node_to_serviceName_mapping = {}
	for node in _wf_info["node_list"]:
		try:
			serviceNode = get_service_node (node, _serviceConfig)
			serviceGraph.append(serviceNode)
			node_to_serviceName_mapping[node["node_name"]] = serviceNode.get_attr("serviceName")
		except Get_Service_Description_Error as e:
			raise Create_Service_Graph_Error ("Error: %s" %(e.msg))		
		
	for line in _wf_info["wf_graph_desc"]:
		pattern_portNumbers = re.compile(r'[0-9]+')
		pattern_nodeName = re.compile (r'([0-9a-zA-Z\_\-]+)')
		two_links = []
		graph_link =  line.split("->")
		if len(graph_link) < 2:
			raise Create_Service_Graph_Error ("Error: Atleast 2: Usage node1[] -> []node2[] -> []node3 -> ...")
		if len(graph_link) >= 2:
			# break it up into 2-links
			j=0
			# TODO No checks in place here
			# validate the cfg
			for i in range(1,len(graph_link)):
				srcNodeDesc = re.findall(pattern_nodeName,graph_link[j])
				if len(srcNodeDesc) == 3:
					srcNodeName = srcNodeDesc[1]
					srcNodePort = srcNodeDesc[2]
				elif len(srcNodeDesc) == 2:
					if re.match(pattern_portNumbers, srcNodeDesc[0]) != None:
						srcNodeName = srcNodeDesc[1]
						srcNodePort = '0'
					else:
						srcNodeName = srcNodeDesc[0]
						srcNodePort = srcNodeDesc[1]

				elif len(srcNodeDesc) == 1:
					srcNodeName = srcNodeDesc[0]
					srcNodePort = '0'
				else :
					raise Create_Service_Graph_Error ("Error: verify graph config syntax")
				# verify that the ports are numbers
				if re.match(pattern_portNumbers, srcNodePort) == None:
					raise Create_Service_Graph_Error ("Error: verify graph config syntax")	
								
				dstNodeDesc = re.findall(pattern_nodeName,graph_link[i])
				if len(dstNodeDesc) == 3 :
					dstNodeName = dstNodeDesc[1]
					dstNodePort = dstNodeDesc[0]
							
				elif len(dstNodeDesc) == 2:
					if re.match(pattern_portNumbers, dstNodeDesc[0]) != None:
						dstNodeName = dstNodeDesc[1]
						dstNodePort = dstNodeDesc [0]
					else:
						dstNodeName = dstNodeDesc[0]
						dstNodePort = '0'

				elif len(dstNodeDesc) == 1:
					dstNodeName = dstNodeDesc[0]
					dstNodePort = '0'
							
				else:
					raise Create_Service_Graph_Error ("Error: verify graph config syntax")
				# map the node name to a service name
				
				# verify that the ports are numbers
				if re.match(pattern_portNumbers, dstNodePort) == None:
					raise Create_Service_Graph_Error ("Error: verify graph config syntax")
				# update service nodes with port information
				srcNodePort = int(srcNodePort)
				dstNodePort = int(dstNodePort)

				src_node_found_flag = False
				dst_node_found_flag = False
				for node in serviceGraph:	
					if node.get_attr("serviceName") == node_to_serviceName_mapping[srcNodeName.strip()]:
						srcNode = node
						src_node_found_flag = True
						if dst_node_found_flag == True:
							break
								
					elif node.get_attr("serviceName") == node_to_serviceName_mapping[dstNodeName.strip()]:
						dstNode = node
						dst_node_found_flag = True
						if src_node_found_flag  == True:
							break
				
				srcNode_egressPorts = srcNode.get_attr("egressPorts")
				if int(srcNodePort) > (len(srcNode_egressPorts)-1):
					raise Create_Service_Graph_Error ("Error:Verify graph config %s"%(srcNode.get_attr("serviceName")))
				if srcNode_egressPorts[srcNodePort].get_attr("neighbor_serviceName") != None:
					raise Create_Service_Graph_Error ("Error:Verify graph config %s"%(srcNode.get_attr("serviceName")))
				srcNode_egressPorts[srcNodePort].set_attr("neighbor_serviceName",dstNode.get_attr("serviceName"))
				srcNode_egressPorts[srcNodePort].set_attr("neighbor_ref", dstNode)

				dstNode_ingressPorts = dstNode.get_attr("ingressPorts")
				if dstNodePort > (len(dstNode_ingressPorts)-1):
					raise Create_Service_Graph_Error ("Error:Verify graph config %s"%(dstNode.get_attr("serviceName")))
				if dstNode_ingressPorts[dstNodePort].get_attr("neighbor_serviceName") != None: 
					raise Create_Service_Graph_Error ("Error:Verify graph config %s"%(dstNode.get_attr("serviceName")))
				dstNode_ingressPorts[dstNodePort].set_attr("neighbor_serviceName",srcNode.get_attr("serviceName"))
				dstNode_ingressPorts[dstNodePort].set_attr("neighbor_ref", srcNode)
				#print (srcNodeName, srcNodePort, "->", dstNodeName, dstNodePort)
				j += 1	
	try:
		verify_serviceGraph(serviceGraph)
	except Verify_Service_Graph_Error as e:
		raise Create_Service_Graph_Error (e.msg)		
	_wfDb["SERVICE_GRAPH"] = serviceGraph
	if serviceGraph[0].get_attr("nodeType")	== "PROXY":
		_wfDb["WF_PROXY_NODE"] = serviceGraph[0]
	else:
		raise Create_Service_Graph_Error ("Service Node 0 of the workflow should be a proxy service")	

def verify_serviceGraph(serviceGraph):
	for serviceNode in serviceGraph:
		for egressPort in serviceNode.get_attr("egressPorts"):
			if egressPort.get_attr("neighbor_serviceName") == None:
				raise Verify_Service_Graph_Error ("Incomplete Service Graph")
	
		for ingressPort in serviceNode.get_attr("ingressPorts"):
			if egressPort.get_attr ("neighbor_serviceName") == None:
				raise Verify_Service_Graph_Error ("Incomplete Service Graph")


def annotate_wf(_wfDb):
	EdgeSiteResourceRequired = 0
	CoreSiteResourceRequired = 0
	serviceGraph = _wfDb["SERVICE_GRAPH"]
	instance_capacity = _wfDb["WF_INSTANCE_CAPACITY"]
	
#	print ("instance_capacity: %s"%(instance_capacity))
	
	# annotate these paramters 
	for serviceNode in serviceGraph:
		if serviceNode.get_attr("nodeType") != "PROXY" :
		#	print ("%s: %s"%(serviceNode.get_attr("serviceName"), serviceNode.get_attr("classifier_type")))
			(core, edge) = compute_res_reqs(serviceNode, instance_capacity)
			CoreSiteResourceRequired += core
			EdgeSiteResourceRequired += edge
	if EdgeSiteResourceRequired > 0:
		_wfDb["WF_REQ_EDGE_SITE"] = True
		_wfDb["WF_REQ_EDGE_RES"] = EdgeSiteResourceRequired
	if CoreSiteResourceRequired > 0:
		_wfDb["WF_REQ_CORE_SITE"] = True
		_wfDb["WF_REQ_CORE_RES"] = CoreSiteResourceRequired	
	
	

def compute_res_reqs (_service, _instance_capacity):
	if _service.get_attr("deploymentSite") == "CORE":
		return _service.get_attr("normResIndex")* _service.get_attr("numIngressPorts") *  _instance_capacity, 0
	elif _service.get_attr("deploymentSite") == "EDGE":
		return 0, _service.get_attr("normResIndex")* _service.get_attr("numIngressPorts") *  _instance_capacity	



