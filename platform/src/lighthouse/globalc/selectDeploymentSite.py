#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


from . import res_allocation_greedy_max_2_site



def selectDeploymentSite(_siteList, _deployment_scenario):
	if _deployment_scenario["WF_RESOURCE_ALLOCATION_METHOD"]  == "greedy_max_2_site":
		flag, selected_coreSite, selected_edgeSite = res_allocation_greedy_max_2_site.select_site (_siteList, _deployment_scenario)
		return flag, selected_coreSite, selected_edgeSite 
	
