#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


#import readConfig
import sys
import os
import re
import copy
from datetime import datetime
import pprint
import zmq
from time import sleep
import uuid
import threading

from appfabric.common.errors import *
from appfabric.common.constants import *
from appfabric.common.msgTypes import *
from appfabric.common import xmlToDict
from appfabric.common import util
from appfabric.common.unbufferedStream import unbuffered

from . import wf
from . import createServiceGraph


class workflowInstanceThread(threading.Thread):
	
	def __init__(self, _wfServiceGraph, _wfProxyNode_blueprint, _wfInstanceID, _wfInstanceCapacity,_wfAvgLoadPerSession,  _wf_overload_notification_level, _zone, _wf_name, _wfID,  _wft_wfInstance_connectString,  _wfInstance_parentID,  _context):
		super(workflowInstanceThread, self).__init__()
		
		# set unbuffered stream
		self.unbufferedWrite= unbuffered(sys.stdout)
		
		self.stopRequest = threading.Event()
		self.stopRequest.set()
		
		#connection to workflow manager
		self.context = _context
		self.wfm_wfInstance_connectString = _wft_wfInstance_connectString
		self.wfm_wfInstance_port = self.context.socket(zmq.DEALER)


		self.poller = zmq.Poller()

		self.wfName = _wf_name
		self.wfID = _wfID
		self.wfInstanceID = _wfInstanceID	
		self.wfServiceGraph = _wfServiceGraph
		self.zone = _zone
		self.wf_overload_notification_level = _wf_overload_notification_level
		self.wfProxyNode_blueprint = _wfProxyNode_blueprint
		self.wfInstanceCapacity = _wfInstanceCapacity
		self.wfAvgLoadPerSession = _wfAvgLoadPerSession
		self.notification_level = self.wf_overload_notification_level * self.wfInstanceCapacity
		self.wf_status = FREE
		self.wfInstance_parentID = _wfInstance_parentID

		self.timeout = 500 # millisecs; arbitrary could be used as a tuning parameter later to factor in delay

		# the workflow Instance object 
		self.wfInstance = None
		self.proxy_node_wf_port_num = None
			
		# map ID to register workflow instance with the nameserver
		self.map_ID = str(uuid.uuid4())		

	def print_status(self, msg):
		self.unbufferedWrite.write("\nWFT <%s, %s, %s>:  %s"%(self.wfName,self.zone, self.wfInstanceID ,msg ))
		# LG: use print_status instead of print in this module
	def run(self):
		self.print_status ("started")
		while True:	
			try:
				self.wfm_wfInstance_port.connect(self.wfm_wfInstance_connectString)
				break
			except:
				sleep(1)
				continue
		try:
			self.poller.register(self.wfm_wfInstance_port, zmq.POLLIN)
		except zmq.ZMQError as e:
			#TODO Log error
			self.print_status ("Error:%s" %(e.__str__()))
			exit()
	
		
		# Create a new workflow instance object, blocking call, retruns only after all the attributes of wfInstance 
		# has been filled
	
		self.wfInstance_create()
		
		# start listening for failure  events from the services
		# Note: Overload is handled by the workflow service by launching a new wfInstance
		
		while self.stopRequest.isSet():
			try:
				socks= dict(self.poller.poll(1))
			except  zmq.ZMQError as e :
				pass

			if socks.get(self.wfm_wfInstance_port) == zmq.POLLIN:
				msg = self.wfm_wfInstance_port.recv_pyobj()
				#print(msg) 
				if msg["TYPE"] == BC_SERVICE_FAIL:
					self.print_status ("service_failed")

				if msg["TYPE"] == BC_SUSPEND_WF_INSTANCE_NOTIFICATION:
					self.print_status ("\n\n\n\n\n\nsuspending  workflow ===========\n\n\n\n\n")
					self.suspend_wf_instance()
			
				if msg["TYPE"] ==  BC_RESUME_WF_INSTANCE_NOTIFICATION:
					self.print_status ("Resuming Workflow ===========")
					self.resume_wf_instance()

				if msg["TYPE"] == WFT_WFINSTANCE_STOP_WORKFLOW_INSTANCE_REQ:
					self.print_status ("Stopping workflow ===========")
					self.stop_wf_instance()	
				#if msg["TYPE"] == I_SC_SERVICE_ANOMLOUS: 
				#	self.print_status(" Global Controller recieved  anomolous signal")		

				if msg["TYPE"] == WFT_WFINSTANCE_RELEASE_PROXY_REP:
					# Ask the parent to send overload notification
					# send this message to the wfthread, that forwards it to the wfInstanceThreead and that sends it to
					#  the correct workflow
					self.print_status("\n\n\n\n Released proxy for wfInstance \n\n\n\n")
					resume_overload_notifications_msg = {
						"TYPE": WFINSTANCE_WFT_RESUME_OVERLOAD_NOTIFICATIONS_FOR_PARENT,
						"WF_INSTANCE_ID": self.wfInstance_parentID,
						"WF_ID": self.wfID
						}
					self.wfm_wfInstance_port.send_pyobj(resume_overload_notifications_msg)	
					self.print_status("\n\n\n\n  WFINSTANCE_WFT_RESUME_OVERLOAD_NOTIFICATIONS_FOR_PARENT  sent \n\n\n\n")
					# delete the rest of the services
				if msg["TYPE"] == WFT_WFINSTANCE_RESUME_OVERLOAD_NOTIFICATIONS:
					msg["TYPE"] = CB_RESUME_OVERLOAD_NOTIFICATIONS	
					msg["HOST_ID"] = self.wfInstance.get_attr("proxyNode").get_attr("hostID")
					msg["RM_ID"] = self.wfInstance.get_attr("proxyNode").get_attr("rmID")
					msg["SERVICE_ID"] = self.wfInstance.get_attr("proxyNode").get_attr("serviceID")
					msg["SERVICE_NAME"] = self.wfInstance.get_attr("proxyNode").get_attr("serviceName")
					msg["WF_PORT_NUM"] = self.proxy_node_wf_port_num
					self.wfm_wfInstance_port.send_pyobj(msg)
					self.print_status("\n\n\n\n CB_RESUME_OVERLOAD_NOTIFICATIONS sent \n\n\n\n")
				# ADDED BY TARA 
				# The response to the anomolous message start here, other codes should just forward it 				
				if msg["TYPE"] in (
						I_SC_SERVICE_ANOM_INTEGRITY,
						I_SC_SERVICE_ANOM_DEST,
						I_SC_SERVICE_ANOM_TRAFFIC,
						I_SC_SERVICE_ANOM_MAX_REQUEST,
						I_SC_SERVICE_ANOM_MAX_CLIENT_REQ
				):
					#self.print_status(" GC Recieved anomolous signal") 
					msg["TYPE"] = CB_RESUME_ANOM	
					msg["HOST_ID"] = self.wfInstance.get_attr("proxyNode").get_attr("hostID")
					msg["RM_ID"] = self.wfInstance.get_attr("proxyNode").get_attr("rmID")
					msg["SERVICE_ID"] = self.wfInstance.get_attr("proxyNode").get_attr("serviceID")
					msg["SERVICE_NAME"] = self.wfInstance.get_attr("proxyNode").get_attr("serviceName")
					msg["WF_PORT_NUM"] = self.proxy_node_wf_port_num
					self.wfm_wfInstance_port.send_pyobj(msg)
	# Notify the name server 
	def suspend_wf_instance(self):
		msg = {
			"TYPE": NAME_TO_APPLICATION_MAPPING_UPDATE_REQ,
			"COMMAND": "SUSPEND_MAPPING",
			"ZONE": self.zone, # for future use - per zone nameservers
			"MAP_ID": self.map_ID,
			"APP_NAME":self.wfName,
			"WF_INSTANCE_ID":self.wfInstanceID,
			"HOST_ID": self.wfInstance.get_attr("proxyNode").get_attr("hostID"),
                        }
		self.wfm_wfInstance_port.send_pyobj(msg)	

	# notify the name server
	def resume_wf_instance(self):
		msg = {
			"TYPE": NAME_TO_APPLICATION_MAPPING_UPDATE_REQ,
			"COMMAND": "RESUME_MAPPING",
			"ZONE": self.zone, # for future use - per zone nameservers
			"MAP_ID": self.map_ID,
			"APP_NAME":self.wfName,
			"WF_INSTANCE_ID":self.wfInstanceID,
			"HOST_ID": self.wfInstance.get_attr("proxyNode").get_attr("hostID"),
			}
		self.wfm_wfInstance_port.send_pyobj(msg)	
		
	def stop_wf_instance(self):
		# Remove entry from fakeNameServer
		msg = {
			"TYPE": NAME_TO_APPLICATION_MAPPING_UPDATE_REQ,
			"COMMAND": "DELETE_MAPPING",
			"ZONE": self.zone,
			"MAP_ID": self.map_ID,
			"APP_NAME":self.wfName,
			"WF_INSTANCE_ID":self.wfInstanceID,
			"HOST_ID": self.wfInstance.get_attr("proxyNode").get_attr("hostID"),
			}		
		self.wfm_wfInstance_port.send_pyobj(msg)
		
		# Release the wfPort in the proxy
		proxyNode = self.wfInstance.get_attr("proxyNode")
		msg = {	
			"TYPE": WFINSTANCE_WFT_RELEASE_PROXY_REQ,
			"WF_INSTANCE_ID":self.wfInstanceID,
			"RM_ID":proxyNode.get_attr("rmID"),
			"HOST_ID": proxyNode.get_attr("hostID"),
			"WF_PORT_NUM": self.proxy_node_wf_port_num,
			"SERVICE_ID":proxyNode.get_attr("serviceID"),
			"SERVICE_NAME": proxyNode.get_attr("serviceName")		
			}
		self.wfm_wfInstance_port.send_pyobj(msg)
	
			


	def wfInstance_create(self):
		# Initializing wfInstance: wfInstanceID attr
		self.wfInstance = wf.wfInstance(self.wfInstanceID)
		
		# setting up the serviceGraphInstance
		try:
			self.wfInstance.set_attr("wfServiceGraphInstance",self.wfServiceGraph)
		except WF_Instance_Init_Error as e:
			#TODO Log the error before exiting and terminate this thread
			self.print_status (e.msg)
			exit()

		# set up the proxyNode
		self.wfInstance_get_proxy()

		# get the core and edge sites
		self.wfInstance_get_resources()

		# start services
		self.wfInstance_start_services()

		# start links
		self.wfInstance_start_links()
		
		# release lock at wfThread
		self.wfm_wfInstance_port.send_pyobj({
						"TYPE": WFINSTANCE_WFT_WORKFLOW_INSTANCE_INITIALIZED_INFO, 
						"WF_INSTANCE_ID": self.wfInstanceID })

		# setup the app routing table
		self.configure_appRoutingTables()
		
		# Activate the proxy port
		# This is equivalent to activating the workflow
		self.activate_proxyPort()
		
		# Add wfInstance to mapping table
		self.register_wfInstance_with_fakeNameServer() 

	def wfInstance_get_resources(self):
		# block for a response	
		msg = {
			"TYPE": WFINSTANCE_WFT_GET_RESOURCES_REQ,
			"WF_INSTANCE_ID": self.wfInstanceID,
			}

		sleep_time = 1
		self.print_status ("Requesting resources")
		while True:
			self.wfm_wfInstance_port.send_pyobj(msg)
			
		
			socks= dict(self.poller.poll(sleep_time))
		
			if socks.get(self.wfm_wfInstance_port) == zmq.POLLIN:
				msg = self.wfm_wfInstance_port.recv_pyobj()
				cmd = msg["TYPE"]
				if cmd == WFINSTANCE_WFT_GET_RESOURCES_REP:
					if msg["STATUS"] == SUCCESS :
						# found sites 
							
						self.wfInstance.set_attr("coreSite", msg["CORE_SITE"])
						self.wfInstance.set_attr("edgeSite", msg["EDGE_SITE"])
						break
					else:
						# sleep longer if you know your request is not being served
						# else keep polling 
						self.print_status("WFINSTANCE_WFT_GET_RESOURCES_REP failed")
						
						if sleep_time < 32:
							sleep_time = sleep_time * 2
						else:
							sleep_time = 1
						



	def wfInstance_get_proxy(self) :
		# blocking function
		msg = {
			"TYPE": WFINSTANCE_WFT_GET_PROXY_REQ,
			"WF_INSTANCE_ID": self.wfInstanceID,
			}
		count = 0
		sleep_time = 1
		while True:
			self.wfm_wfInstance_port.send_pyobj(msg)
			self.print_status("Requesting proxy Node")
				
			socks= dict(self.poller.poll(500))
			if socks.get(self.wfm_wfInstance_port) == zmq.POLLIN:
				rep_msg = self.wfm_wfInstance_port.recv_pyobj()
				cmd = rep_msg["TYPE"]
				# simple scheme: Whoever reports first is chosen
				if cmd == WFINSTANCE_WFT_GET_PROXY_REP:
					if rep_msg["STATUS"] == SUCCESS:
						proxyNode = wf.wfServiceInstanceNode(rep_msg["PROXY_NODE_DESC"]["SERVICE_ID"])
						proxyNode.set_attr("serviceNode",self.wfProxyNode_blueprint )
						proxyNode.set_attr("rmID", rep_msg["PROXY_NODE_DESC"]["RM_ID"])
						proxyNode.set_attr("hostID", rep_msg["PROXY_NODE_DESC"]["HOST_ID"])
						proxyNode.set_attr("hostPublicIP", rep_msg["PROXY_NODE_DESC"]["HOST_EXT_IP"])
						proxyNode.set_attr("hostInternalIP", rep_msg["PROXY_NODE_DESC"]["HOST_INT_IP"])
						self.proxy_node_wf_port_num = rep_msg["PROXY_NODE_DESC"]["WF_PORT_NUM"]
						self.wfInstance.set_attr("proxyNode",proxyNode)
						self.print_status ("proxynode initialized, get resources for the rest")
						break
			sleep(sleep_time)
			if sleep_time > 30:
				sleep_time = 1
			else:
				sleep_time = sleep_time * 2



	
	def wfInstance_start_services(self):
	
		self.print_status("Starting services")
		# send service start messages in parallel to the hosts
			
		service_init_pending_list = {}
		proxyStubNode = None
		for serviceInstanceNode in  self.wfInstance.get_attr("wfServiceGraphInstance"):
			if serviceInstanceNode.get_attr_serviceNode ("nodeType") != "PROXY":
				serviceID = serviceInstanceNode.get_attr("serviceID")
				serviceName = serviceInstanceNode.get_attr_serviceNode("serviceName")
				serviceType = serviceInstanceNode.get_attr_serviceNode("serviceType")
				wfInstanceID = self.wfInstance.get_attr("wfInstanceID")
				if serviceInstanceNode.get_attr_serviceNode("deploymentSite") == "CORE":
					site_rmID = self.wfInstance.get_attr("coreSite")["rm_ID"]
					serviceInstanceNode.set_attr("rmID", site_rmID)
				else:
					site_rmID = self.wfInstance.get_attr("edgeSite")["rm_ID"]
					serviceInstanceNode.set_attr("rmID", site_rmID)
				msg= {
					"MSG_ID":str(uuid.uuid4()),
					"TYPE":CB_SERVICE_START_REQ,
					"WF_ID": self.wfID, 
					"WF_INSTANCE_ID": wfInstanceID,
					"SERVICE_NAME":serviceName, 
					"SERVICE_ID":serviceID,
					"SERVICE_TYPE": serviceType, 
					"ARGS": serviceInstanceNode.get_attr_serviceNode("serviceArgs"), 
					"EXECUTABLE":serviceInstanceNode.get_attr_serviceNode("serviceExecutable"),
					"RM_ID": site_rmID,
					"RESOURCE_REQ":(self.wfInstanceCapacity * self.wfAvgLoadPerSession * serviceInstanceNode.get_attr_serviceNode("normResIndex")* serviceInstanceNode.get_attr_serviceNode("numIngressPorts"))	
				}
				service_init_pending_list[msg["MSG_ID"]] = datetime.now()
			
				# send msg and wait for reply
			#	self.print_status ("Service <%s><%s> CB_SERVICE_START_REQ sent to workflowThread"%(serviceName,serviceID))
				self.wfm_wfInstance_port.send_pyobj(msg)
			else:
				proxyStubNode = serviceInstanceNode
				proxyNode = self.wfInstance.get_attr("proxyNode")
				# fill in the serviceInstanceNode details for this proxyStubObj
				proxyStubNode.set_attr("hostID",proxyNode.get_attr("hostID"))
				proxyStubNode.set_attr("hostPublicIP",proxyNode.get_attr("hostPublicIP"))
				proxyStubNode.set_attr("hostInternalIP",proxyNode.get_attr("hostInternalIP"))
				proxyStubNode.set_attr("rmID", proxyNode.get_attr("rmID"))

				# The proxy stub steals the identity of the proxy node.
				# This is because although they are two different objects in the control plane,
				# they need to point ot the same object in the data plane
				proxyStubNode.set_attr("serviceID", proxyNode.get_attr("serviceID"))
				 


		# how long to wait
		# TODO: what happens if the ack gets lost?
		# maintain a sent and received list at the resource manager
		# the control port at the host should see duplicate CB_SERVICE_START messages and send a message informing the state	
		service_init_initialized_list = []
		try:
			# wait time arbitrarily fixed at 1500 (for each service)
			self.initialize_list_timed_wait(1500 ,service_init_pending_list, service_init_initialized_list, BC_SERVICE_START_REP)
			for rep_msg in service_init_initialized_list:
				for serviceInstanceNode in  self.wfInstance.get_attr("wfServiceGraphInstance"):
					if rep_msg["SERVICE_ID"] == serviceInstanceNode.get_attr("serviceID"):
						serviceInstanceNode.set_attr("hostID", rep_msg["HOST_ID"])
						serviceInstanceNode.set_attr("hostPublicIP",rep_msg["HOST_EXT_IP"])
						serviceInstanceNode.set_attr("hostInternalIP", rep_msg["HOST_INT_IP"])
						self.print_status ("Service <%s> <%s> started on host:%s"%(rep_msg["SERVICE_NAME"], rep_msg["SERVICE_ID"], rep_msg["HOST_EXT_IP"]))
						break
			self.print_status("WF attached to proxy node <%s> on <%s>"%(proxyStubNode.get_attr("serviceName"), proxyStubNode.get_attr("hostPublicIP")))
	
		except WF_Instance_Start_Services_Failed as e:
			raise WF_Instance_Start_Services_Failed(e.msg)
		
		except Attribute_Error as e:
			self.print_status (e.msg)
			pass	



		# fill the neighbr information for each port in each service instance node in the workflow
		self.fill_phyPortDetails()
	# LG: configure layer 4.5 and 3.5 tunnels across the ports	
	def wfInstance_start_links(self):
		self.print_status("setting up links")

		link_setup_init_list = []
		egress_link_setup_pending_list = {}
		egress_link_setup_initialized_list = []
		
		ingress_link_setup_pending_list = {}
		ingress_link_setup_initialized_list = []

		# get a free wf port in the proxy
	#	_wfInstance.get_attr("proxyNode").set_wf_port(wf_portNum, serviceSwitch)
		# send egress link setup messages
		for serviceInstanceNode in  self.wfInstance.get_attr("wfServiceGraphInstance"):
			# setup the egress port
			serviceSwitch = serviceInstanceNode.get_attr("switch")	
			#	if serviceInstanceNode.get_attr_serviceNode ("nodeType") == "PROXY":
			# get a free wf port in the proxy
		#	wf_portNum = _wfInstance.get_attr("proxyNode").get_empty_wf_port()
			# Link the service switch of the proxy stub to the actual proxy
			#	self.print_status("Hi 1.1")
			#	self.wfInstance.get_attr("proxyNode").set_wf_port(wf_portNum, serviceSwitch)
				#else:
			#	wf_portNum = None
			#	self.print_status("Hi 2")

		
			for egressPort in serviceSwitch.get_egressPorts():
				# <Service ID, port Num are the primary keys> to match it to a reply
				msg ={}
				msg= {
					# To allow multiple messages to be sent parallely
					"MSG_ID":str(uuid.uuid4()),
	
					# multiplexing across different type of messages
					"TYPE": CB_SERVICE_PORT_CONFIG_ADD_EGRESS_REQ,

					# multiplexing among workflows and instances
					"WF_ID":self.wfID,
					"WF_INSTANCE_ID":self.wfInstance.get_attr("wfInstanceID"),
						
					# multiplexing among sites and host
					"RM_ID": serviceInstanceNode.get_attr("rmID"),
					"HOST_ID":serviceInstanceNode.get_attr("hostID"),	
					
					# multiplexing across services in the host
					"SERVICE_NAME": serviceInstanceNode.get_attr_serviceNode("serviceName"),
					"SERVICE_ID": serviceInstanceNode.get_attr("serviceID"),
						
					# multiplexing across ports of a service, WF_PORT_NUM is None for all ports except proxy	
					"WF_PORT_NUM": self.proxy_node_wf_port_num,
					"PORT_NUM": egressPort.get_attr("portNum"),
					"TRANSPORT": egressPort.get_attr("transport_type")
						
					}

				# push this in a pending list
				link_setup_init_list.append(msg)
		
		# send out the egress port config messages to all the services
		for msg in link_setup_init_list:
			egress_link_setup_pending_list[msg["MSG_ID"]] =  datetime.now()
			self.wfm_wfInstance_port.send_pyobj(msg)
		#	print ("WFT: egress port setup msg sent %s   ------- %s"%(msg["SERVICE_NAME"], msg["PORT_NUM"]))
		
		try:
			self.initialize_list_timed_wait(1500, egress_link_setup_pending_list, egress_link_setup_initialized_list,BC_SERVICE_PORT_CONFIG_ADD_EGRESS_REP )
		except WF_Instance_Start_Services_Failed as e:
			raise WF_Instance_Start_Services_Failed (e.msg)		
		
		link_setup_init_list = []
		for rep_msg in egress_link_setup_initialized_list:
			# Send the ingress link setup mesages to the neighbors, the rep message already has a connect string field
			# get rid of the status field
			del rep_msg["STATUS"]
			# update the host ID and the RM ID fields to the neighbors
			
			for serviceInstanceNode in  self.wfInstance.get_attr("wfServiceGraphInstance"):
				if rep_msg["SERVICE_ID"] == serviceInstanceNode.get_attr("serviceID"):
					service_switch = serviceInstanceNode.get_attr("switch")
					for egressPort in service_switch.get_egressPorts():
						if egressPort.get_attr("portNum") == rep_msg["PORT_NUM"]:
							rep_msg["TYPE"] = CB_SERVICE_PORT_CONFIG_ADD_INGRESS_REQ
							rep_msg["RM_ID"] = egressPort.get_attr("neighbor_rmID")
							rep_msg["HOST_ID"] = egressPort.get_attr("neighbor_hostID")
							rep_msg["PORT_NUM"] = egressPort.get_attr("neighbor_portNum")
							rep_msg["SERVICE_NAME"] = egressPort.get_attr("neighbor_serviceName")
							rep_msg["SERVICE_ID"] = egressPort.get_attr("neighbor_serviceID")
							break
					break
			link_setup_init_list.append(rep_msg)
						
					# the egress service node is known
		for msg in link_setup_init_list:
			ingress_link_setup_pending_list[msg["MSG_ID"]] = datetime.now()
			self.wfm_wfInstance_port.send_pyobj(msg)
		#	print ("WFT: ingress port setup msg sent %s"%(msg["SERVICE_NAME"]))
		
		try: 
			self.initialize_list_timed_wait(1500, ingress_link_setup_pending_list, ingress_link_setup_initialized_list,BC_SERVICE_PORT_CONFIG_ADD_INGRESS_REP )
			# update the host ID and the RM ID fields to the neighbors
		except WF_Instance_Start_Services_Failed as e:
			raise WF_Instance_Start_Services_Failed (e.msg)
		for rep_msg in ingress_link_setup_initialized_list:	
		#	print ("Ingress port initialized:[%s]%s -> " %(rep_msg["PORT_NUM"], rep_msg["SERVICE_NAME"])) 
			for serviceInstanceNode in self.wfInstance.get_attr("wfServiceGraphInstance"):
				if rep_msg["SERVICE_ID"] == serviceInstanceNode.get_attr("serviceID"):
					service_switch = serviceInstanceNode.get_attr("switch")
					for ingressPort in service_switch.get_ingressPorts():
						if ingressPort.get_attr("portNum") == rep_msg["PORT_NUM"]:
							self.print_status ("%s[%s] --> [%s]%s"%( ingressPort.get_attr("neighbor_serviceName"), ingressPort.get_attr("neighbor_portNum"), rep_msg["PORT_NUM"],  rep_msg["SERVICE_NAME"] ))
							break
					break  
	# LG: Configure application level routing in the sPorts and packet level routing in the tPorts
	# LG: application level router consists of a message classifier and forwarding rule,
	# LG: packet level router has packet classifier and meta-tag

	def configure_appRoutingTables(self):
		appRoutingTable_setup_init_list = []
		appRoutingTable_setup_pending_list = {}
		appRoutingTable_setup_initialized_list = []

		for serviceInstanceNode in self.wfInstance.get_attr("wfServiceGraphInstance"):
			service_switch = serviceInstanceNode.get_attr("switch")
			msg= {
				 # To allow multiple messages to be sent parallely
				"MSG_ID":str(uuid.uuid4()),

				# multiplexing across different type of messages
				"TYPE": CB_SERVICE_PORT_CONFIG_ADD_APP_ROUTER_REQ,

				# multiplexing among workflows and instances
				"WF_ID":self.wfID,
				"WF_INSTANCE_ID":self.wfInstance.get_attr("wfInstanceID"),

				# multiplexing among sites and host
				"RM_ID": serviceInstanceNode.get_attr("rmID"),                                        
				"HOST_ID":serviceInstanceNode.get_attr("hostID"),
				
				# multiplexing across services in the host
				"SERVICE_NAME": serviceInstanceNode.get_attr_serviceNode("serviceName"),
				"SERVICE_ID": serviceInstanceNode.get_attr("serviceID"),
				"WF_PORT_NUM": self.proxy_node_wf_port_num,
				
				# configuration        
				"APP_ROUTING_TABLE": service_switch.get_classifierTable()
				}
		#	self.print_status (msg["APP_ROUTING_TABLE"])
			appRoutingTable_setup_init_list.append(msg)
 
		for msg in appRoutingTable_setup_init_list:
			appRoutingTable_setup_pending_list[msg["MSG_ID"]] = datetime.now()
			try:
				self.wfm_wfInstance_port.send_pyobj(msg)
			#	self.print_status ("sent CB_SERVICE_PORT_CONFIG_ADD_APP_ROUTER_REQ")
			except:
				#TODO: error handling
				self.print_status ("Could not send CB_SERVICE_PORT_CONFIG_ADD_APP_ROUTER_REQ")
		try:
			self.initialize_list_timed_wait(
						1500, 
						appRoutingTable_setup_pending_list, 
						appRoutingTable_setup_initialized_list,
						BC_SERVICE_PORT_CONFIG_ADD_APP_ROUTER_REP
						 )
			
		except WF_Instance_Start_Services_Failed as e:
			raise WF_Instance_Start_Services_Failed (e.msg)

		for rep_msg in appRoutingTable_setup_initialized_list:
			self.print_status("App. Routing Table intialized in <%s> <%s>" %(rep_msg["SERVICE_NAME"], rep_msg["SERVICE_ID"]))	
	
	# LG: workflow instance is connected to a pPort
	def activate_proxyPort(self):
		proxyNode = self.wfInstance.get_attr("proxyNode")
		msg= {
			"MSG_ID":str(uuid.uuid4()),
			"TYPE": CB_SERVICE_PORT_CONFIG_ACTIVATE_WF_REQ,
			"WF_ID":self.wfID,
			"WF_INSTANCE_ID":self.wfInstance.get_attr("wfInstanceID"),
			
			"RM_ID": proxyNode.get_attr("rmID"),
			"HOST_ID":proxyNode.get_attr("hostID"),
			"SERVICE_NAME": proxyNode.get_attr("serviceName"),
			"SERVICE_ID": proxyNode.get_attr("serviceID"),
                        "WF_PORT_NUM": self.proxy_node_wf_port_num,
			"WF_PORT_LOAD_CAPACITY":self.wfInstanceCapacity ,
			"WF_PORT_LOAD_NOTIFICATION_LEVEL":self.notification_level         
			}
		try:
			self.wfm_wfInstance_port.send_pyobj(msg)
			self.print_status ("Sent activate WF message to proxy port"),
		except:	
			#TODO: Error handling
			self.print_status("Could not send activate WF message to proxy port")

		while True:
			rep_msg =  self.wfm_wfInstance_port.recv_pyobj()
			if rep_msg["TYPE"] == BC_SERVICE_PORT_CONFIG_ACTIVATE_WF_REP :
				if rep_msg["STATUS"] == SUCCESS:
					self.wf_status = ACTIVE
					self.print_status ("Workflow <%s><%s> activated ......"%(self.wfID, self.wfInstance.get_attr("wfInstanceID")))
					#Trust with node controller here
					break

				else:
					#TODO Log Error and exit
					reason = rep_msg["REASON"]
					self.print_status ("Reason : %s"%(reason))
					sys.exit()
				
	def initialize_list_timed_wait(self, _timeout,_pending_list, _initialized_list, _msg_type):
		# timed wait on elements in the pending list and return a suspect list when done
		# how long to wait
		# TODO: what happens if the ack gets lost?
		# maintain a sent and received list at the resource manager
		# the control port at the host should see duplicate CB_SERVICE_START messages and send a message informing the state    
		suspect_list = []
		while True:
			temp_key_list = []
			if len(_pending_list) > 0:
				for key,val in _pending_list.items():
					time_elapsed = datetime.now() - val
					if (time_elapsed.microseconds/1000) > _timeout:  # we keep the timeout at 10
						temp_key_list.append(key)
						suspect_list.append(key)
				if len(temp_key_list)> 0:
					for key in temp_key_list:
						del _pending_list[key]
					if len(_pending_list) == 0:
						break
			else:
				break
			socks= dict(self.poller.poll(100))
			if socks.get(self.wfm_wfInstance_port) == zmq.POLLIN:
				rep_msg = self.wfm_wfInstance_port.recv_pyobj()
				#self.print_status (rep_msg)
				cmd = rep_msg["TYPE"]
				if (cmd == _msg_type and rep_msg["STATUS"] == SUCCESS):
						_initialized_list.append(rep_msg) 
						del _pending_list[rep_msg["MSG_ID"]]
                                        # host Info: <host id, host IP> in the message to be put by the hostStubObj
				elif  (cmd == _msg_type and rep_msg["STATUS"] == FAIL):
					suspect_list.append(rep_msg["MSG_ID"])
		# Out of the loop now, check i there were any service that were not started
		if len(suspect_list)> 0:
			self.print_status("suspect list is not empty")
			raise WF_Instance_Start_Services_Failed (suspect_list)

	
	# LG: workflow instance is registered with the name server to make it globally accessible
	# LG: the name server implements weighted round robin schemer
	def register_wfInstance_with_fakeNameServer(self):
		msg = {
			"TYPE": NAME_TO_APPLICATION_MAPPING_UPDATE_REQ,
			"COMMAND": "ADD_MAPPING",
			"ZONE": self.zone, # for future use - per zone nameservers
			"MAP_ID": self.map_ID,
			"APP_NAME":self.wfName,
			"MAPPING": {	
					"WF_INSTANCE_ID":self.wfInstanceID,
					"HOST_ID": self.wfInstance.get_attr("proxyNode").get_attr("hostID"),
					"IP_ADDR": self.wfInstance.get_attr("proxyNode").get_attr("hostPublicIP"),
					"PORT":8080, 
					}
			}
		
		if self.wf_status == ACTIVE:		
			self.wfm_wfInstance_port.send_pyobj(msg)
		else:
			#TODO Log error 
			self.print_status ("Cannot insert entry into the fakeNameServer since WF instance is not yet active")



	def fill_phyPortDetails (self):
		for serviceInstanceNode in  self.wfInstance.get_attr("wfServiceGraphInstance"):
			serviceSwitch = serviceInstanceNode.get_attr("switch")
			for egressPort in serviceSwitch.get_egressPorts():
				neighbor_serviceName = egressPort.get_attr("neighbor_serviceName")
				# look for neighbor
				for neighbor_serviceInstanceNode in self.wfInstance.get_attr("wfServiceGraphInstance"):
					if neighbor_serviceInstanceNode.get_attr_serviceNode("serviceName") == neighbor_serviceName:
						egressPort.set_attr("neighbor_hostID",neighbor_serviceInstanceNode.get_attr("hostID"))
						egressPort.set_attr("neighbor_hostPublicIP", neighbor_serviceInstanceNode.get_attr("hostPublicIP"))
						egressPort.set_attr("neighbor_hostInternalIP",neighbor_serviceInstanceNode.get_attr("hostInternalIP"))
						# fill the phy Port for the ingress port as well
						for ingressPort in neighbor_serviceInstanceNode.get_attr("switch").get_ingressPorts():
							if ingressPort.get_attr("neighbor_serviceName") == serviceInstanceNode.get_attr_serviceNode("serviceName"):
								ingressPort.set_attr("neighbor_hostID",serviceInstanceNode.get_attr("hostID"))
								ingressPort.set_attr("neighbor_hostPublicIP",serviceInstanceNode.get_attr("hostPublicIP"))
								ingressPort.set_attr("neighbor_hostInternalIP",serviceInstanceNode.get_attr("hostInternalIP"))
									
								# set the Port Number  of the egrees port of the source node 
								# and Port Number of the ingress port of the dest node
	
								egressPort.set_attr("neighbor_portNum",ingressPort.get_attr("portNum") )
								ingressPort.set_attr("neighbor_portNum",egressPort.get_attr("portNum") )
									
								# set the rm ID of the neighbor	
								egressPort.set_attr("neighbor_rmID",neighbor_serviceInstanceNode.get_attr("rmID") )
								ingressPort.set_attr("neighbor_rmID",serviceInstanceNode.get_attr("rmID") )						
								# neighbor service name and service ID
								egressPort.set_attr("neighbor_serviceID",neighbor_serviceInstanceNode.get_attr("serviceID") )
								ingressPort.set_attr("neighbor_serviceID",serviceInstanceNode.get_attr("serviceID") )

								# determine if transport is "inproc" or "tcp"
								if serviceInstanceNode.get_attr("hostID") == neighbor_serviceInstanceNode.get_attr("hostID"):
									ingressPort.set_attr("transport_type","INPROC")
									egressPort.set_attr("transport_type","INPROC")
								else:
									ingressPort.set_attr("transport_type","TCP")
									egressPort.set_attr("transport_type", "TCP")	
								break
						break






