#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import pprint
from  appfabric.common import xmlToDict

class sites:
	
	def __init__(self):
		self.sitesConfigDict = xmlToDict.ConvertXmlToDict("/home/openflow/driver_mininet/sites.cfg")

		self.GRDTable = []
		self.populate_GRDTable()
		print (self.GRDTable)

	def populate_siteDesc(self, _zoneInfo, _zoneDesc):	
		if isinstance(_zoneInfo["site"],dict):
			# single site
			siteList = []
			siteList.append(_zoneInfo["site"])
		
		else:
			#multiple sites
			siteList = _zoneInfo["site"]
	
		for site in siteList:
			siteDesc = {}
			siteDesc["site_name"] = site["name"]
			siteDesc["site_addr"] = site["site_addr"]
			if site["site_type"] == "CORE":
				_zoneDesc["core_sites"].append(siteDesc)
			elif site["site_type"] == "EDGE":
				_zoneDesc["edge_sites"].append(siteDesc)
			elif site["site_type"] == "CORE/EDGE" or site["site_type"] == "EDGE/CORE":
				_zoneDesc["core_sites"].append(siteDesc)
				_zoneDesc["edge_sites"].append(siteDesc)
			

	def populate_GRDTable(self):
		zoneInfo =self.sitesConfigDict["site_config"]["zone"]
 
		if isinstance(zoneInfo,dict):

			zoneDesc= {
				"zone_name":None,
				"edge_sites": [],
				"core_sites": []
				}


			zoneDesc["zone_name"] = zoneInfo["name"]
			self.populate_siteDesc (zoneInfo, zoneDesc)
			self.GRDTable.append(zoneDesc)

		else: # multiple zones
			for zone in zoneInfo:
				zoneDesc = {
					"zone_name":None,
					"edge_sites": [],
					"core_sites": []
					}
				zoneDesc["zone_name"] = zone["name"]
				self.populate_siteDesc (zone, zoneDesc)
				self.GRDTable.append(zoneDesc)
		





if __name__ == '__main__':
	resourceDesc = sites() 

