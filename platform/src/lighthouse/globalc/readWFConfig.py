#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import sys
import os
import ast
import re
from appfabric.common.errors import *

class wfConfig: 
	
	def __init__(self):
		self.wfContainer = []
		self.readCfg()	
	


	def readCfg(self):
		try:
			filepath = os.path.expanduser(os.environ["WORKFLOW_CONFIG_FILE"])
			filehandle = open(filepath, "r")
		except:
			#TODO : return a error
			print ("Read configuration error: check filename and path")
		# read the whole configuration in memory
		# assuming configuration files are relatively small
		workflowList  = []	
		workflow_desc_list_temp = []	
		record_wf_desc_flag = False
		for line in filehandle:
			line = line.strip()
			if line.startswith("#"):
				# ignore comment
				continue
			
			if line.startswith("<workflow>"):
				if record_wf_desc_flag == True:
					print ("Error: Need to close a previous workflow first")	
					exit()	
				record_wf_desc_flag = True
				record_wf_desc = []


			if line.startswith ("</workflow>") or line.endswith ("</workflow>"):
				if record_wf_desc_flag == False:
					print ("Error: Need to open a workflow first")
					exit()
				workflow_desc_list_temp.append(record_wf_desc)	
				record_wf_desc_flag = False
				

			if record_wf_desc_flag == True:
				record_wf_desc.append(line)

		for wf_desc in workflow_desc_list_temp:
			self.wfContainer.append(self.create_wf_obj(wf_desc))


	def create_wf_obj(self, _wf_desc):
		wfInfo = {
			"wf_name":None,
			"wf_instance_capacity": 0,
			"wf_zoneList": [],
			"wf_resource_allocation_method": None,
			"wf_avg_load_per_session":5,  # default
			"wf_overload_notification_level": 0,
			"wf_scale_down_level": 0.2, #default =  20 percent avg load"
			"wf_graph_desc": None,
			"wf_per_proxy": 0,
			"node_list":[]	
			}

		record_graph_desc_flag = False
		for line in _wf_desc:
			line = line.strip()
			if line.startswith ("<node-cfg>"): 
				if line.endswith ("</node-cfg>"):
					try:
						line = re.sub('^<node-cfg>', '', line)
						line = re.sub('</node-cfg>$', '', line)
						line = line.strip()
						nodeDesc = ast.literal_eval(line)
						
						wfInfo["node_list"].append(nodeDesc)

					#	self.config.append(ast.literal_eval( str.strip(line.split(">")[1])) )
					except:
						pass
				else: 
					print ("Error: Usage <node-cfg> node configuration </node-cfg>")
	
				
			if line.startswith ("<workflow-properties>"):	
				if line.endswith ("</workflow-properties>"):
					line = re.sub('^<workflow-properties>', '', line)
					line = re.sub('</workflow-properties>$', '', line)
					line= line.strip()
					workflowProperties = ast.literal_eval(line)
					wfInfo["wf_name"] = workflowProperties["name"]
					wfInfo["wf_instance_capacity"] = workflowProperties ["instance_capacity"]	
					wfInfo["wf_per_proxy"] = workflowProperties ["wf_per_proxy"]
					wfInfo["wf_resource_allocation_method"] = workflowProperties ["resource_allocation_method"]
					wfInfo["wf_avg_load_per_session"] = workflowProperties ["avg_load_per_session"]
					wfInfo["wf_overload_notification_level"] = workflowProperties["overload_notification_level"]
					wfInfo["wf_scale_down_level"] = workflowProperties["scale_down_level"]
					for zone in workflowProperties["deployment_sites"]:
						wfInfo["wf_zoneList"].append(zone)	
				else:
					print ("Error: Usage <workflow-properties> workflow properties </workflow-properties>")			


			if line.startswith("<graph>"):
				if record_graph_desc_flag == True:
					print ("Error: Syntax error: Usage <graph> ..... </graph>")
					exit()
				record_graph_desc_flag = True
				record_graph_desc = []
	
			if line.startswith("</graph>") or line.endswith("</graph>"):
				if record_graph_desc_flag == False:
					print ("Error: Syntax error: Usage <graph> ..... </graph>")
					exit()
				record_graph_desc_flag = False
				
			if record_graph_desc_flag == True:
				if not ((line.startswith("<graph>")) or  (line.endswith("<graph>"))) and len(line) > 0:
					record_graph_desc.append(line)

		wfInfo["wf_graph_desc"] = record_graph_desc
		return wfInfo
		#print (wfInfo)
		# self.get_graph_links( record_graph_desc.append)

