#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import os, sys
import copy
import zmq
import threading
from time import sleep

from appfabric.common.msgTypes import *
from appfabric.common.errors import *
from appfabric.common import util
from appfabric.common.unbufferedStream import unbuffered
from appfabric.common.ipStr import ipNum, ipStr

from . import rmStubThread
from .rmStub import rmStub
#TODO: Add loggers

class gc_connectionDispatcher(threading.Thread):
	

	def __init__(self,_context):
		super(gc_connectionDispatcher, self).__init__()

		# set unbuffered stream
		self.unbufferedWrite= unbuffered(sys.stdout)


		self.unbufferedWrite.write ("\nGlobal CD: Intializing GC connection dispatcher")	
		self.stopRequest = threading.Event()
		self.stopRequest.set()

		
		self.context = _context
		self.rmControlPort = self.context.socket (zmq.ROUTER)
		self.wfManagerPort = self.context.socket (zmq.DEALER)
		self.poller = zmq.Poller()
		
		self.rmStub_pending_list = []
		self.unbufferedWrite.write ("\nGlobal CD: initialized")
			
			
	def run(self):
		self.unbufferedWrite.write ("\nGlobal CD: Starting gc connection dispatcher")	
		try:
			# Comment added by DEVAL # 21/07/2014
			# This socket is temporarily used by gc_ConnectionDispatcher (GCD) and lc_ResourceManager (LRM)
			# To exchange initial messages on it. Note its type "ROUTER"
			# Later on, LRM disconnects with it and connects to the rmStubThread (RST)
			self.rmControlPort.bind("tcp://*:5555")
			self.poller.register(self.rmControlPort, zmq.POLLIN)

		except zmq.ZMQError as e :
			self.unbufferedWrite.write ("\nGlobal CD: %s"%(e.__str__()))
			sys.exit()
		
		while True:	
			try:
				self.wfManagerPort.connect(util.createConnectString("inproc", "tmp/inproc/lighthouse",str(os.environ["HOST_NAME"]), "WFM_CD"))
				break
			except:
				sleep(1)
				continue

		try:	
			self.poller.register(self.wfManagerPort, zmq.POLLIN)
		except zmq.ZMQError as e :
			#TODO log the error
			self.unbufferedWrite.write ("\nGlobal CD: %s"%(e.__str__()))
			sys.exit()
		except Invalid_Connect_String_Error as e:
			#TODO log error
			self.unbufferedWrite.write ("\nGlobal CD: %s"%(e.msg))
			sys.exit()
		self.unbufferedWrite.write ("\nGlobal CD: Ready to receive connection requests from Datacenter Controllerss")
		# Added by LG: spawns a new rmStub for each datacenter controller (lc)
		# /LG: rumStub is the communication link between gc and lc
#		while self.stopRequest.isSet():
		while True:
			try:
				socks= dict(self.poller.poll(10))
			except zmq.ZMQError as e:
				#This will throw an error when sockets are shut down
				pass

			if  socks.get(self.rmControlPort) == zmq.POLLIN:
				frame_id = {}
				frame_msg = {}
				try:
					frame_id = self.rmControlPort.recv()
					frame_msg = self.rmControlPort.recv_pyobj() 
				except:
					self.unbufferedWrite.write ("Global CD: Error in receiving message from RM")
					pass	
				if frame_msg["TYPE"] == RM_RMSTUB_INIT_SYN:
					# assign a new socket to the connection and pass it to the thread
					# pass the news of the new thread to resource manager
					
					# Comment added by DEVAL # 21/07/2014
					# LRM will eventually disconnect with GCD and connect to the rmST on this socket
					# Please note the rmStubPortNum, which is passed to LRM and rmST as well.
					rmStubPort = self.context.socket(zmq.DEALER)
					rmStubPortNum = rmStubPort.bind_to_random_port("tcp://*")
					rmStubObj = rmStub() 
					rmStubObj.set_attr("rm_ID", frame_msg["RM_ID"])
					rmStubObj.set_attr("rm_siteType",frame_msg["SITE_TYPE"])
					rmStubObj.set_attr("rm_port", rmStubPortNum)
					rmStubObj.set_attr("rm_addr", frame_msg["RM_ADDR"])
					rmStubObj.set_attr("rm_siteName", frame_msg["SITE_NAME"])
					rmStubObj.set_attr("rm_zoneName", frame_msg["ZONE_NAME"])
					
					# copy:make copy of the rmStubObj object, to avoid race condition(rmStubthread updating rmStubObj before
					# rmInfo has been  read by RM
					rmStubObj_copy = copy.deepcopy(rmStubObj)	

					## Tara 
					## Calculation of trust for global controller should go here !!! 

					# Informing the resource manager of the new rm connection
					try:
						rmThread =  rmStubThread.rmStubThread (rmStubObj, rmStubPort, self.context)
					except:
						#TODO for now pass. Let the host send a request again after timing out
						self.unbufferedWrite.write ("Global CD: Error in initializing rmStubthread")
						pass	
					# construct the RM_RMSTUB_INIT_SYN_ACK  msg for the rm  but do not send it yet
					frame_msg["TYPE"]  = RM_RMSTUB_INIT_SYN_ACK
					frame_msg["PORT"]  = rmStubPortNum
		
					# save the context and wait for workflow manager to respond
					rmStub_saved_context ={}					
					rmStub_saved_context["rm_ID"] = rmStubObj.get_attr("rm_ID")
					rmStub_saved_context["frame_id"] = frame_id
					rmStub_saved_context["frame_msg"] =frame_msg
					rmStub_saved_context["rmThread"] = rmThread

					self.rmStub_pending_list.append(rmStub_saved_context)
					
					try:
						self.wfManagerPort.send_pyobj({
									"TYPE": CD_WFM_INFO_RMSTUB,
									"THREAD_NAME":str(rmThread.name),
									"VRM":rmStubObj_copy.get_rmInfo()
									})
					except zmq.ZMQError as e:
						self.unbufferedWrite.write ("Global CD Error: %s"%(e.__str__())) 

			if  socks.get(self.wfManagerPort) == zmq.POLLIN:
				msg = self.wfManagerPort.recv_pyobj()
				cmd = msg["TYPE"]
				if cmd == WFM_CD_ACK_RMSTUB:
					index_of_items_to_remove = -1
					for i in range (len(self.rmStub_pending_list)):
						rmStub_saved_context = self.rmStub_pending_list[i]
						if msg["RESOURCE_ID"] == rmStub_saved_context["rm_ID"]:
							index_of_items_to_remove = i
						#	rmStub_saved_context = self.rmStub_pending_list[i]
							rmStub_saved_context["rmThread"].start()
							try:
								self.rmControlPort.send(rmStub_saved_context["frame_id"],zmq.SNDMORE)
								self.rmControlPort.send_pyobj(rmStub_saved_context["frame_msg"])
							except zmq.ZMQError as e:
								self.unbufferedWrite.write (e.__str__())
							break
					if index_of_items_to_remove != -1:
						self.rmStub_pending_list.pop(index_of_items_to_remove)
					
					#	self.rmStub_pending_list = [i for j, i in enumerate(self.rmStub_pending_list) if j not in  index_of_items_to_remove]



	
