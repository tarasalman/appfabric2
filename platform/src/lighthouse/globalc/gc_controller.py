#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import os, sys
import zmq
import threading

from appfabric.common.msgTypes import *
from appfabric.common.errors import *
from appfabric.common import util
from appfabric.common.unbufferedStream import unbuffered
from appfabric.common.ipStr import ipNum, ipStr

from .gc_workflowManager import gc_workflowManager
from .gc_connectionDispatcher import gc_connectionDispatcher
#TODO: Add loggers

class gc_controller(threading.Thread):
	
	# LG: starts 3 objects viz globalResourceManager, gc_connection_dispatcher, gc_workFlowManager
	def __init__(self,_context):
		super(gc_controller, self).__init__()
		
		# set unbuffered stream
		self.unbufferedWrite= unbuffered(sys.stdout)
		

		self.stopRequest = threading.Event()
		self.stopRequest.set()
		
		self.context = _context
	
		self.wfManagerPort = self.context.socket (zmq.DEALER)
		
		self.wfManager = None
		self.connectionDispatcher = None

		self.poller = zmq.Poller()

	def run(self):	
		try:
			self.wfManagerPort.bind(util.createConnectString("inproc", "tmp/inproc/lighthouse", str(os.environ["HOST_NAME"]), "WFM_CONTROLLER"))
			self.poller.register(self.wfManagerPort, zmq.POLLIN)
		except zmq.ZMQError as e:
			#TODO log error 
			sys.exit()
		except Invalid_Connect_String_Error as e:
			#TODO log error
			sys.exit()

		try:
			self.wfManager = gc_workflowManager(self.context)
		except  Resource_Manager_Init_Error as e:
			#TODO log the error
			self.unbufferedWrite.write ("WC Controller: Error starting resource manager - %s "%(e.msg()))	
			raise Controller_Init_Error(e.msg)
		
		# LG: not implemented in the current code. Mininet allocates resources statistically.
		# LG: To simulate on-demand allocation, extra resources are allocated in the beginning.
		
		self.wfManager.start()
	
		# LG: The gc workFlowManager does deployment and runtime control of the AppFabric service workflows
		# LG: workFlowManager spawns one workflowThread object for each zone where the application is to be deployed
		# LG: This is done by the application deployment administrator through the management plane
		# LG: Each workflowThread object spawns multiple workflowInstance objects, individual replicated instances of the service workflows.
		
		while self.stopRequest.isSet():
			try:
				socks= dict(self.poller.poll(1))
			except zmq.ZMQError as e:
				#This will throw an error when sockets are shut down
				 pass
			if  socks.get(self.wfManagerPort) == zmq.POLLIN:
				msg = self.wfManagerPort.recv_pyobj()
				
				if msg["TYPE"] == WFM_CONTROLLER_INIT:
					#self.unbufferedWrite.write("\nGC Controller:Workflow Manager(WFM) started...will start Global Connection Dispatcher(CD) now")
					self.unbufferedWrite.write("\n************************************************")
					self.unbufferedWrite.write("\nGC Controller:Workflow Manager(WFM) started...")
					self.unbufferedWrite.write("\n************************************************\n ")
		# LG: handles the communication between the local datacenter controllers and the global controller.
		# LG: Every new datacenter controller registers itself with the global controller.
		# LG: The gc_connection_dispatcher gets registration requests and it spawns a new rmStub object.
		# LG: The rmStub object acts as the local stub for each datacenter controller.
		# LG: The global controller accesses these stub objects locally whenever it needs to communicate with any datacenter controller
		# LG: For example to pass control messages, make resource requests or query resource or application deployment state
					try:
						self.connectionDispatcher = gc_connectionDispatcher(self.context)
			
					except Connection_Dispatcher_Init_Error as e:
						 #TODO log the error
						self.unbufferedWrite.write ("\nWC Controller: Error starting connection dispatcher - %s"%(e.msg()))	
						raise Controller_Init_Error(e.msg)	

					# start connection dispatcher
					self.connectionDispatcher.start()	
		
		

'''
if __name__ == '__main__':
		
	# for Mininet based experiments
#	sys.stdout = unbuffered(sys.stdout)
	
	os.environ["SERVICE_CONFIG_FILE"] = os.path.expanduser("~/driver_mininet/services.cfg")
	os.environ["WORKFLOW_CONFIG_FILE"] = os.path.expanduser("~/driver_mininet/workflowConfig.cfg")
	os.environ["CLASSIFIER_FILES_PATH"] = os.path.expanduser("~/driver_mininet/classifiers")	
	context = zmq.Context()

	try: 
		controller = lc_controller(context)
		controller.start()
	except Controller_Init_Error as e:
		print("Error starting controller", e.msg()) 
		sys.exit()
	
	#TODO propagate kill signals to the threads and close them properly
'''
	# LG: Each workflowInstance needs a proxy node and a service graph instance
	# LG: The proxy node is created by the workflowThread object and may be shared by one or mor workflowInstance objects.
	# LG: If no proxy node is available, the workflowThread spawns a new one for new workflowInstance objects.
	# LG: The workflowThread calls the createService-Graph function which creates the service graph instance.
	# LG: Creation of service graph instance requires querying datacentres for resource availability and choosing the appropriate datacenter
	# LG: Resources are reserved and services are started on different datacenter nodes.
	# LG: The nodes are then connected over the common data plane communication substrate comprising nested tunnels and application-level routing.
