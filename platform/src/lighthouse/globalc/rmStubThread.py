#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import zmq
import threading
from  appfabric.common import util
import sys, os
from time import sleep

from appfabric.common.msgTypes import * 
from appfabric.common.constants import *
from appfabric.common.unbufferedStream import unbuffered
from appfabric.common.aes_msg import AESCipher_Msg
import ast
#from rmStub import rmStub

class rmStubThread(threading.Thread):
	
	def __init__(self,_rmStubObj,_rmPort, _context):
		super(rmStubThread,self).__init__()
		
		# set unbuffered stream
		self.unbufferedWrite= unbuffered(sys.stdout)
		
		self.rmStubObj = _rmStubObj
	#	self.print_status ("Initializing RM Stub thread")
		
		self.stopRequest = threading.Event()
		self.stopRequest.set()
			
	
		self.context = _context
		self.poller = zmq.Poller()
		self.rmPort = _rmPort
 		
		# transferred connection with the resource manager in the local controller (transferred from CD)		
		self.poller.register(self.rmPort, zmq.POLLIN)
		self.print_status("Initialized")
	
		# port for communicating with the wfm
		self.wfmPort = self.context.socket(zmq.DEALER)
#		self.rmStubObj.set_attr("rmStub_to_wfm_connection",wfmPort)

	def print_status(self,msg):
		self.unbufferedWrite.write("\nRM Stub <%s: %s, %s>:  %s"%(self.rmStubObj.get_attr('rm_ID'),self.rmStubObj.get_attr('rm_addr'),self.rmStubObj.get_attr('rm_siteType'),msg ))

	def run(self):
	#	self.print_status ("Starting thread %s"%(str(threading.current_thread().name)))
		while True:
			try:
				self.wfmPort.connect(util.createConnectString("inproc", "tmp/inproc/lighthouse",
											str(os.environ["HOST_NAME"]),(
											"WFM_RM_"+ str(threading.current_thread().name))))
				break
			except:
				sleep(1)
				continue
		try:		
			self.poller.register(self.wfmPort, zmq.POLLIN)
			self.wfmPort.send_pyobj({
						"TYPE":RMSTUB_WFM_INIT, 
						"ID":self.rmStubObj.get_attr("rm_ID"), 
						"AVAILABLE_RESOURCE": self.rmStubObj.get_attr("rm_quota_available")
						})
		#	self.print_status ("sent RMSTUB_WFM_INIT")
		except zmq.ZMQError as e:
			#TODO how to handle this error? Timeout at the rm and at the rm. Host may send a request again to the CD
			# log the error
			self.print_status ("Error- %s"%(e__str__()))
			sys.exit()
	
		while self.stopRequest.isSet():
			try:
				socks= dict(self.poller.poll(1))
			except  zmq.ZMQError as e :
				#TODO log the error
				self.print_status("Error- %s"%(e.__str__()))
				sys.exit()

			if  socks.get(self.rmPort) == zmq.POLLIN:		
				msg = self.rmPort.recv_pyobj()
				cmd = msg["TYPE"]
				if cmd== RM_RMSTUB_INIT_ACK and msg["STATUS"] == SUCCESS:
					self.rmStubObj.set_attr("rm_quota_available", msg["AVAILABLE_RESOURCE"])
				#	self.print_status ("%s"%(msg["AVAILABLE_RESOURCE"]))
					self.send_resource_update_to_wfm()
				#	self.print_status ("connection to local controller RM setup successfully")
				
				if cmd == RM_RMSTUB_RESOURCE_REPORT:
					self.rmStubObj.set_attr("rm_quota_available", msg["AVAILABLE_RESOURCE"])
				#	self.print_status ("%s"%(msg["AVAILABLE_RESOURCE"]))
					self.send_resource_update_to_wfm()

				# Service start reply from the resource manager 	
				if cmd in (BC_SERVICE_START_REP,BC_PROXY_START_REP) :
					# update the qupta available at the rm
					#print( msg)
					f = open(os.path.expanduser("~/AppFabric/experiments/physical_machines/SecretKeys/LC_GC.bin"),"rb") # Get the NC_LC secret key 						(This key is agreed on after the first msg and both parties has it)
					message= f.read()
					message=message.split(b"    ")
					key2= message[1] #key
					iv= message[0] #nonce 
					msg2= msg.pop("TYPE") # Type is not encrypted so that it can be recognized in the if statement 
					#print (msg)
					try:
						msg= AESCipher_Msg(key2.decode()).decrypt(msg,iv) # Decrypt the whole message 
					except:
						pass 	
					msg["TYPE"]= msg2 # return the message TYPE
					msg["AVAILABLE_RESOURCE"]= float(msg["AVAILABLE_RESOURCE"]) # Change resource from string to int 
					msg["STATUS"]= float(msg["STATUS"])# Change from string to int					
					try: 
						#Recieves from LC the node configuration and save it to a file 
						text_file = open("GC/NC_Config_%s.txt"%msg["NC_NAME"], "wb")
						text_file.write(bytes("AVAILABLE_RESOURCE: %s \n"%(msg["AVAILABLE_RESOURCE"]), 'UTF-8'))
						text_file.write(bytes("MAX_REQUESTS: %s \n"%(msg["MAX_REQUESTS"]), 'UTF-8'))
						text_file.write(bytes("MAX_REQUESTS_PER_CLIENT: %s \n"%(msg["MAX_REQUESTS_PER_CLIENT"]), 'UTF-8'))
						text_file.write(bytes("BANNED_DEST: %s \n"%(msg["BANNED_DEST"]), 'UTF-8'))
						text_file.write(bytes("TYPE_OF_TRAFFIC: %s \n"%(msg["TYPE_OF_TRAFFIC"]), 'UTF-8'))
						text_file.close()	
					except: 
							pass
					if msg["STATUS"] == SUCCESS:
						self.rmStubObj.set_attr("rm_quota_available",msg["AVAILABLE_RESOURCE"])
						# send it to the wfmPort
						self.wfmPort.send_pyobj(msg)

				if cmd in (
						BC_SERVICE_PORT_CONFIG_ADD_EGRESS_REP,
						BC_SERVICE_PORT_CONFIG_ADD_INGRESS_REP, 
						BC_SERVICE_PORT_CONFIG_ADD_APP_ROUTER_REP,
						BC_SERVICE_PORT_CONFIG_ACTIVATE_WF_REP,
						BC_OVERLOAD_NOTIFICATION,
						BC_SUSPEND_WF_INSTANCE_NOTIFICATION,
						BC_RESUME_WF_INSTANCE_NOTIFICATION,
						BC_REPORT_LOAD_REP,
						BC_PROXY_WFINSTANCE_RELEASE_REP,
						I_SC_SERVICE_ANOM_INTEGRITY,
						I_SC_SERVICE_ANOM_DEST,
						I_SC_SERVICE_ANOM_TRAFFIC,
						I_SC_SERVICE_ANOM_MAX_REQUEST,
						I_SC_SERVICE_ANOM_MAX_CLIENT_REQ
					) :	
					if cmd in (
						I_SC_SERVICE_ANOM_INTEGRITY,
						I_SC_SERVICE_ANOM_DEST,
						I_SC_SERVICE_ANOM_TRAFFIC,
						I_SC_SERVICE_ANOM_MAX_REQUEST,
						I_SC_SERVICE_ANOM_MAX_CLIENT_REQ
						): 
						# ADDED BY TARA 
						#Recieve anomolous messages, save it to a file ans pass the messages  
						# Also hande the decrption from the NC --> GC 
						f = open("/home/openstack/AppFabric/experiments/physical_machines/SecretKeys/LC_GC.bin","rb") # Get the NC_LC secret key 							(This key is agreed on after the first msg and both parties has it)
						message= f.read()
						message=message.split(b"    ")
						key2= message[1] #key
						iv= message[0] #nonce 
						msg2= msg.pop("TYPE") # Type is not encrypted so that it can be recognized in the if statement 

						#print (msg)
						try:
							msg= AESCipher_Msg(key2.decode()).decrypt(msg,iv) # Decrypt the whole message 
						except:
							pass 	
						msg["TYPE"]= msg2 # return the message TYPE
						msg["PROXY_PORT_LIST"]=ast.literal_eval(msg["PROXY_PORT_LIST"])
						try: 
							text_file = open("/home/openstack/AppFabric/experiments/physical_machines/GC/ANom.txt","a+")
							text_file.write("\n%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s"%(msg["NoOfRequests"],msg["NoOfRequestsPerClient"],msg["NoOfRequestsPerClient_dest"],msg["SourceIP"],msg["SourcePort"],msg["User"],msg["DestinationIP"],msg["DestinationPort"],msg["DestinationURL"],msg["NC"],msg["ZONE_NAME"],msg["SITE_TYPE"],msg["TYPE"]))
							text_file.close()
			
						except: 
							pass 
					
						if cmd== I_SC_SERVICE_ANOM_INTEGRITY: 
							print ("\n GC Recieved anomolous integrity check failed from %s"%(msg["User"])) 
						if cmd== I_SC_SERVICE_ANOM_DEST: 
							print ("\n GC Recieved invalid destination from %s"%(msg["User"]))
						if cmd== I_SC_SERVICE_ANOM_TRAFFIC: 
							print ("\n GC Recieved invalid traffic from %s"%(msg["User"]))
						if cmd== I_SC_SERVICE_ANOM_MAX_REQUEST: 
							print ("\n GC Recieved NC acceeding maximum resources from when user %s was requesting "%(msg["User"]))
						if cmd== I_SC_SERVICE_ANOM_MAX_CLIENT_REQ: 
							print ("\n GC Recieved user %s acceede max requests"%(msg["User"]))
					self.wfmPort.send_pyobj(msg)
		
				if cmd == GEN_REP_PROXY_RESOURCE:
					self.wfmPort.send_pyobj(msg)

			if socks.get(self.wfmPort) == zmq.POLLIN:
				msg = self.wfmPort.recv_pyobj()
				cmd = msg["TYPE"]
				if cmd in (CB_SERVICE_START_REQ,CB_PROXY_START_REQ):	
					# send it to the relevant resource manager 	
					#self.print_status ("received CB_SERVICE_START_REQ from workflow manager port")
					newResourceQuota = self.rmStubObj.get_attr("rm_quota_available") - msg["RESOURCE_REQ"]
					self.rmStubObj.set_attr("rm_quota_available",newResourceQuota)
					self.rmPort.send_pyobj(msg)
				if cmd in ( 
						CB_SERVICE_PORT_CONFIG_ADD_EGRESS_REQ, 
						CB_SERVICE_PORT_CONFIG_ADD_INGRESS_REQ, 
						CB_SERVICE_PORT_CONFIG_ADD_APP_ROUTER_REQ,
						CB_SERVICE_PORT_CONFIG_ACTIVATE_WF_REQ,
						CB_STOP_OVERLOAD_NOTIFICATIONS,
						CB_REPORT_LOAD_REQ,
						CB_PROXY_WFINSTANCE_RELEASE_REQ,
						CB_RESUME_OVERLOAD_NOTIFICATIONS,
						CB_RESUME_ANOM
						):
					#ADDED BY TARA 
					#JUst pass anomolous response from GC and eventually to LC
					# Encryption will be done here for CB_RESUME_ANOM only 
					if (cmd== CB_RESUME_ANOM): 
						f = open("/home/openstack/AppFabric/experiments/physical_machines/SecretKeys/LC_GC.bin","rb") # Get the NC_LC secret key 							(This key is agreed on after the first msg and both parties has it)
						message= f.read()
						message=message.split(b"    ")
						key2= message[1] #key
						iv= message[0] #nonce 
						msg2= msg.pop("TYPE") # Type is not encrypted so that it can be recognized in the if statement 
						try:
							msg= AESCipher_Msg(key2.decode()).encrypt(msg,iv) # Decrypt the whole message 
						except:
							pass 	
						#print(msg)
						msg["TYPE"]= msg2 # return the message TYPE
					self.rmPort.send_pyobj(msg)

				if cmd ==  GEN_QUERY_PROXY_RESOURCE:	
				#	self.print_status ("Received GEN_QUERY_PROXY_RESOURCE")
					self.rmPort.send_pyobj(msg)

	def send_resource_update_to_wfm(self):
		msg = {
			"TYPE": RMSTUB_WFM_RESOURCE_REPORT, 
			"ID":self.rmStubObj.get_attr("rm_ID"), 
			"AVAILABLE_RESOURCE": self.rmStubObj.get_attr("rm_quota_available")
			}
		self.wfmPort.send_pyobj(msg)	



