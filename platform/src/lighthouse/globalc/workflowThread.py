#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


#import readConfig
import sys
import os
import re
import copy
from datetime import datetime
from time import sleep
import uuid
import pprint
import threading
import zmq
import math

from appfabric.common.errors import *
from appfabric.common.constants import *
from appfabric.common.msgTypes import *
from appfabric.common import xmlToDict
from appfabric.common.unbufferedStream import unbuffered
from appfabric.common import util

from . import createServiceGraph, selectDeploymentSite, wf
from .workflowInstanceThread import workflowInstanceThread




class workflowThread(threading.Thread):
	
	def __init__(self, _wfDesc, _wfID,  _zone, _wfm_wft_connectString, _context):
		super(workflowThread, self).__init__()

		# set unbuffered stream
		self.unbufferedWrite= unbuffered(sys.stdout)

		
		self.stopRequest = threading.Event()
		self.stopRequest.set()
		
		#connection to workflow manager
		self.context = _context
		self.wfm_wft_connectString = _wfm_wft_connectString
		self.wfm_wft_port = self.context.socket(zmq.DEALER)
		
		# conection to fakeNameServer
		self.fakeNameServer_connectString = util.createConnectString ("tcp", os.environ["NAME_SERVER_ADDR"], os.environ["NAME_SERVER_UPDATE_PORT"])
	#	print ("fakenameserver connect string: %s"%(self.fakeNameServer_connectString))
		self.fakeNameServerPort = self.context.socket(zmq.DEALER)
		
		self.poller = zmq.Poller()
		self.wfDb = {}
		self.wfDb = {
			"WF_NAME": _wfDesc["wf_name"],
			"WF_ID": _wfID,
			"WF_GRAPH_DESC": _wfDesc["wf_graph_desc"],
			"WF_INSTANCE_CAPACITY": _wfDesc["wf_instance_capacity"],
			"WF_OVERLOAD_NOTIFICATION_LEVEL":_wfDesc["wf_overload_notification_level"],
			"WF_SCALE_DOWN_LEVEL": _wfDesc["wf_scale_down_level"],
			"WF_WF_PER_PROXY": _wfDesc["wf_per_proxy"],
			"WF_RESOURCE_ALLOCATION_METHOD": _wfDesc["wf_resource_allocation_method"],
			"WF_AVG_LOAD_PER_SESSION": _wfDesc["wf_avg_load_per_session"],
			"WF_REQ_CORE_SITE": False,
			"WF_REQ_EDGE_SITE": False,
			"WF_REQ_CORE_RES":0,
			"WF_REQ_EDGE_RES":0,
			"WF_ZONE": _zone,
			"WF_PROXY_NODE": None,
			"WF_END_NODE": None,
			"WF_ACTIVE_PROXY_NODE_LIST": [],
			"SERVICE_GRAPH": None,
			"CORE_SITES": [],
			"EDGE_SITES": [],
			"WF_INSTANCES": {},
			"WF_INSTANCES_SCALING_LIST": []
			}

		self.max_init_numTries = 1000 # to handle delay due to system startup
		self.max_numTries = 10 # Magic number, no sound reason why; 
		self.numTries = 0
		
		# overlaod condition 
		self.overload_panic_tipping_point = 2
		self.overload_notification_valid_interval = 30000 # milli secs
		self.overload_notification_list = []
		self.launch_wfInstance_flag = False	
		# check if workflow is correctly specified
		# including the service names are in the config files 
		try:
			serviceConfigDict = xmlToDict.ConvertXmlToDict(os.environ["SERVICE_CONFIG_FILE"])
			conf = serviceConfigDict['service_config']['service']
			if isinstance(conf,dict):
				self.serviceConfig = []
				self.serviceConfig.append(conf)
			else:
				self.serviceConfig = conf
			
		except:
			raise WorkFlow_Init_Error ("XML Parse Error: Check your service config file")
		
		try:		
			createServiceGraph.create_serviceGraph(_wfDesc, self.serviceConfig, self.wfDb)
		except Create_Service_Graph_Error as e:
			self.print_status (e.msg)
			sys.exit()
		createServiceGraph.annotate_wf(self.wfDb)

		# list of wfInstances with pending proxy request
		self.proxy_init_state= { "STATE":PROXY_INIT,"TIME": 0}
		self.temp_proxyNode = None
		self.waitTime_timeout = 300
		self.temp_wf_per_proxy = 0
		self.pending_resource_request = {}

		# poll the active workflows for their loads 
		self.polling_for_load_interval = 1000  # milli  seconds 
		self.polling_start_flag = False
		self.polling_for_load_start_time = 0
		self.num_load_reqs_sent = 0
		self.poll_for_load_reports = {}
		self.poll_id = 0


	def print_status(self, _msg):
		self.unbufferedWrite.write("\nWFT <%s, %s>:  %s"%(self.wfDb["WF_NAME"],self.wfDb["WF_ZONE"],_msg ))

	def run(self):
		self.print_status ("started")

		# setup connection with the workflow Manager Thread
		while True:
			try:
				self.wfm_wft_port.connect(self.wfm_wft_connectString)
				break
			except:
				sleep (1)
				continue
			
		try:
			self.poller.register(self.wfm_wft_port, zmq.POLLIN)
		except zmq.ZMQError as e:
			self.print_status ("Error:%s" %(e.__str__()))
			exit()
		while True:
			try:	
				self.fakeNameServerPort.connect(self.fakeNameServer_connectString)
				self.print_status ("connected to fake name server port")
				break
			except zmq.ZMQError as e:
				#TODO Log error
				sleep(1)
				continue

		# Launch the first workflow instance
		# INIT_WF_INSTANCE_ID -> The first wf instance has no parent 
		
		self.wfInstanceThread_launch(INIT_WF_INSTANCE_ID)
		
		# Now wait on the wfInstance Thread

		while self.stopRequest.isSet():

			# poll for load
			if self.polling_start_flag == False:
				self.num_load_reqs_sent = 0
				msgList = []
				if len(self.wfDb["WF_ACTIVE_PROXY_NODE_LIST"]) > 0:
				
					# ensure everyone is replying to the same poll
					self.poll_id = str(uuid.uuid4())
				
					for proxyNodeObj in self.wfDb["WF_ACTIVE_PROXY_NODE_LIST"]:
						for wfInstanceInfo in proxyNodeObj.get_attr("workflowList"):
							poll_for_load_msg = {
										"TYPE":CB_REPORT_LOAD_REQ,
										"POLL_ID": self.poll_id,
										"SERVICE_NAME": proxyNodeObj.get_attr("serviceName"),
										"SERVICE_ID":proxyNodeObj.get_attr("serviceID"),
										"WF_ID":self.wfDb["WF_ID"],
										"WF_INSTANCE_ID":wfInstanceInfo["WF_INSTANCE_ID"],
										"WF_PORT_NUM": wfInstanceInfo["WF_PORT_NUM"],
										"HOST_ID" :proxyNodeObj.get_attr("hostID"),
										"RM_ID": proxyNodeObj.get_attr("rmID")
									}
							self.num_load_reqs_sent += 1
							msgList.append(poll_for_load_msg)
					
					# Do not bother sendng out a message if there is only one active workflow
					# we never scale-down below 1					
					if self.num_load_reqs_sent > 1:
						self.poll_for_load_reports = {}
						self.poll_for_load_reports[self.poll_id] =[]
						while len(msgList) > 0:
							self.wfm_wft_port.send_pyobj(msgList.pop())

						self.polling_for_load_start_time = datetime.now()
						self.polling_start_flag = True
	
				# we never scale down below 1 instance
			elif (self.num_load_reqs_sent > 1 and len(self.poll_for_load_reports[self.poll_id]) == self.num_load_reqs_sent) or ((datetime.now() - self.polling_for_load_start_time).microseconds/1000 >= self.polling_for_load_interval):
					
					
					num_respondents = len(self.poll_for_load_reports[self.poll_id])	
					# number of respondents should be atleast 50% of active wf to make a quorum
					quorum = math.ceil(0.5 * self.num_load_reqs_sent)	
					if num_respondents >= quorum:
						total_load = 0
						for respondent_load in self.poll_for_load_reports[self.poll_id]:
							total_load += respondent_load

						avg_load = total_load/num_respondents
						# fraction of total capacity
						load_fraction = avg_load/self.wfDb["WF_INSTANCE_CAPACITY"]
						if (load_fraction <= self.wfDb["WF_SCALE_DOWN_LEVEL"]):
							# scale down always by 1 wf Instance 
							# TODO for now scale down by simply putting the wfInstance in a deleted list in fake nameserver
							# need to actually remove the instance and give free the resources
							if (len(self.wfDb["WF_INSTANCES_SCALING_LIST"])> 1):
								wfInstanceID_to_be_removed = self.wfDb["WF_INSTANCES_SCALING_LIST"].pop()
								self.wfDb["WF_INSTANCES"][wfInstanceID_to_be_removed].send_pyobj({
																"TYPE": WFT_WFINSTANCE_STOP_WORKFLOW_INSTANCE_REQ
																})
																 		
										
							
					else:
						pass
					
					# get ready for next poll
					self.polling_start_flag = False

		

			# check for timeout values
			# resend proxy resource requests after downgrading the proxy
			# Implement timeout when gc_workflow implements a timeout

			# Timeout is required for cases in the begining when ther are no active sites
			# FAIL indicates lack of resource
			# No reply implies that the workflowmanager does not have any active sites

			'''
			if self.proxy_init_state["STATE"] == PROXY_RESOURCE_REQ_SENT:
				time_elapsed = datetime.now() - self.proxy_init_state["TIME"]
				if time_elapsed.microseconds/1000 > self.waitTime_timeout:
					#send resource request again
					self.wf_get_resource_for_proxy()
					self.proxy_init_state["TIME"] = datetime.now()
				
			
					
					if len(self.wfDb["WF_ACTIVE_PROXY_NODE_LIST"]) == 0:
						# very high number to make sure that if it is the first proxy it does not get downgraded
					
						if self.numTries < self.max_init_numTries:
						#	self.print_status ("Trying to get resource for proxy: 1")
							self.numTries +=1
							self.wf_get_resource_for_proxy()
							self.proxy_init_state["TIME"] = datetime.now()
						else:
							# Downgrade the proxy by reducing the number of workflows it can support
							if self.temp_wf_per_proxy > 1:
								self.temp_wf_per_proxy -=1
								self.numTries = 0
								self.wf_get_resource_for_proxy()
								self.proxy_init_state["TIME"] = datetime.now()	
							
                			
					else:
						if self.numTries < self.max_numTries:
							self.numTries +=1
							self.wf_get_resource_for_proxy()
							self.proxy_init_state["TIME"] = datetime.now()
						else:
							# Downgrade the proxy by reducing the number of workflows it can support
							if self.temp_wf_per_proxy > 1:
								self.temp_wf_per_proxy -=1
								self.numTries = 0
								self.wf_get_resource_for_proxy()
								self.proxy_init_state["TIME"] = datetime.now()
			'''	
	
			try:
				socks= dict(self.poller.poll(0.5))
			except  zmq.ZMQError as e :
				#TODO log the error
				self.print_status ("Error: "+ e.__str__())
				sys.exit()
			
			# wait on the sockets		
			if len(self.wfDb["WF_INSTANCES"]) > 0:
				for wfInstanceID in self.wfDb["WF_INSTANCES"]:
					if socks.get(self.wfDb["WF_INSTANCES"][wfInstanceID]) == zmq.POLLIN:
						msg = self.wfDb["WF_INSTANCES"][wfInstanceID].recv_pyobj()
					#	print (msg)
						cmd = msg["TYPE"]
						if cmd == WFINSTANCE_WFT_GET_PROXY_REQ:
							proxyNode = self.wfInstance_select_proxy()
							if proxyNode != None:
								msg["TYPE"] = WFINSTANCE_WFT_GET_PROXY_REP
								msg["STATUS"] = SUCCESS
								msg["PROXY_NODE_DESC"] = {
											"SERVICE_ID": proxyNode.get_attr("serviceID"),
											"RM_ID": proxyNode.get_attr("rmID"),
											"HOST_ID": proxyNode.get_attr("hostID"),
											"HOST_EXT_IP": proxyNode.get_attr("hostPublicIP"),
											"HOST_INT_IP":proxyNode.get_attr("hostInternalIP"),
											"WF_PORT_NUM":proxyNode.get_empty_wf_port()
										}
								
								proxyNode.set_wf_port(msg["PROXY_NODE_DESC"]["WF_PORT_NUM"], wfInstanceID)
								#proxyNode.set_attr("workflowList", wfInstanceID)
								self.wfDb["WF_INSTANCES"][wfInstanceID].send_pyobj(msg)
							else:
								# ask the workflow to try again later 
								msg["TYPE"] = WFINSTANCE_WFT_GET_PROXY_REP
								msg["STATUS"] = FAIL
								msg["PROXY_NODE_DESC"] = None
								self.wfDb["WF_INSTANCES"][wfInstanceID].send_pyobj(msg)
								# start the process of deploying a proxy
								if self.proxy_init_state["STATE"] == PROXY_INIT:
									self.temp_wf_per_proxy = copy.deepcopy(self.wfDb["WF_WF_PER_PROXY"])
									self.proxy_init_state = {"STATE":PROXY_RESOURCE_REQ_SENT,"TIME": datetime.now()}
									self.wf_get_resource_for_proxy()

						elif cmd == WFINSTANCE_WFT_GET_RESOURCES_REQ:
							# if request is already pending ignore it
							# at a time only allow one wfInstance to get resources
							# to avoid contention, ask others to come back later
							if len(self.pending_resource_request) == 0:
								self.pending_resource_request[msg["WF_INSTANCE_ID"]] = self.wfDb["WF_INSTANCES"][wfInstanceID]
								self.wfInstance_get_resources(msg)
							else:
								if msg["WF_INSTANCE_ID"] not in self.pending_resource_request:
									msg["TYPE"] = WFINSTANCE_WFT_GET_RESOURCES_REP
									msg["STATUS"] = FAIL
									self.wfDb["WF_INSTANCES"][wfInstanceID].send_pyobj(msg)
						

						elif cmd == WFINSTANCE_WFT_RELEASE_PROXY_REQ:
							msg["TYPE"] = CB_PROXY_WFINSTANCE_RELEASE_REQ
							self.wfm_wft_port.send_pyobj(msg)	
										
						#if cmd == WFINSTANCE_WFT_WORKFLOW_INSTANCE_INITIALIZED_INFO:
						#	if len(self.pending_resource_request) > 0:
						#		if msg["WF_INSTANCE_ID"] in self.pending_resource_request:
						#			del self.pending_resource_request[msg["WF_INSTANCE_ID"]] 
						#			print ("done")

						elif cmd == CB_SERVICE_START_REQ:
							self.wfm_wft_port.send_pyobj(msg)
						
						
						elif cmd == CB_SERVICE_PORT_CONFIG_ADD_EGRESS_REQ:
							self.wfm_wft_port.send_pyobj(msg)

						elif cmd == CB_SERVICE_PORT_CONFIG_ADD_INGRESS_REQ:
							self.wfm_wft_port.send_pyobj(msg)
								
						elif cmd == CB_SERVICE_PORT_CONFIG_ADD_APP_ROUTER_REQ:
							self.wfm_wft_port.send_pyobj(msg)
						
						elif cmd ==  CB_SERVICE_PORT_CONFIG_ACTIVATE_WF_REQ:
							self.wfm_wft_port.send_pyobj(msg)
							# Add the active workflow to the proxynode's list of workflows
							for proxyNodeObj in self.wfDb["WF_ACTIVE_PROXY_NODE_LIST"]:
								if proxyNodeObj.get_attr("serviceID") == msg["SERVICE_ID"]:
									proxyNodeObj.set_attr("workflowList", { 
														"WF_INSTANCE_ID":msg["WF_INSTANCE_ID"],
														"WF_PORT_NUM": msg["WF_PORT_NUM"]})
									break
							# Allow overload information to be recorded again	
							self.launch_wfInstance_flag = False
							
	

						elif cmd == WFINSTANCE_WFT_RESUME_OVERLOAD_NOTIFICATIONS_FOR_PARENT:	
							msg["TYPE"] = WFT_WFINSTANCE_RESUME_OVERLOAD_NOTIFICATIONS
							self.wfDb["WF_INSTANCES"][msg["WF_INSTANCE_ID"]].send_pyobj (msg)
		
						elif cmd == CB_RESUME_OVERLOAD_NOTIFICATIONS:
							self.print_status("\n\n\n\n WFT: sending RESUME OVERLOAD NOTIFICATION\n\n\n\n")
							self.wfm_wft_port.send_pyobj(msg)
						#ADDED BY TARA 
						#Just forwarding back anomolous response to gc-workflow manager
						elif cmd == CB_RESUME_ANOM:
							self.print_status("\n\n\n\n WFT: sending ANOM RESPONSE AND ACTION\n\n\n\n")
							self.wfm_wft_port.send_pyobj(msg)

						elif cmd == NAME_TO_APPLICATION_MAPPING_UPDATE_REQ:		
							self.fakeNameServerPort.send_pyobj(msg)
					

			if socks.get(self.wfm_wft_port) == zmq.POLLIN:
				msg = self.wfm_wft_port.recv_pyobj()
				cmd = msg["TYPE"]

				# General load reports from Workflow Instances
				if cmd == BC_REPORT_LOAD_REP:
				#	self.print_status("BC_REPORT_LOAD Msg :%s"%(msg))
					if msg["POLL_ID"] in self.poll_for_load_reports:
						self.poll_for_load_reports[msg["POLL_ID"]].append(msg["LOAD"])

				# if the proxy resource request is returned	
				if cmd == GEN_REP_PROXY_RESOURCE:
					if msg["STATUS"]== SUCCESS:
						if self.proxy_init_state ["STATE"] != PROXY_DEPLOYING_PROXY:
							self.proxy_init_state = { "STATE":PROXY_DEPLOYING_PROXY,"TIME": datetime.now()}
							# launch the proxy
							rmID = msg["RM_ID"]
							self.wf_deploy_proxy(rmID, self.temp_wf_per_proxy)
					elif msg["STATUS"] == FAIL:
						if msg["REASON"] == OUT_OF_RESOURCE: 
							# Downgrade the proxy
							if self.proxy_init_state ["STATE"] != PROXY_DEPLOYING_PROXY:
								if self.temp_wf_per_proxy > 1:
									self.temp_wf_per_proxy -= 1
								self.proxy_init_state = {"STATE":PROXY_RESOURCE_REQ_SENT,"TIME": datetime.now()}
								self.wf_get_resource_for_proxy()

						elif msg["REASON"] == SITES_UNINITIALIZED:
							if self.proxy_init_state ["STATE"] != PROXY_DEPLOYING_PROXY:
								self.proxy_init_state = {"STATE":PROXY_RESOURCE_REQ_SENT,"TIME": datetime.now()}
								self.wf_get_resource_for_proxy()
					
				elif cmd == BC_PROXY_START_REP:
					if msg["STATUS"] == SUCCESS:
						# setup the proxy
						self.temp_proxyNode.set_attr("hostID", msg["HOST_ID"])
						self.temp_proxyNode.set_attr("hostPublicIP",msg["HOST_EXT_IP"])
						self.temp_proxyNode.set_attr("hostInternalIP", msg["HOST_INT_IP"])
						self.temp_proxyNode.set_attr("rmID", msg["RM_ID"])
						self.wfDb["WF_ACTIVE_PROXY_NODE_LIST"].append(self.temp_proxyNode)

						# set the state back to init
						self.temp_proxyNode = None
						self.proxy_init_state = { "STATE":PROXY_INIT,"TIME":0}
					elif msg["STATUS"] == FAIL:
						# failed to allocate proxy
						self.proxy_init_state = { "STATE":PROXY_INIT,"TIME":0}
				
				elif cmd == WFT_WFM_REP_ACTIVE_SITES:
					if len (msg["SITE_LIST"]) > 0:
						# select the site and send it to the requesting wfInstance
						deployment_scenario = {
							"WF_RESOURCE_ALLOCATION_METHOD":self.wfDb["WF_RESOURCE_ALLOCATION_METHOD"],
							"WF_REQ_CORE_SITE": self.wfDb["WF_REQ_CORE_SITE"],
							"WF_REQ_EDGE_SITE": self.wfDb["WF_REQ_EDGE_SITE"],
							"WF_REQ_CORE_RES":  self.wfDb["WF_REQ_CORE_RES"],
							"WF_REQ_EDGE_RES":  self.wfDb["WF_REQ_EDGE_RES"],
							"CORE_SITES": self.wfDb["CORE_SITES"],
							"EDGE_SITES": self.wfDb["EDGE_SITES"],
							}								

						flag, selected_coreSite, selected_edgeSite = selectDeploymentSite.selectDeploymentSite(msg["SITE_LIST"],deployment_scenario) 
						del msg["SITE_LIST"]
						if flag == True:
							#send the sites
							msg["TYPE"] = WFINSTANCE_WFT_GET_RESOURCES_REP
							msg["STATUS"] = SUCCESS
							msg["CORE_SITE"] = selected_coreSite
							msg["EDGE_SITE"] = selected_edgeSite
							if selected_coreSite != None:
								if selected_coreSite not in self.wfDb["CORE_SITES"]:
									self.wfDb["CORE_SITES"].append(selected_coreSite)
							if selected_edgeSite != None:
								if selected_edgeSite not in self.wfDb["EDGE_SITES"]:
									self.wfDb["EDGE_SITES"].append(selected_edgeSite)
							# Note: we are not removing it from the dictionery to force a strict sequential order
							# in starting workflow. Only after the workflow Instance thread sends a message saying that the
							# the workflow instantiation is complete will it remove the entry to allow other 
							# workflow instances to request resources
							# This approach is sufficient since we will proactively start new workflow instances
							self.pending_resource_request[msg["WF_INSTANCE_ID"]].send_pyobj(msg)
							del self.pending_resource_request[msg["WF_INSTANCE_ID"]]
						else:
							msg["TYPE"] = WFINSTANCE_WFT_GET_RESOURCES_REP
							msg["STATUS"] = FAIL
							# free the lock 
							self.pending_resource_request[msg["WF_INSTANCE_ID"]].send_pyobj(msg)
							del self.pending_resource_request[msg["WF_INSTANCE_ID"]]
							
					else:
						msg["TYPE"] = WFINSTANCE_WFT_GET_RESOURCES_REP
						msg["STATUS"] = FAIL	
						# free the lock 
						self.pending_resource_request[msg["WF_INSTANCE_ID"]].send_pyobj(msg)
						del self.pending_resource_request[msg["WF_INSTANCE_ID"]]

				elif cmd in (	BC_SERVICE_START_REP,
						BC_SERVICE_PORT_CONFIG_ADD_EGRESS_REP,
						BC_SERVICE_PORT_CONFIG_ADD_INGRESS_REP,
						BC_SERVICE_PORT_CONFIG_ADD_APP_ROUTER_REP,
						BC_SERVICE_PORT_CONFIG_ACTIVATE_WF_REP,
						):
			
					self.wfDb["WF_INSTANCES"][msg["WF_INSTANCE_ID"]].send_pyobj (msg,zmq.NOBLOCK)
				
				elif cmd == BC_OVERLOAD_NOTIFICATION:
				#	print ("WFT: received overload notification")
					if (self.launch_wfInstance_flag == False):	
						notification_list_length = len(self.overload_notification_list)
						if notification_list_length == 0:
							self.overload_notification_list.append (datetime.now())
						else:
							time_now = datetime.now()
							last_notification_event_time = self.overload_notification_list.pop()
							if  (time_now - last_notification_event_time).microseconds/1000 < self.overload_notification_valid_interval:
								self.overload_notification_list.append(last_notification_event_time)
								self.overload_notification_list.append(time_now)
							else:
								self.overload_notification_list = []
								self.overload_notification_list.append(time_now)
					#	print ("WFT: notification_list: %s"%(self.overload_notification_list))
						if len(self.overload_notification_list) == self.overload_panic_tipping_point:
							#launch new workflowInstance
							self.print_status ("Time to launch new workflow")
							self.launch_wfInstance_flag = True
							
							# set the proxy port that caused this new wf launch to change state
							# such that it does not send overload notifications till it has gone down below 
							# the overloaded state atleast

							proxy_service_id = msg["SERVICE_ID"]
							for proxyNodeObj in self.wfDb["WF_ACTIVE_PROXY_NODE_LIST"]:
								if proxyNodeObj.get_attr("serviceID") == proxy_service_id:
									msg["TYPE"]= CB_STOP_OVERLOAD_NOTIFICATIONS
									msg["SERVICE_NAME"] = proxyNodeObj.get_attr("serviceName")	
									msg["HOST_ID"] = proxyNodeObj.get_attr("hostID")
									msg["RM_ID"] = proxyNodeObj.get_attr("rmID")
									self.wfm_wft_port.send_pyobj(msg)
									break	
					
							#TODO handle resource failures
							self.wfInstanceThread_launch(msg["WF_INSTANCE_ID"])
							self.overload_notification_list = []

				#elif cmd == I_SC_SERVICE_ANOMLOUS: 
				#	self.print_status(" GC Recieved anomolous signal") 

				elif cmd == BC_PROXY_WFINSTANCE_RELEASE_REP:
					self.print_status("\n\n\n\n Received proxy release reply \n\n\n\n")
					for proxyNodeObj in self.wfDb["WF_ACTIVE_PROXY_NODE_LIST"]:
						if proxyNodeObj.get_attr("serviceID") == msg ["SERVICE_ID"]:
							proxyNodeObj.add_back_wf_port(msg["WF_PORT_NUM"])
							break
					msg["TYPE"] = WFT_WFINSTANCE_RELEASE_PROXY_REP
					self.wfDb["WF_INSTANCES"][msg["WF_INSTANCE_ID"]].send_pyobj(msg)	
			

				elif cmd == BC_SUSPEND_WF_INSTANCE_NOTIFICATION:
					# suspend workflowInstance in nameserver
					self.print_status("Received suspend wf notification :%s"%(msg))
					self.wfDb["WF_INSTANCES"][msg["WF_INSTANCE_ID"]].send_pyobj(msg,zmq.NOBLOCK)	
	
				elif cmd == BC_RESUME_WF_INSTANCE_NOTIFICATION:
					self.print_status("Received resume wf notification :%s"%(msg))
					self.wfDb["WF_INSTANCES"][msg["WF_INSTANCE_ID"]].send_pyobj(msg,zmq.NOBLOCK)
				# Forwarding anomolous messages recieved from clients to workflow instanse threads 		
				elif cmd in (
						I_SC_SERVICE_ANOM_INTEGRITY,
						I_SC_SERVICE_ANOM_DEST,
						I_SC_SERVICE_ANOM_TRAFFIC,
						I_SC_SERVICE_ANOM_MAX_REQUEST,
						I_SC_SERVICE_ANOM_MAX_CLIENT_REQ
						): 
						self.wfDb["WF_INSTANCES"][msg["WF_INSTANCE_ID"]].send_pyobj(msg,zmq.NOBLOCK)


	def wfInstanceThread_launch(self, _wfInstance_parentID):
		wfInstanceID = str(uuid.uuid4())
		wfInstance_connectString = util.createConnectString("inproc","tmp/inproc/lighthouse", self.wfDb["WF_ID"][:16], wfInstanceID[:16])

		wfInstance_port_socket = self.context.socket(zmq.DEALER)
		wfInstance_port_socket.bind(wfInstance_connectString)
		self.poller.register( wfInstance_port_socket, zmq.POLLIN)
		self.wfDb["WF_INSTANCES"][wfInstanceID] = wfInstance_port_socket
		self.wfDb["WF_INSTANCES_SCALING_LIST"].append(wfInstanceID)
		#Initialize the thread  	
		wfInstanceThread = workflowInstanceThread(self.wfDb["SERVICE_GRAPH"], 
							self.wfDb["WF_PROXY_NODE"],
							wfInstanceID,
							self.wfDb["WF_INSTANCE_CAPACITY"], 
							self.wfDb["WF_AVG_LOAD_PER_SESSION"],
							self.wfDb["WF_OVERLOAD_NOTIFICATION_LEVEL"],
							self.wfDb['WF_ZONE'],
							self.wfDb["WF_NAME"],
							self.wfDb["WF_ID"],
							wfInstance_connectString,
							_wfInstance_parentID, 
							self.context) 
	
		try:
			wfInstanceThread.start ()
		except:
			# TODO Log the error
			raise WFInstance_Create_Error("could not start wfInstance <%s:%s> <%s>"%(self.wfDb["WF_NAME"], self.WFDb["zone"], wfInstanceID))
	



	def wfInstance_get_resources(self, msg):
		# TODO : add a timer for timeout
		msg["TYPE"] = WFT_WFM_QUERY_ACTIVE_SITES 
		msg["ZONE"] = self.wfDb['WF_ZONE']
		self.wfm_wft_port.send_pyobj(msg)
	#	self.print_status("Sent query for active sites")
			



	def wf_get_resource_for_proxy(self) :
		resourceReq = self.wfDb["WF_INSTANCE_CAPACITY"] * self.wfDb["WF_AVG_LOAD_PER_SESSION"] * self.wfDb["WF_PROXY_NODE"].get_attr("normResIndex") * self.temp_wf_per_proxy
		msg = {
			"TYPE": GEN_QUERY_PROXY_RESOURCE,
			"RESOURCE_REQ": resourceReq,
			"WF_ID": self.wfDb["WF_ID"],
			"ZONE": self.wfDb["WF_ZONE"]
			}
		# TODO: Other algos may be tried
		# for now just select the first site that responds
		#TODO set up conditios for timeout
		self.wfm_wft_port.send_pyobj(msg)
				

	
	def wf_deploy_proxy(self, _rmID, _temp_wf_per_proxy ):
		# get resource first
		proxyNode = wf.wfProxyNode(self.wfDb["WF_PROXY_NODE"])
		serviceID = proxyNode.get_attr("serviceID")
		serviceName = proxyNode.get_attr("serviceName")
		serviceType = proxyNode.get_attr("serviceType")
		normResIndex = proxyNode.get_attr("normResIndex")
		resourceReq = self.wfDb["WF_INSTANCE_CAPACITY"] * self.wfDb["WF_AVG_LOAD_PER_SESSION"] * normResIndex * _temp_wf_per_proxy

		# setting the known parameters	
		proxyNode.set_attr("rmID",_rmID)
		proxyNode.set_attr("max_load",_temp_wf_per_proxy) # also intializes the wfports(number of ports = max_load) and sets them to None 
		
		# start the proxy	
		msg = { 
			"TYPE":CB_PROXY_START_REQ,
			"WF_ID": self.wfDb["WF_ID"],
			"SERVICE_ID":serviceID,
			"SERVICE_NAME":serviceName,
			"EXECUTABLE": proxyNode.get_attr("serviceExecutable"),
			"ARGS":proxyNode.get_attr("serviceArgs"),
			"RESOURCE_REQ":resourceReq,
			"NUM_WORKFLOWS": _temp_wf_per_proxy,
			"ZONE": self.wfDb["WF_ZONE"],
			"RM_ID": proxyNode.get_attr("rmID"),
			"NUMPORTS":int(self.wfDb["WF_INSTANCE_CAPACITY"]* 0.2),
			"PORT": 8080,
			"PROTO_TYPE": "http"
		}		
		# save reference to the proxyNode
		self.temp_proxyNode = proxyNode	
		self.wfm_wft_port.send_pyobj(msg)


	def wfInstance_select_proxy(self):

		# Query for a proxy with available capacity in the zone
		# if, proxy available, configure proxy
		# else start a new proxy
		proxyNodeObj = None
		proxy_found_flag = False
		if len(self.wfDb["WF_ACTIVE_PROXY_NODE_LIST"]) > 0:
			# see if there is place in on of the proxies else start a new proxy
			for proxyNodeObj in self.wfDb["WF_ACTIVE_PROXY_NODE_LIST"]:
				if proxyNodeObj.check_empty_wf_ports_left() == True:
					return proxyNodeObj
				
			return None
		return None

	
	
