#
#       Copyright © 2014 Washington University in Saint Louis
#
#       Website: https://sites.google.com/site/applicationfabric/home
#
#       This file is part of AppFabric.
#
#       AppFabric is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       AppFabric is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with AppFabric.  If not, see <http://www.gnu.org/licenses/>.
#
#
#       AUTHORS: Subharthi Paul <spaul@wustl.edu, subharthipaul@gmail.com>
#


import os, sys
import zmq
import threading
import pickle

from appfabric.common.msgTypes import *
from appfabric.common.errors import *
from appfabric.common.constants import *
from appfabric.common import util
from appfabric.common.unbufferedStream import unbuffered
from appfabric.common.ipStr import ipNum, ipStr
from Crypto.PublicKey import RSA


#TODO: Add loggers

class fakeNameServer(threading.Thread):
	

	def __init__(self,_context):
		super(fakeNameServer, self).__init__()

		# set unbuffered stream
		self.unbufferedWrite= unbuffered(sys.stdout)

		
		self.stopRequest = threading.Event()
		self.stopRequest.set()
		self.context = _context

		try:
			self.host_name = os.environ["HOST_NAME"]
			self.ip_addr = os.environ["IP_ADDR"]
			self.update_mapping_port = os.environ ["UPDATE_MAPPING_PORT"]
			self.read_mapping_port = os.environ["READ_MAPPING_PORT"]
			
			self.unbufferedWrite.write ("update mapping port: %s"%(self.update_mapping_port))
			self.unbufferedWrite.write ("read  mapping port: %s"%(self.read_mapping_port))
		except KeyError as e:
			self.unbufferedWrite.write ("Exiting...fakeNameServer Key Error: %s" %(e.__str_()))
			#TODO log the error
			sys.exit()

		self.updateMappingPort = self.context.socket (zmq.DEALER)
		self.readMappingPort= self.context.socket (zmq.ROUTER)
		
		self.mappingTable = {}
	
		# suspending a mapping entry due to overload 
		self.suspendedTable = {}

		self.poller = zmq.Poller()

	def run(self):	
		try:
			self.updateMappingPort.bind("tcp://*:" + self.update_mapping_port)
			 
			self.readMappingPort.bind("tcp://*:" + self.read_mapping_port)

			
			self.poller.register(self.updateMappingPort, zmq.POLLIN)
			self.poller.register(self.readMappingPort, zmq.POLLIN)

		except zmq.ZMQError as e:
			#TODO log error
			self.unbufferedWrite.write ("Fake Name Server: Error initializing") 
			sys.exit()
		except Invalid_Connect_String_Error as e:
			#TODO log error
			self.unbufferedWrite.write ("FNS:%s"%(e.msg))
			sys.exit()
		
		count = 0

		while self.stopRequest.isSet():
			
			try:
				socks= dict(self.poller.poll(1))
			except zmq.ZMQError as e:
				#This will throw an error when sockets are shut down
				pass

			if  socks.get(self.updateMappingPort) == zmq.POLLIN:
				msg = self.updateMappingPort.recv_pyobj()
				if msg["TYPE"] == NAME_TO_APPLICATION_MAPPING_UPDATE_REQ:
					if msg["COMMAND"] == "ADD_MAPPING":
						# Each wfInstance adds a mapping
						# and they are all appended to the mapping table (without looking for duplicates)
						# this implements a weighted proxy mechanism 
						# where each proxy has 'n' entries equal o the number of workflows
						if msg["APP_NAME"] not in self.mappingTable:
							self.mappingTable[msg["APP_NAME"]] = []

						self.mappingTable[msg["APP_NAME"]].append(msg["MAPPING"]) 
					#	self.unbufferedWrite.write ("fakeNS: Added mapping: %s"%(msg["MAPPING"]))
					
					elif msg["COMMAND"] == "SUSPEND_MAPPING":
					#	self.unbufferedWrite.write ("Suspend mapping %s"%(msg))
						#TODO: Implement later
						index = 0
						index_of_entry_to_be_suspended = -1
						if msg["APP_NAME"]  in self.mappingTable:
							for mapping in self.mappingTable[msg["APP_NAME"]]:
								if (mapping["WF_INSTANCE_ID"] == msg["WF_INSTANCE_ID"]) and (mapping["HOST_ID"] == msg["HOST_ID"]):
								
									index_of_entry_to_be_suspended = index
									break
								index +=1

						if index_of_entry_to_be_suspended != -1:
							if msg["APP_NAME"] not  in self.suspendedTable:
								self.suspendedTable[msg["APP_NAME"]] = []
							tableEntry = self.mappingTable[msg["APP_NAME"]].pop(index_of_entry_to_be_suspended)
							self.suspendedTable[msg["APP_NAME"]].append(tableEntry)
							
						
					elif msg["COMMAND"] == "RESUME_MAPPING":
					#	self.unbufferedWrite.write ("Resume mapping %s"%(msg))
						index = 0
						index_of_entry_to_be_resumed = -1 
						if msg["APP_NAME"]  in self.suspendedTable:
							for mapping in self.suspendedTable[msg["APP_NAME"]]:
								if mapping["WF_INSTANCE_ID"] == msg["WF_INSTANCE_ID"] and mapping["HOST_ID"] == msg["HOST_ID"]:
									index_of_entry_to_be_resumed = index
									break
								index +=1
						if index_of_entry_to_be_resumed != -1:	
							self.mappingTable[msg["APP_NAME"]].append(self.suspendedTable[msg["APP_NAME"]].pop(index_of_entry_to_be_resumed))
					elif msg["COMMAND"] == "DELETE_MAPPING":
						index = 0
						index_of_entry_to_be_deleted = -1	
						# First look for in the mapping table
						if msg["APP_NAME"]  in self.mappingTable:
							for mapping in self.mappingTable[msg["APP_NAME"]]:
								if mapping["WF_INSTANCE_ID"] == msg["WF_INSTANCE_ID"] and mapping["HOST_ID"] == msg["HOST_ID"]:
									index_of_entry_to_be_deleted = index
									break
								index +=1
							if index_of_entry_to_be_deleted != -1: 
								del self.mappingTable[msg["APP_NAME"]][index_of_entry_to_be_deleted]
								
						elif msg["APP_NAME"]  in self.suspendedTable:
							for mapping in self.suspendedTable[msg["APP_NAME"]]:
								if mapping["WF_INSTANCE_ID"] == msg["WF_INSTANCE_ID"] and mapping["HOST_ID"] == msg["HOST_ID"]:
									index_of_entry_to_be_deleted = index
									break
								index +=1
							if index_of_entry_to_be_deleted != -1:
								del self.suspendedTable[msg["APP_NAME"]][index_of_entry_to_be_deleted]


																												



					elif msg["COMMAND"] == "ACTIVATE_MAPPING":
						if msg["APP_NAME"]  in self.mappingTable:
							for mapping in self.mappingTable[msg["APP_NAME"]]:	
								if mapping ["MAP_ID"] == msg["MAP_ID"]:
									mapping["STATE"] = "ACTIVE"
									break

			elif socks.get(self.readMappingPort)  == zmq.POLLIN:
				connID = self.readMappingPort.recv()
				msg = pickle.loads(self.readMappingPort.recv())
				#print(msg)
				if msg["TYPE"] == NAME_TO_APPLICATION_MAPPING_RESOLVE_REQ:
					record_index = -1
					f = open(os.path.expanduser("~/AppFabric/experiments/physical_machines/key.txt"),"r")
					Server_public_key=("%s"%f.read())
					#print (Server_public_key)
					Server_public_key= RSA.importKey(Server_public_key)
					if (msg["APP_NAME"] in self.mappingTable) and (len(self.mappingTable[msg["APP_NAME"]])> 0):
						count += 1
						#self.unbufferedWrite.write("\n\nMapping Table:%s: count = %s"%(self.mappingTable[msg["APP_NAME"]],count))
						#self.unbufferedWrite.write("%s"%msg["WANTED"])	
						
						mapping = self.mappingTable[msg["APP_NAME"]].pop()
						rep_msg = {
							"TYPE":NAME_TO_APPLICATION_MAPPING_RESOLVE_REP,
							"STATUS": SUCCESS,
							"IP_ADDR": mapping["IP_ADDR"],
							"PORT": str(mapping["PORT"]),
							"SPU":Server_public_key
							}
						self.mappingTable[msg["APP_NAME"]].insert(0,mapping)
			
					elif (msg["APP_NAME"] in self.mappingTable) and (len(self.mappingTable[msg["APP_NAME"]]) == 0):
						# this ensures that the service does not halt completely due to overload
						# It tries to optimally balance overload
						# This is imnsportant when all instances are overloaded and there is a little
						# delay in a new instance to come up.
						# or a new instance cannot be started because all resources have been used up 
						if msg["APP_NAME"] in self.suspendedTable and len(self.suspendedTable[msg["APP_NAME"]]) >0:
							count += 1
							#self.unbufferedWrite.write("\n\nSuspended Table:%s: count = %s"%(self.suspendedTable[msg["APP_NAME"]],count))
							#ADDED BY TARA 
							# SPU is the server public key, it is sent back to client when he wants to connect 
							mapping = self.suspendedTable[msg["APP_NAME"]].pop()
							rep_msg = {
								"TYPE":NAME_TO_APPLICATION_MAPPING_RESOLVE_REP,
								"STATUS": SUCCESS,
								"IP_ADDR": mapping["IP_ADDR"],
								"PORT": str(mapping["PORT"]), 
								"SPU":Server_public_key
								}
							self.suspendedTable[msg["APP_NAME"]].insert(0,mapping)
						else:
							rep_msg = {
								"TYPE":NAME_TO_APPLICATION_MAPPING_RESOLVE_REP,
								"STATUS": FAIL
								}	
					else:
						rep_msg = {
							"TYPE":NAME_TO_APPLICATION_MAPPING_RESOLVE_REP,
							"STATUS": FAIL
							}
					#print (connID) 
					#print (rep_msg)
					self.readMappingPort.send(connID, zmq.SNDMORE)
					self.readMappingPort.send(pickle.dumps(rep_msg))

'''
if __name__ == '__main__':
		
	# for Mininet based experiments
#	sys.stdout = unbuffered(sys.stdout)

	
	context = zmq.Context()

	try: 
		fakeNS = fakeNameServer(context)
		fakeNS.start()
	except Fake_Nameserver_Init_Error as e:
		#self.unbufferedWrite.write ("Error starting fake name server", e.msg()) 
		sys.exit()
	
	#TODO propagate kill signals to the threads and close them properly
'''
