Scripts to run the AppFabric platform on physical machines.

Files:
	- 01-nameserver.sh		Starts the fake nameserver
	- 02-global_controller.sh	Starts the global controller
	- 03-local_controller.sh	Starts the local controller (1 per zone)
	- 04-node_controller.sh		Starts the node controller (a.k.a. cPort, 1 per server)
	- 05-client.sh			Starts a client which connects to the proxy service

Installation:
	- Make sure that every path inside the scripts are accurate.
	- Some of the Python scripts require a symbolic link to python3 at /usr/local/bin/python3 path. Create the link if it's not present.
	- Adjust the parameters to the physical environment. The handling arguments is done via environmental variables.

Configuration:
	- Nameserver parameters:
		* HOST_NAME			Name of the host, used in identification. (?)
		* IP_ADDR			IP address of the machine running the nameserver.
		* UPDATE_MAPPING_PORT		Port used between Global controller and fake nameserver to update the location of the proxy service. (?)
		* READ_MAPPING_PORT		Port used between client and fake nameserver to get the IP address of the proxy port.

	- Global controller
		* HOST_NAME			Name of the host running the GC.
		* NAME_SERVER_ADDR		IP address of the fake nameserver.
		* NAME_SERVER_UPDATE_PORT	Port used between GC and nameserver. Same as Nameserver's UPDATE_MAPPING_PORT.

	- Local controller
		* HOST_NAME			Name of the host running the LC
		* SITE_NUM			Site ID number. (?)
		* IP_ADDR			IP address of the host running the LC
		* ZONE_NAME			The LC's Zone's name. It should be present in sites.cfg in runtime/configurations/ directory.
		* SITE_NAME			The LC's Site's name. It should be present in sites.cfg in runtime/configurations/ directory.
		* SITE_TYPE			The type of the site. Possible values are: EDGE, CORE and CORE/EDGE. Currently there is no difference between the values.
		* GC_ADDRESS			Address of the Global controller. Same as Global controller's IP_ADDR parameter.
		* GC_PORT			Port number of the Global controller. The port number 5555 is hard-coded.

	- Node controller
		* HOST_NAME			Name of the host running the Node controller (cPort).
		* IP_ADDR			IP address of the host running the Node controller.
		* LC_ADDR			IP address of the Local controller. Same as LC's IP_ADDR parameter.
		* LC_PORT			Port number of the Local controller. Currently it is hard-coded to 1234.
		* MAX_RESOURCE_LIMIT		Number representing the amount of resource that can be allocated on this server.

Usage:
	- Start the scripts one by one in numbering order to observe the startup phase.
	- When 2 or more Node controllers are used, it is best to start in the following order to use all the physical machines:
		1. Fake nameserver
		2. Start every Local controller
		3. Start every Node controller
		4. Start the Global controller

