#!/bin/bash
#Node
export HOST_NAME=Node1
export IP_ADDR=10.0.1.1
export LC_ADDR=10.0.1.1
export LC_PORT=1234
export NODE_HOST_DIR="/home/appfabric-alpha"
export MAX_RESOURCE_LIMIT=10000
export MAX_REQUESTS=100 
export MAX_REQUESTS_PER_CLIENT=70 
export BANNED_DEST=""
export TYPE_OF_TRAFFIC="GET" 
python3 ../../runtime/scripts/openadn/cInit.py 

