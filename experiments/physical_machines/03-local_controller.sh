#!/bin/bash

#Local Controller
export HOST_NAME=LC
export SITE_NUM=1
export IP_ADDR=10.0.1.1
export ZONE_NAME=US-E
export SITE_NAME=DC1
export SITE_TYPE=CORE/EDGE
export GC_ADDRESS=10.0.1.3 
export GC_PORT=5555
python3 ../../runtime/scripts/lighthouse/localc/local_controller_init.py 

