#!/usr/local/bin/python
import sys
import os
import threading
import http.client
import time
import zmq
import pickle
import ssl 
from time import sleep
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto import Random
from Crypto.Cipher import AES
from Crypto.Signature import PKCS1_v1_5
from base64 import b64decode
from aes_cbc import AESCipher 
from hashlib import md5
import netifaces as ni

home= os.environ["HOST_DIR"]
NAME_TO_APPLICATION_MAPPING_RESOLVE_REQ = 2001
NAME_TO_APPLICATION_MAPPING_RESOLVE_REP = 2002
SUCCESS =  1
FAIL = 0
BS=16
pad= lambda s: s + (BS-len(s) % BS)*chr(BS-len(s)% BS).encode()
uppad = lambda s: s [:-ord(s[len(s)-1:])]
mapping_request = {
		"TYPE": NAME_TO_APPLICATION_MAPPING_RESOLVE_REQ,
		"APP_NAME": os.environ["APP_NAME"]
		}

class httpClientThreadHandler(threading.Thread):


	def __init__(self, sender, context, config, urlList =[]):
		super (httpClientThreadHandler, self).__init__()
		self.urlList = urlList
		self.sender = sender
		self.context = context
		self.httpConnection = None

		self.application_IP = None
		self.application_port = None
		
		self.nameServer_connectionString = ("tcp://" + os.environ["NAMESERVER_ADDR"] + ":" +  os.environ["NAMESERVER_READ_MAPPING_PORT"]) 
		self.nameServer_connection = self.context.socket(zmq.DEALER)
		self.poller = zmq.Poller()

		self.config = config
		

	def run(self):
		
		try:
			self.nameServer_connection.connect(self.nameServer_connectionString)	
			self.poller.register(self.nameServer_connection, zmq.POLLIN)
		except zmq.ZMQError as e:
			print (e.__str__())
		numOfURL=0 
		# keep polling the nameserver
		while True:
			self.nameServer_connection.send(pickle.dumps(mapping_request))
			socks= dict(self.poller.poll(100))
			if socks.get(self.nameServer_connection) == zmq.POLLIN:
				rep_msg = pickle.loads(self.nameServer_connection.recv())
				if rep_msg["TYPE"]== NAME_TO_APPLICATION_MAPPING_RESOLVE_REP and rep_msg["STATUS"] == SUCCESS:
					self.application_IP = rep_msg["IP_ADDR"] 
					self.application_port = rep_msg["PORT"]
					Server_public_key= rep_msg["SPU"] # The server public key is returned with the nameserver response 
					break
				else:
					sleep (10)
		#############################################
		# ADDED BY TARA
		# The following code does the main security tasks from the client prespectives 
		# After getting the server public key, the client would send an anitiation message and wait for response 
		# The first message is a setup message where the client send his public key with some other stuff 
		# Then the messages are send and recieved normally based on client-server agreement 
		
		#First generate public-private key of the client   
		random_generator = Random.new().read #Random number 
		key = RSA.generate(1024, random_generator) #private key
		public_key = key.publickey() #public key 
		public_key2=public_key.exportKey("PEM", passphrase=None, pkcs=1)
		self.httpConnection = http.client.HTTPConnection(self.application_IP +":" + self.application_port)	
		User = Server_public_key.encrypt(("%s"%self.sender).encode('utf-8'),64) #username encrypted with server public key
		Password='VIDE'
		Password = Server_public_key.encrypt(("%s"%Password).encode('utf-8'),64) #password encrypted with server public key
		
		msg = "f"	#just an indication message that will be signed 
		msg= msg.strip(' ') 
		hash = SHA256.new(msg.encode('utf-8')).digest() #hash value
		signature = key.sign(hash, '') #signiture with client private key
		self.httpConnection.request("GET", msg,"",{'User':'%s'%User,'Password':"%s"%Password, 'Signature':'%s'%signature,'Key':'%s'%public_key2,'CryptoType':'PKI'}) #send as http request 	
		try:
			httpResponse = self.httpConnection.getresponse() #waiting for the reposnse 
		except:
			pass
		if self.config["PRINT_REP_MSGS"] == 1:
			response =httpResponse.read().decode('UTF-8') #response comming back
		try: 
			split_message = response.split("   ",4) # The response has four parts so split them
			# should be encrypted but not yet encypted			
			signature="%s"%split_message[1] # Signature encrypted 
			message= split_message[0] #response encrypted 
			key2= key.decrypt(eval(split_message[2])) #key
			iv= key.decrypt(eval(split_message[3])) #nonce 
			key_to_be_used= md5(key2).hexdigest()
			SecretKey= open("SecretKeys2/%s.bin"%self.sender,"wb") #saving the key somewhere to be accessed later 
			[SecretKey.write(x) for x in (iv,b"    ",key2)]
			SecretKey.close()
			Secretkey= AES.new(key_to_be_used,AES.MODE_CBC,iv)					
			
			hash = SHA256.new(message.encode('utf-8')).digest() #Hash value 
			if(Server_public_key.verify(hash, eval(signature))):
				dec=key.decrypt(eval("%s"%message)) #decrypting message with private key
				decrypted="%s"%dec
				decrypted2= decrypted.replace("\\n","\n") 
				decrypted2= decrypted2.strip('b')
				decrypted2= decrypted2.strip('\'')
				print ("Sender:%s , "%(self.sender)) #printing
				print ("The server is verified .... Message Recieved\n%s "%(decrypted2))		
			else: 
				print ("Sender:%s "%(self.sender)) #varifying server not successed 
				print("*********************** The server message is rejected ***************************\n")
		except: 
			print ("Something went wrong with response:")						
			print (" %s"%response)
		while (len(self.urlList) != 0):
			url = self.urlList.pop()
			url= url.strip(" ")
			#Decide to go with PKI or Secret key sharing (SKS)
			if (self.config["Security"] == "PKI"):
				Cryptotype="PKI"
			else: 
				Cryptotype="SKS"
			message_org= "HELLO WORLD" #Message to be signed and encrypted 
			if (Cryptotype == "SKS"):
				enc=AESCipher(key2.decode()).encrypt(message_org,iv) #encrypt with secret key
					
			elif (Cryptotype == "PKI"):
				message_Enc=Server_public_key.encrypt(message_org.encode('utf-8'),64) #encrypt with server public key 
				hash = SHA256.new(message_org.encode('utf-8')).digest()
				signature = key.sign(hash, '') #signature 
			if (numOfURL ==5): 
				break
			#if ( url== "/images/logo.gif"):
			#	x=90 
			#else: 
			#	x=1 
			#The following for was done for testing mulitple sents by the same client 
			for trail in range (1):
				try:							
					print ("Requesting %s \n"%url) 	
					#Sending the message 
					if (Cryptotype == "SKS"):
						self.httpConnection.request("GET", url,"",{'User':'%s'%self.sender, 'CryptoType':Cryptotype, 'Data':enc, "SourcePort":"%s"%self.httpConnection.sock.getsockname()[1], "SourceIP":ni.ifaddresses('eth0')[2][0]['addr']})  #in case of SKS, add crypto type, user, and encrypted message along with some other message paramteters
					elif (Cryptotype == "PKI"):
						self.httpConnection.request("GET", url,"",{'User':'%s'%self.sender,'Signature':'%s'%signature,'Msg':'%s'%message_Enc,'CryptoType':Cryptotype, "SourcePort":"%s"%self.httpConnection.sock.getsockname()[1], "SourceIP":ni.ifaddresses('eth0')[2][0]['addr']}) #in case of PKI send crypto type, user, encrypted message, signature along with some other message paremeters 
					#sleep(5)
				except:
					try:
						self.httpConnection.request("PUT", url,"",{'User':'%s'%self.sender}) 
					except:
						pass

				trail+=1			
				try:
					httpResponse = self.httpConnection.getresponse() #get the response 
				except:
					pass
				if self.config["PRINT_REP_MSGS"] == 1:
					response =httpResponse.read().decode('UTF-8')
					if(Cryptotype=="PKI"): 
						try: 
							split_message = response.split("   ",2) #IF PKI, then it is message and signature 
							signature="%s"%split_message[1] #Signature
							message= split_message[0] #message
					
							hash = SHA256.new(message.encode('utf-8')).digest()
							if(Server_public_key.verify(hash, eval(signature))): #Verify signature 
								dec=key.decrypt(eval("%s"%message)) #decrypt message
								decrypted="%s"%dec
								decrypted2= decrypted.replace("\\n","\n") 
								decrypted2= decrypted2.strip('b')
								decrypted2= decrypted2.strip('\'')
								print ("Sender:%s , URL: %s"%(self.sender,url))
								print ("The server is verified .... Message Recieved\n%s "%(decrypted2))
								 
							else: 
								print ("Sender:%s , URL: %s"%(self.sender,url))
								print("*********************** The server message is rejected ***************************\n")
						except:	
							print ("Sender:%s , URL: %s"%(self.sender,url))	
							print (" %s"%response)
					elif (Cryptotype=="SKS"):
						try: 
							f = open("%s/AppFabric/experiments/physical_machines/SecretKeys/%s.bin"%(home,self.sender),"rb") #IF sks then read private key and decrypt 
							message= f.read() #read secret key 
							message=message.split(b"    ")
							key2= message[1] #key
							iv= message[0] #nonce 
							rec_msg ="%s"%response 
							rec_msg= rec_msg.strip('b')
							rec_msg= rec_msg.strip('\'')
							rec_msg= AESCipher(key2.decode()).decrypt(rec_msg.encode(),iv)# Secret Key decryption
							print ("Sender:%s , URL: %s"%(self.sender,url))	
							print ("The server is verified .... Message Recieved\n%s"%rec_msg)		
						except:	
						
							print ("Sender:%s , URL: %s"%(self.sender,url))	
							print (" %s"%response)
		
		
		if self.config["LONG_LIVED_SESSION_MODE"] == 0:
			self.httpConnection.close()

	def join(self):
		if self.httpConnection != None:
			self.httpConnection.close()
		
