
# Author: Subharthi Paul
# Washington University in Saint Louis

'''

	This module simulates multiple clients.
	For each client it starts a new client thread 

'''





import sys
import re
import zmq
import time
import httpClientThread
import httpClientThread_DoS
from signal import *
import os
import getopt
from time import sleep

from Crypto.PublicKey import RSA
from Crypto import Random
def main(argv):
	
	# configuration foreach of the client threads
	threadConfig = {}

	# default values
	threadConfig["LONG_LIVED_SESSION_MODE"] = 1
	threadConfig["PRINT_REP_MSGS"] = 0
 	
	try:
		opts,args = getopt.getopt(argv,"hn:d:f:m:p:s:", ["help","num_clients=","lient_access_distr_method=","input_trace_file="] )
	except getopt.GetoptError:
		#usage()
		sys.exit(2)
        
	for opt, arg in opts:
		if opt in ("-h", "--help"):
			#usage()
			sys.exit()
		elif opt in ("-n","--num_clients"):
			num_clients = int(arg)

		elif opt in ("-d", "--client_access_distr_method"):
			client_access_distr_method = arg
                
		elif opt in ("-f", "--input_trace_file"):
			input_trace_file = arg   
		
		elif opt in ("-m", "--long_lived_session_mode"):
			threadConfig["LONG_LIVED_SESSION_MODE"] = int(arg)
		
		elif opt in ("-p", "--print_rep_msgs"): 
			threadConfig["PRINT_REP_MSGS"] = int(arg)
		elif opt in ("-s", "--secutiry"): 
			threadConfig["Security"] = arg
		
	#print (threadConfig["Security"])
	fd = open(os.path.expanduser(input_trace_file), "r", errors = 'ignore')
	token_sender = re.compile('(^[^\s]+)')
	token_url = re.compile('"(GET|PUT|POST)([^"]+)"')

	context = zmq.Context()

	senderList = { }
	activeThreadPool = [ ]
	
	def sigHandler(rSignal,frame):
		#for threadObject in activeThreadPool:
		#	threadObject.join()
		#	threadObject.exit()
		exit ()

	signal(SIGINT, sigHandler)		
	i = 0	
	for line in fd:	
		if i == 50000:
			break
		try:
			sender = token_sender.search(line).group()
			url = token_url.search(line).group(2)
			 
			url = re.sub(r'HTTP/1.0', r' ', url)
			#url = re.sub(r'HTTP/1.1', r' ', url)
			#url = url + " " + r"HTTP/1.1"
			url_verb = token_url.search(line).group(1)
			if sender and url:
				if sender in senderList:
					senderList[sender].append(url)
				else:
					# setup new http connection
					senderList[sender] = [ ]
					senderList[sender].append(url)
			i = i+1
		except:
			pass
	i=0
	
	# let the system start up
	
	for key in senderList:
		if i == num_clients :
			break
		threadObject = httpClientThread.httpClientThreadHandler(key,context,threadConfig, senderList[key])
		threadObject.start()
		activeThreadPool.append(threadObject)
		i=i+1
		#if i > 150 and i< 250:	
		#sleep(150)
		#	print ("Number of threads reached ", i)
	print ("Number of threads reached ", i)
	while (True):
		pause()


class unbuffered:
	def __init__(self, stream):
		self.stream = stream
	
	def write(self, data):
		self.stream.write(data)
		self.stream.flush()

	def __getattr__(self, attr):
		return getattr(self.stream, attr)		


if __name__ == '__main__':
	# for Mininet based experiments
	sys.stdout = unbuffered(sys.stdout)
	main(sys.argv[1:])
	
