iport os
import sys
import re

# to cut out the trace file to smaller size


input_trace_file = "~/AppFabric/experiments/mininet_simulations/AppFabric_client/usask_access_log"
output_trace_file = "~/AppFabric/experiments/mininet_simulations/AppFabric_client/usask_access_log_temp"

fd = open(os.path.expanduser(input_trace_file), "r", errors = 'ignore')
fd_w = open(os.path.expanduser(output_trace_file), "w+", errors = 'ignore')


senderList = { }
approved_list  = set()

token_sender = re.compile('(^[^\s]+)')
token_url = re.compile('"(GET|PUT|POST)([^"]+)"')

for line in fd:
	try:
		sender = token_sender.search(line).group()
		url = token_url.search(line).group(2)

		url = re.sub(r'HTTP/1.0', r' ', url)
		url_verb = token_url.search(line).group(1)
		if sender and url:
			if sender in senderList:
				senderList[sender].append(url)
			else:
				senderList[sender] = [ ]
				senderList[sender].append(url)
	except:
		pass

fd.close()
i = 0

for sender, msg in senderList.items():
	if len(msg) >= 5:
		approved_list.add(sender) 	
		i +=1
	if i == 10000: # 10000 user sessions each with 5 or more messages per session
		break

# write the new shortened trace file
fd = open(os.path.expanduser(input_trace_file), "r", errors = 'ignore')
for line in fd:
	sender = (line.split("- -"))[0].strip()
	if sender in approved_list:
		fd_w.write(line)

fd_w.close()




