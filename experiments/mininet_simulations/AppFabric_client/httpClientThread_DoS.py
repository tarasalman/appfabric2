import sys
import os
import threading
import http.client
import time
import zmq
import pickle
from time import sleep


# For separately verifying scale-up and scale-down behavior

# LONG_LIVED_SESSION_MODE = 1 # 1 for closing the connection cleanly

NAME_TO_APPLICATION_MAPPING_RESOLVE_REQ = 2001
NAME_TO_APPLICATION_MAPPING_RESOLVE_REP = 2002
SUCCESS =  1
FAIL = 0

mapping_request = {
		"TYPE": NAME_TO_APPLICATION_MAPPING_RESOLVE_REQ,
		"APP_NAME": os.environ["APP_NAME"]
		}

class httpClientThreadHandler(threading.Thread):


	def __init__(self, sender, context, config, urlList =[]):
		super (httpClientThreadHandler, self).__init__()
		self.urlList = urlList
		self.sender = sender
		self.context = context
		self.httpConnection = None

		self.application_IP = None
		self.application_port = None
		
		self.nameServer_connectionString = ("tcp://" + os.environ["NAMESERVER_ADDR"] + ":" +  os.environ["NAMESERVER_READ_MAPPING_PORT"]) 
		self.nameServer_connection = self.context.socket(zmq.DEALER)
		
		self.poller = zmq.Poller()

		self.config = config

	def run(self):
		
		try:
			self.nameServer_connection.connect(self.nameServer_connectionString)	
			self.poller.register(self.nameServer_connection, zmq.POLLIN)
		except zmq.ZMQError as e:
			print (e.__str__())
		numOfURL=0 
		# keep polling the nameserver
		while True:
			self.nameServer_connection.send(pickle.dumps(mapping_request))
			socks= dict(self.poller.poll(100))
			if socks.get(self.nameServer_connection) == zmq.POLLIN:
				rep_msg = pickle.loads(self.nameServer_connection.recv())
			#	print ("HTTP Client: %s"%(rep_msg))
				if rep_msg["TYPE"]== NAME_TO_APPLICATION_MAPPING_RESOLVE_REP and rep_msg["STATUS"] == SUCCESS:
					self.application_IP = rep_msg["IP_ADDR"]
					self.application_port = rep_msg["PORT"]
					
					break
				else:
					sleep (10)
		#print ("Client: Resolver Proxy Address: %s"%(self.application_IP +":" + self.application_port))	
		self.httpConnection = http.client.HTTPConnection(self.application_IP +":" + self.application_port )	
		#print (self.httpConnection)	
		 
		while (len(self.urlList) != 0):
			#remove this line
		#	if len(self.urlList) == 1:
		#		time.sleep (300)
			url = self.urlList.pop()

			if (numOfURL ==5): 
				break
			for trail in range (0,6):
				try:				
					self.httpConnection.request("GET", url)
				except:
					try:
						self.httpConnection.request("PUT", url)
					except:
						pass
				#print ("Request Sent by: Sender:%s  for URL: %s"%(self.sender,url))
				try:
					httpResponse = self.httpConnection.getresponse()
				except:
					pass
				trail= trail+1 
						
			#sleep(1)
				if self.config["PRINT_REP_MSGS"] == 1:
					print ("Sender:%s , URL: %s, Response: %s"%(self.sender,url,httpResponse.read().decode('UTF-8')))
			numOfURL= numOfURL+1
	#	print ("Sender:%s is closing the connection" %(self.sender))
		
		if self.config["LONG_LIVED_SESSION_MODE"] == 0:
			self.httpConnection.close()

	def join(self):
		if self.httpConnection != None:
			self.httpConnection.close()
		
