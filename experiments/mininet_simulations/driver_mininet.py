#!/usr/bin/python
from time import sleep, time
from select import poll, POLLIN 
import sys, os
from mininet import util
from mininet.node import  Node
from mininet.net import Mininet,init
from mininet.log import setLogLevel,lg,info, output, error
import pprint
import getopt

''' implement a simple API for geting/releasing host resources 
assuming that they are all connected to a single switch and 
all have connectivity to each other

The  mininet resource provider unlike EC2 does not implement a 
standard API or a generic server,at present. This is only to test the implementation

The only link is that it returns a set of host objects to the driver
For EC2 Appfabric will have to deal with EC2' host objects


Currently only a subnet having a single switch topology is implemented,
may be extended to different topology types later if requried

Also, due to limitations of mininet, static infrastructure with pre_allocated resources are allowed
Only one tenant is allowed currently
''' 
# Added by LG: driver_mininet does the function of the global resource manager
# resources are allocated statically at the start of the application.
# For on demand application simulation, extra resources are allocated in the beginning.
# /Added by LG: Initiates the bootstrap process

GLOBAL_CONTROLLER = '/home/appfabric/AppFabric/runtime/scripts/lighthouse/globalc/global_controller_init.py'
LOCAL_CONTROLLER= '/home/appfabric/AppFabric/runtime/scripts/lighthouse/localc/local_controller_init.py'
NODE_CONTROLLER = '/home/appfabric/AppFabric/runtime/scripts/openadn/cInit.py'
FAKE_NAME_SERVER = '/home/appfabric/AppFabric/runtime/scripts/nameserver/nameserver_init.py'
CLIENT = '/home/appfabric/AppFabric/experiments/mininet_simulations/AppFabric_client/httpClient.py'

class mininetDriver :

	def __init__(self, _numHosts, _numSites, _appName, _clientConfig):
		self.numHosts = _numHosts   # under each site
		self.numSites = _numSites  
		self.app_name = _appName
		# global nameserver
		self.ns_mac = util.macColonHex(1000)
		self.ns_ip = util.ipStr(util.ipNum(100,100,0,1))
		self.ns_update_mapping_port = str(1234)
		self.ns_read_mapping_port = str(4321)
		self.gc_fakeNameServer = None
		self.clientConfig = _clientConfig
	
		# For this simulation a single client emulates many different clients
		# each client runs in a separate thread with each independantly querying the 
		# name server	
		self.client_host_mac = util.macColonHex(1001) 
		self.client_host_ip = util.ipStr(util.ipNum(200,200,0,1))
		self.client_host = None

		self.switch = None
		self.gc_lighthouseController = None
		self.net = Mininet()
		self.siteDescList = []
		self.allocate_singleSwitchTopo()	
	
	def allocate_singleSwitchTopo(self):
		self.switch = self.net.addSwitch('s1')	
		self.net.addController( 'c0' )
		
		# Fake name server
		self.gc_fakeNameServer = self.net.addHost("FakeNS", mac=self.ns_mac, ip=self.ns_ip )
		self.gc_fakeNameServer.linkTo(self.switch)
		print ("adding global nameserver<%s, %s, %s>"%(self.gc_fakeNameServer.name, self.ns_mac, self.ns_ip))	

		# global controller
		mac= util.macColonHex(1)
		ip = util.ipStr(util.ipNum(10,10,0,1))
		self.gc_lighthouseController = self.net.addHost("gc", mac=mac, ip=ip)
		self.gc_lighthouseController.linkTo(self.switch)	
		print ("adding global lighthouse controller <%s, %s, %s>"%(self.gc_lighthouseController.name, mac, ip))		
		
		# client host
		self.client_host = self.net.addHost("client", mac=self.client_host_mac, ip=self.client_host_ip)
		self.client_host.linkTo(self.switch)
		print ("adding client host <%s, %s, %s>"%(self.client_host.name, self.client_host_mac,self.client_host_ip ))


		# allocate a host and connect it to the switch
		count = 2
		siteDesc = [ {"site_num": 1, "lc_controller": None,"hostList": [],"site_name": "site_1", "zone_name": "US-E"},
			{"site_num": 2, "lc_controller": None,"hostList": [],"site_name": "site_2", "zone_name": "US-E"},
			{"site_num": 3, "lc_controller": None,"hostList": [],"site_name": "site_3", "zone_name": "US-E"},
			{"site_num": 4, "lc_controller": None,"hostList": [],"site_name": "site_4", "zone_name": "US-E"},
			]
		for i in range (1, self.numSites+1):
		
			for j in range (1, self.numHosts+1):	
				mac =  util.macColonHex(count)
				count = count +1
				ip = util.ipStr(util.ipNum(10,10,i,j))
				if j == 1:
					hostName = "lc_" + "s_" + str(i)
					print ("adding lighthouse controller for site %s: <%s, %s, %s>"%(i,hostName, mac, ip))
					host = self.net.addHost(hostName, mac=mac, ip=ip)
					lc_lighthouseController = host
					host.linkTo(self.switch)
					siteDesc[i-1]["lc_controller"] = host

				
				else:
					
					hostName = "h" + str(j-1)+ "s"+ str(i)
					print ("adding host to site %s: <%s, %s, %s>" %(i, hostName,  mac, ip) )
					host = self.net.addHost(hostName,mac=mac, ip=ip)
					host.linkTo( self.switch )
					# store info for each host
					hostInfo = {}
					hostInfo["host"] = host
					hostInfo["lc_controller_addr"]= lc_lighthouseController.defaultIP
					hostInfo["host_addr"] = host.defaultIP
					
					siteDesc[i-1]["hostList"].append(hostInfo)
				#	self.hostList[i].append (hostInfo)
			self.siteDescList.append(siteDesc[i-1])
		
	
	def start_topo(self):
		print ("Starting network")
		self.net.start()
	#	print ("Testing connectivity:")
	#	self.net.pingAll()

	def start_gc_lighthouseController(self):
		print ("2. Starting Global Lighthouse Controller"),
		self.gc_lighthouseController.cmd('export HOST_NAME=%s'%(self.gc_lighthouseController.name))
		self.gc_lighthouseController.cmd('export NAME_SERVER_ADDR=%s'%(self.gc_fakeNameServer.defaultIP))
		self.gc_lighthouseController.cmd('export NAME_SERVER_UPDATE_PORT=%s'%(self.ns_update_mapping_port))
		self.gc_lighthouseController.cmd("python3 %s &"%(GLOBAL_CONTROLLER))
		print (".... started\n")
		sleep(5)

	def start_lc_lighthouseControllers(self):
		print ("\n3. Starting Local Lighthouse Controllers:")
		for i in range(self.numSites):
			print ("\tStarting Local Lighthouse Controller <Site= %s, %s>"%(i+1, self.siteDescList[i]["lc_controller"].defaultIP)),	
			IP = self.siteDescList[i]["lc_controller"].defaultIP
			wfm_addr = self.gc_lighthouseController.defaultIP	
			wfm_port = str(5555)
			controller_host = self.siteDescList[i]["lc_controller"]
			controller_host.cmd('export HOST_NAME=%s'%(controller_host.name))
			controller_host.cmd('export SITE_NUM=%s'%(self.siteDescList[i]["site_num"]))
			controller_host.cmd('export IP_ADDR=%s'%(IP))
			controller_host.cmd('export ZONE_NAME=%s'%(self.siteDescList[i]["zone_name"]))
			controller_host.cmd('export SITE_NAME=%s'%(self.siteDescList[i]["site_name"]))
			controller_host.cmd('export SITE_TYPE=%s'%("CORE/EDGE"))
			controller_host.cmd('export GC_ADDRESS=%s'%(wfm_addr))
			controller_host.cmd('export GC_PORT=%s'%(wfm_port))

			controller_host.cmd('python3 %s &'%(LOCAL_CONTROLLER))
			print (".... started")

	def start_fakeNameServer(self):
		print ("1. Starting Global Fake Nameserver (as a replacement of DNS)"),
		self.gc_fakeNameServer.cmd('export HOST_NAME=%s'%(self.gc_fakeNameServer.name))
		self.gc_fakeNameServer.cmd('export IP_ADDR=%s'%(self.gc_fakeNameServer.defaultIP))
		self.gc_fakeNameServer.cmd('export UPDATE_MAPPING_PORT=%s'%(self.ns_update_mapping_port))
		self.gc_fakeNameServer.cmd('export READ_MAPPING_PORT=%s'%(self.ns_read_mapping_port))		
		self.gc_fakeNameServer.cmd('python3 %s &'%(FAKE_NAME_SERVER)) 
		print (".... started\n")
		sleep (5)
	#	self.lighthouseController.cmd('sudo python3 /home/openflow/AppFabric/platform/scripts/controller.py &')
	#	sleep(5)

	def start_client_host(self):
		print ("5. Starting client host"),
		self.client_host.cmd('export APP_NAME=%s'%(self.app_name))
		self.client_host.cmd('export NAMESERVER_ADDR=%s'%(self.gc_fakeNameServer.defaultIP))
		self.client_host.cmd('export NAMESERVER_READ_MAPPING_PORT=%s'%(self.ns_read_mapping_port))
		self.client_host.cmd ('python3 %s -n %s -d %s -f %s -m %s -p %s &'%(CLIENT, self.clientConfig["NUM_CLIENTS"], self.clientConfig["CLIENT_ACCESS_DISTR_METHOD"],self.clientConfig["INPUT_TRACE_FILE"], self.clientConfig["LONG_LIVED_SESSION_MODE"], self.clientConfig["PRINT_REP_MSGS"] ))
		print (".... started\n")
		


		
	def start_hosts (self):
		sleep(5)
		for i in range (self.numSites):
			print ("\n\t Site:%s:"%(i))
			for j in range(self.numHosts-1):
				hostInfo  = self.siteDescList[i]["hostList"][j]
				print ("\t\tStarting host <Site = %s, Controller = %s,  %s, %s>" %(i+1, hostInfo["lc_controller_addr"],  hostInfo["host"].name, hostInfo["host"].defaultIP)),
				host= hostInfo["host"]
				host.cmd('export HOST_NAME=%s'%(hostInfo["host"].name))
				host.cmd('export IP_ADDR=%s'%(hostInfo["host_addr"]))
				host.cmd('export LC_ADDR=%s'%(hostInfo["lc_controller_addr"]))
				host.cmd('export LC_PORT=%s'%str(1234))
				host.cmd('export MAX_RESOURCE_LIMIT=%s'%str(10000))
			#	host.cmd('export LOCAL_GRAPHING=ON')
			#	host.cmd('export GRAPHING_CMD=/home/appfabric/AppFabric/experiments/mininet_simulations/live_graph/LiveGraph.2.0.beta01.Complete.jar')
				host.cmd('python3 %s &'%(NODE_CONTROLLER))
				print(".... started")

	def stop_topo(self):
		self.net.stop()			



def start_sim(_runTime, _maxResources, _numSites, _appName, _clientConfig):
	
	simNetwork = mininetDriver(_maxResources,_numSites, _appName, _clientConfig)
	simNetwork.start_topo()
	poller = poll()

	fds = [ ]
	fds.append(simNetwork.gc_lighthouseController.stdout.fileno())
	fds.append(simNetwork.gc_fakeNameServer.stdout.fileno())
	fds.append(simNetwork.client_host.stdout.fileno())

	#monitor outputs:
	for i in range (_numSites):
		fds.append(simNetwork.siteDescList[i]["lc_controller"].stdout.fileno())
		for j in range(_maxResources-1):
			#fds = [ host.stdout.fileno() for host in (hostInfo["host"] for  hostInfo in  simNetwork.siteDescList[i]["hostList"]) ]
			hostInfo = simNetwork.siteDescList[i]["hostList"][j]
			host = hostInfo["host"]
			fds.append(host.stdout.fileno())

	for fd in fds:
		poller.register(fd,POLLIN) 
	
	#start the processes in the hosts
	print ("-------------\n")
	simNetwork.start_fakeNameServer()
	simNetwork.start_gc_lighthouseController()
	simNetwork.start_lc_lighthouseControllers()
	print ("\n")
	print ("4. Starting hosts:")
	simNetwork.start_hosts()

	simNetwork.start_client_host()
	print ("-------------\n")

	#start the monitoring
	endTime = time() + _runTime
	while time()< endTime:
		readable = poller.poll(1)
		for fd, _mask in readable:
			node = Node.outToNode[ fd ]
			outString = node.monitor().strip()
			if len(outString) > 0:
				print '\n%s:' % node.name,outString
				#print '\n%s:' % node.name, node.monitor().strip()




	simNetwork.stop_topo()	


class unbuffered:
	def __init__(self, stream):
		self.stream = stream
	def write(self, data):
		self.stream.write(data)
		self.stream.flush()
	def __getattr__(self, attr):
		return getattr(self.stream, attr)

if __name__ == '__main__':
	sys.stdout = unbuffered(sys.stdout)
	

	#default client configuration
	client_config = {}
	client_config["NUM_CLIENTS"] = 500
	client_config["CLIENT_ACCESS_DISTR_METHOD"] = "None"
	client_config["INPUT_TRACE_FILE"] = "~/AppFabric/experiments/mininet_simulations/AppFabric_client/usask_access_log"
	client_config["LONG_LIVED_SESSION_MODE"] = 1
	client_config["PRINT_REP_MSGS"] = 0
	#read client configuration from the command line
	if len(sys.argv) > 0:

		arg = sys.argv[1:]
		try:
			opts,args = getopt.getopt(arg,"hn:d:f:m:p:", ["help","num_clients=","client_access_distr_method=","input_trace_file="] )
		except getopt.GetoptError:
		#	usage()
			sys.exit(2)
		for opt, arg in opts:
                	if opt in ("-h", "--help"):
                        	#usage()
				sys.exit()
			elif opt in ("-n","--num_clients"):
				client_config["NUM_CLIENTS"] = arg
		
			elif opt in ("-d", "--client_access_distr_method"):
				client_config["CLIENT_ACCESS_DISTR_METHOD"] = arg
		
			elif opt in ("-f", "--input_trace_file"):
				client_config["INPUT_TRACE_FILE"] = arg
			
			elif opt in ("-m", "--long_lived_session_mode"):
				client_config["LONG_LIVED_SESSION_MODE"] = arg

			elif opt in ("-p", "--print_rep_msgs"): 
				client_config["PRINT_REP_MSGS"] = arg

	runTime = 10000
	appName = "ABC"
	maxResources = 8
	numSites = 2
	start_sim(runTime, maxResources, numSites, appName, client_config)


